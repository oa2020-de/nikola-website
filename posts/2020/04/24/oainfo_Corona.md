<!--
.. title: Open Access während der Covid-19 Pandemie
.. slug: openaccess_covid19
.. date: 2020-04-24 08:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## Eine kollaborative Sammlung zu Open Access während der COVID-19­-Pandemie

Auf der Informationsplattform Open Access gibt es eine [Übersicht](https://open-access.net/informationen-zu-open-access/open-access-waehrend-der-covid-19-pandemie) zu verschiedenen Angeboten für das Finden und Veröffentlichen von Open-Access-Forschung zum Corona-Virus und darüber hinaus. Die Sammlung soll kollaborativ entstehen und erhebt keinen Anspruch auf Vollständigkeit – im Gegenteil: Es handelt sich um ein Angebot, das laufend ergänzt werden soll. Dafür steht ein öffentliches [Pad](https://pad.gwdg.de/oannetwork-corona) zur Verfügung.

Hier geht es zur Informationssammlung: [https://open-access.net/informationen-zu-open-access/open-access-waehrend-der-covid-19-pandemie](https://open-access.net/informationen-zu-open-access/open-access-waehrend-der-covid-19-pandemie).

Wer mitmachen möchte, kann das gerne im Pad tun: [https://pad.gwdg.de/oannetwork-corona](https://pad.gwdg.de/oannetwork-corona).

An dieser Stelle noch der Hinweis, das nicht alles, was "offen" zur Verfügung gestellt wird, auch Open Access ist. Oft werden die Kriterien Nachhaltigkeit (im Sinne von einem Zugang, der unwiderufbar offen ist) und freie Lizenzierung nicht erfüllt, sodass die freigegebenen Inhalte nur schwer nachnutzbar sind und der Zugang temporär erfolgt. In Zeiten, in denen Offenheit ein wichtiger Parameter für die globale Zusammenarbeit geworden ist (und das nicht nur auf Ebene der Wissenschaft), braucht es aber nachhaltige Strukturen und Angebote für den Wissenstransfer, auch über die Krisenzeit hinaus. Das zu ermöglichen, ist eine Aufgabe von uns allen.
