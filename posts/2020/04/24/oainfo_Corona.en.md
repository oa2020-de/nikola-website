<!--
.. title: Open Access during the Covid-19 pandemic
.. slug: openaccess_covid19
.. date: 2020-04-24 08:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## A collaborative collection on open access during the COVID 19 pandemic

The information platform Open Access provides an [overview](https://open-access.net/informationen-zu-open-access/open-access-waehrend-der-covid-19-pandemie) of various offers for finding and publishing open-access research on the corona virus and beyond. The collection is intended to be collaborative and does not claim to be complete - on the contrary: it is an offer that is to be continuously expanded. A public [pad](https://pad.gwdg.de/oannetwork-corona) is available for this purpose.

Click here for information collection: [https://open-access.net/informationen-zu-open-access/open-access-waehrend-der-covid-19-pandemie](https://open-access.net/informationen-zu-open-access/open-access-waehrend-der-covid-19-pandemie).

If you want to join in, you can do so in the pad: [https://pad.gwdg.de/oannetwork-corona](https://pad.gwdg.de/oannetwork-corona).

At this point I would like to point out that not everything that is made available "openly" is also open access. Often the criteria of sustainability (in the sense of access that is irrevocably open) and free licensing are not met, so that the released content is difficult to re-use and access is temporary. In times when openness has become an important parameter for global cooperation (and not only at the level of science), however, sustainable structures and offers for knowledge transfer are needed, even beyond the crisis period. Making this possible is a task for us all.
