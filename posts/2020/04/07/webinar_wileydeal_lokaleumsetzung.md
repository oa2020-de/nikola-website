<!--
.. title: Webinar "Lokale Umsetzung von Open-Access-Transformationsverträgen am Beispiel des DEAL-Wiley-Vertrages"
.. slug: webinar_wileydeal_lokaleumsetzung
.. date: 2020-04-07 08:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## Webinar statt Workshop

Aufgrund der Corona-Situation konnte der Nationale Open-Access-Kontaktpunkt (OA2020-DE) seinen für den 09. und 10. März 2020 angedachten 5. Transformationsworkshop zur lokalen Umsetzung von Open-Access-Transformationsverträgen am Beispiel des DEAL-Wiley-Vertrages nicht wie geplant durchführen. Um trotzdem den inhaltlichen Input allen Interessierten zur Verfügung zu stellen, wurde kurzfristig am 09. März ein Webinar mit allen beteiligten Referent*innen auf die Beine gestellt (ein herzliches Dankeschön dafür an dieser Stelle!). Auch wenn ein solches Webinar den persönlichen Erfahrungsaustausch im Rahmen eines Workshops nicht ersetzen kann, sind wir zuversichtlich, dass die vorgestellten (und in Teilen aufgezeichneten) Erfahrungsberichte und Beiträge von großem Interesse sind. Wir bemühen uns, den Workshopteil schnellstmöglich nachzuholen, sobald Veranstaltungen wieder zulässig sind.
<!-- TEASER_END -->

Im Folgenden finden Sie alle Vorträge hintereinander aufgelistet, inklusive Referent*innenvorstellung und den Fragen aus dem Chat, zum Anschauen und Durchscrollen. Wem das zu viel ist: am Ende des Blogposts gibt es eine [tabellarische Übersicht](#Tabelle) mit allen Vorträgen.

- - -

### Begrüßung und Einführung

Zu Beginn des Webinars erfolgte die Begrüßung durch Alexandra Jobmann (Projektmitarbeiterin bei OA2020-DE) und eine Einführung in das Thema durch Dirk Pieper (Projektleiter von OA2020-DE).
<br />

<iframe src="https://uni-bielefeld.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=8a7c31e7-907f-41c9-ace5-ab9100b14ae3&autoplay=false&offerviewer=true&showtitle=true&showbrand=false&start=0&interactivity=all" width=720 height=405 style="border: 1px solid #464646;" allowfullscreen allow="autoplay"></iframe>

- - -

### Erfahrungsbericht Universitätsbibliothek Bielefeld

Dr. Silvia Herb ist Dezernentin für Medienbearbeitung an der Universitätsbibliothek Bielefeld. Nach einer Ausbildung zur Diplom-Bibliothekarin an der Deutschen Nationalbibliothek studierte sie Sozialwissenschaften, Psychologie und Jura und absolvierte anschließend ein Referendariat für den höheren Bibliotheksdienst.

Link zu den [Folien (CC-BY-NC)](/assets/files/2020-03-09_Webinar_DEALWiley_UBBielefeld.pdf).
<br />

<iframe src="https://uni-bielefeld.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=404c831b-a0fb-4d59-8f1a-ab9100b14b63&autoplay=false&offerviewer=true&showtitle=true&showbrand=false&start=0&interactivity=all" width=720 height=405 style="border: 1px solid #464646;" allowfullscreen allow="autoplay"></iframe>

<br />
**Fragen an Frau Herb**

* Jede/r Referent*in soll bitte dazu sagen, ob sie von einem ein- oder zweischichtigen Bibliothekssystem spricht? Die UB Bielefeld ist einschichtig.
* Entspricht die Anzahl der publizierten Artikel Ihren angenommenen Artikelmengen? Lange Zeit im Verlaufe des Jahres sah es so aus und dann hat sich zum Ende des Jahres doch nochmal ein deutlicher Anstieg gezeigt. D.h. wenn man die Zahlen von 2019 hochrechnet, hätten wir ungefähr einen Anstieg von 15-20% zu erwarten.
* Erfassen Sie alle Einzelartikel mit bibliografischen Angaben? Ja. Wir pflegen die Artikel in unser Bibliothekssystem ein, als unselbsständige Publikation.
* Wie erfolgt die Zuordnung der Artikel zu den Fachbudgets? Die Kosten werden beim direkt Fachbudget eingetragten, als hätten wir den Artikel erworben.
* Welche PAR-Fee setzen Sie an? Die PAR-Fee, Service-Fee und die Mehrwertsteuer, also 3.451€.

- - -

### Erfahrungsbericht Universitäts- und Landesbibliothek Münster

Dr. Peter te Boekhorst ist Leiter der Abteilung Integrierte Medienbearbeitung an der Universitäts- und Landesbibliothek Münster sowie Leiter der Bibliothek des Hauses der Niederlande. Nach dem Studium der Anglistik und Geschichte und seinem Bibliothekreferendariat ist er seit 1989 an der ULB Münster tätig.

Link zu den [Folien (CC-BY-NC-SA)](/assets/files/2020-03-09_Webinar_DEALWiley_ULBMünster.pdf).
<br />

<iframe src="https://uni-bielefeld.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=da1675e9-182f-4d34-91da-ab9100b18e8c&autoplay=false&offerviewer=true&showtitle=true&showbrand=false&start=0&interactivity=all" width=720 height=405 style="border: 1px solid #464646;" allowfullscreen allow="autoplay"></iframe>

<br />
**Fragen an Herrn te Boekhorst**

* Wird sich denn die Beteiligung der ULB an den Kosten ändern wenn ab 2022 publikationsbasiert abgerechnet wird? Wir werden bemüht sein, immer einen finanziellen Anteil zu haben, da wir uns für die Read-Komponente zuständig sehen.
* Sie geben die Kosten über 2000€ weiter an die Fachbereiche, das ist an sich ein relativ hoher administrativer Aufwand. Werden Sie das für Springer dann auch so machen bzw. was sind Ihre Prognosen für den Springervertrag? Wir werden das weiter betreiben, weil wir davon ausgehen, dass unter dem neuen Bibliothekssystem ALMA dann ein besseres Nachweisen möglich sein wird. Wir wollen dann auch sehen, dass wir über eine FiBu-Schnittstelle Dinge schneller ins SAP rein bekommen. Da wird es noch neue Workflows geben. Aber grundsätzlich werden wir diese Verteilungslogik beibehalten.
* Wie gehen Sie mit den Fachbereichen um, bei denen das Lesen einer Zeitschrift eher relevant ist als das Veröffentlichen darin? Man könnte sagen, bisher zahlt die UB die Read-Komponente und künftig wird diese dann genau auf solche Fachbereiche umgelegt. Nur habe ich keine Handhabe dafür zu prüfen, wer von wo aus einen Wiley-Artikel liest. Die Read-Komponente muss eine zentrale Sache bleiben und sollte daher auch nicht unterverteilt werden.
* Stellen Sie die Artikel den Fachbereichen in Rechnung oder dürfen Sie deren Kostenstellen bebuchen? Letzteres.
* Wie berechnen Sie die Read-Komponente? Die UB zahlt 25%, aber die 3.451€ PAR-Fee gehen komplett an die Fachbereiche.
* Ist beim Fachbereich "Medizin" auch gleichzeitig das Universitätsklinikum Münster gemeint? Auf Ebene der Publikationen, ja (gibt eine enge Verbindung zwischen beiden).
* Wieviel Personalressourcen benötigen Sie für den vorgestellen Workflow? 1,5 Stellen

- - -

### Erfahrungsbericht Bibliothek der FH Bielefeld

Dr. Karin Ilg leitet die Hochschulbibliothek und ist Mitglied des Leitungsteams Medien- und Informationsdienste MIND der Fachhochschule Bielefeld.
<br />

Link zu den [Folien](/assets/files/2020-03-09_Webinar_DEALWiley_FHBielefeld.pdf).


- - -

### Erfahrungsbericht Leibniz-Gemeinschaft am Beispiel IGB

Lydia Koglin hat Kunstgeschichte und Kulturanthropologie in Frankfurt am Main studiert. Nach diversen Stationen als wissenschaftliche Mitarbeiterin und dem Masterstudium Bibliotheks- und Informationswissenschaft an Humboldt-Universität zu Berlin leitet sie seit 2016 die Bibliothek am Leibniz-Institut für Gewässerökologie und Binnenfischerei in Berlin.

Link zu den [Folien (CC-BY)](/assets/files/2020-03-09_Webinar_DEALWiley_IGB.pdf).
<br />

<iframe src="https://uni-bielefeld.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=e1ec3f62-fd0b-4f4f-8f6e-ab9100b2fd92&autoplay=false&offerviewer=true&showtitle=true&showbrand=false&start=0&interactivity=all" width=720 height=405 style="border: 1px solid #464646;" allowfullscreen allow="autoplay"></iframe>

<br />
**Frage an Frau Koglin**

* Wie gewährleisten Sie, dass zuerst die Drittmitteln für die Open-Access-Kosten eingesetzt werden, bevor der Bibliotheksetat belastet wird? Noch gibt es dafür keinen Workflow. Momentan wird der Autor/ die Autorin gefragt, ob es dafür Drittmittel gibt und dann wird gehofft, dass die Antwort ehrlich ist. Der perspektivische Weg wird sein, eine Datenbank zu Überprüfung zu haben (z.B. ein CRIS/FIS).

- - -

### Erfahrungsbericht Helmholtz-Gemeinschaft am Beispiel FZ Jülich & Der Springer Nature-DEAL-Vertrag: Gemeinsamkeiten und Unterschiede zum Wiley-Vertrag

Dr. Bernhard Mittermaier ist promovierter Chemiker und leitet seit 2008 die Zentralbibliothek des Forschungszentrums Jülich. Von 2007 bis 2011 war er Mitglied im Vorstand des Verbands der Bibliotheken Nordrhein-Westfalens vbnw. Er ist außerdem sellvertretender Leiter des Projektes „Nationaler Open-Access-Kontaktpunkt“ und Teil der DEAL-Verhandlungsgruppe.

Link zu den [Folien (CC-BY)](/assets/files/2020-03-09_Webinar_DEALWiley_HGF_SNundWiley.pdf).
<br />

<iframe src="https://uni-bielefeld.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=66546ee5-b90f-4320-9bf4-ab9100b3caa2&autoplay=false&offerviewer=true&showtitle=true&showbrand=false&start=0&interactivity=all" width=720 height=405 style="border: 1px solid #464646;" allowfullscreen allow="autoplay"></iframe>

<br />
**Fragen an Herrn Mittermaier**

* Das Publikationsaufkommen kann sehr heterogen sein - bestellen Sie dann tatsächlich jedes Jahr Zeitschriften ab bzw. an, je nach Abhängigkeit vom Fonds? Wir planen die Subskriptionsausgaben vorausschauend, sodass wir jederzeit sicher sind, alle Publikationsausgaben bestreiten zu können. Und wenn dann am Ende des Jahres noch Geld übrig bleibt, dann wird das noch in eBooks investiert. Wir planen die Subskriptionsausgaben so, dass wir alle Publikationsgebühren zahlen können und die Sicherheit dafür entnehmen wir der Extrapolation der Erfahrungswerte aus den Vorjahren.
* Gab es vor 2015 auch schon Publikationskosten außerhalb des Etats der Bibliothek? Und wenn ja, sind die dann umgeschichtet worden? Vor dem Beschluss des Vorstands hat die Bibliothek auch schon Publikationsausgaben bezahlt, seinerzeit wurden sogar alle Publikationsausgaben übernommen (also auch die Ausgaben in Subskriptionszeitschriften wie Color Charges, Page Charges etc.) und der Vorstand hat dann explizit zur Stärkung des Open-Access-Gedankens beschlossen, dass nur noch Open-Access-Ausgaben aus diesem Publikationsfonds bestritten werden dürfen, alles andere müssen jetzt die Institute selbst bezahlen. Es läuft aber nach wie vor zum Großteil über die Bibliothek, sodass sie Kenntnis über diese Ausgaben hat.
* Werden die Publikationsausgaben an die einzelnen Institute kommuniziert? Ja, im Rahmen der Bemühungen um Open Access werden die Institute besucht und das Publikationsprofil vor Ort vorgestellt (wo sie in Gold-OA-Zeitschriften publizieren oder wo in Subskriptionszeitschriften), vorwiegend mit der Zielrichtung, den OA-Gedanken zu stärken und viel weniger mit der Zielrichtung, die Kosten zu verschieben.
* Wie gehen Sie mit Projektpublikationskosten um (z.B. bei EU-Projekten), zahlt die Bibliothek oder das Projekt? Wenn immer es möglich ist, Publikationskosten auf die dazugehörigen Projekte umzulegen, wird das gemacht.
* Inwiefern ist tatsächlich ein Transformationsmechanismus in den DEAL-Verträgen integriert? Gibt es Absprachen oder Diskussionen mit den Verlagen, dass ab einem bestimmten Prozentsatz die Zeitschriften in den Open Access geflippt werden? Der Transformationsgedanke ist im Vertrag nicht enthalten, in der gesamte Denke ist er in zweifacher Hinsicht enthalten: Die eine Hinsicht ist, dass aufgrund der Umsetzung des Vertrags es notwendig ist, dass Einrichtungen sich auf den Weg der Transformation begeben. Die althergebrachten Wege der Finanzierung müssen nochmal neu durchdacht werden. Grundsätzlich gilt die Aussage, dass genug Geld im System ist. Das gilt auf jeden Fall für die Makroebene, aber auch für die Mikroebene, sprich auf der Ebene einer einzelnen Einrichtung. Nur ist es dort noch nicht zwangsmäßig richtig allokiert. Wenn bislang die Publikationen aus den Instituten bestritten werden, dann muss man sich überlegen, wie man das in Zukunft aufstellt. Der zweite Aspekt der Transformation ist die komplette Umstellung auf Publikationskosten als Abrechnungslogik. Die Idee war, wir versuchen es mit einem ersten Schritt und hoffen, das andere Länder nachziehen. Und das Nachziehen ist bereits geschehen. Die Hoffnung ist also, wenn es mehr und mehr Länder gibt, die solche Verträge abschließen, dann kommt der Umstieg von selbst. Es war auch nie angedacht, das in den Vertrag rein zu schreiben, dafür ist Deutschland viel zu klein.
* Die sogenannte „kleine PAR-Fee“ bei Springer Nature für Editorials, Letters etc. sowie die Aufnahme von Brief Communication in den Kreis der Articles und Reviews wird den Kommunikationsaufwand in der Beratung der Autor*innen und generell der DEAL-Verträge in die Uni hinein stark erhöhen. Wird das nicht auch für viel Verärgerung sorgen, wenn so viel Geld für Editorial bezahlt werden soll? Ja. Es ging darum, die Kostenneutralität für den Verlag zu wahren und das war das Mittel zum Zweck. Ansonsten hätte der Verlag nicht zugestimmt. Die Alternative wäre gewesen, die PAR-Fee höher anzusetzen.
* Könne Sie den Ausschüttungsbetrag von 47,5% erläutern? Das liegt daran, dass derzeit der Springer-Vertrag vom Gesamtvolumen, das die MPDL-Service GmbH abgeschlossen hat, 47,5% ist und entsprechend andersrum der Wiley-Vertrag 52,5% ausmacht. Und entsprechend dieser Anteile wird dann ein Jahresüberschuss ausgeschüttet. Es ist ja keineswegs zwangsläufig so, dass alle Einrichtungen bei allen Verträgen mitmachen. Das muss also immer passgenau auf den jeweiligen Vertrag ausgerichtet sein.
* Bei Wiley fällt auf, dass zum Jahr 2020 ein paar der hybriden Zeitschriften zu Gold OA geflippt worden sind.
Wie wird in den kommenden 3 Jahren ein solches Flipping verhindert? Die PAR-Fee sind ja nun für 3 Jahre festgeschrieben. Außerdem sind vertraglich 9.500 Publikationen deutschlandweit vereinbart. Was passiert, wenn es deutlich weniger Zeitschriftenartikel in hybriden Zeitschriften werden? Es gibt im Vertrag einen Passus, dass bei massiven Änderungen eine Sonderkündigung möglich ist bzw. man noch mal ins Gespräch kommt.
* Gibt es schon Überlegungen zum Vorgehen, falls die freiwilligen Nachzahlungen nicht im erwarteten Maß geleistet werden? Das weiß ich nicht. Antwort von Frau Overkamp: es gibt noch keine konkreten Pläne, aber wenn die Einrichtungen keine Nachzahlungen leisten, kann es auch keine Rückvergütung vorab gezahlter Publikationsgebühren geben.
* Warum werden durch "M" Gold-OA-Discounts angeboten (5%-Discount), die wir selbst finanzieren? Wo liegen die Mehrwerte in dieser Umverteilung? Geht dies nicht zulasten der Vielpublizierer, die ohnehin schon Mehrbelastungen stemmen müssen?: Bei der Umsetzung des Wiley-Vertrags hat sich gezeigt, dass für manche Bibliotheken, die bislang keinen Open Access-Publikationsfonds haben, die Gold Open Access-Komponente ein besonders Problem darstellt, weil in diesen Fällen für die Bibliothek völlig neue Ausgaben entstehen, die bislang nicht im Etat vorgesehen waren (dass die Ausgaben von anderen Organisationseinheiten der Einrichtung finanziert wurden hilft oft nur in der Theorie). Bei Springer ist dieses Problem angesichts der wesentlich größeren Gold OA-Anteile wesentlich größer. Die Rabattierung kann dem ein bisschen entgegenwirken. Des Weiteren führt die Rabattierung dazu, dass die Teilnahme am DEAL-Vertrag auch mit Blick auf Gold OA attraktiver ist als die Nicht-Teilnahme, weil in den bestehenden BiomedCentral-Konsortien Nature Communications und Scientific Reports nicht enthalten und somit auch nicht rabattiert sind. Und drittens wird so vermieden, dass die APC in einer Gold OA-Zeitschrift (Nature Communications) höher ist als die PAR Fee in den Subskriptionszeitschriften.

- - -

### Informationen zur möglichen DFG-Förderung

Dr. Angela Holzer, Referentin bei der Deutschen Forschungsgemeinschaft (DFG), ist unter anderem für die Förderprogramme Open Access Publizieren und Überregionale Lizenzierung zuständig. Sie ist außerdem Mitglied der Arbeitsgruppe Wissenschaftliches Publikationssystem in der Schwerpunktinitiative Digitale Information der Allianz der deutschen Wissenschaftsorganisationen.

Link zu den [Folien](/assets/files/2020-03-09_Webinar_DEALWiley_FörderangebotederDFG.pdf).
<br />

<iframe src="https://uni-bielefeld.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=5e3f51d5-6bb2-4732-82be-ab9100b44b9e&autoplay=false&offerviewer=true&showtitle=true&showbrand=false&start=0&interactivity=all" width=720 height=405 style="border: 1px solid #464646;" allowfullscreen allow="autoplay"></iframe>

<br />
**Fragen an Frau Holzer**

* "Ergänzt" im Sinne von z.B. Artikeln über 2.000 €? D.h. fällt diese Grenze durch ein mögliches Kostensplitting? Ja, die Obergrenze fällt weg. Die Kostengrenze wird deswegen fallen, weil sie jetzt schon nur für Gold-OA gilt und so komplexe Doppelstrukturen entstehen, wenn höhere Gebühren übernommen werden müssen. Eine absolute Preisobergrenze wird von der DFG nicht mehr vorgesehen.
* Dürfen dann tatsächlich ausschliesslich Publikationen abgerechnet werden, die aus DFG-Förderprojekten entstanden sind? Auch bei Transformationsverträgen? Nein, das ist nicht so. Genau deswegen haben wir jetzt die Programmlösung erst mal gewählt, weil über die Pauschale müsste aktuell zuwendungsrechtlich eine Korrelation hergestellt werden: Mitteleinsatz & Finanzierung von Publikationen aus der DFG-Förderung. Das bringt einen zu großen Verwaltungsaufwand mit sich. Das gilt dann auch für Transformationsverträge.
* Wie bemisst sich der "festgelegte Zuschuß" zu OA-Publikationen? Anhand der momentanen Durchschnittskosten für eine Gold-Open-Access-Publikation.
* Gelten Zusatzvereinbarungen aus Allianzverträgen auch als Transformationsverträge (z.B. RSC, IOP)? Ja, wenn Double Dipping ausgeschlossen ist.
* Wie werden die anteiligen Zuschüsse gem. Durchschnittskosten bei Monografien berechnet (ein kleiner dt. Universitätsverlag hat deutlich niedrigere BPC als ein großer kommerzieller)? Der Zuschussbetrag orientiert sich an kommerziellen Durchschnittspreisen, die zwar weit unter dem liegen, was der Schweizer Nationalfond oder was der FWF bereit sind zu zahlen, der aber, je näher der Preis an einem Universitätsverlag dran ist, umso kostendeckender ist.
* Gibt es auch eine Förderung für PAR-Fees? Ja, es wird ein Zuschussbetrag festgelegt für jede Art der förderfähigen Publikation. Es gibt dann keine Diskriminierung zwischen Open Access Gold und Open Access aus Transformationsverträgen, der über PAR-Fees berechnet wird.
* In welchem Bereich wird der feste Zuschuss liegen: 500 €, 1000 €, 1500 €? Unter 1000€
* Welche anderen Finanzierungsquellen gibt es denn noch? Rückstellungen aus Elsevier-Verträgen, Mittel, die auf Länderebene für Open Access zur Verfügung gestellt werden, Drittmittel etc.
* Es können 65% der Defizitsumme beantragt werden - gibt es dazu einen verpflichtenden Eigenanteil? Es gibt keinen weiteren verpflichtenden Eigenanteil.
* Erwarten Sie Anträge von einzelnen Einrichtungen oder eher zentrale Anträge z.B. von der MPDL? Erwarten eigentlich Anträge von einzelnen Einrichtungen, die eben da eine besonders große Notwendigkeit sehen, zusätzliche Mittel einzuwerben.
* Ist die Förderlinie zeitlich begrenzt? Aktuell ist die Förderlinie Teil des Programms überregionale Lizenzierung, das voraussichtlich nur noch bis Ende dieses Jahres läuft. Es besteht aber die Möglichkeit, die Ausschreibung noch ein Jahr länger offen zu halten, das ist aber noch nicht abschließend geklärt.
* Wenn es eine Länderförderung gibt, ist dann dennoch eine DFG-Kofinanzierung möglich? Ja, in dem Maße, in dem Mittel nicht gedeckt werden können.
* Sind auch kleinere, nicht so forschungsintensive Einrichtungen wie z.B. FHs berechtigt, DFG Fördermittel zu beantragen? Ja, gilt für alle.
* Gibt es Ausschlusskriterien? Für die aktuelle Förderung: nein, keine. Außer, dass ein Defizit nachweisbar sein muss.
* Wird es parallel zu den Zuschüssen zu den OA-Publikationen auch weiterhin eine direkte projektinterne Publikationsförderung für OA-Veröffentlichungen geben? Die projektinterne Publikationsförderung ist ja nicht auf Open Access zugeschnitten. Das sind im Prinzip Mittel, die schon sehr lange in den Forschungsprojekten beinhaltet sind und die eigentlich noch in der Druckwelt entstanden sind und im Prinzip ursprünglich gedacht waren, Color Charges, Page Charges etc. noch zu decken. Langfristig dürften die im Open-Access-Kontext nicht mehr anfallen, aber die Abschaffung wird nicht von heute auf morgen möglich sein und es wird wahrscheinlich eine gewisse Überlappung geben.
* Wird es den OA Fonds bzw. die Förderung durch die DFG noch geben? Nein, die werden durch die neue Open-Access-Kosten/-Publikationskostenförderung abgelöst.
* Noch eine nachträgliche Frage zu diesem temporären DFG-Förderprogramm: wird das wie im jetzigen Fonds auf Basis einer "Prognose" vorab geschehen ODER anlassbezogen beim Vorliegen einer Rechnung (das wird dann administrativ aufwändig) ODER wird das im nachinein abrechenbar gestaltet werden (z.B. gesammelt für alle anfallenden Rechnungen für ein 1/2 Jahr)? Auf der Basis einer Prognose.
* Haben alternative OA-Finanzierungsmodelle (bei denen nicht auf Basis des Publikationsvolumens finanziert wird) in den Planungen 2021-2027 einen Ort? Ja, es sollen auch Mittel für konsortiale Modelle u.a. für den OA eingeworben werden können, wenn die Kosten belegbar sind.
* Vielleicht eine Frage sowohl an Frau Holzer als auch an Frau Overkamp: bis wann werden die Ausgleichszahlungen erwartet und ist es realistisch, dass die DFG entsprechende Anträge (bei Defizitnachweis etc.) in dieser Zeit prüft und bewilligt? Antwort Frau Holzer (DFG): Die Anträge können jederzeit in diesem Jahr gestellt werden und es kann mit einer Entscheidung in der Regel innerhalb von maximal einem halben Jahr gerechnet werden. Wenn die Wiley-Ausgleichsanträge also im ersten Halbjahr eingehen, ist eine Entscheidung in diesem Jahr möglich. Ab nächstem Jahr soll ein anderer Fördermechanismus greifen.  [*Anmerkung aus dem Chat zu diesem Thema*: Die Beantragung von Mitteln für die Ausgleichsfinanzierung ist für einzelne Einrichtungen nicht wirklich attraktiv. Da die einzelne Einrichtung nicht unmittelbar von einem Antrag profitiert und gleichzeitig die Voraussetzungen für einen Antrag eigentlich bei allen Betroffenen gleich sind, sogar die Daten zentral erhoben werden, drängt sich aus meiner Sicht eine zentral koordinierte Beantragung auf.] <br />
Antwort Frau Overkamp: Wiley und MPDL Services GmbH stehen weiterhin im engen Kontakt, um die Datenlieferung zu den im Rahmen der DEAL-Vereinbarung anzurechnenden Artikel in den Hybrid-Zeitschriften des Anbieters zu finalisieren. Mittlerweile bin ich vorsichtig optimistisch, dass der Prozess in den nächsten Wochen final abgeschlossen werden kann und hoffe, dass die Bereitstellung der 2019er Hybrid-Publikationsreports noch in diesem Halbjahr erfolgen kann. Da die Ausgleichszahlungen - aufgrund der fortgeschrittenen Zeit - vermutlich nicht für den buchhalterischen Jahresabschluss 2019 der MPDL Services 2019 berücksichtigt werden können, werden wir wohl keine "festen" Fristen definieren. Es ist verständlich, dass Einrichtungen nur dann Zahlungszusagen machen können, wenn dafür auch die finanziellen Mittel gesichert sind (z.B. aus Rückstellungen oder aus dem DFG-Antragswesen).


- - -

### Statusbericht der MPDL Services GmbH

Inga Overkamp ist Diplom-Bibliothekarin, seit 1999 in der MPG und seit 2007 als Mitarbeiterin der Max Planck Digital Libary tätig. Aktuell ist sie teilweise von den Tätigkeiten für die MPG/MPDL freigestellt und in der MPDL Services GmbH zuständig für die Umsetzung der DEAL-Teilnahmeprozesse.

Link zu den [Folien (CC-BY)](/assets/files/2020-03-09_Webinar_DEALWiley_StatusberichtMPDLServiceGmbH.pdf).

- - -

### Reporting als Grundlage für Kostenverteilungsmodelle

Dirk Pieper verantwortet an der Universitätsbibliothek Bielefeld als stellvertretender Direktor den Projekt- und Innovationsbereich. Zuvor war er in Bielefeld unter anderem für die Medienerwerbung und die Lizenzierung von elektronischen Inhalten zuständig. Als Leiter des von der Allianz der deutschen Wissenschaftsorganisationen geförderten Projekts "Nationaler Open-Access-Kontakpunkt OA2020-DE" ist er an der Entwicklung von Open-Access-Geschäftsmodellen beteiligt.

Link zu den [Folien (CC-BY)](/assets/files/2020-03-09_Webinar_DEALWiley_openapc.pdf)
<br />

<iframe src="https://uni-bielefeld.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=d7bfac47-3e15-4360-89ef-ab9100b56d00&autoplay=false&offerviewer=true&showtitle=true&showbrand=false&start=0&interactivity=all" width=720 height=405 style="border: 1px solid #464646;" allowfullscreen allow="autoplay"></iframe>

<br />
**Fragen an Herrn Pieper**

* Wer meldet denn die Wiley-Open-Access-APCs an OpenAPC? Bislang machen das die Einrichtungen selber, denkbar wäre aber auch eine zentrale Datenlieferung.
* Wer entscheidet denn eine zentrale Meldung? Aus meiner Sicht müsste das von der MPDL Services GmbH bzw. innerhalb der DEAL Projektgruppe entschieden werden.
* Wird berücksichtigt, dass nur ein halbes Jahr gemeldet wird? Ja, das war Gegenstand von umfänglichen Diskussionen in der openAPC-Mailingliste. Wir haben uns letztendlich dazu entschlossen, tatsächlich den Betrag der jährlichen Vorausszahlung, den einige Institutionen schon gemeldet hatten, durch zwei zu teilen. Das hat Vor- und Nachteile, wobei wir mehr Vorteile sehen. Insbesondere auch dann wenn es darum geht, diese Daten von 2019 mit den Folgedaten von 2020 und 2021 zu vergleichen. Ein Nachteil ist natürlich, dass die tatsächlichen Kosten für 2019 verzerrt dargestellt werden.
* Empfehlen Sie, vor der Meldung auf die Jahresdaten von Wiley zu warten, da die bislang den Institutionen unbekannte Zahl der Opt-out-Artikel ja für die Berechnung der PAR-Fee relevant ist? Ja, das würde ich so empfehlen. Das war nach meinem Verständnis dann auch das Fazit der Diskussion, die wir in der openAPC-Mailingliste hatten. Einige Einrichtungen, die über die Kapazitäten verfügen das selber bibliometrisch zu erfassen, aus Datenbanken wie Web of Science z.B., haben vielleicht jetzt schon ein größeres Verständnis oder einen besseren Einblick darüber, wie hoch denn der Anteil von Opt-Out-Artikeln sein könnte. Aber ich denke wenn wir das alle auf einer gemeinsamen Basis tun - und das können nun mal nur die Vergleichsrechnungen der MPDL Services-GmbH sein – dann haben wir auch eine gemeinsame Datengrundlage.


- - -

**<a name="Tabelle"/>**

## Tabellarische Übersicht aller beim Webinar gehaltenen Vorträge

| Thema                        | Referent*in                     | Folien | Aufzeichnung | Lizenz Vortrag |
|------------------------------|---------------------------------|--------|--------------|----------------|
| Begrüßung & Einführung       | Alexandra Jobmann,  Dirk Pieper |    -   | [Link](https://uni-bielefeld.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=8a7c31e7-907f-41c9-ace5-ab9100b14ae3) |              |
| Bericht UB Bielefeld         | Dr. Silvia Herb                 | [Link](/assets/files/2020-03-09_Webinar_DEALWiley_UBBielefeld.pdf) | [Link](https://uni-bielefeld.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=404c831b-a0fb-4d59-8f1a-ab9100b14b63) | CC-BY-NC     |
| Bericht ULB Münster          | Dr. Peter te Boekhorst          | [Link](/assets/files/2020-03-09_Webinar_DEALWiley_ULBMünster.pdf) | [Link](https://uni-bielefeld.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=da1675e9-182f-4d34-91da-ab9100b18e8c) | CC-BY-NC-SA  |
| Bericht FH Bielefeld         | Dr. Karin Ilg                   | [Link](/assets/files/2020-03-09_Webinar_DEALWiley_FHBielefeld.pdf) |     -        | Urheberrecht |
| Bericht Leibniz-Gemeinschaft | Lydia Koglin                    | [Link](/assets/files/2020-03-09_Webinar_DEALWiley_IGB.pdf) | [Link](https://uni-bielefeld.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=e1ec3f62-fd0b-4f4f-8f6e-ab9100b2fd92) | CC-BY        |
| Bericht Helmholtz-Gemeinschaft & Vergleich Wiley & SpringerNature | Dr. Bernhard Mittermaier | [Link](/assets/files/2020-03-09_Webinar_DEALWiley_HGF_SNundWiley.pdf) | [Link](https://uni-bielefeld.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=66546ee5-b90f-4320-9bf4-ab9100b3caa2) | CC-BY |
| Informationen zur möglichen DFG-Förderung | Dr. Angela Holzer  | [Link](/assets/files/2020-03-09_Webinar_DEALWiley_FörderangebotederDFG.pdf) | [Link](https://uni-bielefeld.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=5e3f51d5-6bb2-4732-82be-ab9100b44b9e) | Urheberrecht |
| Statusbericht der MPDL Services GmbH | Inga Overkamp           | [Link](/assets/files/2020-03-09_Webinar_DEALWiley_StatusberichtMPDLServiceGmbH.pdf) |     -        | CC-BY        |
| Reporting als Grundlage für Kostenverteilungsmodelle | Dirk Pieper | [Link](/assets/files/2020-03-09_Webinar_DEALWiley_openapc.pdf) | [Link](https://uni-bielefeld.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=d7bfac47-3e15-4360-89ef-ab9100b56d00) | CC-BY        |
