<!--
.. title: 2nd DEAL done – All German research articles in Springer Nature journals to be published open access under new transformative agreement
.. slug: seconddealdone_springer
.. date: 2020-01-09 13:00:00+1:00
.. author:
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Germany’s Projekt DEAL and the publisher Springer Nature have - like DEAL and Wiley one year before - entered a ground-breaking transformative agreement, in line with the objectives of the Open Access 2020 initiative.

Through the agreement, authors affiliated with the 700+ German academic and research institutions which are part of Projekt DEAL, will be able to publish their accepted manuscripts immediate (gold) open access in both Springer Nature hybrid and fully open-access journals, with the relative costs managed centrally by their institutions. The agreement is expected to see well over 13,000 articles a year from German researchers published open access, making it the largest of its kind.
Dated 1 January 2020, the agreement provides open-access publishing services and full reading access to Springer Nature journals to scholars and students from across the German research landscape.

The signed agreement will be fully published on the Projekt DEAL website in conjunction with the start of the sign-up process for German institutions, towards the end of January 2020.

Read the official press releases of the [HRK](https://www.hrk.de/press/press-releases/press-release/meldung/springer-nature-and-germanys-projekt-deal-finalise-worlds-largest-transformative-open-access-agree/) and [Springer Nature](https://group.springernature.com/gp/group/media/press-releases/springer-nature-projekt-deal/17553230).
