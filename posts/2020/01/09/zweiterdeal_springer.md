<!--
.. title: Zweiter DEAL abgeschlossen - Wissenschaftliche Publikationen aus Deutschland werden künftig Open Access bei Springer Nature publiziert
.. slug: zweiterdealabgeschlossen_springer
.. date: 2020-01-09 13:00:00+1:00
.. author:
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Das deutsche Projekt DEAL und der Verlag Springer Nature haben, ebenso wie Projekt DEAL und Wiley vor einem Jahr, eine zukunftsorientierte Partnerschaft zur Erprobung neuer Publikationsmodelle geschlossen, ganz im Sinne der Open-Access-Transformation und der Prinzipien der OA2020-Initiative.

Durch den Vertrag können Autor_innen der mehr als 700 teilnahmeberechtigten deutschen Wissenschafts- und Forschungseinrichtungen ihre Manuskripte im Gold-Open-Access-Standard in Springer Nature Hybrid- oder reinen Open-Access-Zeitschriften veröffentlichen. Im Rahmen der Vereinbarung werden jährlich weit über 13.000 Open-Access-Artikel von deutschen Forscher_innen erwartet. Damit ist der Vertrag aktuell der weltweit größte seiner Art.
Bereits seit dem 1. Januar 2020 können Wissenschaftler_innen und Studierende aus Deutschland Open Access bei Springer Nature veröffentlichen und erhalten umfangreichen Lesezugriff auf die Zeitschriften-Inhalte des Verlags.

Der komplette Vertragstext wird parallel zum Start des Registrierungsprozesses für deutsche Einrichtungen Ende Januar 2020 auf der Projekt DEAL-Webseite zur Verfügung stehen.

Lesen Sie dazu auch die offiziellen Pressemitteilungen der [HRK](https://www.hrk.de/presse/pressemitteilungen/pressemitteilung/meldung/springer-nature-und-projekt-deal-unterzeichnen-weltweit-umfangreichsten-open-access-transformationsv/) und von [Springer Nature](https://group.springernature.com/gp/group/media/press-releases/springer-nature-projekt-deal/17553230).
