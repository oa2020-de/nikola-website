<!--
.. title: Und ein zweites Mal werden die Neuerscheinungen des Programmbereichs Politikwissenschaft 2020 Open Access
.. slug: transcriptOPENLibrary_weitererErfolg
.. date: 2020-01-20 14:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## Die transcript Open Library-Community hat es wieder geschafft: Alle Neuerscheinungen des Programmbereichs Politikwissenschaft 2020 werden Open Access!

BIELEFELD. Das Open-Access-Modell des Bielefelder Wissenschaftsverlages transcript war erneut erfolgreich. Ein Netzwerk aus 47 Förderern finanziert zum zweiten Mal die Open-Access-Bereitstellung aller Neuerscheinungen aus dem Programmbereich Politik und trägt damit zur Verstetigung eines transparenten und wirtschaftlich tragbaren Open-Access-Publikationsmodells bei.

**Wir danken den Förderern, die mit ihrem Beitrag dafür sorgen, dass die Politik-Frontlist im Open Access für alle Mitglieder des Wissenschaftsbetriebs zur Verfügung steht!**

Durch die hohe Akzeptanz des Projekts und dank der Beteiligung des FID Politikwissenschaft in Höhe von 25% des Gesamtvolumens, betragen die tatsächlichen Kosten pro Einrichtung nur noch rund 76€ pro Publikation. Ein Freistellungsbetrag für Open Access, der im Rahmen dessen liegt, was für konventionelle eBooks als Lizenz gezahlt wird und der deutlich unter den durchschnittlichen üblichen Book Processing Charges für eine Einrichtung liegt. Um allen Open-Access-Unterstützer_innen eine Teilnahme zu ermöglichen, gibt es neben dem vollen Sponsoring zwei weitere Preisstufen: Das Sponsoring Light (38€ pro Publikation) und das neue [Mikrosponsoring](https://www.transcript-verlag.de/open-library-politikwissenschaft) (13,64€ pro Publikation).
<!-- TEASER_END -->

Das Publikationsmodell folgt dem Modell »E-Book-Paket«. Statt des Erwerbs einer Campuslizenz mit den bekannten Begrenzungen der Nutzungsmöglichkeiten wird die Open-Access-Bereitstellung einer Frontlist finanziert und steht damit allen Wissenschaftsakteuren zur Verfügung. Konkret werden über das Projekt 22 Novitäten aus dem Programmbereich Politikwissenschaft (d.h. Monografien und Sammelbände aus den Bereichen internationale und europäische Politik und Globalisierung, Parteien, Soziale Bewegung und Zivilgesellschaft, Policy, Politics, Politische Bildung, Politische Theorie und Polity) im Open Access erscheinen.


### Die Teilnehmer_innen

Die „transcript Open Library Politikwissenschaft“ ist ein gemeinschaftliches Projekt des transcript Verlages und des Nationalen Open Access Kontaktpunktes OA2020-DE. Es wird durch die Infrastruktur von Knowledge Unlatched sowie der Mitarbeit der Bibliothekslieferanten Dietmar Dreier Wissenschaftliche Versandbuchhandlung GmbH, Missing Link Versandbuchhandlung eG und der Schweitzer Fachinformation unterstützt. Die transcript Open Library Community besteht aus dem FID Politikwissenschaft als Hauptsponsor, 42 Vollzahler_innen, sowie jeweils zwei Beteiligungen im Rahmen des Sponsoring Lights und des Mikrosponsorings.


### Die Fördercommunity

*25 Prozent des Gesamtvolumens:* POLLUX - Informationsdienst Politikwissenschaft

*Sponsoring Light:* Bundesministerium der Verteidigung Bonn, Landesbibliothek Oldenburg

*Mikrosponsoring:* Deutsches Institut für Internationale Politik und Sicherheit Berlin , Leibniz-Institut für Europäische Geschichte Mainz

*Full pledges:* Universitätsbibliothek Bayreuth, UB der HU Berlin, Staatsbibliothek zu Berlin, UB der FU Berlin, Universitätsbibliothek Bielefeld, UB der Ruhr-Universität Bochum, Universitäts- und Landesbibliothek Darmstadt, SLUB Dresden, UB der Universität Duisburg-Essen, Universitäts- und Landesbibliothek Düsseldorf, UB Erlangen-Nürnberg, UB der Johann-Christian-Senckenberg-Universität Frankfurt am Main, UB der Justus-Liebig-Universität Gießen, SUB Göttingen, Universitätsbibliothek Graz, UB der Fernuniversität Hagen, ULB Sachsen-Anhalt Halle (Saale), Staats- und Universitätsbibliothek Hamburg, Technische Informationsbibliothek Hannover, Niedersächsische Landesbibliothek Hannover, Universitätsbibliothek Kassel, Universitäts- und Stadtbibliothek Köln, Kommunikations-, Informations- und Medienzentrum Konstanz, Universitätsbibliothek Koblenz-Landau, Universitätsbibliothek Leipzig, Zentral- und Hochschulbibliothek Luzern, UB der Johannes-Gutenberg-Universität Mainz, Universitätsbibliothek Marburg, UB der LMU München, Max Planck Digital Library, ULB Münster, Universitätsbibliothek Osnabrück, Universitätsbibliothek Passau, Universitätsbibliothek Potsdam, Universitätsbibliothek Siegen, Universitätsbibliothek Vechta, UB der Bauhaus-Universität Weimar, Universitätsbibliothek Wien, UB der Bergischen Universität Wuppertal, Universitätsbibliothek Würzburg, Zentralbibliothek Zürich


[[Link zur Pressemitteilung](/assets/files/PM_OLPolWI2020.pdf)]
