<!--
.. title: Why competition is necessary for APCs
.. slug: competiton_apcs
.. date: 2020-01-13 08:00:00+1:00
.. author: Nina Schönfelder
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

### Elsevier could raise APCs by more than 50% to preserve its revenue and profit in a pure OA publishing landscape, as a new case study concludes.

A new [research article](https://doi.org/10.3390/publications8010003), published by Sergio Copiello in *Publications*, studies how Elsevier, as an example of a very big, for-profit, subscription-based publisher, could adapt its business model to the open-access transition.

To preserve its revenue and profit on the 2017/2018 level in an open access only publishing landscape, Elsevier could raise the list-price APCs by more than 50% from an average of USD 2,824 to USD 4,173–4,482 (excl. VAT). That would safeguard Elsevier a 37% profit margin if all authors (possibly funded by their affiliated institutions) adopt the open access model and subscriptions to ScienceDirect become worthless. Currently, Elsevier generates profits margins two- to three-times higher than the overall publishing sector. If Elsevier’s profit margin aligns to the market benchmark (for example, because of increasing competition), the average APC could be within a range of USD 3,066–3,308. An alternative strategy could be to secure revenues by increasing the acceptance rate of submitted manuscripts from currently approx. 27% to approx. 45%. Of course, Elsevier could also preserve its profitability by reducing investment and operational costs.

Link to the study: Copiello, S. (2020). Business as Usual with Article Processing Charges in the Transition towards OA Publishing: A Case Study Based on Elsevier. *Publications, 8*(1). doi:[10.3390/publications8010003](https://doi.org/10.3390/publications8010003)
