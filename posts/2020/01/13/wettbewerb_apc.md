<!--
.. title: Warum Wettbewerb bei APCs notwendig ist
.. slug: wettbewerb_apcs
.. date: 2020-01-13 08:00:00+1:00
.. author: Nina Schönfelder
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

### Eine neue Fallstudie zeigt, dass Elsevier die APCs um mehr als 50% erhöhen könnte, um seinen Umsatz und Profitabilität in einer reinen Open-Access Publikationslandschaft zu erhalten.

Ein [aktueller Beitrag](https://doi.org/10.3390/publications8010003) von Sergio Copiello (publiziert in der Fachzeitschrift *Publications*) untersucht, wie Elsevier, stellvertretend für andere sehr große, kommerzielle Subskriptionsverlage, auf die Open-Access-Transformation mit der Anpassung seines Geschäftsmodells reagieren könnte.

Um Umsatz und Gewinn auf den Vorjahresniveaus zu halten, könnte Elsevier in einer Open-Acces-Welt seine Listen-APCs um etwa 50% von durchschnittlich 2.824 USD auf 4.173–4.482 USD (exkl. MwSt.) erhöhen. Dies würde dem Verlag weiterhin eine Umsatzrendite von etwa 37% sichern, wenn alle Autor_innen bzw. ihre affiliierten Einrichtungen die Open-Access-Option nutzen und somit das Abo für ScienceDirect wertlos werden würde. Dabei erzielt Elsevier zurzeit Renditen, die etwa doppelt bis dreifach so hoch sind wie die des Publikationsmarktes allgemein. Würde Elsevier also eine marktdurchschnittliche Rentabilität in Kauf nehmen bzw. durch die Etablierung von Wettbewerbsmechanismen im APC-Bereich nur eine marktdurchschnittliche Rentabilität erzielen können, so könnten sich die APCs auf durchschnittlich 3.066–3.308 USD einpendeln. Eine alternative verlagsseitige Strategie könnte es sein, den Umsatz durch eine Erhöhung der Akzeptanzquote eingereichter Manuskripte von derzeit ca. 27% auf etwa 45% zu sichern. Selbstverständlich könnte die aktuelle Rentabilität auch über die Senkung von Kosten erzielt werden.

Der Link zur Studie: Copiello, S. (2020). Business as Usual with Article Processing Charges in the Transition towards OA Publishing: A Case Study Based on Elsevier. *Publications, 8*(1). doi:[10.3390/publications8010003](https://doi.org/10.3390/publications8010003)
