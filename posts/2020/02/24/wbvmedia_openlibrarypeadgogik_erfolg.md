<!--
.. title: wbv Media und Knowledge Unlatched (KU) schließen Pilotprojekt zur Open-Access-Finanzierung in der Pädagogik ab
.. slug: erfolg_openlibrarypeadgogik
.. date: 2020-02-24 10:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## Auch in der erziehungswissenschaftlichen Community gibt es nun ein Open-Access-Angebot für Bücher

*Bielefeld/Berlin, 19. Februar 2020.* 27 wissenschaftliche Einrichtungen beteiligen sich am kooperativen Finanzierungsmodell für Open-Access-Monografien und Sammelbände. Damit werden 20 Neuerscheinungen aus der Erwachsenenbildung sowie Berufs- & Wirtschaftspädagogik 2020 von wbv Media als frei verfügbare eBook-Version erscheinen.

Das Publikationsmodell folgt dem Modell »E-Book-Paket«. Doch statt des Erwerbs einer Campuslizenz mit den bekannten Grenzen der Nutzungsmöglichkeiten, wird die Open-Access-Bereitstellung einer Frontlist von 20 Titeln finanziert und steht damit allen Wissenschaftsakteuren zur Verfügung. Die Kosten für jeden Titel betragen 120 € netto pro Institution, in der Sponsoring-Light Version sind es lediglich 60 € netto pro Institution. Zusätzlich zu dem freien digitalen Zugang erhält jede_r Finanzierungspartner_in ein gedrucktes Freiexemplar. Zur aktuellen Frontlist gehören Titel aus den wbv-Reihen "Erwachsenenbildung und lebensbegleitendes Lernen", "Hochschulweiterbildung in Theorie und Praxis", "Berufsbildung, Arbeit und Innovation", sowie "Wirtschaft – Beruf – Ethik".

[Zur vollständigen Pressemitteilung.](https://knowledgeunlatched.org/2020/02/knowledge-unlatched-und-wbv-2020-2/)
