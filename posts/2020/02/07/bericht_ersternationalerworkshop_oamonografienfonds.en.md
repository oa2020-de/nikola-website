<!--
.. title: Report from the first national best practice workshop of the German open access monograph funds
.. slug: report_firstnationalworkshop_oamonographfonds
.. date: 2020-02-07 08:00:00+1:00
.. author: Thomas Jung (DIE), Monika Pohlschmidt (IDS), Christoph Schindler (DIPF) & Olaf Siegert (ZBW)
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

** Disclaimer: This is a guest contribution from the organizers.**

On 3 December 2019, the project team of the open access fund for monographs launched by the Leibniz Association invited other fund operators from the German scientific community to an exchange of information. The workshop was hosted by the Leibniz Institute for the German Language (IDS) in Mannheim, which is in charge of the [Leibniz Open Access Monograph Publishing Fund](https://www.leibniz-gemeinschaft.de/en/research/open-science-and-digitalisation/open-access/monograph-publishing-fund.html). Participants included the project participants from the other six Leibniz Institutes (DIE - German Institute for Adult Education, DIPF - Leibniz Institute for Research and Information in Education, GNM - Germanisches Nationalmuseum, IfZ - Institute of Contemporary History, TIB - Leibniz Information Centre for Science and Technology & University Library and ZBW - Leibniz Information Centre for Economics), representatives of 11 publication funds for monographs that have been created or are planned throughout Germany in the last two years, and institutions that promote open access monographs by other means: Freie Universität Berlin, Humboldt-Universität zu Berlin, Technische Universität Berlin, Bielefeld University, Universität Duisburg-Essen, Technische Universität Darmstadt, University of Konstanz, Johannes Gutenberg University Mainz, University of Münster, the Fraunhofer-Gesellschaft and the Lower Saxony Consortium of the planned Open Access Publication Fund of the State of Lower Saxony.
<!-- TEASER_END -->

The venue was not chosen at random: As an institute for the humanities, the IDS stands as an example of those disciplines that have a special interest in establishing financing possibilities for books that are to be published in gold open access and yet are to be realised in cooperation with publishers. In addition to digital visions, pragmatic, financial and licensing solutions are sought for the actually more traditional form of scientific communication "book". Thus, the aim of the workshop was to exchange experiences of the fund-operating institutions, to inform each other about funding criteria and sums, and to harmonise the workflow of application and processing. The workshop was chaired by Olaf Siegert (ZBW), who is active in various science policy contexts as spokesperson for the Leibniz Association's Working Group Open Access.

### Eligibility criteria of the open access monograph funds

The one-day workshop began after the welcoming address by host Monika Pohlschmidt (IDS) with a short introduction of the fund representatives, in which general questions about the participating funds were briefly and concisely reported (the majority of the slides are available [here](/assets/files/20191203_Vorträge_OAMonografienWorkshop.pdf) (only in german)). In this way, those present were able to gain an overview of key data and framework conditions in the various federal states and institutions. The exchange focused on questions regarding the financial volume, duration and general eligibility criteria of the funds. In addition, the participants discussed which specific services are funded by publishers and service providers and how the compliance with the funding criteria and the scientific quality of the publication to be funded can be ensured.

One of the open points of the discussion was the question, which publishing costs are considered as reimbursable open access costs for the fund operators. This concerns, for example, the costs of typesetting: Some funds support precisely this service, as all manuscripts are usually converted from WORD to XML data and typeset in this format; other funds do not. To a large extent, the process of creating a digital file for an open access publication is identical to the process from which a print file is created. It is therefore a particular challenge for publishers when drawing up offers and contracts to show open access costs clearly and transparently.

In addition, some publishers charge for a „loss of sales“ from the funders, while other publishers charge a more or less flat-rate open access fee based on the amount of the fund. That means, calculating the open access costs remains difficult, especially for those disciplines in the social sciences and humanities in which the book continues to play an important role in the struggle for reputation and impact and which would like to offer a printed version in addition to the open access version. The fund operators take into account the special position of the book in these disciplines, but when accepting applications they must pay close attention to the calculated publisher costs that may not be used to produce the printed edition. In the [OGeSoMo project](https://www.uni-due.de/ogesomo/), criteria and a description of individual items of publishing services have been developed together with publishers. These criteria and descriptions can be seen as a development step on the way to transparent cost models and can be used as guidelines for negotiations with other publishers. The [Fair Open Access Alliance](https://www.fairopenaccess.org/) project has developed similar proposals.

### Model publishing contract and other detailed questions

Furthermore the workshop participants discussed to set up a special open access contract about in addition to model calculations. As there are still no long-term financing models for books in open access in Germany, some publishers are still hesitant to draw up new contracts for open access monographs and are supplementing their existing publishing contracts instead with additional paragraphs for licensing in open access. Nevertheless, the workshop participants identified indispensable elements of a possible model contract:

* CC licence
* DOI allocation
* Long-term archiving
* Simple right of use for the publisher
* Clear identification of the work as OA 'gold' in the imprint
* Reference to funding by funds in the imprint and, last but not least,
* Immediate publication in open access without embargo

A model contract of this kind, which is being used by a pioneering publisher in the field of open access, is now available and can be viewed [here](/assets/files/Autorenvertrag_OA_Muster.pdf) (only in german). From among the participants, it was also suggested that the National Contact Point Open Access OA2020-DE should contact the German Publishers & Booksellers Association (Börsenverein des Deutschen Buchhandels) to promote greater openness to open access and to work towards offering interested publishers legal assistance in drafting contracts and imprints.

There was broad agreement that there should be uniform price caps for the promotion of open access monographs throughout Germany. Currently the maximum sums range between EUR 4,500 and 6,000, depending on the fund. There was also an ongoing discussion about the situation in the Austrian and Swiss National Science Foundation, both of which are funding the publication of open access monographs with considerably higher sums.

What is not yet in sight with the existing funds are detailed questions as to which types of book formats should be eligble for funding. So far, the practice has focused on single monographs and anthologies, whereas textbooks and dictionaries are mainly excluded from funding because of the high sums, that publishers would charge here for an open access version. Dissertations on the other hand are eligible for funding at least for some funders.

As far as quality standards for eligible books are concerned, there are now very helpful recommendations available. Although these cannot be implemented in every case, they can serve as a good guideline both in the design of the funding criteria of a monograph fund and in the processing of applications (e.g. the quality criteria of [Knowledge Unlatched and the National Contact Point OA2020-DE](http://nbn-resolving.de/urn:nbn:de:0070-pub-29321893) or the criteria from the [Working Group of German University Presses](https://blog.bibliothek.kit.edu/ag_univerlage/wp-content/uploads/2018/09/Anforderungen-an-OA-Monografien_ag_universitaetsverlage_20180904.pdf) (only in german)). In any case, it is important to look for quality assurance appropriate to the discipline in question.

### Lessons learned

At the end of an extremely stimulating workshop, it was agreed that many pragmatic issues still need to be clarified, e.g. the development of model contracts or elegible publishing costs for funding. There also remain some potential conflicts, as there are still no procedures for reviewing and enforcing some of the eligibility criteria.
Thus, the workshop participants agreed to exchange ideas in the future and to meet again on an annual basis. For one thing is certain: the number of open access actors and funds will grow.

If you have any further questions about the workshop or the open access monograph fund, please feel free to contact the organisers at any time:<br />
Dr. Thomas Jung, DIE - German Institute for Adult Education, <i class="fa fa-envelope" aria-hidden="true"></i>  [E-Mail](mailto:thomas.jung@die-bonn.de)<br />
Monika Pohlschmidt, IDS - Leibniz Institute for the German Language, <i class="fa fa-envelope" aria-hidden="true"></i>  [E-Mail](mailto:pohlschmidt@ids-mannheim.de)<br />
Dr. Christoph Schindler, DIPF - Leibniz Institute for Research and Information in Education, <i class="fa fa-envelope" aria-hidden="true"></i>  [E-Mail](mailto:schindler@dipf.de)<br />
Olaf Siegert, ZBW - Leibniz Information Centre for Economics, <i class="fa fa-envelope" aria-hidden="true"></i>  [E-Mail](mailto:O.Siegert@zbw.eu)<br />

