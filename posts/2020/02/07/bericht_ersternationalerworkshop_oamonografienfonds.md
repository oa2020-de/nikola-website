<!--
.. title: Bericht vom ersten nationalen Best-Practice-Workshop der deutschen Open-Access-Monografienfonds
.. slug: bericht_ersternationalerworkshop_oamonografienfonds
.. date: 2020-02-07 08:00:00+1:00
.. author: Thomas Jung (DIE), Monika Pohlschmidt (IDS), Christoph Schindler (DIPF) & Olaf Siegert (ZBW)
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

**Disclaimer: Dies ist ein Gastbeitrag von den Veranstalter_innen.**

Am 3. Dezember 2019 luden das Projektteam des von der Leibniz-Gemeinschaft aufgelegten Open-Access-Fonds für Monografien andere Fondsbetreiber aus der deutschen Wissenschaftslandschaft zu einem Informationsaustausch ein. Der Workshop wurde ausgerichtet vom Leibniz-Institut für Deutsche Sprache (IDS) in Mannheim, das den [Leibniz-Monografienfonds](https://www.leibniz-gemeinschaft.de/forschung/open-science-und-digitalisierung/open-access/publikationsfonds-monografien.html) federführend betreut. Zugegen waren neben den Projektbeteiligten der anderen sechs Leibniz-Institute (DIE - Deutsches Institut für Erwachsenenbildung, DIPF - Leibniz-Institut für Bildungsforschung und Bildungsinformation, GNM - Germanisches Nationalmuseum, IfZ - Institut für Zeitgeschichte, TIB - Leibniz-Informationszentrum Technik und Naturwissenschaften und Universitätsbibliothek und ZBW - Leibniz-Informationszentrum Wirtschaft), Repräsentantinnen und Repräsentanten von 11 deutschlandweit in den letzten zwei Jahren entstandenen oder geplanten Publikationsfonds für Monografien sowie Einrichtungen, die Open-Access-Monografien auf anderem Wege fördern: Freie Universität Berlin, Humboldt-Universität zu Berlin, Technische Universität Berlin, Universität Bielefeld, Universität Duisburg-Essen, Technische Universität Darmstadt, Universität Konstanz, Johannes-Gutenberg-Universität Mainz, Westfälische Wilhelms-Universität Münster, die Fraunhofer-Gesellschaft sowie das Niedersachsen-Konsortium des geplanten Open-Access-Publikationsfonds des Landes Niedersachsen.
<!-- TEASER_END -->

Der Veranstaltungsort war nicht zufällig gewählt: Als geisteswissenschaftliches Institut steht das IDS quasi exemplarisch für jene Disziplinen, die ein besonderes Interesse haben, Finanzierungsmöglichkeiten für Bücher zu etablieren, die im goldenen Open Access veröffentlicht und dennoch gemeinsam mit Verlagen realisiert werden sollen. Neben digitalen Visionen sind für die eigentlich eher traditionelle Form der Wissenschaftskommunikation „Buch“ vor allem pragmatische, finanzielle und lizenzrechtliche Lösungen gesucht.
So war es das Anliegen des Workshops, Erfahrungen der fondsbetreibenden Einrichtungen auszutauschen, einander über Förderkriterien und Fördersummen zu informieren und den Workflow bei der Antragstellung und -abwicklung abzugleichen. Moderiert wurde der Workshop von Olaf Siegert (ZBW), der als Sprecher des AK Open Access der Leibniz-Gemeinschaft in verschiedenen wissenschaftspolitischen Kontexten für die Transformation hin zu Open Access aktiv ist.

### Förderkriterien der Open-Access-Monografienfonds

Der eintägige Workshop begann nach der Begrüßung durch Gastgeberin Monika Pohlschmidt (IDS) mit einer Kurzvorstellung der Fondsvertreterinnen und -vertreter, in denen allgemeine Fragen zum jeweiligen Fonds kurz und prägnant berichtet wurden (die Mehrzahl der Folien ist [hier](/assets/files/20191203_Vorträge_OAMonografienWorkshop.pdf) verfügbar). Auf diese Weise konnten sich die Anwesenden einen Überblick über Eckdaten und Rahmenbedingungen in den verschiedenen Bundesländern und Institutionen verschaffen. Im Zentrum des Austauschs standen Fragen nach dem Finanzvolumen, der Laufzeit und den allgemeinen Förderkriterien der Fonds. Diskutiert wurde zudem, welche konkreten Leistungen von Verlagen und Dienstleistenden gefördert werden und wie die Einhaltung der Förderkriterien und der wissenschaftlichen Qualität der zu fördernden Publikation gesichert werden könne.

Ein offener Punkt der Diskussion blieb dabei, welche Verlagskosten für die Fondsbetreiber als erstattungsfähige Open-Access-Kosten gelten. Dies betrifft zum Beispiel die Kosten der Satzlegung: Manche Fonds fördern eben diese Dienstleistung, da in der Regel alle Manuskripte von WORD- in XML-Daten überführt und in diesem Format gesetzt werden; andere Fonds tun dies nicht. Weitgehend ist der Prozess des Erstellens einer digitalen Datei für eine Open-Access-Publikation mit jenem Prozess identisch, aus dem eine Druckdatei erstellt wird. Daher ist es für Verlage bei der Angebots- und Vertragsgestaltung eine besondere Herausforderung, Open-Access-Kosten eindeutig und transparent auszuweisen.

Bei einigen Verlagen wird darüber hinaus eine Verkaufseinbuße berechnet, andere Verlage veranschlagen eine mehr oder minder pauschale Open-Access-Gebühr, die sich an der Höhe der Fondsausstattung orientiert. Schwierig bleibt eine Kalkulation der Open-Access-Kosten allzumal für jene sozial- und geisteswissenschaftlichen Disziplinen, in denen das Buch auch weiterhin eine wichtige Rolle im Ringen um Reputation und Impact spielt und die neben der Open-Access-Ausgabe auch eine gedruckte Ausgabe anbieten möchten. Die Fondsbetreiberinnen und -betreiber berücksichtigen die Sonderstellung des Buches in diesen Disziplinen, müssen bei der Antragsstellung aber genau auf die kalkulierten Kosten schauen, die nicht in die Herstellung der gedruckten Ausgabe fließen dürfen. Im Projekt [OGeSoMo](https://www.uni-due.de/ogesomo/) wurden gemeinsam mit Verlagen Kriterien und eine Beschreibung einzelner Posten der verlegerischen Dienstleistungen erarbeitet, die als Entwicklungsschritt auf dem Weg zu transparenten Kostenmodellen angesehen und als Anhaltspunkte für Verhandlungen mit anderen Verlagen verwendet werden können. Das Projekt [Fair Open Access Alliance](https://www.fairopenaccess.org/) hat ähnliche Vorschläge entwickelt.

### Muster-Verlagsvertrag und weitere Detailfragen

Darüber hinaus wurde im Workshop vorgeschlagen, neben Modellrechnungen auch Musterverträge zur Verfügung zu stellen. Da es in Deutschland noch keine langfristigen Finanzierungsmodelle für Bücher im Open Access gibt, sind einige Verlage noch zögerlich gegenüber dem Aufsetzen neuer Verträge und ergänzen bestehende Verlagsverträge um zusätzliche Paragraphen für Lizensierungen im Open Access. Von den Workshop-Teilnehmenden wurden gleichwohl unverzichtbare Elemente eines guten Verlagsvertrags benannt:

* CC-Lizenz,
* DOI-Vergabe,
* de-facto-Langzeitarchivierung,
* einfaches Nutzungsrecht für den Verlag,
* eindeutige Kennzeichnung des Werks als OA ‚gold‘ im Impressum,
* Verweis auf die Förderung durch Fonds im Impressum und nicht zuletzt
* sofortiges Erscheinen im Open Access.

Ein solcher Mustervertrag, der von einem Vorreiterverlag in Sachen Open Access genutzt wird, liegt mittlerweile vor und kann [hier](/assets/files/Autorenvertrag_OA_Muster.pdf) eingesehen werden. Aus dem  Kreis der Teilnehmenden wurde zudem vorgeschlagen, dass der Nationale Open-Access-Kontaktpunkt OA2020-DE sich an den [Börsenverein des Deutschen Buchhandels wendet](https://www.boersenverein.de/), um dort für  mehr Offenheit gegenüber Open Access zu werben und darauf hinzuwirken, interessierten Verlagen juristischen Beistand bei der Gestaltung von Verträgen und Impressen anzubieten.

Es gab weitgehend Einigkeit darüber, dass es deutschlandweit einheitliche Höchstsummen für die Förderung von Open-Access-Monografien geben solle. Diese pendeln sich je nach Fonds derzeit auf einem Niveau zwischen 4.500 und 6.000 EUR ein. Dabei hatten die Teilnehmenden sicherlich mit einem neidischen Auge die österreichischen und schweizerischen Nationalfonds im Blick, die beide die Publikation von Open-Access-Monografien mit wesentlich höheren Summen fördern.

Noch nicht im Blick sind bei den existierenden Fonds Detailfragen, welche Buchtypen zu unterscheiden und zu fördern sind. Die bisherige Praxis schaut auf Monografien und Sammelbände; Lehr- und Wörterbücher sind bisher weder beantragt noch gefördert worden – obwohl doch auch solche Bücher bei den Leserinnen und Lesern im Open Access willkommen wären. Auch die Förderung von Dissertationen wird sehr unterschiedlich gehandhabt.

Bei den Qualitätsstandards für förderfähige Bücher liegen mittlerweile sehr hilfreiche Empfehlungen vor, die zwar nicht in jedem Fall umsetzbar sind, aber als eine gute Richtschnur sowohl bei der Gestaltung der Förderkriterien eines Monografienfonds als auch bei der Bearbeitung von Anträgen dienen können (Qualitätskriterien von [Knowledge Unlatched und Nationalem Open-Access-Kontaktpunkt](http://nbn-resolving.de/urn:nbn:de:0070-pub-29321893)  sowie von der [AG Universitätsverlage](https://blog.bibliothek.kit.edu/ag_univerlage/wp-content/uploads/2018/09/Anforderungen-an-OA-Monografien_ag_universitaetsverlage_20180904.pdf)). Wichtig ist es in jedem Fall, auf eine der jeweiligen Disziplin angemessene Qualitätssicherung zu schauen.

### Gewonnene Erkenntnisse

Am Ende eines äußerst anregenden Workshops war man sich einig darüber, dass noch viele pragmatische Fragen zu klären sind, denkt man allein an die Entwicklung von Musterverträgen und Preisempfehlungen für Open-Access-Dienstleistungspakte, aber auch an potenzielle Konflikte, denn noch fehlt es an Verfahren zur Überprüfung und Durchsetzung so mancher Förderkriterien. So bauten die Anwesenden darauf, in Zukunft – vielleicht schon im Herbst dieses Jahres – wieder zusammentreffen und sich austauschen zu können. Denn eines ist gewiss: Die Zahl der Open-Access-Akteure und -Fonds wird wachsen.


Wenn Sie noch Fragen zu dem Workshop oder zum Thema Open-Access-Monografienfonds haben, können Sie sich gern jederzeit an die Veranstalter_innen wenden:<br />
Dr. Thomas Jung, DIE - Deutsches Institut für Erwachsenenbildung, <i class="fa fa-envelope" aria-hidden="true"></i>  [E-Mail](mailto:thomas.jung@die-bonn.de)<br />
Monika Pohlschmidt, IDS - Institut für Deutsche Sprache, <i class="fa fa-envelope" aria-hidden="true"></i>  [E-Mail](mailto:pohlschmidt@ids-mannheim.de)<br />
Dr. Christoph Schindler, DIPF - Leibniz-Institut für Bildungsforschung und Bildungsinformation, <i class="fa fa-envelope" aria-hidden="true"></i>  [E-Mail](mailto:schindler@dipf.de)<br />
Olaf Siegert, ZBW - Leibniz-Informationszentrum Wirtschaft, <i class="fa fa-envelope" aria-hidden="true"></i>  [E-Mail](mailto:O.Siegert@zbw.eu)<br />
