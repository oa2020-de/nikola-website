<!--
.. title: A smooth transition from subscriptions to APCs
.. slug: smoothtransition
.. date: 2020-02-18 08:00:00+1:00
.. author: Nina Schönfelder
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## Proposal for a new model of transformative agreements: A smooth transition from subscriptions to APCs

The strategic goal of the National Contact Point Open Access OA2020-DE is to create requirements for the large-scale open access transformation in accordance with the Alliance of Science Organisations in Germany. This also includes analysing existing transformation approaches and formulating changes or adjustments that make sense from our perspective.
This is also the reason why we see the need for a more realistic transformation model in the journal sector, because existing models as Publish & Read and Read & Publish do not best serve the needs of libraries and publishers in transforming their acquisition budgets and revenues, respectively, from subscriptions to APCs.

* For P&R agreements to transform the scholarly journals successfully to open access, huge coordination efforts, a massive and instantaneous re-allocation of funds, or permanently large additional funds are necessary. Moreover, publishers will not offer P&R agreements to individual libraries or loose consortia with liberal opt-in or opt-out regulations.
* With R&P agreements, the open-access transformation will stuck at low OA shares. To progress further, a switch to the P&R model would be necessary with its own obstacles.

Therefore, we see the need for a new transformative model that facilitates the switch from a pure R&P to a pure P&R model by lowering coordination costs, avoiding disruptive workflow and cost adjustments, and putting less pressure on timing.
To support the arguments of this, we run a simulation on a [global model](https://pub.uni-bielefeld.de/download/2939995/2940353/Smooth_transition_model.html).
Please note: The model presented here is a proposal and approach that should help to identify and describe possible paths for the open access transformation. Indeed, the R&P model and the P&R model can be regarded as corner solutions of this.
<!-- TEASER_END -->

**Requirements on the model**

We want the new model to fulfill the following requirements:

* No permanent or serious changes in global library spending
* No permanent or serious changes in publishers‘ revenues
* No disruptive changes in libraries‘ individual expenditures
* Smooth transition from subscription to APC revenues for publisher
* Smooth transition from content access to OA funding for libraries

### The smooth transition model

Libraries pay one fee for unlimited open-access publishing in hybrid journals and reading the complete closed-access content for the publisher’s complete journal portfolio. Alternatively, the model can also be set up for a single journal, or a subset of the journal portfolio.
At the starting point, the fee corresponds to the historical subscription fee for each individual library. The agreement can also include APCs for publishing in pure OA journals on top, or an additional one-time fee for perpetual access rights for archival content, but neither of these components are at the core of the contract or affect the model.
Within the OA transition the proportion of the fee that attributes to access closed content shrinks and the proportion of the fee that attributes to OA publishing rises according to the closed-access/open-access ratio of the whole journal (portfolio) content.

The OA-publishing part of the fee for a particular library is calculated as:

`Publish fee = # OA articles × share of OA content × APC`

Variables:

* OA articles = open-access articles of eligible corresponding authors from the respective institution in hybrid journals (# OA articles are first estimated, and then tracked. Optionally, a lump sum based on past year # OA articles is set)
* Share of OA content = open-access articles / all articles (share of OA content is published each year and verifiable via DOIs and Unpaywall)
* APC= total subscription revenue / # articles in t=0 (APC is fixed (in real terms) over the transition phase and published)

The reading part of the fee for a particular library relates to the share of the closed-access content and its individual, initial subscription costs:

`Read fee = Subscription in t=0 × (1− share of OA content)`

If necessary, the read fee can be adjusted for growth in closed-access content and inflation.

To sum up, libraries pay one fee for unlimited open-access publishing in hybrid journals and reading the complete closed-access content so that the total fee amounts to:

`Publish fee + Read fee = Smooth Transition Fee`

To see some examples for fees and revenues in Smooth Transition Agreements please visit: [https://Smooth_transition_model.html](https://pub.uni-bielefeld.de/download/2939995/2940353/Smooth_transition_model.html)

### Conclusion

The real advantage of the smooth transition model above R&P or P&R is that library acquisition budgets have the chance to evolve gradually from subscription expenditures to APCs. Neither instantaneous policy changes for library funding are required, nor massive coordination effort. However, even with the smooth transition model, an open-access transformation will not succeed if high-publishing organizations do not commit to the open-access principle and do not receive adequate additional funds in the long term. The attraction of the smooth transition model is that libraries, governments and funders gain time to develop, adopt, evaluate and revise funding reallocation and workflows as the open-access transition progresses. Temporary mistakes in policy changes do not cause severe damage as library expenditures rely in large parts on historical subscriptions in the first half of the global open-access transition even at high-publishing organizations. This also reduces the uncertainty at the libraries’ and publishers’ side surrounding the switch from subscriptions to APCs and from the R&P to P&R model, respectively.

*For the full discussion paper, the simulation and more information material see also:
Schönfelder, N. (2020). Proposal for a new model of transformative agreements: A smooth transition from subscriptions to APCs. doi:[10.4119/unibi/2939995](https://doi.org/10.4119/unibi/2939995)*
