<!--
.. title: Establishing tender procedures and competition within the framework of national library consortia for open access journals
.. slug: establishing_tenderprocedures_national_oaconsortia
.. date: 2020-02-05 10:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## Description of a pilot project

The open access transformation is a declared goal of the [Coalition S](https://www.coalition-s.org/) and the [OA2020 initiative](https://oa2020.org/) and the institutions supporting them. In order to achieve a large-scale open access transformation of journals, as many established subscription journals as possible shall be transformed into open access. To achieve this goal, transformative agreements are concluded such as those the [DEAL project](https://www.projekt-deal.de/about-deal/) has been negotiating for several years with the three major international scientific publishers (Elsevier, Springer Nature and Wiley). In Germany, the 13+ group established by the “Alliance of Science Organisations” Working Group "Scientific Publication System" is aiming at similar negotiations with further thirteen large publishing houses. In addition, the DFG programme "Open Access Transformation Agreements" provides funding for transformative agreements.


The existing transformative agreements do not include mechanisms for the definitive flipping of journals into open access and no mechanisms to limit cost increases in the long term, as demanded by the [European Commission](https://doi.org/10.2777/836532) and the [European University Association](https://eua.eu/resources/publications/829:2019-big-deals-survey-report.html), for example.  Indeed, APC-based, genuine open access journals also lack mechanisms for the long-term limitation of cost increases. The price caps currently implemented in the (DFG-funded) publication funds are of limited suitability. On the one hand, they are too high for the mass of open access journals; on the other hand, they are set too low for highly selective and high-quality open access journals that are attractive to many researchers.

*Against this background, we suggest to conclude pure open access contracts and, if applicable, contract components for pure open access journals within the framework of transformative agreements by tendering in secret bidding procedures as practiced by SCOAP³. The now published concept describes the intended objectives, the services to be put out to tender and a proposal for organisational implementation.*
<!-- TEASER_END -->

The following points summarise the objectives of the concept:

* Establishing price and service competition between publishers by means of centralised tendering.
* Reduction of the average article costs for consortia within the framework of open access contracts to the level of SCOAP³.
* Clear definition of the services to be provided by the publishers.
* Structural anchoring of APC funding for affiliated scientists.

**We would like to discuss the concept and the idea with you and therefore we have made it available in German and English as a Discussion Paper on the Institutional Repository PUB of Bielefeld University and as GoogleDoc:**

Schönfelder, Nina; Pieper, Dirk (2020). Establishing tender procedures and competition within the framework of national library consortia for open access journals: Description of a pilot project. Universität Bielefeld. DOI: [10.4119/unibi/2939999](https://doi.org/10.4119/unibi/2939999)

GoogleDoc: [https://docs.google.com/document/d/1ojD2wVZuWfYsTda5UJvjXSI5Kz-lf0UP/edit](https://docs.google.com/document/d/1ojD2wVZuWfYsTda5UJvjXSI5Kz-lf0UP/edit)

Background: Blogpost [What follows transformative agreements?](https://oa2020-de.org/en/blog/2019/12/09/what_follows_transformative_agreements/)
