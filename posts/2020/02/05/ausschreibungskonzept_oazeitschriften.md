<!--
.. title: Etablierung von Ausschreibungs- und Wettbewerbsmechanismen im Rahmen von nationalen Open-Access-Zeitschriftenkonsortien
.. slug: etablierung_ausschreibung_nationales_oakonsortium
.. date: 2020-02-05 10:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## Beschreibung eines Pilotvorhabens

Die Open-Access-Transformation ist erklärtes Ziel der [Coalition S](https://www.coalition-s.org/) und der [OA2020-Initiative](https://oa2020.org/) sowie der sie unterstützenden Institutionen. Um eine großflächige Open-Access-Transformation von Zeitschriften zu erreichen, sollen möglichst viele etablierte Subskriptionszeitschriften inden Open Access transformiert werden. Dies soll derzeit im Rahmen von Transformationsverträgen, wie sie vom [DEAL-Projekt](https://www.projekt-deal.de/) seit mehreren Jahren mit den drei großen internationalen Wissenschaftsverlagen (Elsevier, Springer Nature und Wiley) verhandelt werden, umgesetzt werden. Die von der Allianz AG [„Wissenschaftliches Publikationssystem“](https://www.allianzinitiative.de/handlungsfelder/wissenschaftliches-publikationssystem/) etablierte 13+-Gruppe strebt analoge Verhandlungen mit weiteren dreizehn großen  Verlagen an. Zusätzlich gibt es über das DFG-Programm [„Open-Access-Transformationsverträge“](https://www.dfg.de/foerderung/info_wissenschaft/2017/info_wissenschaft_17_12/index.html) eine Förderung für Projekte zur Transformation von Zeitschriften.

Die bisher bekannten Transformationsverträge beinhalten keine Mechanismen für das konkrete Flipping von Zeitschriften in den Open Access und keine Mechanismen zur langfristigen Begrenzung von Kostensteigerungen, wie sie beispielsweise von der [Europäischen Kommission](https://doi.org/10.2777/836532) und der [European University Association](https://eua.eu/resources/publications/829:2019-big-deals-survey-report.html) gefordert werden. Aber auch bei den APC-basierten, genuinen Open-Access-Zeitschriften mangelt es an Mechanismen zur langfristigen Begrenzung von Kostensteigerungen. Die derzeit noch in den (DFG-geförderten) Publikationsfonds implementierten Preisobergrenzen sind dafür nur bedingt geeignet. Auf der einen Seite sind sie für die Masse der Open-Access-Zeitschriften zu hoch, auf der anderen Seite für hochselektive und -qualitative Open-Access-Zeitschriften, die für viele Wissenschaftler_innen attraktiv sind, zu niedrig angesetzt.

*Vor diesem Hintergrund regen wir an, reine Open-Access-Verträge und ggf. Vertragsbestandteile zu reinen Open-Access-Zeitschriften im Rahmen von Transformationsverträgen durch Ausschreibungen in geheimen Bieterverfahren, wie es von SCOAP³ praktiziert worden ist, zu schließen. Das jetzt veröffentlichte Konzept beschreibt die damit intendierten Ziele, die auszuschreibenden Services sowie einen Vorschlag zur organisatorischen Umsetzung.*
<!-- TEASER_END -->

Die Zielsetzung des Konzeptes ist,

* die Etablierung eines Preis-und Servicewettbewerbs zwischen Verlagen mittels zentraler Ausschreibungen,
* die Senkung der durchschnittlichen Artikelkosten für Konsorten im Rahmen von Open-Access-Verträgen auf das Niveau von SCOAP³,
* eine klare Definition der von den Verlagen zu erbringenden Services und
* eine strukturelle Verankerung der APC-Finanzierung für affiliierte Wissenschaftler_innen.

**Wir möchten gern das Konzept und die Idee mit Ihnen und Euch diskutieren und haben es deshalb auf deutsch und auf englisch als Discussion Paper auf dem Institutionellen Repositorium PUB der Universität Bielefeld und als GoogleDoc zur Verfügung gestellt:**

Schönfelder, Nina; Pieper, Dirk(2020). Etablierung von Ausschreibungs-und Wettbewerbsmechanismen im Rahmen von nationalen Open-Access-Zeitschriftenkonsortien: Beschreibung eines Pilotvorhabens. Universität Bielefeld. DOI: [10.4119/unibi/293999](https://doi.org/10.4119/unibi/2939999)

GoogleDoc: [https://docs.google.com/document/d/1ojD2wVZuWfYsTda5UJvjXSI5Kz-lf0UP/edit](https://docs.google.com/document/d/1ojD2wVZuWfYsTda5UJvjXSI5Kz-lf0UP/edit)


Als Hintergrund: Blogbeitrag [Was kommt nach den Transformationsverträgen?](https://oa2020-de.org/blog/2019/12/09/was_kommt_nach_Transformationsvertraegen/)
