<!--
.. title: Pledging-Frist verlängert: Bis zum 14.02.2020 die wbv Open Library 2020 unterstützen
.. slug: verlängerung_pledging_wbvopenlibrary
.. date: 2020-02-04 08:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

### Ermöglichen für Alle statt Medienerwerb für Einzelne

Vielen Dank an alle bisherigen Unterstützer_innen der wbv Open Library 2020!<br />
Bis zum **14. Februar** können auch **SIE** noch Open-Access-Enabler werden und dazu beitragen, dass 20 Novitäten aus dem Bereich Erwachsenenbildung und Berufs- und Wirtschaftspädagogik im Open Access erscheinen können.

<img src="/images/wbvopenlibrarysupporter.png" alt="wbv Open Library Supporter" width="450" height="300" display="block" />

Weitere Informationen zum Modell erhalten Sie unter: [https://www.wbv.de/openaccess/wbv-openlibrary.html](https://www.wbv.de/openaccess/wbv-openlibrary.html)

Bei Fragen und Interesse an einer Teilnahme wenden Sie sich bitte an:<br />
Catherine Anderson (Knowledge Unlatched), Mail: [catherine@knowledgeunlatched.org](mailto:catherine@knowledgeunlatched.org)
