<!--
.. title: Nationaler Open-Access-Kontaktpunkt um ein Jahr verlängert
.. slug: OA2020-DE verlaengert
.. date: 2020-06-02 14:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

### OA2020-DE setzt seine Arbeit für ein weiteres Jahr fort

Gute Nachrichten für unsere Projektpartner*innen, ENABLE!, die Open-Access-Community und alle am Projekt interessierten Menschen und Einrichtungen: Der Nationale Open-Access-Kontaktpunkt OA2020-DE ist um ein Jahr verlängert worden und wird sein Engagement für die Open-Access-Transformation bis Ende Juli 2021 fortsetzen können.

Die Schwerpunkte liegen dabei vor allem auf der Umsetzung begonnener Pilotvorhaben wie das [Subscribe to Open-Modell mit dem De Gruyter Verlag](/pages/S2O_BFP/), der Unterstützung von Transformationsvorhaben und der weiteren Diskussion verschiedener Open-Access-Modelle mit Wissenschaftsverlagen und Erwerbungsfachleuten aus den Bibliotheken.

Wir freuen uns darauf, mit Ihnen und Euch gemeinsam die Open-Access-Transformation in Deutschland weiter voran zu bringen und bedanken uns für die bisherige erfolgreiche Zusammenarbeit!
