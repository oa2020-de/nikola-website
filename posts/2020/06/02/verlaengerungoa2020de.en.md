<!--
.. title: National Contact Point Open Access extended by one year
.. slug: OA2020-DE extended
.. date: 2020-06-02 14:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

### OA2020-DE continues its work for another year

Good news for our project partners, ENABLE!, the open-access community and all people and institutions interested in the project: The National Contact Point Open Access OA2020-DE has been extended by one year and will be able to continue its commitment to the open-access transformation until the end of July 2021.

The main focus will be on the implementation of pilot projects that have already been started, such as the [Subscribe to Open model with De Gruyter](/en/pages/S2O_BFP/), support for transformation projects and further discussion of various open-access models with scientific publishers and acquisition experts from libraries.

We look forward to working with you to advance the open-access transformation in Germany and thank you for the successful cooperation to date!
