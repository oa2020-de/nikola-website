<!--
.. title: Informationsmaterial & Handouts aktualisiert und ergänzt
.. slug: infomaterialaktualisiert
.. date: 2020-06-10 08:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

### Sammlung an frei lizenziertem Material erweitert

Der Nationale Open-Access-Kontaktpunkt stellt auf seinen Webseiten ja nicht nur einzelne Projektergebnisse dar, sondern bietet auch eine kleine, aber feine Sammlung an Informationsmaterialien und Handouts rund um die Open-Access-Transformation an. So gibt es eigene Werke wie den Entscheidungsbaum für Fachgesellschaften, die Informationsgrafiken zur Open-Access-Transformation, diverse Poster und die Qualitätsstandards für die Open-Access-Stellung von Büchern (alles selbstverständlich CC-BY-lizenziert).

Gleichzeitig haben wir auch Material von Kolleg*innen aufgenommen, das i.d.R. offen lizenziert ist und ganz konkrete Transformations- und Open-Access-Vorhaben unterstützt, wie z.B. das Handbuch "Open-Access-Publikationsworkflow für akademische Bücher" der HTWK Leipzig, das Flussdiagramm zum Thema Zweitveröffentlichung des Bundesinstituts für Berufsbildung und die Handreichungen zum Projekt OGeSoMo und weitere Materialien rund um das Thema Open Acces.

Schauen Sie doch einfach mal vorbei unter [https://oa2020-de.org/pages/materialien/](https://oa2020-de.org/pages/materialien/)!
