<!--
.. title: Information material & handouts updated and supplemented
.. slug: infomaterialupdated
.. date: 2020-06-10 08:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

### Collection of openly licensed material expanded

The National Contact Point Open Access not only presents individual project results on its website, but also offers a small but fine collection of information materials and handouts on all aspects of open-access transformation. For example, there are its own works such as the decision tree for learned societies, the information graphics on open-access transformation, various posters and the quality standards for the open-access provision of books (all of which are of course CC-BY licensed).

At the same time, we have also included material from colleagues, which is generally openly licensed and supports very specific transformation and open-access projects, such as the handbook "Open Access Publication Workflow for Academic Books" by the HTWK Leipzig, the flowchart on secondary publication by the Federal Institute for Vocational Education and Training and the handouts on the OGeSoMo project and other materials relating to open access.

Just take a look at [https://oa2020-de.org/en/pages/materials/](https://oa2020-de.org/en/pages/materials/)!
