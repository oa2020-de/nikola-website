<!--
.. title: Strengthening the green open access
.. slug: strengthening_greenOA
.. date: 2020-03-25 16:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

On 31 January 2020, the Wellcome Trust described the new coronavirus as a "significant and urgent threat to global health" and called on researchers, journals and funders to ensure that research findings and data on coronaviruses are shared rapidly and openly. Signatories of the declaration ["Sharing research data and findings relevant to the novel coronavirus (COVID-19) outbreak"](https://wellcome.ac.uk/press-release/sharing-research-data-and-findings-relevant-novel-coronavirus-covid-19-outbreak) include major publishers, research funders and learned and professional societies.

On 5 March 2020, Vincent Larivière, Fei Shu and Cassidy Sugimoto stated in their article ["The Coronavirus (COVID-19) outbreak highlights serious deficiencies in scholarly communication"](https://blogs.lse.ac.uk/impactofsocialsciences/2020/03/05/the-coronavirus-covid-19-outbreak-highlights-serious-deficiencies-in-scholarly-communication/) for the LSE Impact blog that, based on data from the Web of Science, just over half of the articles published on coronaviruses are in closed access. Once again, this shows that the scarcity of access to the results of scientific research within the still common subscription system does not benefit either science itself or society as a whole.
<!-- TEASER_END -->

Our Dutch colleagues from "open access.nl" yesterday also launched the website ["Open Access to COVID-19 and related research"](https://www.openaccess.nl/en/open-access-to-covid-19-and-related-research), which links to the initiative ["You share, we take care"](https://www.openaccess.nl/en/in-the-netherlands/you-share-we-take-care), within which Dutch university libraries have been offering a service for the open-access provision of publications in repositories since January 2020.

Ongoing transformation contracts are making an increasing contribution to boost the number of open-access articles in journals. At the same time, the green road to open access offers the opportunity to significantly increase the overall open-access share of research results through the consistent use of secondary publication rights or the open-access provision of articles after embargo periods have expired, and to make these results available quickly and freely to all via repositories.

Many academic libraries are currently closed or in emergency operation. At the universities, the coming summer semester will most likely be conducted with considerably less on-site presence than usual. The National Contact Point Open Access and the [open-access.network](https://www.bildung-forschung.digital/de/offizieller-projektstart-von-open-access-network-2763.html), which is funded by the Federal Ministry of Science and Research and is currently being set up, therefore encourage libraries to use human resources that are not otherwise required to identify publications that may be made freely accessible under current law or publishing agreements and to make them available via their institutional repositories.

**In particular, the establishment or expansion of already existing secondary publication services now offers libraries the opportunity to make a further contribution to the open-access provision of publications from their own institution. For more information on how to proceed, please refer to the practical guide by Blasetti et al., Smash the Paywalls: Workflows and Tools for the Green Road to Open Access (published 2019 by Informationspraxis, [https://doi.org/10.11588/ip.2019.1.52671](https://doi.org/10.11588/ip.2019.1.52671)).**
