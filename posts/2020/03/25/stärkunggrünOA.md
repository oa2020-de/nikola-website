<!--
.. title: Stärkung des grünen Open Access
.. slug: staerkung_gruenesOA
.. date: 2020-03-25 16:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Am 31. Januar 2020 bezeichnete der Wellcome Trust das neue Coronavirus als "erhebliche und dringende Bedrohung der globalen Gesundheit" und forderte Forscher_innen, Zeitschriften und Geldgeber auf, dafür zu sorgen, dass die Forschungsergebnisse und Daten zu Coronaviren schnell und offen ausgetauscht werden. Zu den Unterzeichner_innen der Erklärung [„Sharing research data and findings relevant to the novel coronavirus (COVID-19) outbreak“](https://wellcome.ac.uk/press-release/sharing-research-data-and-findings-relevant-novel-coronavirus-covid-19-outbreak) gehören maßgebliche Verlage, Forschungsförderer und wissenschaftliche Fachgesellschaften.

Am 05. März 2020 haben Vincent Larivière, Fei Shu und Cassidy Sugimoto in ihrem Beitrag [„The Coronavirus (COVID-19) outbreak highlights serious deficiencies in scholarly communication“](https://blogs.lse.ac.uk/impactofsocialsciences/2020/03/05/the-coronavirus-covid-19-outbreak-highlights-serious-deficiencies-in-scholarly-communication/) für den Blog „Impact of Social Sciences“ der London School of Economics festgestellt, dass auf Basis von Daten des Web of Science etwas mehr als die Hälfte der zu Coronaviren insgesamt publizierten Artikel im closed access stehen. Einmal mehr wird damit deutlich, dass die Verknappung des Zugangs zu den Ergebnissen wissenschaftlicher Forschung innerhalb des immer noch gängigen Subskriptionssystems weder der Wissenschaft selbst noch der Gesellschaft als Ganzes dient.
<!-- TEASER_END -->

Unsere niederländischen Kolleg_innen von „open access.nl“ haben weiterhin gestern die Webseite ["Open Access to COVID-19 and related research"](https://www.openaccess.nl/en/open-access-to-covid-19-and-related-research) ins Leben gerufen, auf der unter anderem auf die Initiative ["You share, we take care"](https://www.openaccess.nl/en/in-the-netherlands/you-share-we-take-care) verlinkt wird, innerhalb derer die niederländischen Universitätsbibliotheken seit Januar 2020 einen Service für die Open-Access-Stellung von Publikationen in Repositorien anbieten.

Laufende Transformationsverträge leisten einen zunehmenden Beitrag, die Anzahl von Open-Access-Artikeln in Zeitschriften zu steigern. Zugleich bietet der grüne Weg des Open Access die Möglichkeit, über die konsequente Nutzung des Zweitveröffentlichungsrechts oder die Open-Access-Stellung von Artikeln nach Ablauf von Embargofristen den Open-Access-Anteil von Forschungsergebnissen insgesamt deutlich zu steigern und diese schnell und für alle frei über Repositorien zur Verfügung zu stellen.

Viele wissenschaftliche Bibliotheken sind derzeit geschlossen oder im Notbetrieb. An den Hochschulen wird das kommende Sommersemester sehr wahrscheinlich mit deutlich weniger Präsenz vor Ort als gewohnt durchgeführt werden. Der Nationale Open-Access-Kontaktpunkt und das vom Bundesministerium für Wissenschaft und Forschung geförderte und im Aufbau befindliche [open-access.network](https://www.bildung-forschung.digital/de/offizieller-projektstart-von-open-access-network-2763.html) regen daher an, anderweitig nicht benötigte personelle Ressourcen in den Bibliotheken zur Ermittlung von Publikationen zu nutzen, die nach gültigem Recht bzw. Verlagsverträgen frei zugänglich gemacht werden dürfen und über ihre institutionellen Repositorien zur Verfügung zu stellen.

**Insbesondere der Aufbau oder Ausbau von bereits bestehenden Zweitveröffentlichungsservices bietet den Bibliotheken jetzt die Möglichkeit, einen weiteren Beitrag für die Open-Access-Stellung von Publikationen ihrer eigenen Einrichtung zu leisten. Für das konkrete Vorgehen verweisen wir gerne auf die praxisnahe Handreichung von Blasetti et al., Smash the Paywalls: Workflows und Werkzeuge für den grünen Weg des Open Access (erschienen 2019 bei Informationspraxis, [https://doi.org/10.11588/ip.2019.1.52671](https://doi.org/10.11588/ip.2019.1.52671)).**

*Nachtrag (08.04.2020).* Von der Universitätsbibliothek Kassel gibt es ein Musteranschreiben für die Wissenschaftler_innen, das die Fachreferent_innen und Bibliotheksmitarbeiter_innen verwenden können, um verstärkt zur Zweitveröffentlichung aufzurufen und das wir hiermit gerne zusätzlich zur Verfügung stellen. Vielen Dank an dieser Stelle an die Kolleg_innen der UB Kassel und der UB Erlangen-Nürnberg für die Anregungen und die Formulierungshilfe.

[Musteranschreiben](/assets/files/Musterschreiben-via-Fachreferenten.docx)

[Flussdiagramm zur Zweitveröffentlichung vom BIBB](/assets/files/BIBB_Flussdiagramm Aufruf zur Zweitveröffentlichung.pdf)

