<!--
.. title: Ein Entscheidungsbaum für Fachgesellschaften auf dem Weg zur Open-Access-Transformation
.. slug: entscheidungsbaum_fachgesellschaften_oatransformation
.. date: 2020-03-03 08:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

[Plan S](https://www.coalition-s.org/addendum-to-the-coalition-s-guidance-on-the-implementation-of-plan-s/principles-and-implementation/), [DEAL](https://www.projekt-deal.de/aktuelles/) und der Abschluss verschiedener Transformationsverträge in den letzten Monaten (siehe z.B. [hier](https://www.jisc.ac.uk/news/jisc-secures-two-year-pilot-transitional-open-access-agreement-with-microbiology-society-16-oct), [hier](https://www.vsnu.nl/nl_NL/nieuwsbericht/nieuwsbericht/552-dutch-research-institutions-and-elsevier-reach-framework-agreement.html) und [hier](https://www.acm.org/media-center/2020/january/acm-open)) haben den Druck auf die wissenschaftlichen Verlage erhöht. Sie sind dazu aufgefordert, ihre Geschäftsmodelle an das sich verändernde wissenschaftliche Kommunikationsverhalten der Forscher_innen und an die wissenschaftspolitischen Forderungen nach Open Access und Open Sciene auszurichten. Dies betrifft auch Fachgesellschaften als Herausgeberinnen von Fachzeitschriften und Schriftenreihen. Von den von einer deutschen wissenschaftlichen Fachgesellschaft herausgegeben 182 Fachzeitschriften liegen bisher nur 7,14% als Open-Access-Zeitschrift vor, wie eine Studie zum Thema ["Wissenschaftliche Fachgesellschaften und Open Access"](https://doi.org/10.2312/os.helmholtz.009) von Pampel und Strecker (2020) ergab. 55,49% haben eine Hybrid-Option und 37,36% sind in keiner Weise frei verfügbar.

Auch wenn die Fachgesellschaftszeitschriften, die bei Springer Nature oder Wiley erscheinen, mit Hilfe der DEAL-Verträge in den Open Access überführt werden sollen bzw. überhaupt erst eine Open-Access-Option erhalten, so gibt es doch bisher keine klaren Aussagen zum letzlichen Open-Access-Status dieser Zeitschriften nach Auslaufen der Verträge. Das heißt für die Zeit nach DEAL und für die übrigen Zeitschriften, die im Eigenverlag oder bei einem Nicht-DEAL-Verlag erscheinen, müssen weitere bzw. andere Wege gefunden werden, um den Anforderungen gerecht zu werden.
<!-- TEASER_END -->

Um die Fachgesellschaften (und natürlich auch alle anderen Herausgeber_innen von Zeitschriften) bei der Open-Access-Transformation zu unterstützen, hat der Nationale Open-Access-Kontaktpunkt zum einen eine Informationsgrafik mit verschiedenen Transformationsmodellen erarbeitet und zum anderen einen Entscheidungsbaum entworfen, mit dessen Hilfe Herausgeber_innen identifizieren können, welcher Weg hin zu Open Access für ihre Fachgesellschaft und ihre Zeitschrift möglich ist.

<iframe width="400" height="1335" frameborder="0"scrolling="no" style="overflow-y:hidden;" src="https://create.piktochart.com/embed/43267415-how-to-open-up-my-journal" ></iframe>

[Link zum Entscheidungsbaum](http://doi.org/10.5281/zenodo.3693598).

[Link zur Informationsgrafik Transformationsmodelle](/assets/files/2020-02-15-open-access-transformationmodels_de.pdf).





