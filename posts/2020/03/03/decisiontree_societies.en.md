<!--
.. title: A decision tree for learned & professional societies on the way to open access transformation
.. slug: decisiontree_societies_oatransformation
.. date: 2020-03-03 08:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

[Plan S](https://www.coalition-s.org/addendum-to-the-coalition-s-guidance-on-the-implementation-of-plan-s/principles-and-implementation/), [DEAL](https://www.projekt-deal.de/about-deal/) and the conclusion of various transformation contracts in recent months (see e.g. [here](https://www.jisc.ac.uk/news/jisc-secures-two-year-pilot-transitional-open-access-agreement-with-microbiology-society-16-oct), [here](https://www.vsnu.nl/nl_NL/nieuwsbericht/nieuwsbericht/552-dutch-research-institutions-and-elsevier-reach-framework-agreement.html) and [here](https://www.acm.org/media-center/2020/january/acm-open)) have increased the pressure on scientific publishers. They are called upon to align their business models with the changing scientific communication behaviour of researchers and with the science policy demands for open access and open sciene. This also affects learned societies as publishers of scientific journals and book series. Of the 182 journals published by a German learned society, only 7.14% are available as open access journals, according to a study on ["Learned Societies and Open Access"](https://doi.org/10.2312/os.helmholtz.009) by Pampel and Strecker (2020). 55.49% have a hybrid option and 37.36% are not freely available in any way.

Even if the journals of the learned societies published by Springer Nature or Wiley are to be converted to open access with the help of the DEAL agreements or are to be given an open access option in the first place, there are as yet no clear statements on the ultimate open access status of these journals after expiry of the agreements. This means that for the period after DEAL and for the remaining journals that are published either by the journal's own publishers or by a non-DEAL publisher, further or other ways must be found to meet the requirements.
<!-- TEASER_END -->

In order to support learned societies (and, of course, all other editors of journals) in the process of the open access transformation, the National Contact Point Open Access has, on the one hand, developed an information chart with various transformation models and, on the other hand, designed a decision tree to help editors identify which path to open access is possible for their learned society and their journal.

<iframe width="400" height="1335" frameborder="0"scrolling="no" style="overflow-y:hidden;" src="https://create.piktochart.com/embed/43267415-how-to-open-up-my-journal" ></iframe>

[Link to the decision tree](http://doi.org/10.5281/zenodo.3693598).

[Link to the information chart on transformation models](/assets/files/2020-02-15-open-access-transformationmodels_en.pdf).
