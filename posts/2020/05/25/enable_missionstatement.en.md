<!--
.. title: ENABLE! - Mission Statement of the Initiative for Open Access in the Social Sciences and Humanities published
.. slug: enable_missionstatement
.. date: 2020-05-25 07:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

**BIELEFELD/GERMANY, 2020-05-25. Libraries, publishers, repositories and the book trade are joining forces to shape the open-access transformation in the social sciences and humanities.**

The development of an inclusive open-access culture in the humanities and social sciences is the goal of the community project ["ENABLE! Libraries, publishers and authors for open access in the social sciences and humanities"](https://enable-oa.org/). The aim is to bring together all those involved in scientific publishing: Scientists, their universities and research institutes, libraries, professional associations, repositories, publishers, booksellers and service providers. ENABLE! aims to bundle existing local approaches, methods and initiatives and transfer them into a so-called co-publishing model in which all actors work together on an equal footing and jointly implement open access publishing projects.
<!-- TEASER_END -->

In a joint mission statement, the ENABLE! community has defined and published its tasks and objectives. The mission statement sees itself as a starting point for a transformative, collaborative publication culture, which perspectively aims to provide free access to valuable scientific content. Among the first signatories are:

* De Gruyter publishing house
* Dietmar Dreier GmbH
* FAU University Press
* Georg Olms publishing house
* Information Center for Education, DIPF | Leibniz Institute for Research and Information in Education
* Knowledge Unlatched
* Lehmanns Media
* Monika Pohlschmidt (Leibniz Institute for the German Language)
* Open access repository for the Media Sciences media/rep/
* peDOCS repository, DIPF | Leibniz Institute for Research and Information in Education
* Publisher Barbara Budrich GmbH
* Publisher Julius Klinkhardt KG
* Schüren publishing house
* SSOAR repository, GESIS Leibniz Institute for the Social Sciences
* transcript publishing house
* University Library Duisburg-Essen
* University Library Münster
* Waxmann publishing house
* wbg
* wbv Publication (wbv Media GmbH & Co. KG)

By signing this mission statement, everyone has the opportunity to commit to the goals set out in it and to say yes to joint activities that advance the open-access transformation in the humanities and social sciences in a community-based and discipline-oriented manner.

## [Mission Statement](https://enable-oa.org/mission-statement)

You too can become part of the open-access community for the humanities and social sciences!




