<!--
.. title: ENABLE! - Mission Statement der Initiative für Open Access in den Social Sciences und Humanities veröffentlicht
.. slug: enable_missionstatement
.. date: 2020-05-25 07:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

**BIELEFELD/DEUTSCHLAND, 25.05.2020. Bibliotheken, Verlage, Repositorien und der Buchhandel schließen sich zusammen, um gemeinsam die Open-Access-Transformation in den Geistes- und Sozialwissenschaften zu gestalten.**

Die Entwicklung einer inklusiven und von allen Beteiligten getragene Open-Access-Kultur in den Geistes- und Sozialwissenschaften ist das Ziel des Community-Projektes ["ENABLE! Bibliotheken, Verlage und Autor*innen für Open Access in den Social Sciences und Humanities"](https://enable-oa.org/). Dafür sollen alle Akteure des wissenschaftlichen Publizierens zusammenkommen: Wissenschaftler*innen, ihre Hochschulen, Bibliotheken, Fachverbände, Fachrepositorien, Verlage sowie Buchhandel und Dienstleister. ENABLE! will bestehende lokale Ansätze, Methoden und Initiativen bündeln und in ein sogenanntes Co-Publishing-Modell überführen, bei dem alle Akteure auf Augenhöhe zusammenarbeiten und gemeinsam Open-Access-Publikationsprojekte umsetzen.
<!-- TEASER_END -->

In einem gemeinsamen Mission Statement hat die ENABLE!-Community ihre Aufgaben und Zielsetzung festgehalten und veröffentlicht. Das Mission Statement begreift sich als Startpunkt für eine transformativ ausgerichtete kollaborative Publikationskultur, die perspektivisch darauf abzielt, freien Zugang zu wertvollen wissenschaftlichen Inhalten zu ermöglichen. Zu den Erstunterzeichnern gehören:

* De Gruyter Verlag
* Dietmar Dreier GmbH
* Fachrepositorium peDOCS des DIPF | Leibniz-Institut für Bildungsforschung und Bildungsinformation
* Fachrepositorium SSOAR der GESIS Leibniz Institut für Sozialwissenschaften
* FAU University Press
* Georg Olms Verlag AG
* Informationszentrum Bildung (IZB) des DIPF | Leibniz-Institut für Bildungsforschung und Bildungsinformation
* Knowledge Unlatched
* Lehmanns Media
* Monika Pohlschmidt (Leibniz-Institut für Deutsche Sprache)
* Open Access Repositorium der Medienwissenschaft media/rep/
* Schüren Verlag
* transcript Verlag
* Universitätsbibliothek Duisburg-Essen
* Universitäts- und Landesbibliothek Münster
* Verlag Barbara Budrich GmbH
* Verlag Julius Klinkhardt KG
* Waxmann Verlag GmbH
* wbg – Wissen. Bildung. Gemeinschaft.
* wbv Publikation (wbv Media GmbH & Co. KG)

Hiermit erhalten alle die Gelegenheit, sich ebenfalls durch ein Unterzeichnen dieses Mission Statements zu den dort genannten Zielen zu bekennen und ja zu sagen zu gemeinsamen Aktivitäten, die die Open-Access-Transformation in den Geistes- und Sozialwissenschaften community-basiert und disziplinorientiert voran bringen.

## [Mission Statement](https://enable-oa.org/mission-statement)

Werden auch Sie Teil der Open-Access-Community für die Geistes- und Sozialwissenschaften!
