<!--
.. title: Analyse der Springer Offsetting Agreements - erste Ergebnisse
.. slug: analyse-springer-offsetting-agreements
.. date: 2018-03-23 08:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

This is a repost from our partner project INTACT, you can find the original post [here](https://www.intact-project.org/general/openapc/2018/03/22/offsetting-coverage/)
---------------------------------

Als ein Ergebnis des ersten [ESAC Offsetting Workshops 2016](http://esac-initiative.org/activities/offsetting-workshop-2016/) wurde die [Erfassung](https://github.com/OpenAPC/openapc-de/tree/master/data/offsetting) von Artikeln, publiziert unter einem Offsetting-Vertrag wie den Springer Compact Agreements, als ein Teilprojekt von OpenAPC entwickelt. Datenlieferanten sind die Kooperation E-Medien Österreich (KEMÖ), die Max Planck Digital Library, VSNU/UKB für alle niederländischen Universitäten, das Bibsam-Konsortium für Schweden und JISC Collections für Großbritannien. Die Datensammlung begann mit den ersten Daten aus dem Jahre 2015, mittlerweile sind auch die Jahre 2016 und 2017 vollständig verfügbar.

Auch wenn diese Artikel nicht im Sinne von APCs mit Kostendaten verknüpft sind, ist dennoch eine Aggregierung und Visualisierung mit [Treemaps](https://treemaps.intact-project.org/apcdata/offsetting/) sinnvoll, wobei eine einfache numerische Zählung als Maß verwendet wird. Mit dieser Offsetting-Kollektion, die mehr als 13.000 Artikel aus drei Jahren umfasst, ist es so möglich, die Abdeckung der Offsetting-Verträge zu messen und darzustellen, indem drei Fragen beantwortet werden:
<!-- TEASER_END -->

Für jede hybride Zeitschrift, die im Offsetting-Datensatz enthalten ist:
<br />
1. Wie viele Artikel wurden in einem bestimmten Zeitraum veröffentlicht (üblicherweise in einem Jahr)?<br />
2. Wie viele von diesen Artikeln wurden Open Access publiziert?<br />
3. Wie viele von diesen Artikeln wurden unter den Offsetting-Bedingungen publiziert?

Besonders reizvoll ist die Analyse für die Vereinbarungen von Springer Compact: Da alle aktuellen Projektmitglieder ihre Daten an OpenAPC melden, sollte die Artikelbasis (fast) vollständig sein. Dies würde wichtige Erkenntnisse darüber liefern, in welchem Umfang die durch die genannten Offsettingverträge finanzierten Artikel die Open-Access-Anteile in den Springer Compact Zeitschriften erhöht haben und wie das Offsetting zum Ziel der OA2020-Initiative beiträgt, Zeitschriften aus dem Subskriptionssystem in Open Access umzuwandeln.


## Methode

Während die Idee an sich einfach ist, erwies es sich als schwierig, die erforderlichen Daten zu den Publikationzahlen der Zeitschriften zu erhalten. Unser erster Ansatz, der vom [Hybrid-OA-Monitor](https://najkoja.shinyapps.io/hybridoa/) unseres ehemaligen Kollegen Najko Jahn inspiriert wurde, bestand darin, die [Crossref-API](https://github.com/CrossRef/rest-api-doc) zu nutzen. Ein Beispiel: mit Hilfe dieser [Abfrage](https://api.crossref.org/works?filter=issn:0938-7994,from-pub-date:2015-01-01,type:journal-article&facet=license:*) erhält man Kennzahlen zu einer der größten Zeitschriften in unserer Sammlung (in Bezug auf Offsetting-Artikel), [European Radiology](https://link.springer.com/journal/330), von 2015 bis jetzt. Das manuelle Durchsuchen der zurückgegebenen JSON-Struktur ist umständlich, aber die relevanten Informationen können recht einfach gefunden werden:
~~~~~
"license": {
    "value-count": 3,
        "values": {
            "http:\/\/www.springer.com\/tdm": 1263,
            "http:\/\/creativecommons.org\/licenses\/by\/4.0": 171,
            "http:\/\/creativecommons.org\/licenses\/by-nc\/4.0": 43
        }
    }
},
~~~~~
und
~~~~~
"total-results": 1772,
~~~~~

Entsprechend der Crossref-Ergebnisse wurden also von 2015 bis heute (21.03.2018) insgesamt 1.772 Artikel der Zeitschrift *European Radiology* publiziert, davon sind 214 Open Access (ermittelt durch die Summe aller CC-Lizenz-Angaben). Auch wenn diese Ergebnisse nicht unwahrscheinlich erscheinen, versuchten wir, sie über eine zweite Quelle zu verifizieren: SpringerLink, die verlagseigene Webseite inklusive Zeitschriftensuche. Bei der Verwendung der gleichen Parameter erhielten wir diese [Ergebnisseite](https://link.springer.com/search?date-facet-mode=between&facet-journal-id=330&facet-end-year=2018&query=&facet-start-year=2015) für die Gesamtanzahl an Artikeln und diese [Seite](https://link.springer.com/search?facet-journal-id=330&package=openaccessarticles&search-within=Journal&query=&date-facet-mode=between&facet-start-year=2015&facet-end-year=2018) für die Menge an Open-Access-Artikeln.
Für den angegebenen Zeitraum liegen laut Verlagsseite insgesamt 1.956 Artikel vor, davon 272 als Open Access. Es gibt also eine recht große Spanne in den Zahlen, und das ist leider kein Ausnahmefall. Hier sind einige weitere Ergebnisse für die 5 Zeitschriften, die am häufigsten in dem Offsetting-Datensatz vorkommen, ebenfalls erhalten am 21.03.2018:

| Journal | # Articles (Crossref) | # Articles (SpringerLink) | # OA Articles (Crossref) | # OA Articles (SpringerLink) |
|:--------|:-------:|--------:|--------:|--------:|
| European Radiology | 1772 | 1956 | 214 | 272 |
| Synthese | 1117 | 1242 | 176 | 187 |
| Diabetologia | 1098 | 1185 | 241 | 321 |
| J. of Autism and Developmental Disorders | 1180 | 1371 | 132 | 174 |
| J. of Business Ethics | 1316 | 1859 | 119 | 142 |

<br />
Die Ergebnisse sind eindeutig: wenn es um Zeitschriften-Kennzahlen geht (sowohl Open Access als auch alle anderen), sind die Crossref-Daten zu lückenhaft, um sich darauf zu verlassen.

Das bringt uns direkt zu unserem zweiten Ansatz: Verwendung der Statistiken von SpringerLink statt von Crossref. Technisch gesehen gibt es zwei Möglichkeiten dies zu tun: Die erste besteht darin, die Suchergebnisse von SpringerLink im CSV-Format herunterzuladen und die Anzahl der darin enthaltenen Einträge zu zählen. Die zweite Möglichkeit besteht darin, [Web Scraping](https://en.wikipedia.org/wiki/Web_scraping) zu verwenden, um die Ergebnismenge direkt von der Suchseite aus zu lesen. Während dieser Ansatz zu einer genauen Anzahl von Artikeln führte, hatte er einen unvorhergesehenen Nachteil, der ihn am Ende unbrauchbar machte: Der Zeitrahmen der Artikel auf SpringerLink ist nicht identisch mit dem in unserer Sammlung. Während sich das Feld "Periode" im Offsetting-Datensatz auf das Annahmedatum eines Artikels bezieht, bezieht sich der Datumsfilter bei SpringerLink auf das Druckveröffentlichungsdatum, das in der Regel erst später, oft sogar erst im nächsten Kalenderjahr erfolgt. Unserer Erfahrung nach gibt es keine Möglichkeit, einen anderen Datumstyp als Filter für SpringerLink zu verwenden. Eine einfache Verwendung dieser Zahlen würde somit zu falschen Vergleichen führen und ist daher keine Option.

Am Ende mussten wir einen zusätzlichen Normalisierungsschritt einplanen, um die Daten kompatibel zu machen: Dazu würden wir die oben genannten CSV-Listen für jede in unserem Datensatz enthaltene Zeitschrift von SpringerLink herunterladen, jeden einzelnen Offsetting-Artikel in diesen Listen (über die DOI) nachschlagen und das Veröffentlichungsdatum der Druckausgabe ermitteln. Dies würde unsere Offsetting-Daten effektiv in eine "Springer-Druck-Publikationszeit" umwandeln und vergleichbar mit den SpringerLink-Kennzahlen machen. Wir würden dann die Artikel in Zeitschriften und Jahren wieder zusammenfassen und einen [OLAP-Cube](https://olap.intact-project.org/cube/offsetting_coverage/aggregate) und eine [Treemap-Visualisierung](https://treemaps.intact-project.org/apcdata/offsetting-coverage/#publisher/Springer%20Nature/) erstellen.

Auch wenn dieser Ansatz erfolgreich geprüft wurde und eine Analyse der Offsetting-Abdeckung ermöglicht, gibt es zwei potentielle Probleme:

* Da der Zeitraum des Datensatzes *offsetting_coverage* geändert wurde, ist er nicht mehr mit der ursprünglichen [Offsetting-Kollektion](https://treemaps.intact-project.org/apcdata/offsetting/) im Bereich Jahr vergleichbar (z. B. *offsetting* listet 6.892 Artikel für den Zeitraum 2016 auf, im Gegensatz zu 5.113 Artikel in *offsetting_coverage*).

* Es ist keine Komplettlösung, da es auf Springers Webportal und Suchmaschine basiert. Ein langfristiger Ansatz sollte auf einer öffentlichen, verlegerunabhängigen Datenquelle wie Crossref basieren, aber wie wir gesehen haben, erfordert dies Verbesserungen des Datenverständnisses und der Datenqualität.


## Ergebnisse

Die folgende kurze Darstellung der Ergebnisse bezieht sich auf die Publikationsjahre 2016 und 2017, da die Daten für die eingangs genannten Einrichtungen für diese beiden Jahre vollständig vorliegen. Insgesamt wurden 13.941 Artikel über die Offsetting-Verträge in den Open Access überführt. Dies entspricht 4,28% der Gesamtmenge an Artikeln in den Springer Compact Zeitschriften (326.008) in diesem Zeitraum. Insgesamt konnten wir in den Springer Compact Zeitschriften 27.622 Open-Access-Artikel finden, was einem Anteil von 8,47% entspricht. So war Offsetting also für etwas mehr als die Hälfte aller Open-Access-Artikel in den hybriden Springer Compact Zeitschriften verantwortlich.

Das folgende Bild zeigt alle hybriden Springer Compact Zeitschriften mit einem Open-Access-Anteil von mindestens 50%.
<img src="/images/top_oa_journals_springer_compact.png" alt="top_oa_journals_springer_compact" width="800" height="250" display="block" />

2017 wurde in 1.311 Springer Compact Zeitschriften wenigstens ein Offsetting-Artikel publiziert, von diesen haben gerade mal 13 Zeitschriften einen Open-Access-Anteil von 50% oder mehr erreicht. Geht man von [insgesamt](https://www.springer.com/en/open-access/springer-open-choice/springer-compact) 1.700 Springer Compact Zeitschriften aus, wurden in rund 400 Titeln überhaupt keine Offsetting-Artikel veröffentlicht. Basierend auf den 1.700 Springer Compact Zeitschriften im Jahr 2017 haben nur 0,76% der Zeitschriften einen Open-Access-Anteil von 50% oder mehr erreicht.

Darüber hinaus beobachten wir starke Schwankungen innerhalb einzelner Zeitschriften, insbesondere wenn die Anzahl der veröffentlichten Artikel pro Zeitschrift insgesamt niedrig ist. Im Jahr 2017 hatte die Zeitschrift *Psychotherapie Forum* beispielsweise einen Open-Access-Anteil von 95%. Davon entfielen lediglich 5,26% auf Offsetting. Im Jahr 2016 hatte die gleiche Zeitschrift einen Open-Access-Anteil von 10,71%, von denen 100% durch Offsetting finanziert wurden.

Die erste Zwischenbilanz ist also, dass Offsetting in einigen der hybriden Springer Compact Zeitschriften von 2016 bis 2017 zu einem deutlichen Anstieg der Open-Access-Artikel beigetragen hat. Rund 24% aller Springer Compact Zeitschriften scheinen für die Institute, die an den Offsettingverträgen beteiligt sind, als Publikationsorte nicht relevant zu sein. Bisher reichen die Anzahl und die Verteilung zusätzlicher Open-Access-Artikel, die durch die genannten Offsettingverträge generiert werden, noch nicht aus, um einzelne Zeitschriften vollständig in Open Access umzuwandeln.
