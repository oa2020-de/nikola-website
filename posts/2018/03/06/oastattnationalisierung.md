<!--
.. title: Open Access statt "Verstaatlichung der Wissenschaftsverlage"
.. slug: oa-statt-verstaatlichung-Wissenschaftsverlage
.. date: 2018-03-06 08:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

In ihrem Beitrag "Is it time to nationalise academic publishers?" vom 02.03.2018 rückt Times Higher Education ([Quelle](https://www.timeshighereducation.com/blog/it-time-nationalise-academic-publishers)) die großen Wissenschaftsverlage in den Fokus einer Diskussion, die die Zerschlagung von wirtschaftlichen Monopolen auf die Agenda gesetzt hat. Die aufgezählten Gewinne im wissenschaftlichen Publikationssystem sind in der Tat eindrucksvoll:

* Der Gewinn von Elsevier für den Shareholder RELX betrug 2016 über 900 Millionen GBP mit einer Umsatzrendite in Höhe von 36,8%.

* Die Verlage Taylor & Francis und Routledge als Teile von Informa erwirtschafteten 2016 einen Gewinn von mehr als 160 Millionen GBP (Umsatzrendite 38%).

* Wiley erreichte 2017 einen Gewinn in Höhe von 183 Millionen GBP und eine Marge in Höhe von 29,6%.

Gewinne und Margen für Springer Nature wurden in dem Times-Higher-Education-Beitrag nicht genannt, gleichwohl sind die Zahlen für die Open-Access-Bewegung ernüchternd, da der überwiegende Teil der Gewinne immer noch aus dem Subskriptionssystem kommt, das maßgeblich von den Erwerbungsetats der Bibliotheken finanziert wird. Times-Higher-Education fragt in dem Artikel deshalb zwischen den Zeilen auch zurecht, warum Bibliotheken trotz Open Access und SciHub solche Renditen mit finanzieren.

Wenn die Bibliotheks-Community es tatsächlich mit Open Access Ernst meint, dann muss - wie Ralf Schimmer es an gleicher Stelle im letzten Jahr formulierte - der "Stecker aus dem Subskriptionssystem gezogen werden" ([Quelle](https://www.timeshighereducation.com/news/open-access-campaigners-toughen-stance-towards-publishers)), wenn Verlage die Transformation in den Open Access nicht mit gehen wollen.  Alternative Verwendungsmöglichkeiten für Bibliotheksetats gibt es jedenfalls jetzt schon reichlich:

* Finanzierung von Publikationsfonds für reine Open-Access-Verlage
* Unterstützung von Mitgliedschaftsmodelle wie SCOAP³, Open Library of Humanities, u.a.
* Beteiligung an Modellen zur Finanzierung von Open-Access-Ebooks
* Unterstützung der öffentlichen Open-Access-Infrastruktur (institutionelle und fachbezogene Repositorien, zentrale Open-Access-Services wie das DOAJ u.a.)

Wie lange wollen Bibliotheken noch warten? It´s time to take [action](https://oa2020.org/take-action/)!

