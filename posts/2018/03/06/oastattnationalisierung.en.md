<!--
.. title: Open Access instead of "nationalization of science publishers"
.. slug: oa-instead-nationalization-publisher
.. date: 2018-03-06 08:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

In her article "Is it time to nationalise academic publishers?" from 2018-03-02, Times Higher Education ([source](https://www.timeshighereducation.com/blog/it-time-nationalise-academic-publishers)) puts the big science publishers in the spotlight of a discussion that has put the break-up of economic monopolies on the agenda. The listed profits in the scientific publication system are indeed impressive:

* Elsevier's shareholder RELX profit in 2016 was over GBP 900 million, with a 36.8% return on sales.

* Taylor & Francis and Routledge, part of Informa, generated more than £ 160 million in profits in 2016 (38% return on sales).

* Wiley achieved a profit of £ 183m in 2017 and a margin of 29.6%.

Profits and margins for Springer Nature were not mentioned in the Times Higher Education article, but the figures for the open access movement are sobering, as most of the profits still come from the subscription system, which is largely determined by acquisition budgets funded by the libraries. Times-Higher-Education therefore asks in the article between the lines also why libraries, in spite of Open Access and SciHub finance such returns.

If the library community really means open access seriously then - as Ralf Schimmer put it in the same place last year - the "plug from the subscription system" has to be drawn ([source](https://www.timeshighereducation.com/news/open-access-campaigners-toughen-stance-towards-publishers)) if publishers do not want to go with the transformation into open access. In any case, there are already plenty of alternative uses for library budgets:

* Organizing publication funds for pure open access publishers
* Support for membership models such as SCOAP³, Open Library of Humanities, etc.
* Participation in models for financing open access ebooks
* Support for public open access infrastructure (institutional and subject repositories, central open access services such as DOAJ and others)

How long do libraries want to wait? It´s time to take [action](https://oa2020.org/take-action/)!
