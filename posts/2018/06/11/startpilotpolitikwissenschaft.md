<!--
.. title: Start des Pilotprojektes OPEN Library Politikwissenschaft
.. slug: start-pilot-politikwissenschaft
.. date: 2018-06-11 07:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

**Pilotprojekt OPEN Library Politikwissenschaft gestartet**

*(Bielefeld, 11. Juni 2018)*

Der Nationale Open-Access-Kontaktpunkt OA2020-DE, der Verlag transcript, der Fachinformationsdienst (FID) Politikwissenschaft an der Staats- und Universitätsbibliothek Bremen sowie Knowledge Unlatched starten heute ein Projekt, dass es Politkwissenschaftler_innen ermöglicht, ihre Bücher direkt im Open Access zu veröffentlichen.

Ziel des gemeinsamen Projekts ist die Entwicklung eines für Verlag und Bibliotheken gleichermaßen tragbaren, transparenten und ökonomisch nachhaltigen Open-Access-E-Book-Geschäftsmodells. Die Literaturerwerbungsmittel der Bibliotheken fließen damit anteilig statt in den Erwerb kostenpflichtiger E-Books für eine einzelne Bibliothek in die Finanzierung der freien Verfügbarkeit für alle.

Open Access verfolgt das Ziel, die Chancen der Digitalisierung für Autor_innen, Verlage und Bibliotheken gleichermaßen zu nutzen. Mit Hilfe von nachhaltigen und transparenten Angeboten seitens der Verlage sowie der finanziellen Beteiligung durch Bibliotheken ergeben sich für alle Akteure neue Möglichkeiten zur Positionierung im wissenschaftlichen
Publikationssystem.

Das Modell bringt im Vergleich zur gängigen Praxis der Lizenzierung von E-Books u. a. folgende Vorteile mit sich:

+ Lizenzkosten für E-Books entfallen, Finanzierung durch Umschichtung im Etat möglich
+ Keine Einschränkungen für die Nutzung der Inhalte
+ Sichtbarkeit der mitfinanzierenden Einrichtungen durch Sponsoring
+ Verankerung des FID und der Bibliothek im fachbezogenen Open-Access-Publikationsprozess

<br />
**Interessiert?**

Weitere Informationen und Beteiligungsmöglichkeiten für Bibliotheken, die Open Access unterstützen möchten, finden Sie auf den Webseiten des Kontaktpunkts
unter: [https://oa2020-de.org/pages/transcriptopenlibrarypowi/](https://oa2020-de.org/pages/transcriptopenlibrarypowi/)<br />
auf den Webseiten des transcript-Verlages: [https://www.transcript-verlag.de/open-access-politikwissenschaft](https://www.transcript-verlag.de/open-access-politikwissenschaft)<br />
und bei Knowledge Unlatched: [http://knowledgeunlatched.org/political-science/](http://knowledgeunlatched.org/political-science/)

<br />
**Über den Nationalen Open-Access-Kontaktpunkt OA2020-DE**

Strategisches Ziel des Nationalen Open-Access-Kontaktpunkt OA2020-DE ist das Schaffen von Voraussetzungen für die großflächige Open-Access-Transformation
in Übereinstimmung mit der Allianz der deutschen Wissenschaftsorganisationen. Dazu entwickelt OA2020-DE unter anderem neue, kooperative Open-Access-Geschäftsmodelle.<br />
Weitere Informationen über den Kontaktpunkt unter [http://oa2020-de.org/pages/ziele/](http://oa2020-de.org/pages/ziele/).
