<!--
.. title: Start of the pilot project for the open access transformation of E-Books
.. slug: start-pilot-politicalsscience
.. date: 2018-06-11 07:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

**Start of the pilot project OPEN Library Political Science**

*(Bielefeld, 2018-06-11)*

The National Contact Point Open Access OA2020-DE, the publisher transcript, the Political Science Information Service (FID) at the Bremen State and University Library and Knowledge Unlatched are launching a project today that will enable political scientists to publish their books directly in Open Access.

The aim of the pilot project is the development of a publisher and library equally manageable, transparent and economically sustainable open-access e-book business model. That means, that instead of buying the E-Books, the participating libraries enable the open access publication of all forthcoming books. Through that, the library budgets unlatch the titles to the benefit of everybody instead of supporting isolated access for single institutions.

Open Access pursues the goal of making equal use of the opportunities of digitization for authors, publishers and libraries alike. With the help of sustainable and transparent offers on the part of publishers as well as the financial participation by libraries new possibilities for the positioning in the scientific publication system arise for all actors.

Our model has the following advantages compared to the common practice of e-book licensing:

* License costs for e-books are eliminated, financing through reallocations in the budget possible
* no restrictions on the use and provision of Open Access publications in teaching (digital semester apparatus, etc.)
* Visibility of the co-financing institutions through sponsorship
* Anchoring the FID in the subject-specific open access publication process

<br />
**Interested?**

Find more information on our project website: [https://oa2020-de.org/en/pages/transcriptopenlibrarypols/](https://oa2020-de.org/en/pages/transcriptopenlibrarypols/)<br />
Website of transcript publishing house: [https://www.transcript-verlag.de/open-access-politikwissenschaft](https://www.transcript-verlag.de/open-access-politikwissenschaft)<br />
Or Knowledge Unlatched: [http://knowledgeunlatched.org/political-science/](http://knowledgeunlatched.org/political-science/)

If you have any other questions, please contact us, we are happy to help!

<br />
**About the National Contact Point Open Access OA2020-DE**

The strategic goal of the National Contact Point Open Access OA2020-DE is to create prerequisites for the large-scale open access transformation
in accordance with the Alliance of Science Organisations in Germany. Among other things, OA2020-DE is developing new, cooperative open access business models.<br />
More information about the Contact Point: [http://oa2020-de.org/en/pages/aims/](http://oa2020-de.org/en/pages/aims/).


