<!--
.. title: Sweden's dealings with Elsevier
.. slug: swedens-dealings-with-elsevier
.. date: 2018-06-26 08:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

The Bibsam consortium has published a revealing [Q&A](http://openaccess.blogg.kb.se/2018/06/20/qa-about-the-cancellation-of-the-agreement-with-elsevier-commencing-1-july/) as further information aabout the cancellation of the agreement with Elsevier commencing 1 July in Sweden. The numbers given there illustrate why Elsevier is closed to an open access transformation.

* In 2017 the turnover for Elsevier journals within the consortium was 12. 559 062 EUR.
* In 2017 researchers affiliated with Swedish HEIs published around 4000 articles in Elsevier’s journals.
* In 2017 the publication fees (APCs) for Elsevier open-access articles amounted to 1. 300 000 EUR.
* Calculating the expenses together, this gives an average value of approx. EUR 3.465 per Elsevier article.

Comparing the data reported to the openAPC project, Swedish HEIs and research institutions paid an average of APCs of EUR 2.136 to Elsevier for 165 open access articles in 2015-2016 (70% for hybrid and 30% for publication in open-access journals).

Add to this the Elsevier list prices for open access journal articles ([Elsevier OA Price List](https://www.elsevier.com/__data/promis_misc/j.custom97.pdf), Date: 16-Jun-2018), the average price per article for 2055 hybrid journals and 342 OA journals is 2.313 USD (converted about 1.984 EUR at the daily current exchange rate of 1,16 EUR / USD, in addition VAT would have to be added).

The average APCs actually paid in Sweden as well as the average prices for APCs according to the Elsevier price list are thus significantly lower than the equivalent of EUR 3.465 per article in the previous system. Switching to a pay-as-you-publish model would therefore be financially feasible and meaningful for Sweden.

We wish the colleagues in Sweden and of course the colleagues in the DEAL project every success in their negotiations.
