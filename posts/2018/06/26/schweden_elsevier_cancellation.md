<!--
.. title: Schwedens Umgang mit Elsevier
.. slug: schwedens-umgang-mit-elsevier
.. date: 2018-06-26 08:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Das Bibsam-Konsortium hat als weitere Information zur Stornierung der Elsevier-Zeitschriften in Schweden ein aufschlussreiches [Q&A](http://openaccess.blogg.kb.se/2018/06/20/qa-about-the-cancellation-of-the-agreement-with-elsevier-commencing-1-july/) veröffentlicht. Die dort genannten Zahlen verdeutlichen, warum Elsevier sich einer Open-Access-Transformation verschließt.

* Die Zahlungen des Bibsam-Konsortiums 2017 für Elsevier-Zeitschriften betrugen: 12. 559 062 EUR.
* Im gleichen Jahr veröffentlichten die Angehörigen schwedischer Hochschulen und Universitäten rund 4000 Artikel in Elsevier-Zeitschriften.
* Die Ausgaben für Article Processing Charges bei Elsevier lagen 2017 bei ca. 1. 300 000 EUR.
* Rechnet man die Ausgaben zusammen ergibt das einen Durchschnittswert von ca. 3.465 EUR pro Elsevier-Artikel.

Betrachtet man dazu im Vergleich die an das openAPC-Projekt gemeldeten Daten, so zahlten 2015-2016 schwedische wissenschaftliche Einrichtungen für 165 Open-Access-Artikel durchschnittlich APCs in Höhe von 2.136 EUR an Elsevier (70% für hybride und 30% für Publikation in Open-Access-Zeitschriften).

Zieht man desweiteren die Elsevier-Listenpreise für Open-Access-Zeitschriftenartikel hinzu ([Elsevier OA Price List](https://www.elsevier.com/__data/promis_misc/j.custom97.pdf), Date: 16-Jun-2018), beträgt der Durchschnittspreis pro Artikel für 2055 Hybrid Journals und 342 OA-Journals 2.313 USD (umgerechnet etwa 1.984 EUR zum tages-aktuellen Währungskurs von 1,16 EUR/USD, zusätzlich müsste noch Umsatzsteuer hinzu addiert werden).

Die tatsächlich in Schweden gezahlten durchschnittlichen APCs und auch die Durchschnittspreise für APCs laut Elsevier-Preisliste sind also deutlich niedriger als die umgerechnet 3.465 EUR pro Artikel im bisherigen System. Ein Umstieg auf ein pay-as-you-publish-Modell wäre für Schweden daher finanziell möglich und sinnvoll.

Wir wünschen den Kolleg_innen in Schweden und natürlich auch den Kolleg_innen im DEAL-Projekt viel Erfolg bei ihren Verhandlungen.
