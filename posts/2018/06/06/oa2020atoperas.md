<!--
.. title: Gemeinsame Strategie statt Konkurrenz - OA2020 auf der OPERAS Konferenz
.. slug: oa2020-operasconference
.. date: 2018-06-06 14:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Ende letzter Woche fand in Athen die erste [OPERAS Konferenz](https://operas.hypotheses.org/conference-2018-05) "Open Scholarly Communication in Europe. Addressing the Coordination Problem” statt. OPERAS ist ein europäisches Forschungsinfrastrukturprojekt zur Förderung von Open Access bzw. Open Science speziell in den Sozial- und Geisteswissenschaften. Die [OA2020-Initiative](https://oa2020.org/) war in Form des OA2020-DE-Projektleiters Dirk Pieper in der Session "Flipping Journals or Changing the System? The Need for Coordination" vertreten. Ziel der [Präsentation](/assets/files/oa2020_operas_20180601.pdf) war es aufzuzeigen, dass OA2020 mehrere Wege der Open-Access-Transformation unterstützt und damit mehr ist als Journal Flipping und APCs. Gleichzeitig sollte deutlich gemacht werden, dass in diesen Bereichen empirisch relevantes OA-Publizieren stattfindet und APCs und Offsetting wesentliche Faktoren der Transformation sind.

<blockquote class="twitter-tweet" data-partner="tweetdeck"><p lang="en" dir="ltr">Dirk Pieper <a href="https://twitter.com/Di_Pieper?ref_src=twsrc%5Etfw">@Di_Pieper</a>, University of Bielefeld, on the <a href="https://twitter.com/hashtag/OA2020?src=hash&amp;ref_src=twsrc%5Etfw">#OA2020</a> Initiative for transformation of publishing system, replacing the subscription business model with new ones that ensure open, re-usable &amp; economically sustainable outputs <a href="https://twitter.com/hashtag/OPERASconf?src=hash&amp;ref_src=twsrc%5Etfw">#OPERASconf</a> <a href="https://t.co/hPC1tD8vis">https://t.co/hPC1tD8vis</a> <a href="https://t.co/sykWAtzQXp">pic.twitter.com/sykWAtzQXp</a></p>&mdash; ΕΚΤ (@EKTgr) <a href="https://twitter.com/EKTgr/status/1002488002394902528?ref_src=twsrc%5Etfw">June 1, 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Des Weiteren verwies Herr Pieper darauf, dass OA2020 und [FAIR OA](https://www.fairopenaccess.org/) sowie der [Jussieu Call](http://jussieucall.org/index.html) als komplementäre Ansätze der Open-Access-Transformation zu verstehen sind: ihre gemeinsame Verbindung liegt u. a. in dem Entzug von Budgets aus dem Subskriptionssystem, um diese Gelder dann in konkrete Open-Access-Transformationsansätze (z.B. Fair OA, OLH, Infrastrukturen wie DOAJ, Repositorien oder OJS, Offsetting, APCs, Ebook-OA-Projekte etc.) umzuleiten. Daher ist die Entwicklung einer gemeinsamen Strategie sinnvoll und notwendig. OA2020-DE und DEAL unterstützen OA2020 dabei in Deutschland und darüber hinaus.

<blockquote class="twitter-tweet" data-partner="tweetdeck"><p lang="en" dir="ltr">Great work from <a href="https://twitter.com/Di_Pieper?ref_src=twsrc%5Etfw">@Di_Pieper</a>  at the <a href="https://twitter.com/hashtag/OPERASconf?src=hash&amp;ref_src=twsrc%5Etfw">#OPERASconf</a> today on behalf of <a href="https://twitter.com/oa2020ini?ref_src=twsrc%5Etfw">@oa2020ini</a> and <a href="https://twitter.com/oa2020de?ref_src=twsrc%5Etfw">@oa2020de</a> highlighting how <a href="https://twitter.com/hashtag/openaccess?src=hash&amp;ref_src=twsrc%5Etfw">#openaccess</a> initiatives are more effective when working together to form a comprehensive strategy! <a href="https://t.co/Cm3a5OQ3M0">pic.twitter.com/Cm3a5OQ3M0</a></p>&mdash; Open Access 2020 (@oa2020ini) <a href="https://twitter.com/oa2020ini/status/1002573606805737474?ref_src=twsrc%5Etfw">June 1, 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
