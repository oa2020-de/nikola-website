<!--
.. title: Comprehensive strategy instead of competition - OA2020 at OPERAS Conference
.. slug: oa2020-operasconference
.. date: 2018-06-06 14:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

At the end of last week, the first [OPERAS Conference](https://operas.hypotheses.org/conference-2018-05) "Open Scholarly Communication in Europe. Addressing the Coordination Problem” took place in Athens. OPERAS is a European research infrastructure project for the promotion of open access and open science especially in the social sciences and humanities. The [OA2020 initiative](https://oa2020.org/) took part in form of the OA2020-DE project leader Dirk Pieper in the session "Flipping Journals or Changing the System? The Need for Coordination". The aim of the [presentation](/assets/files/oa2020_operas_20180601.pdf) was to show that OA2020 supports multiple ways of open access transformation, which is more than journal flipping and APCs, and at the same time to made clear that empirically relevant OA publishing in these areas takes place and APCs and offsetting are essential factors of the transformation.

<blockquote class="twitter-tweet" data-partner="tweetdeck"><p lang="en" dir="ltr">Dirk Pieper <a href="https://twitter.com/Di_Pieper?ref_src=twsrc%5Etfw">@Di_Pieper</a>, University of Bielefeld, on the <a href="https://twitter.com/hashtag/OA2020?src=hash&amp;ref_src=twsrc%5Etfw">#OA2020</a> Initiative for transformation of publishing system, replacing the subscription business model with new ones that ensure open, re-usable &amp; economically sustainable outputs <a href="https://twitter.com/hashtag/OPERASconf?src=hash&amp;ref_src=twsrc%5Etfw">#OPERASconf</a> <a href="https://t.co/hPC1tD8vis">https://t.co/hPC1tD8vis</a> <a href="https://t.co/sykWAtzQXp">pic.twitter.com/sykWAtzQXp</a></p>&mdash; ΕΚΤ (@EKTgr) <a href="https://twitter.com/EKTgr/status/1002488002394902528?ref_src=twsrc%5Etfw">June 1, 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Furthermore, Mr Pieper pointed out that OA2020 and [FAIR OA](https://www.fairopenaccess.org/) as well as [Jussieu Call](http://jussieucall.org/index.html) are to be understood as complementary approaches of the open access transformation: their joint connection is, inter alia, in the withdrawal of budgets from the subscription system to redirect these funds into concrete open access transformation approaches (e.g., Fair OA, OLH, infrastructures such as DOAJ, repositories or OJS, offsetting, APCs, ebook OA projects, etc.). Therefore, the development of a common strategy is useful and necessary. OA2020-DE and DEAL support OA2020 thereby in Germany and beyond.

<blockquote class="twitter-tweet" data-partner="tweetdeck"><p lang="en" dir="ltr">Great work from <a href="https://twitter.com/Di_Pieper?ref_src=twsrc%5Etfw">@Di_Pieper</a>  at the <a href="https://twitter.com/hashtag/OPERASconf?src=hash&amp;ref_src=twsrc%5Etfw">#OPERASconf</a> today on behalf of <a href="https://twitter.com/oa2020ini?ref_src=twsrc%5Etfw">@oa2020ini</a> and <a href="https://twitter.com/oa2020de?ref_src=twsrc%5Etfw">@oa2020de</a> highlighting how <a href="https://twitter.com/hashtag/openaccess?src=hash&amp;ref_src=twsrc%5Etfw">#openaccess</a> initiatives are more effective when working together to form a comprehensive strategy! <a href="https://t.co/Cm3a5OQ3M0">pic.twitter.com/Cm3a5OQ3M0</a></p>&mdash; Open Access 2020 (@oa2020ini) <a href="https://twitter.com/oa2020ini/status/1002573606805737474?ref_src=twsrc%5Etfw">June 1, 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
