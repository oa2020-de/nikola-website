<!--
.. title: Zwang zum Open-Access-Publizieren oder die Macht des Faktischen?
.. slug: Zwang-zum-Open-Access-Publizieren
.. date: 2018-01-29 08:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Die Frankfurter Allgemeine Zeitung hat am 25.01.2018 erneut einen Artikel zu Open Access veröffentlicht: "Der neue Zwang zum Open Access". Diesmal im Politik-Teil statt im Feuilleton, in dem sonst die Beiträge zum Thema erscheinen (siehe z.B. [hier](http://www.faz.net/aktuell/feuilleton/forschung-und-lehre/open-access-strategie-staatsautoritarismus-gross-geschrieben-14454601.html), [hier](http://www.faz.net/aktuell/feuilleton/debatten/open-access-eine-heimliche-technokratische-machtergreifung-1775488.html) und [hier](http://www.faz.net/aktuell/feuilleton/debatten/open-access-publikationssystem-laeuft-auf-kontrollsystem-aus-14538833.html)).

Im Vergleich zu den bisher ausnahmslos kritischen Beiträgen zum Thema darf Bernhard Kempen, Präsident des Deutschen Hochschulverbandes, in der FAZ Open Access immerhin als das Publikationssystem der Zukunft beschreiben. Gleichwohl wird in diesem Artikel der Open-Access-Bewegung (zum wiederholten Male) vorgeworfen, die Wissenschaftsfreiheit einzuschränken.
<!-- TEASER_END -->
Weder die Open-Access-Community noch §38, Abs. 4 UrhG (Zweitveröffentlichungsrecht) schreiben den Wissenschaftler_innen vor, wann und wo sie zu publizieren haben. Schon gar nicht wird verlangt, alles *ohne* Verlagsumwege ins Internet zu stellen. Das Zweitveröffentlichungsrecht eröffnet vielmehr die Möglichkeit, unter den im Urheberrecht definierten [Rahmenbedingungen](https://www.allianzinitiative.de/de/archiv/rechtliche-rahmenbedingungen/faq-zvr.html) zusätzlich zur Verlagsveröffentlichung die Publikation einer größeren Öffentlichkeit zur Verfügung zu stellen.

Das wissenschaftliche Kommunikationssystem ist ganz anderen Zwängen als dem genannten unterworfen. So schreibt Herr Kempen selbst: "Die Zahl der Publikationen und die durch den Impactfaktor gemessene Wertigkeit einer Zeitschrift gelten als  Qualitätsausweis. Deshalb bleibt die Publikationsstrategie jedes Wissenschaftlers darauf gerichtet, möglichst viele Beiträge in möglichst hochrangigen Zeitschriften zu veröffentlichen". Schränkt also nicht vielmehr der Zwang, in High-Impact-Journals zu publizieren, die Wissenschaftsfreiheit ein? Auch gibt es bisher keinen Beleg dafür, dass die Anwendung des Zweitveröffentlichungsrechtes dazu geführt hätte, dass Einreichungen von Autor_innen von einem Verlag nicht mehr angenommen wurden.

In dem jüngst erschienen Report ["Open access availability of scientific publications "](http://www.science-metrix.com/en/publications/reports#/en/oa-report) wurde festgestellt, dass in den publikationsstärksten Ländern mehr als 50% der zwischen 2010 und 2014 erschienen Publikationen im goldenen und grünen Open Access frei verfügbar sind. Im Bereich Gold Open Access, also unmittelbar frei zugänglich, liegt der Anteil laut [INTACT-Forschungsbericht](http://doi.org/10.13140/RG.2.2.33235.89120) bei den 10 publikationsstärksten Ländern in den Jahren 2008-2016 zwischen 8% und 13% (auf Basis von Web of Science- und Scopus-Daten). Von den Top-20-Zeitschriften weltweit (gemessen anhand der publizierten Artikelmenge pro Jahr) liegen inzwischen 8 als [Gold-Open-Access-Zeitschrift](http://oa2020-de.org/blog/2017/11/13/FairOA/) vor und werden somit offensichtlich von den Autor_innen als Publikationsort ausgewählt.

Den vermeintlichen Zwang zum Open-Access-Publizieren bezeichnet Kempen als "kulturfeindlich, urheberfeindlich, antiindividualistisch und antifreiheitlich." Eigentlich ist genau das Gegenteil der Fall: freie Forschungsergebnisse unterstützen den freien Austausch von Wissen innerhalb der Wissenschaft und der Gesellschaft. Öffentlich frei zugängliche Forschungsergebnisse sind also vielmehr kulturfördernd, urheberschützend und freiheitlich.
