<!--
.. title: Alternative Publishing Models to Support Gold Open Access
.. slug: alternativepublishingmodels
.. date: 2018-01-17 08:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

The year 2018 starts with a positive message: even if the number of institutions without Elsevier contract in Germany with the new year has risen to over 200, the publisher continues to grant access to its content. That said, the negotiations between DEAL and Elsevier will probably continue: "We will continue our conversations in the first quarter of 2018 to find an access solution for German researchers in 2018 and a longer-term national agreement," says Harald Boersma, spokesman for [Elsevier](https://www.nature.com/articles/d41586-018-00093-7).

Nevertheless, it makes sense to look for alternative publishing models in Gold Open Access. In addition to the quasi-standard article / book processing charges to pay for an open access publication, there are other variants, ranging from a publication without payment of a publication fee to co-operative models.
<!-- TEASER_END -->

<br />
**1. Publication without payment of a publication fee**<br />
The publisher charges the author no publication fees for the publication of his article. In addition to temporary variants, such an offer may also be permanent if an institutional infrastructure and / or another financing model is available to finance the publication activities. For example, if the journal is run on an academic basis and thus the entire publication process is in the hands of representatives of a scholarly community and its institutions. These models are often found in the social sciences and humanities. The organization and financing of such self-publishing models can be done by the scientific institutions (universities or research institutions and their libraries), but also by subsidies from foundations or the government.

*Examples:* [BieJournals](http://oa.uni-bielefeld.de/en/ojs.html), [heiJOURNALS](http://journals.ub.uni-heidelberg.de/index.php/ojs/index), [heiBOOKS](http://books.ub.uni-heidelberg.de/heibooks?lang=en), [Emerging Infectious Diseases](https://wwwnc.cdc.gov/eid/)

<br />
**2. Publication after payment of a reduced publication fee**<br />
Reduction of a publication fee can be achieved, on the one hand, by funding a Gold Open Access journal by a learned society whose members pay a lower price or by organizing APC payments through the institution and related models. In the case of a pre-paid membership, the institutions pay publication fees in advance to the publisher and the authors can publish open access without their own payment. The often discounted fees will then be deducted from the pre-payment. The institutions may also pay an annual fee as the basis of a defined, fixed rebate granted to the authors on the publication fee per article. Or they pay a fee as a lump sum for all authors of the institution (so-called 'flat fees').

*Examples:* [Springer Membership](https://www.springeropen.com/about/institutional-support/membership), [Taylor&Francis Pre-Pay-Membership](http://www.tandfonline.com/openaccess/members), [Publisher Deals in the Netherlands](http://openaccess.nl/en/in-the-netherlands/publisher-deals)

<br />
**3. Payment of a fee for an unlimited number of articles on condition of cooperation**<br />
Authors become a permanent member of an Open Access journal after paying a one-time fee and undertake, e.g. to participate as a reviewer in the publication process. In return, they will not be charged any fees or otherwise reduced fees for publishing their articles in the respective journal. The model can also be applied to author groups and institutions.

*Examples:* [PeerJ](https://peerj.com/), [IOP referee reward scheme](https://publishingsupport.iopscience.iop.org/questions/article-processing-charge-discount/)

<br />
**4. Consortium to finance the transformation of journals from subscription to open access**<br />
Institutions merge into a consortium, pooling their open access publishing activities and cooperating with one or more publisher. The goal is to empower publisher to convert their subscription-based business model to a publication-cost-based or cooperative model.

*Examples:* [SCOAP³](https://scoap3.org/), [Open Library of Humanities](https://www.openlibhums.org/site/academics/journal-applications-to-join-the-olh/), [LingOA](http://www.lingoa.eu/about/mission/), [MathOA](http://www.mathoa.org/), [PsyOA](http://psyoa.org/), [Open Access Network](http://openaccessnetwork.org/)

<br />
**5. Consortial or cooperative open access models**<br />
Institutions merge into a consortium and cooperatively finance the open access publications by paying a defined fee per institution. Such a cooperative can bring together learned societies, libraries, researchers and funders to provide scalable and cost-effective open access publishing services. Authors are not financially burdened with the model.

*Examples:* [Knowledge Unlatched](http://www.knowledgeunlatched.org/), [eLife](https://elifesciences.org/), [SciELO](http://scielo.org/php/index.php?lang=en), [Palaeontologia Electronica](http://palaeo-electronica.org/owner.htm)

<br />
This listing is far from complete, but covers the most widely used alternative publishing models for Gold Open Access. More descriptions can be found [here](http://oad.simmons.edu/oadwiki/OA_journal_business_models), [here](http://doi.org/10.2312/allianzoa.008) (only german) and [here](https://doi.org/10.5281/zenodo.1323707).

OA2020-DE recommends that you familiarize yourself with alternatives to the standard APC model (pay what the publisher requires) and provide funding from the library budget.
