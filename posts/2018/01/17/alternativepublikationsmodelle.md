<!--
.. title: Alternative Publikationsmodelle im Gold Open Access
.. slug: alternativepublikationsmodelle
.. date: 2018-01-17 08:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Das Jahr 2018 startet mit einer positiven Nachricht: auch wenn die Anzahl der Einrichtungen ohne Elsevier-Vertrag in Deutschland mit dem neuen Jahr auf über 200 gestiegen ist, gewährt der Verlag weiterhin Zugriff auf seinen Content. Das heißt, die Verhandlungen zwischen DEAL und Elsevier werden wohl weiter gehen: “We will continue our conversations in the first quarter of 2018 to find an access solution for German researchers in 2018 and a longer-term national agreement” sagt Harald Boersma, Sprecher von [Elsevier](https://www.nature.com/articles/d41586-018-00093-7).

Dennoch ist es sinnvoll, sich nach alternativen Publikationsmodellen im Gold Open Access umzuschauen. Neben dem Quasi-Standard Article/Book-Processing-Charges für eine Open-Access-Veröffentlichung zu zahlen gibt es noch andere Varianten, die von einer Veröffentlichung ohne Zahlung einer Publikationsgebühr bis zu kooperativen Modellen reichen.
<!-- TEASER_END -->

<br />
**1. Veröffentlichung ohne Zahlung einer Publikationsgebühr** <br />
Der Anbieter erhebt vom Publizierenden keine Publikationsgebühren für die Veröffentlichung seines Artikels. Neben temporären Varianten kann ein solches Angebot auch von Dauer sein, wenn zur Finanzierung der Publikationsaktivitäten eine institutionelle Infrastruktur und / oder ein anderes Finanzierungsmodell vorliegt. Z.B. wenn die Zeitschrift in akademischer Trägerschaft betrieben wird und somit der gesamte Publikationsprozess in den Händen von Vertretern einer Community und deren Einrichtungen liegt. Diese Modelle findet man häufig bei den Sozial- und Geisteswissenschaften. Die Organisation und Finanzierung solch eigenverlegerischer Modelle kann durch die wissenschaftlichen Einrichtungen (Universitäten oder Forschungseinrichtungen und deren Bibliotheken) erfolgen, aber auch durch Subventionen von Stiftungen oder staatlicher Seite.

*Beispiele:* [BieJournals](http://oa.uni-bielefeld.de/ojs.html), [heiJOURNALS](http://journals.ub.uni-heidelberg.de/index.php/ojs/index), [heiBOOKS](http://books.ub.uni-heidelberg.de/heibooks), [Emerging Infectious Diseases](https://wwwnc.cdc.gov/eid/)

<br />
**2. Veröffentlichung nach Bezahlung einer reduzierten Publikationsgebühr** <br />
Die Reduktion einer Publikationsgebühr kann zum einen dadurch zustande kommen, dass eine Gold-Open-Access-Zeitschrift durch eine Fachgesellschaft finanziert wird, deren Mitglieder einen geringeren Preis bezahlen oder durch die Organisation der APC-Zahlungen über die Institution und damit verbundene Modelle: Bei einer Pre-paid-Membership zahlen die Institutionen dem Anbieter Publikationsgebühren im Voraus und die Autor_innen können ohne eigene Zahlung Open Access publizieren. Die oft rabattierten Gebühren werden dann von der Vorausszahlung abgezogen. Die Institutionen können auch eine Jahresgebühr als Grundlage eines definierten, fixen Rabattes zahlen, der den Autor_innen auf die Publikationsgebühr pro Artikel gewährt wird. Oder sie zahlen eine Gebühr als Pauschalsumme für alle Publizierenden der Einrichtung (sogenannte 'Flat fees').

*Beispiele:* [Springer Membership](https://www.springeropen.com/about/institutional-support/membership), [Taylor&Francis Pre-Pay-Membership](http://www.tandfonline.com/openaccess/members), [Publisher Deals der Niederlande](http://openaccess.nl/en/in-the-netherlands/publisher-deals)

<br />
**3. Zahlung einer Gebühr für eine unbegrenzte Anzahl an Artikeln unter der Bedingung der Mitarbeit** <br />
Publizierende werden nach Zahlung einer einmaligen Gebühr dauerhaft 'Mitglied' bei einer Open-Access-Zeitschrift und verpflichten sich, z.B. als Reviewer am Publikationsprozess mitzuwirken. Im Gegenzug werden ihnen keine Gebühren oder aber reduzierte Gebühren für die Veröffentlichung ihrer Artikel in der betreffenden Zeitschrift berechnet. Das Modell kann auch auf Autorengruppen und Institutionen angewendet werden.

*Beispiele:* [PeerJ](https://peerj.com/), [IOP referee reward scheme](https://publishingsupport.iopscience.iop.org/questions/article-processing-charge-discount/)

<br />
**4. Konsortium zur Finanzierung der Transformation von Zeitschriften von Subskription zu Open Access** <br />
Institutionen schließen sich zu einem Konsortium zusammen, bündeln ihre Open-Access-Publikationsaktivitäten und kooperieren mit einem oder mehreren Anbietern. Ziel ist es, die Anbieter dazu zu befähigen ihr subskriptionsbasiertes Geschäftsmodell auf ein publikationskostenbasiertes oder ein kooperatives Modell umzustellen.

*Beispiele:* [SCOAP³](https://www.scoap3.de/home/), [Open Library of Humanities](https://www.openlibhums.org/site/academics/journal-applications-to-join-the-olh/), [LingOA](http://www.lingoa.eu/about/mission/), [MathOA](http://www.mathoa.org/), [PsyOA](http://psyoa.org/), [Open Access Network](http://openaccessnetwork.org/)

<br />
**5. Konsortiale bzw. kooperative Open-Access-Modelle** <br />
Institutionen schließen sich zu einem Konsortium zusammen und finanzieren kooperativ die Open-Access-Publikationen durch die Zahlung einer definierten Gebühr pro Einrichtung. Eine solche Kooperative kann Fachgesellschaften, Bibliotheken, Wissenschaftler_innen und Geldgeber zusammenbringen, um skalierbare und kosteneffiziente Open-Access-Publishing-Dienste bereitzustellen. Die Publizierenden werden bei dem Modell finanziell nicht belastet.

*Beispiele:* [Knowledge Unlatched](http://www.knowledgeunlatched.org/), [eLife](https://elifesciences.org/), [SciELO](http://scielo.org/php/index.php?lang=en), [Palaeontologia Electronica](http://palaeo-electronica.org/owner.htm)

<br />
Diese Auflistung ist bei weitem nicht vollständig, deckt aber die am stärksten verbreiteten alternativen Publikationsmodelle für Gold Open Access ab. Weitere Darstellungen findet man [hier](http://oad.simmons.edu/oadwiki/OA_journal_business_models), [hier](http://doi.org/10.2312/allianzoa.008) und [hier](https://doi.org/10.5281/zenodo.1323707).

OA2020-DE empfiehlt, sich mit den Alternativen zum Standard-APC-Modell (zahlen was der Verlag verlangt) vertraut zu machen und Mittel aus dem Bibliotheksetat dafür bereit zu stellen.

