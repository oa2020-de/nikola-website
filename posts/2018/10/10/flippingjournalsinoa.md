<!--
.. title: Open Access zum 3. - Neuer Anlauf Abo-Zeitschriften in den Open Access zu Flippen
.. slug: flipping-journals-inoa
.. date: 2018-10-10 11:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

# OA2020-DE und Knowledge Unlatched wollen in großem Stil Zeitschriften Open Access stellen

OA2020-DE, der National Open Access Kontaktpunkt, und [Knowledge Unlatched (KU)](http://knowledgeunlatched.org), die zentrale Plattform für die nachhaltige Finanzierung von Open-Access-Modellen, kooperieren bei der großflächigen Umstellung wissenschaftlicher Zeitschriften aus dem Abomodell zu Open Access. Insgesamt 50 Journals von zehn Verlagen sollen ab 2019 für jedermann kostenfrei nutzbar gemacht werden. Finanziert werden soll die Umstellung für die Dauer von drei Jahren durch ein internationales Konsortium. In Deutschland sollen dazu auch Teile von Subskriptionsmittel von Bibliotheken genutzt werden können, die durch die bislang nicht erfolgreichen DEAL-Verhandlungen mit dem Verlag Elsevier frei geworden sind.
<!-- TEASER_END -->

>„Der Wechsel zu Open Access bei Zeitschriften muss beschleunigt werden, und Bibliotheken spielen in dieser Phase eine zentrale Rolle. Gerade durch die alternative Verwendung von frei gewordenen Mitteln für die Open-Access-Transformation können Bibliotheken Open Access im großen Maßstab Realität werden lassen.“ (Dirk Pieper, Projektleiter OA2020-DE)


Zudem streben OA2020-DE und KU die Einbeziehung internationaler Konsortien und weiterer Einrichtungen als Partner an.

>„Knowledge Unlatched strebt seit seiner Gründung an, möglichst viele Buch- und Zeitschrifteninhalte Open Access zu stellen. Wir freuen uns, OA2020-DE und weitere internationale Partner beim Big Flip mit unserem Knowhow und unserer Infrastruktur unterstützen zu können.“ (Dr. Sven Fund, Managing Director von KU)

50 Zeitschriften aus allen Wissenschaftsgebieten wurden von zehn Verlagen eingereicht und werden nun durch ein Title Selection Committee ausgewählt. Folgende Verlage beteiligen sich an dem Projekt: American Institute of Physics, Berghahn, Brill, De Gruyter, Emerald, John Benjamins und weitere Verlage, deren Titel sich in der Endabstimmung befinden. Bibliotheken können ab sofort ihre finanzielle Unterstützung über Knowledge Unlatched erklären.

Interessierte Einrichtungen wenden sich bitte an Herrn Philipp Hess, Communications KU: [philipp@knowledgeunlatched.org](mailto:philipp@knowledgeunlatched.org)

[Pressemitteilung](/assets/files/PR_BigFlip_20181009_DE.pdf)

[Artikel im Buchreport](https://www.buchreport.de/2018/10/09/open-access-push-ueber-den-3-weg/?utm_source=buchreport&utm_medium=link&0=utm_campaign&1=lp-gesehen&2=utm_content&3=Open+Access%3A+Der+3.+Weg%2C+Zeitschriften+frei+zug%C3%A4nglich+machen)
