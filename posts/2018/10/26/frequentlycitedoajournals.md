<!--
.. title: OA2020-DE bietet Übersicht zu häufig zitierten Open-Access-Zeitschriften
.. slug: Liste-meistzitierte-OAZeitschriften
.. date: 2018-10-26 08:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Wir haben die [Internationale Open-Access-Woche](http://www.openaccessweek.org/) und den Artikel in der Süddeutschen Zeitung zum Thema [Predatory Publishing](https://www.sueddeutsche.de/wissen/wissenschaft-hochschulen-reagieren-auf-pseudojournale-1.4167118) zum Anlass genommen und ein neues Feature auf unserer Homepage installiert:

Ab sofort gibt es unter der Kategorie Ressourcen eine [Übersicht](/pages/frequentlycitedoajournals/) mit ca. 700 häufig zitierten Open-Access-Fachzeitschriften aus fast allen wissenschaftlichen Disziplinen. Ziel dieser Übersicht ist es, etablierte und relevante Open-Access-Zeitschriften für Forscher_innen sichtbar zu machen und ihre Auffindbarkeit zu erhöhen. Auf diese Weise unterstützen wir die Forscher_innen dabei, geeignete und einflussreiche Open-Access-Zeitschriften aus ihrer Disziplin für das Einreichen ihres Manuskripts auszuwählen.

Weiterhin schlagen wir vor, dass wissenschaftliche Bibliotheken, die ihre Wissenschaftler_innen über Open Access informieren, die Liste der "Meist-zitierten Open-Access-Zeitschriften" auf ihrer Homepage verlinken. Zum Beispiel wie folgt:

*Meist-zitierte Open-Access-Zeitschriften zusammengestellt vom Nationalen Open-Access-Kontaktpunkt OA2020-DE*<br />
*https://oa2020-de.org/pages/frequentlycitedoajournals/*


[Link zur Liste](/pages/frequentlycitedoajournals/)
