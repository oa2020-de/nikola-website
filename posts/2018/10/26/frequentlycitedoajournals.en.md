<!--
.. title: OA2020-DE provides an overview of frequently cited open access journals
.. slug: frequentlycitedoajournals
.. date: 2018-10-26 08:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

We used the [International Open Access Week](http://www.openaccessweek.org/) and the article in the Süddeutsche Zeitung on [Predatory Publishing](https://www.sueddeutsche.de/wissen/wissenschaft-hochschulen-reagieren-auf-pseudojournale-1.4167118) as an opportunity and installed a new feature on our homepage:

From now on there is an overview under the category [Resources](/en/pages/frequentlycitedoajournals/) about 700 frequently cited open access journals from nearly all scientific disciplines. The aim of this review is to make established and relevant open access journals visible to researchers and to increase their discoverability. In this way, we help researchers to select suitable and influential open access journals from their discipline for submitting their manuscript.

Furthermore, we suggest that academic libraries that inform their scientists about open access link to the list of "frequently cited open access journals" on their homepage. For example as follows:

*Frequently cited open access journals compiled by the National Contact Point Open Access OA2020-DE*<br />
*https://oa2020-de.org/en/pages/frequentlycitedoajournals/*

[Link to the list](/en/pages/frequentlycitedoajournals/)
