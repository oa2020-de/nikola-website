<!--
.. title: Open Access und Medienerwerb: Erster Transformationsworkshop des Nationalen Open-Access-Kontaktpunkts in Bielefeld
.. slug: openaccess-und-medienerwerb-erster-workshop
.. date: 2018-04-26 14:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

>„Wir brauchen eine ergänzende Zielsetzung für den bibliothekarischen Medienerwerb: es geht nicht mehr nur darum, Medien zu kaufen und in ein Regal zu stellen oder für einen Campus zu lizenzieren. Es geht zunehmend um einen Beitrag von Bibliotheken, damit wissenschaftliche Literatur überhaupt im Open Access publiziert werden kann.“

Dies ist eine zentrale Erkenntnis des OA2020-DE Transformationsworkshops für Erwerbungsleiter_innen, der am 19. und 20. April 2018 in Bielefeld stattfand. Der Workshop behandelte die für eine Open-Access-Transformation relevanten Themen "Erhebung von Kosten- und Publikationsdaten in einer Universität" sowie "alternative Publikationsmodelle für Open-Access-Zeitschriften und Open-Access-Monographien" [Agenda](/assets/files/Transformationsworkshop_UBErwerber_20180419.pdf). Die kurze Vorstellungsrunde zu Beginn zeigte, dass das Thema nicht nur für Universitäten, sondern zunehmend auch für Fachhochschulen an Relevanz gewinnt.
<!-- TEASER_END -->

**Open-Access-Datenanalysen**

Nach einem einleitenden Vortrag von Kai Geschuhn, der die Leitgedanken der von der Max Planck Digital Library initiierten Open-Access-Initiative [OA2020](https://oa2020.org) vorstellte, lag der Schwerpunkt des ersten Tages auf der Thematik Datenanalysen. Ausgehend von der im Max-Planck-[Whitepaper](http://hdl.handle.net/11858/00-001M-0000-0026-C274-7) durchgeführten Analyse des weltweiten Marktes für Publikationen in wissenschaftlichen Zeitschriften, stellten Dr. Nina Schönfelder und Dr. Niels Taubert Ansätze vor, wie die Finanzierbarkeit der Open-Access-Transformation auf nationaler und institutioneller Ebene abgeschätzt werden kann. Herr Taubert verwies dabei darauf, dass eine Open-Access-Datenanalyse für Institutionen auch organisationsspezifische Indikatoren (z. B. Open-Acess-Infrastrukturen und Fächerspektren) berücksichtigen sollte. Ausgehend von dem Versuch, die [Pay-it-forward-Studie](http://icis.ucdavis.edu/wp-content/uploads/2015/07/UC-Pay-It-Forward-Project-Final-Report.pdf) auf Deutschland anzuwenden, stellte Frau Schönfelder einen alternativen Ansatz zur Berechnung einer Open-Access-Transformation der Erwerbungsetats [vor](/assets/files/Schoenfelder_ Datenanalysen_Intro_CCBY.pdf). Dazu entwickelte sie mit Hilfe einer multivariaten Regressionsanalyse eine Methode zur Schätzung der künftigen Ausgaben für Open-Access-Artikelbearbeitungsgebühren (APCs). Darüber hinaus bestimmte sie Faktoren, die die Höhe von APCs beeinflussen.

In den an die Vorträge anschließenden Arbeitsgruppen wurden dann folgende Fragestellungen diskutiert:

* Wie können in meiner Einrichtung die nötigen Kosteninformationen für Subskriptionen und Publikationen erhoben werden?
* Gibt es dafür einen Workflow? Wenn nicht, wie könnte dieser aussehen?
* Mit welchen Personen muss ich dafür zusammenarbeiten?

<img style="display:auto; margin:0 20px" src="/images/Gruppenarbeit1_TransfWS.jpg" alt="Ergebnisse Gruppe 1" width="240" height="400" />
<img style="display:auto; margin:20px 20px" src="/images/Gruppenarbeit2a_TransfWS.jpg" alt="Ergebnisse A Gruppe 2" width="240" height="400" />
<img style="display:auto; margin:20px 20px" src="/images/Gruppenarbeit2b_TransfWS.jpg" alt="Ergebnisse B Gruppe 2" width="240" height="400" />
<img style="display:auto; margin:20px 20px" src="/images/Gruppenarbeit3_TransfWS.jpg" alt="Ergebnisse Gruppe 3" width="240" height="400" />
<img style="display:auto; margin:20px 20px" src="/images/Gruppenarbeit4a_TransfWS.jpg" alt="Ergebnisse A Gruppe 4" width="240" height="400" />
<img style="display:auto; margin:20px 20px" src="/images/Gruppenarbeit4b_TransfWS.jpg" alt="Ergebnisse B Gruppe 4" width="240" height="400" />

**Open-Access-Investitionsmöglichkeiten**

Der zweite Tag stand dann ganz im Zeichen von Open-Access-Investitionsmöglichkeiten für Hochschulbibliotheken. Dr. Anja Oberländer von der Universität Konstanz  [präsentierte](/assets/files/Oberländer_Alternative Publikationsmodelle für Open-Access-Zeitschriften_CCBY.pdf) verschiedene Publikationsinitiativen im Zeitschriftenbereich, u. a. die [Fair Open Access Alliance](https://www.fairopenaccess.org/) und die [Open Library of Humanities](https://www.openlibhums.org/). Mit Hilfe eines BMBF-Projekts will die Universität Konstanz den Umstieg deutschsprachiger Subskriptionszeitschriften auf das OLH-Modell fördern und die Beteiligung deutscher Institutionen daran stärken. <br />
Dr. Karin Werner und Stefanie Hanneken vom transcript Verlag stellten ein mit dem Nationalen Open-Access-Kontaktpunkt entwickeltes Modell für die Transformation von eBooks in den Open Access [vor](/assets/files/Werner&HannekenOpenLibraryPolwi_CCBYNCND.pdf). Ziel des Modells ist es, mit Hilfe eines Konsortiums aus Hochschulbibliotheken und Fachinformationsdiensten, fachbezogene Neuerscheinungen direkt im Open Access erscheinen zu lassen. Bibliotheken werden gemeinsam mit Autor_innen, Verlagen und Fachcommunities sichtbarer Teil eines Co-Publishing-Netzwerks. Zum Abschluss des Workshops stellte Dirk Pieper ein weiteres Konzept des Nationalen Open-Access-Kontaktpunkts [vor](/assets/files/Pieper_pilotcopernicus_workshop_20180420_CCBY.pdf), das die Verstetigung des kostenpflichtigen Open-Access-Publizierens in wissenschaftlichen Zeitschriften genuiner Open-Access-Verlage zum Ziel hat. Hier hatten die Teilnehmenden des Workshops Gelegenheit, ihre Vorstellungen und Anregungen zur Bildung einer solchen Konsortialstruktur einzubringen und zu diskutieren.

In der Abschlussdiskussion zum gesamten Workshop wurden viele der angesprochenen Punkte noch einmal aufgegriffen und der alltäglichen Praxis in den Erwerbungsabteilungen gegenüber gestellt.

<img style="display:auto; margin:0 20px" src="/images/Abschlussdiskussion1_TransfWS.jpg" alt="Abschlussdiskussion Seite 1" width="240" height="400" />
<img style="display:auto; margin:20px auto" src="/images/Abschlussdiskussion2_TransfWS.jpg" alt="Abschlussdiskussion Seite 2" width="240" height="400" />


**Gewonnene Erkenntnisse**

So wie das wissenschaftliche Publikationssystem in Bewegung ist, sind es auch die Bibliotheken aufgrund ihres Auftrages einer adäquaten Literaturversorgung. Forschungsförderer und Wissenschaftspolitik haben sich eindeutig pro Open Access positioniert, Literaturversorgung findet zunehmend auch außerhalb von Bibliotheken statt. Für Hochschulbibliotheken ist das nicht nur eine Bedrohung, vielmehr ergeben sich dadurch auch neue Chancen. Der Fokus verschiebt sich dabei immer mehr in die Richtung, dass Erwerbungsetats für die Finanzierung von Open-Access-Publikationen eingesetzt werden. Bibliotheken tragen damit aktiv zur Entstehung von Publikationen bei und gewinnen ein neue, sichtbare Rolle im wissenschaftlichen Publikationssystem. So können die eingerichteten Open-Access-Publikationsfonds nicht nur für Artikelbearbeitungsgebühren in den Naturwissenschaften, sondern auch für die Förderung von Monografien, das Publikationsverhalten in den Geistes- und Sozialwissenschaften oder Open-Access-Infrastrukturen verwendet werden. Der Nationale Open-Access-Kontaktpunkt OA2020-DE begleitet die wissenschaftlichen Bibliotheken dabei und entwickelt zusammen mit ihnen, den Verlagen und anderen Akteuren des Publikationssystems weitere Konzepte und Modelle zur Open-Access-Transformation.
