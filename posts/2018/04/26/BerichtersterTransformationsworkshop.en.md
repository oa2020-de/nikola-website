<!--
.. title: Open access and media acquisition: First transformation workshop of the National Contact Point Open Access in Bielefeld
.. slug: openaccess-and-aquisition-first-workshop
.. date: 2018-04-26 14:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

>"We need a complementary objective for the library media acquisition: it is no longer just about buying media and putting it on a shelf or licensing for a campus. It is increasingly a contribution from libraries, so that scientific literature can even be published in open access."

This is a key finding of the OA2020-DE transformation workshop for acquisition librarians, which took place on 19 and 20 April 2018 in Bielefeld. The workshop covered topics relevant to the open access transformation: "collecting cost and publication data at a university" and "alternative publication models for open access journals and open access monographs" [Agenda](/assets/files/Transformationsworkshop_UBErwerber_20180419.pdf)). The short introduction round at the beginning showed that the topic is gaining relevance not only for universities, but increasingly also for universities of applied sciences.
<!-- TEASER_END -->

**Open access data analysis**

After an introductory lecture by Kai Geschuhn, who presented the main ideas of the open access initiative [OA2020](https://oa2020.org) initiated by the Max Planck Digital Library, the first day focused on the topic of data analysis. Based on the analysis of the worldwide market for publications in scientific journals conducted in the Max Planck [White Paper](http://hdl.handle.net/11858/00-001M-0000-0026-C274-7), Dr. Nina Schönfelder and Dr. Niels Taubert approaches how the financial viability of an open access transformation can be estimated at the national and institutional level. Mr Taubert pointed out that open access data analysis for institutions should also take into account organization-specific indicators (eg open access infrastructures and subject classification). Starting from the attempt to apply the [pay-it-forward](http://icis.ucdavis.edu/wp-content/uploads/2015/07/UC-Pay-It-Forward-Project-Final-Report.pdf) study to Germany, Ms Schönfelder [presented](/assets/files/Schoenfelder_ Datenanalysen_Intro_CCBY.pdf) an alternative approach to the calculation of an open access transformation of acquisition budgets. To this end, she used a multivariate regression analysis to develop a methodology for estimating future expenses for open access article processing charges (APCs). In addition, she determined factors that affect the level of APCs.

In the subsequent working groups the following questions were discussed:

* How can the necessary cost information for subscriptions and publications be collected in my institution?
* Is there a workflow for it? If not, how could this look like?
* With which persons do I have to work for it?

<img style="display:auto; margin:0 20px" src="/images/Gruppenarbeit1_TransfWS.jpg" alt="Results working group 1" width="240" height="400" />
<img style="display:auto; margin:20px 20px" src="/images/Gruppenarbeit2a_TransfWS.jpg" alt="Results A working group 2" width="240" height="400" />
<img style="display:auto; margin:20px 20px" src="/images/Gruppenarbeit2b_TransfWS.jpg" alt="Results B working group 2" width="240" height="400" />
<img style="display:auto; margin:20px 20px" src="/images/Gruppenarbeit3_TransfWS.jpg" alt="Results working group 3" width="240" height="400" />
<img style="display:auto; margin:20px 20px" src="/images/Gruppenarbeit4a_TransfWS.jpg" alt="Results A working group 4" width="240" height="400" />
<img style="display:auto; margin:20px 20px" src="/images/Gruppenarbeit4b_TransfWS.jpg" alt="Results B working group 4" width="240" height="400" />


**Open Access investment opportunities**

The second day was dedicated to concrete open access investment opportunities for university libraries. Dr. Anja Oberländer from the University of Konstanz [presented](/assets/files/Oberländer_Alternative Publikationsmodelle für Open-Access-Zeitschriften_CCBY.pdf) various publication initiatives in the field of journals, i.a. the [Fair Open Access Alliance](https://www.fairopenaccess.org/) and the [Open Library of Humanities](https://www.openlibhums.org/). With the help of a BMBF project, the University of Konstanz wants to promote the conversion of German-language subscription journals to the OLH model and to strengthen the involvement of German institutions. <br />
Dr. Karin Werner and Stefanie Hanneken from transcript Verlag [presented](/assets/files/Werner&HannekenOpenLibraryPolwi_CCBYNCND.pdf) a model for the transformation of eBooks into Open Access developed with the National Contact Point Open Access OA2020-DE. The aim of the model is to use a consortium of university libraries and FIDs ("Fachinformationsdienste") to publish subject-related new publications directly in open access. Libraries, together with authors, publishers, and subject communities, become visible parts of a co-publishing network.
At the end of the workshop, Dirk Pieper [presented](/assets/files/Pieper_pilotcopernicus_workshop_20180420_CCBY.pdf) another concept of the National Contact Point Open Access, which aims to consolidate fee-based open access publishing in scientific journals of genuine open access publishers. Here the participants of the workshop had the opportunity to present and discuss their ideas and suggestions for the formation of such a consortium structure.

In the concluding discussion of the entire workshop, many of the points raised were taken up again and contrasted with everyday practice in the acquisition departments.

<img style="display:auto; margin:0 20px" src="/images/Abschlussdiskussion1_TransfWS.jpg" alt="concluding discussion 1" width="240" height="400" />
<img style="display:auto; margin:20px auto" src="/images/Abschlussdiskussion2_TransfWS.jpg" alt="concluding discussion 2" width="240" height="400" />


**Key lessons learned**

Just as the scientific publication system is in motion, so too are the libraries on account of their task of providing an adequate supply of literature. Research funding and science policy have clearly positioned themselves for open access, literature supply is increasingly taking place outside of libraries. For university libraries, this is not only a threat, it also creates new opportunities. The focus shifts more and more in the direction that acquisition budgets are used to finance open access publications. Libraries contribute actively to the emergence of publications and gain a new, visible role in the scientific publishing system. Thus, the established open access publication funds can be used not only for article processing charges in the natural sciences, but also for the promotion of monographs, the publication behavior of the humanities and social sciences or open access infrastructures. The National Contact Point Open Access OA2020-DE accompanies the scientific libraries and develops together with them, the publishers and other actors of the publication system further concepts and models for open access transformation.
