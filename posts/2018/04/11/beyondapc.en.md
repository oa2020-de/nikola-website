<!--
.. title: Beyond APCs - OpenAIRE workshop on alternative open access publishing business models
.. slug: beyondapcs-openaire-workshop
.. date: 2018-04-11 16:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

OpenAIRE, an EC-funded initiative to support the Open Access policy of the European Commission via a technical infrastructure, aims also to fund and foster the development of non-APC based publishing models. For this they made two calls, the first in 2016, where projects received funding for the development and implementation of technical improvements in open science infrastructures (the full report can be found [here](https://docs.google.com/document/d/1KxwqODTTqLbh2rp4WTm0MMZ32lDjGMhxezd25Mw0gsk/edit)). The second call last year focused on supporting initiatives with alternative open access business models.

To present different aspects of non-APC models and the mid-term reports of the bids from the second call, OpenAIRE organised a [workshop](https://www.openaire.eu/beyond-apcs-alternative-open-access-publishing-business-models) on April 5th and 6th 2018 at the Royal Library in The Hague.
<!-- TEASER_END -->

**Six initiatives are funded**

The bids supported by the 2nd call for alternative funding mechanisms are SciPost, IBL PAN, FOAA, Language Science Press, Mattering Press and Open Library of Humanities.

+ [SciPost](https://scipost.org/) is a complete scientific publishing portal, running by professional scientists, for journals, commentaries and theses links. They try to reform the whole scientific publishing system with open access and new forms of peer review. Their financial model is based on (inter)national funding agencies, universities & libraries, government, foundations and also benefactors, and they get donations and financial support of affiliated institutions.
+ [IBL PAN](http://ibl.waw.pl/) - The Institute of Literary Research of the Polish Academy of Sciences will design a sustainable open access business model for their journals, starting with a pilot for the most prominent journal in their portfolio ‘Teksty Drugie’. They cooperate with [OPERAS](https://operas.hypotheses.org/) to implement the OpenEdition revues.org platform.
+ [FOAA](https://fairoa.org/) - Fair Open Access Alliance works on an advisory level, so they accompany and support subscription based journals who want to flip to an open access model without APC. They try to find funding for the first 3 years - after this the (HSS)journals go to Open Library of Humanities as a sustainable publishing platform.
+ [Language Science Press](http://langsci-press.org/) works on a manual for community-based open access publishing initiatives, together with the business data and business model – in order to provide an evidence-based business model canvas for start-up initiatives in linguistics and other disciplines. You can get an idea of this on [Github](https://github.com/langsci/opendata).
+ [Mattering Press](https://www.matteringpress.org/) is a consortium of 6 scholar-led open access book publisher and investigates how to transition away from charging book processing charges by developing a single platform for open access books that will experiment with alternative means of funding. Their focus is a pool of ressources and sharing knowledge.
+ [Open Library of Humanities](https://www.openlibhums.org/) wants to solidify and scale-up their business model, in order to ensure long-term sustainability by increasing the number of libraries and consortia participating. So they use their bid especially for marketing activities.

**Workshop objectives**

The objective of the OpenAIRE Workshop was to discuss on the non-fee-based open access publishing models as well as outreach and dissemination/communication aspects, so the focus for the first day was on scalability and sustainability. The second day was reserved for the mid-term reports of the bids and the discussion of lessons learned within the group.

The bids should also get the chance to discuss their business model with a tool called "Canvas for Alternative Open Access Publishing Business Models" (based on this [model](https://strategyzer.com/canvas/business-model-canvas)).
This is how it looks like in original:
<img style="display:block; margin:0 auto" src="/images/Business_Model_Canvas.png" alt="Business model Canvas" width="640" height="453" />
*By Business Model Alchemist - http://www.businessmodelalchemist.com/tools, CC BY-SA 1.0, https://commons.wikimedia.org/w/index.php?curid=11892574*

After the discussion:
<img style="display:block; margin:0 auto" src="/images/Canvas-example-768x1024.jpg" alt="Example of a Canvas for Alternative Open Access Publishing Business Models" width="450" height="600" />
*Photo by Sofie Wennström*

**Key lessons learned**

 I have learned from this workshop that a large share in these publication initiatives depends on volunteering and individual people. Therefore, the relevant keywords are: **adaptability**, to meet the requirements of all stakeholders in the field of science communication, and **collaboration**. All initiatives have different business models, but what they all have in common is that they do not need any author fees to cover all the costs and to be sustainable organisations. Which may be related to the lack of competition between the models. The National Contact Point Open Access OA2020-DE will continue to follow the developments and develop their own ideas and approaches.
