<!--
.. title: Beyond APCs - OpenAIRE Workshop über alternative Publikationsmodelle
.. slug: beyondapcs-openaire-workshop
.. date: 2018-04-11 16:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

OpenAIRE ist eine von der Europäischen Kommission geförderte Initiative, die die Open Access Policy der EU durch die Entwicklung und Bereitstellung technischer Infrastruktur unterstützen soll. Ein weitere Aufgabe ist die Unterstützung nicht-APC-basierter Publikationsmodelle. Innerhalb von OpenAIRE (und dem Nachfolger OpenAIRE Advance) gibt es dazu verschiedene Projekte und Calls. Im ersten CAll 2016 erhielten 11 Projekte eine Förderung für die Entwicklung und Implementierung technischer Verbesserungen in Open-Science-Infrastrukturen (den vollständigen Bericht dazu gibt es [hier](https://docs.google.com/document/d/1KxwqODTTqLbh2rp4WTm0MMZ32lDjGMhxezd25Mw0gsk/edit)). Der zweite Call letztes Jahr fokussierte sich dann auf die Unterstützung alternativer Open-Access-Publikationsmodelle.

Um verschiedene Aspekte nicht-APC-basierter Modelle zu präsentieren und den Fördermittelempfänger_innen des 2. Calls die Möglichkeit für einen Zwischenbericht zu geben, organisierte OpenAIRE einen [Workshop](https://www.openaire.eu/beyond-apcs-alternative-open-access-publishing-business-models) an der Niederländischen Koninklijke Bibliotheek in Den Haag vom 05.-06. April 2018.
<!-- TEASER_END -->

**Die 6 geförderten Initiativen**

Die Fördermittelempfänger_innen aus dem 2. Call für alternative Finanzierungsmechanismen sind SciPost, IBL PAN, FOAA, Language Science Press, Mattering Press und die Open Library of Humanities.

+ [SciPost](https://scipost.org/) ist eine Publikationsportal für wissenschaftliche Zeitschriften, Kommentare und verlinkte Thesen/Abschlussarbeiten, betrieben von Wissenschaftler_innen selbst. Sie wollen das komplette wissenschaftliche Publikationssystem mit neuen Formen von Open Access und Peer-Review-Modellen revolutionieren. Ihr finanzielles Modell basiert v.a. auf Förderorganisationen, Universitäten und deren Bibliotheken, staatlicher Förderung, Stiftungen und Wohltäter_innen, sowie finanzieller Unterstützung durch die Einrichtungen der beteiligten Wissenschaftler_innen.
+ [IBL PAN](http://ibl.waw.pl/) - Das Institute of Literary Research of the Polish Academy of Sciences entwickelt ein nachhaltiges Businessmodelle für die Zeitschriften, die es herausgibt, angefangen bei ihrer prominentesten "Teksty Drugie". Sie kooperieren mit [OPERAS](https://operas.hypotheses.org/), um dafür die OpenEdition Plattform revues.org zu implementieren.
+ [FOAA](https://fairoa.org/) - Die Fair Open Access Alliance arbeitet in erster Linie als Beraterin und Begleiterin von subskriptionsbasierten Zeitschriften, die in den Open Access flippen wollen. Dabei versucht sie für diese Zeitschrift eine Förderung für die ersten 3 Jahre zu organisieren, in der Zeit sollen diese ein eigenes Modell entwickeln und wenn sie zu den Geisteswissenschaften gehören, können sie im Anschluss zur Open Library of Humanities als Publikationsplattform wechseln.
+ [Language Science Press](http://langsci-press.org/) arbeitet an einem Handbuch für community-basierte Open-Access-Publikationsinitiativen. Zusammen mit den Geschäftsdaten und dem Businessmodell soll damit ein evidenzbasiertes Businessmodell-Canvas für Start-Up-Initiativen in der Linguistik und anderen Disziplinen entstehen. Einen Blick darauf kann man bei Github werfen: [https://github.com/langsci/opendata](https://github.com/langsci/opendata).
+ [Mattering Press](https://www.matteringpress.org/) ist eine Konsortium von 6 Wissenschaftler_innen-geführten Open-Access-Buchverlagen und untersucht, wie man von den Book Processing Charges wegkommen kann, z. B. durch die Entwicklung einer einzelnen Plattform für Open-Access-Bücher, die mit alternativen Finanzierungsmechanismen experimentiert. Ihr Fokus liegt auf der gemeinsamen Nutzung von Ressourcen und dem Teilen von Wissen und Erfahrungen.
+ Die [Open Library of Humanities](https://www.openlibhums.org/) möchte ihr bestehendes Businessmodell festigen und ausbauen, um eine dauerhafte Nachhaltigkeit durch eine wachsende Menge an teilnehmenden Bibliotheken und Konsortien zu sichern. Daher verwenden sie ihre Förderung hauptsächlich für Marketingaktivitäten.

**Workshopziele**

Das Ziel des OpenAIRE-Workshops war zum einen die Diskussion der nicht-APC-basierten Open-Access-Publikationsmodelle ebenso wie Aspekte der Reichweite und Verbreitung/Kommunikation. Daher lag der Fokus am ersten Tag auf Nachhaltigkeit und Skalierbarkeit. Zum anderen wurden am zweiten Tag die Zwischenstandsberichte der Fördermittelempfänger_innen präsentiert und die ersten Ergebisse innerhalb der Gruppe diskutiert.

Außerdem sollten die Projekte die Möglichkeit haben, ihre Businessmodelle mit Hilfe des Tools "Canvas for Alternative Open Access Publishing Business Models" (basierend auf diesem [Modell](https://strategyzer.com/canvas/business-model-canvas)) mit den Teilnehmer_innen zu diskutieren.

Im Original sieht das Canvas so aus:
<img style="display:block; margin:0 auto" src="/images/Business_Model_Canvas.png" alt="Business model Canvas" width="640" height="453" />
*By Business Model Alchemist - http://www.businessmodelalchemist.com/tools, CC BY-SA 1.0, https://commons.wikimedia.org/w/index.php?curid=11892574*

Am Ende des ersten Tages lagen dann für alle Projekte ausgefüllte Canvas vor:
<img style="display:block; margin:0 auto" src="/images/Canvas-example-768x1024.jpg" alt="Example of a Canvas for Alternative Open Access Publishing Business Models" width="450" height="600" />
*Photo by Sofie Wennström*

**Gewonnene Erkenntnisse**

Mitgenommen habe ich von diesem Workshop vor allem, dass sehr viel in diesen Publikationsinitiativen von Freiwilligenarbeit und einzelnen Personen abhängt. Daher sind die relevanten Stichworte: **Adaptierbarkeit**, um den Bedürfnissen der Beteiligten im Bereich Wissenschaftskommunikation entgegen zu kommen, und **Kollaboration**. Alle Initiativen haben verschiedene Businessmodelle, aber allen gemeinsam ist, dass sie keine Autorengebühren benötigen, um die Kosten zu tragen und sich nachhaltig zu organisieren. Was möglicherweise mit dem fehlenden Wettbewerbsgedanken zwischen den Modellen zusammenhängt. Wir vom Nationalen Open-Access-Kontaktpunkt werden die Entwicklungen weiter verfolgen und auch eigene Ideen und Ansätze entwickeln.



