<!--
.. title: Zweiter Open-Access-Transformationsworkshop des Nationalen Open-Access-Kontaktpunkts in Bielefeld
.. slug: 2oatransformationsworkshop
.. date: 2018-11-20 08:00:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

# Workshop zur Open-Access-Transformation rückt Rahmenbedingungen in den Fokus

Am 17. und 18. Oktober 2018 fand zum zweiten Mal ein von OA2020-DE organisierter Open-Access-Transformationsworkshop statt. Ziele dieses Workshops waren die Vernetzung der Unterzeichner_innen der „Expression of Interest“ der OA2020-Initiative, die Gewinnung neuer Unterstützer_innen sowie ein Austausch über konkrete Aktivitäten zur Umsetzung der Open-Access-Transformation. Die Teilnehmer_innen setzten sich diesmal vor allem aus Open-Access-Beauftragten und Bibliotheksleiter_innen zusammen. Der erste Tag beschäftigte sich schwerpunktmäßig mit Strategien zur Open-Access-Transformation auf nationaler und internationaler Ebene, der zweite Tag beleuchtete konkrete Transformationsprozesse an außeruniversitären Einrichtungen und Universitätsbibliotheken ([Agenda](/assets/files/OA2020DE_Agenda_Workshop20181017-18.pdf)).
<!-- TEASER_END -->


**Open-Access-Strategien**

Dr. Saskia de Vries von der [Fair Open Access Alliance](https://www.fairopenaccess.org/) zeigte in ihrem [Vortrag](/assets/files/2y0181017_OA2020DE_Workshop_TTOA_SaskiadeVries.pdf) auf, unter welchen Rahmenbedingungen ein transparenter Übergang zu Open Access unter Einbeziehung aller Stakeholder und PlanS-konform möglich ist (TTOA - A Transparent Transition to Open Access). Dazu gehört zum einen, dass Verlage ihre direkten und indirekten Kosten offen legen, sodass Transparenz für Biblioheken und Forschungsförderer entsteht. Zum anderen soll eine gemeinsame Plattform für alle an TTOA beteiligten Verlage aufgebaut werden, über die das Einreichen, Begutachten, Publizieren und Archivieren wissenschaftlicher Zeitschriftenartikel transparent und nachhaltig organisiert wird.
Daran anschließend stellte Dr. Ralf Schimmer die aktuellen Bemühungen der [Internationalen Open Access 2020 Initiative](/assets/files/20181017_OA2020DE_Workshop_Strategiender OA2020-Initiativeiminternationalen Kontext_Schimmer.pptx) vor. Kern der Aktivitäten ist die Erkenntnis, dass viel Macht in den Erwerbungsentscheidungen der einzelnen Einrichtungen und Konsortien liegt, die genutzt werden muss (Open Access als natürliche Evolutionsform der bisherigen Lizenzen).

<img style="display:auto; margin:0 20px" src="/images/20181017_OA2020DE_Workshop_trendoapublishing.png" alt="Trend beim Open-Access-Publizieren" width="300" height="200" />

Der neue Ansatz, um Open Access zum Standard zu machen, läuft über das Geld, dass schon im Publikationssystem vorhanden ist und über die Umstellung von (subskriptionsbasierten) Zeitschriften und Finanzströme. Einen entsprechenden Ansatz verfolgt das [DEAL-Projekt](https://www.projekt-deal.de/) der Allianz der deutschen Wissenschaftsorganisationen: Durch das Verhandeln von Publish & Read-Modellen wird die Open-Access-Transformation in Deutschland aktiv voran getrieben. Hier berichtete (wiederum) Herr Dr. Schimmer über den aktuellen [Verhandlungsstand](/assets/files/20181017_OA2020DE_Workshop_DEAL_Schimmer.pdf).

<img style="display:auto; margin:20px 20px" src="/images/20181017_OA2020DE_Workshop_howtogetpdf.png" alt="Aufstehen vom Verhandlungstisch" width="300" height="200" />

Zum Abschluss des ersten Tages fasste Herr Dirk Pieper das bisherige zusammen und stellte zusätzlich [Modelle](/assets/files/20181017_OA2020DE_Workshop_OA2020DE_Pieper.pdf) des Nationalen Open-Access-Kontaktpunkts zur Open-Access-Transformation auf nationaler Ebene vor. Neben dem Pilotprojekt [transcript OPEN Library Politikwissenschaft](/pages/transcriptopenlibrarypowi/), das das Gold-Open-Access-Publizieren von Monographien zum Ziel hat, gab er einen Einblick in das zusammen mit Knowledge Unlatched entwickelte [Modell zum Flipping](/pages/KUjournalflipping/) von subskriptionsbasierten oder hybriden Zeitschriften in den Open Access.



**Open Access in wissenschaftlichen Einrichtungen**

Der zweite Workshop-Tag widmete sich ganz dem Thema Umsetzung der Open-Access-Transformation an außeruniversitären Forschungseinrichtungen und Universitätsbibliotheken. Vertreter_innnen der [Fraunhofer-Gesellschaft](/assets/files/20101818_OA2020DE_Workshop_Fraunhofer_Seeh.pdf), der [Leibniz-Gemeinschaft](/assets/files/20181018_OA2020DE_Workshop_Leibniz_Siegert.pdf), der [Max-Planck-Gesellschaft](/assets/files/20181018_OA2020DE_Workshop_MPG_Schimmer.pdf), der [Helmholtz-Gemeinschaft](http://gfzpublic.gfz-potsdam.de/pubman/item/escidoc:3613889) sowie der [SUB Göttingen](/assets/files/20181018_OA2020DE_Workshop_OATransformationSUB Göttingen_Horstmann.pdf) stellten die verschiedenen Ansätze in ihren jeweiligen Forschungsgemeinschaften und Einrichtungen vor. Alle Vortragenden betonten die Notwendigkeit von institutionellen Publikationsdatenanalysen und einer möglichst einheitlichen Steuerung der relevanten Etats/Kostenstellen als wichtige Instrumente zur institutionellen Umsetzung der Open-Access-Transformation.
Prof. Dr. Wolfram Horstmann von der SUB Göttingen betonte zudem, dass lokales und internationales Handeln Hand in Hand gehen müssen, um eine großflächige Open-Access-Transformation Realität werden zu lassen.



**Gewonnene Erkenntnisse**

Damit die Open-Access-Transformation in wissenschaftlichen Einrichtungen effizient umgesetzt und gesteuert werden kann, sollten bestimmte organisatorische Rahmenbedingungen erfüllt sein. Im Einzelnen sind das insbesondere:

+ der Betrieb von Open-Access-relevanten Infrastrukturen (Repositorien, Publikationsplattformen wie z.B. OJS),
+ institutionelle Publikationsdaten- und Kostenanalysen,
+ eine möglichst ganzheitliche Steuerung der relevanten Etats/Kostenstellen (Erwerbungsetats, Publikationsfonds etc.),
+ die funktional-organisatorische Verankerung der Open-Access-Transformation in den einzelnen Einrichtungen, z.B. durch die Einrichtung abteilungsübergreifender Teamstrukturen.

Die Workshop-Teilnehmer_innen konnten durch die gewonnenen Erkenntnisse konkrete Strategien und Möglichkeiten zur Umsetzung der Open-Access-Transformation in ihre Einrichtungen zurück tragen. Dazu gehört auch das Unterzeichnen der [„Expression of Interest“](https://openaccess.mpg.de/2172617/Expression-of-Interest).







