<!--
.. title: Second OA2020-DE transformation workshop at University Bielefeld
.. slug: 2oatransformationsworkshop
.. date: 2018-11-20 08:00:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

#  Workshop on open-access transformation focuses on framework conditions

On 17 and 18 October 2018, an open-access transformation workshop organised by OA2020-DE took place for the second time. The aims of this workshop were to network the signatories of the "Expression of Interest" of the OA2020 initiative, to win new supporters and to exchange information on concrete activities to implement the open-access transformation. This time, the participants mainly consisted of open-access officers and library directors. The first day focused on strategies for open-access transformation at national and international level, while the second day focused on concrete transformation processes at research institutions and university libraries ([Agenda](/assets/files/OA2020DE_Agenda_Workshop20181017-18_en.pdf)).
<!-- TEASER_END -->

**Open-access strategies**

In her [presentation](/assets/files/2y0181017_OA2020DE_Workshop_TTOA_SaskiadeVries.pdf), Dr. Saskia de Vries from the [Fair Open Access Alliance](https://www.fairopenaccess.org/) pointed out the framework conditions under which a transparent transition to open access is possible with the involvement of all stakeholders and in compliance with PlanS (TTOA - A Transparent Transition to Open Access). On the one hand, this means that publishers must disclose their direct and indirect costs so that transparency is created for libraries and research funding bodies. On the other hand, a common platform is to be established for all publishers involved in TTOA, through which the submission, review, publication and archiving of scientific journal articles can be organised in a transparent and sustainable manner.
Dr. Ralf Schimmer then presented the current efforts of the [International Open Access 2020 Initiative](/assets/files/20181017_OA2020DE_Workshop_Strategiender OA2020-Initiativeiminternationalen Kontext_Schimmer.pptx). The core of the activities is the recognition that a lot of power lies in the acquisition decisions of the individual institutions and consortia, which must be used (Open Access as a natural evolutionary form of the previous licenses).

<img style="display:auto; margin:0 20px" src="/images/20181017_OA2020DE_Workshop_trendoapublishing.png" alt="Trend beim Open-Access-Publizieren" width="300" height="200" />

The new approach to making open access the standard involves the money already available in the publication system and the conversion of (subscription-based) journals and financial flows. The [DEAL project](https://www.projekt-deal.de/) of the Alliance of Science Organisations in Germany is pursuing a similar approach: Through the negotiation of publish & read models, open-access transformation is actively promoted in Germany. Here (again) Dr. Schimmer reported on the current state of [negotiations](/assets/files/20181017_OA2020DE_Workshop_DEAL_Schimmer.pdf).

<img style="display:auto; margin:20px 20px" src="/images/20181017_OA2020DE_Workshop_howtogetpdf.png" alt="Aufstehen vom Verhandlungstisch" width="300" height="200" />

At the end of the first day, Mr. Dirk Pieper summarized the previous situation and additionally presented [models](/assets/files/20181017_OA2020DE_Workshop_OA2020DE_Pieper.pdf) of the National Contact Point Open Access for the open-access transformation at the national level. In addition to the pilot project [transcript OPEN Library Political Science](/en/pages/transcriptopenlibrarypols/), which aims to promote the gold open access publishing of monographs, he also provided insight into the [model](/en/pages/KUjournalflipping/) developed together with Knowledge Unlatched for flipping subscription-based or hybrid journals into open access.


**Open access in research institutions**

The second day of the workshop was devoted entirely to the topic of implementing the open-access transformation at non-university research institutions and university libraries. Representatives of the [Fraunhofer Society](/assets/files/20101818_OA2020DE_Workshop_Fraunhofer_Seeh.pdf), the [Leibniz Association](/assets/files/20181018_OA2020DE_Workshop_Leibniz_Siegert.pdf), the [Max Planck Society](/assets/files/20181018_OA2020DE_Workshop_MPG_Schimmer.pdf), the [Helmholtz Association](http://gfzpublic.gfz-potsdam.de/pubman/item/escidoc:3613889) and the [SUB Göttingen](/assets/files/20181018_OA2020DE_Workshop_OATransformationSUB Göttingen_Horstmann.pdf) presented the various approaches in their respective research communities and institutions. All speakers emphasised the need for institutional publication data analysis and the most uniform possible control of the relevant budget/cost centres as important instruments for the institutional implementation of open-access transformation.
Prof. Dr. Wolfram Horstmann from the SUB Göttingen also stressed that local and international action must go hand in hand in order to make large-scale open-access transformation a reality.


**Lessons learned**

In order for the open-access transformation in scientific institutions to be implemented and managed efficiently, certain organizational framework conditions should be fulfilled. In particular, these are:

+ the operation of infrastructures relevant to open access (repositories, publication platforms such as OJS),
+ institutional publication data and cost analyses,
+ a holistic control of the relevant budgets / cost centers (acquisition budgets, publication funds etc.),
+ the functional and organisational anchoring of the open-access transformation in the individual institutions, e.g. by setting up cross-departmental team structures.

The workshop participants were able to use the findings gained to bring back concrete strategies and possibilities for implementing the open-access transformation to their institutions. This also includes signing the ["Expression of Interest"](https://openaccess.mpg.de/2172617/Expression-of-Interest).
