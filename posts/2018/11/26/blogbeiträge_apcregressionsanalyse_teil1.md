<!--
.. title: APCs – Spiegel des Impact-Factors oder Erbe des Subskriptionsmodells? Eine Einleitung.
.. slug: APCregressionsanalyse_einleitung
.. date: 2018-11-26 08:00:00+01:00
.. author: Nina Schönfelder
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Der Nationale Open-Access-Kontaktpunkt OA2020-DE hat einen Bericht erstellt, der ein erster Baustein zur Beantwortung der Frage ist, ob die wissenschaftspolitisch angestrebte großflächige Transformation von Fachzeitschriften in den Open Access (Journal-Flipping) für deutsche Hochschul- und Forschungseinrichtungen finanziell tragbar ist. Das zurzeit dominierende Geschäftsmodell im Bereich von Open-Access-Zeitschriften basiert auf dem Erheben von Artikelbearbeitungsgebühren (Article Processing Charges – APCs), die in der Regel den einreichenden Autor_innen in Rechnung gestellt werden. In Deutschland übernehmen u. a. DFG-geförderte Publikationsfonds diese APCs bis zu einer Höhe von 2.000 EUR. Es ist davon auszugehen, dass das Geschäftsmodell im Zuge der Open-Access-Transformation weiter an Bedeutung gewinnen wird. Daher ist es für eine finanzielle Abschätzung unerlässlich zu wissen, wie hoch die APCs nach einen umfassenden Journal-Flipping sein werden – sowohl im Durchschnitt, als auch in ihrer Verteilung. Für eine solche Prognose muss man die Faktoren kennen, die heute schon die Höhe von APCs beeinflussen. Das ist der Kern des Berichts.

Anhand des [OpenAPC-Datensatzes](https://treemaps.intact-project.org/), der im INTACT-Projekt an der Universitätsbibliothek Bielefeld entsteht, wird analysiert, was die Höhe von tatsächlich gezahlten Artikelbearbeitungsgebühren (in Gegensatz zu Listenpreisen) beeinflusst. In Frage kommen die Relevanz bzw. das Renommee der Zeitschrift gemessen an dem Zitations-Impact, die Erscheinungsform der Zeitschrift (open access oder hybrid), der Fachbereich der Zeitschrift sowie der zugehörige Verlag. Die Ergebnisse der Studie werden in den folgenden Blogbeiträgen sukzessive vorgestellt und diskutiert. Die statistisch-technischen Details werden im Bericht dokumentiert:

Schönfelder, Nina (2018). *APCs — Mirroring the impact factor or legacy of the subscription-based model?*. Universität Bielefeld. doi:[10.4119/unibi/2931061](https://dx.doi.org/10.4119/unibi/2931061)

Blogbeitrag 2 - [APCs – Spiegel des Impact-Factors oder Erbe des Subskriptionsmodells? Die Datenbasis.](/blog/2018/12/10/APCregressionsanalyse_Datenbasis/)

Blogbeitrag 3 - [APCs – Spiegel des Impact-Factors oder Erbe des Subskriptionsmodells? Beschreibende Statistiken.](/blog/2019/01/08/APCregressionsanalyse_beschreibendestatistik/)
