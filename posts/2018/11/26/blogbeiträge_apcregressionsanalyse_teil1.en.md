<!--
.. title: APCs — Mirroring the impact factor or legacy of the subscription-based model? An introduction.
.. slug: APCregressionanalysis_introduction
.. date: 2018-11-26 08:00:00+01:00
.. author: Nina Schönfelder
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

OA2020-DE prepared a report that is a first step forward to answer the question whether the large-scale open-access transformation of scientific journals is a financially viable way for German research and higher-education institutions. Usually, article processing charges (APCs) are charged to the submitting/corresponding author’s account for publishing scientific articles in open access. In Germany, the DFG-funded publication funds accept the costs for APCs up to EUR 2.000. With the ongoing open-access transformation, APCs are gaining importance as relevant business model for open-access journals. For a financial assessment, it is of utmost importance to predict the APC-levels after a comprehensive journal flipping — both the average APC and the distribution of APCs. To predict APCs, we need to know the factors determining APC-levels today. This is the core of the paper.

With data from [OpenAPC](https://treemaps.intact-project.org/), which is part of the INTACT project at the Bielefeld University Library, Germany, we analyzed the determinants for APCs actually paid (in contrast to catalogue prices). Price-determining factors could be the reputation and quality perception of a journal proxied by its citation impact, whether the journal is open access or hybrid, the publisher and the subject area of the journal. We are going to present and discuss the core results of the study in several blog posts. For the statistical and technical details, we refer to the full report.

Schönfelder, Nina (2018). *APCs — Mirroring the impact factor or legacy of the subscription-based model?*. Universität Bielefeld. doi:[10.4119/unibi/2931061](https://dx.doi.org/10.4119/unibi/2931061)

Blogpost 2 - [APCs — Mirroring the impact factor or legacy of the subscription-based model? The database.](/en/blog/2018/12/10/APCregressionsanalysis_database/)

Blogpost 3 - [APCs — Mirroring the impact factor or legacy of the subscription-based model? Descriptive statistics.](/en/blog/2019/01/08/APCregressionsanalyse_descriptivestatistics/)
