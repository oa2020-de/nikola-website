<!--
.. title: Budgetary concerns over open access
.. slug: budgetaryconcernsopenaccess
.. date: 2018-11-06 08:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## Open access and budgetary law

In discussions about the pilot projects of the National Contact Point Open Access as well as the open access transformation in general, budgetary concerns are often voiced. Central to this is the question of whether a payment to an open access infrastructure or for an open access publication, a specific consideration can be assigned. In principle, only those expenses are permitted that are necessary for the fulfillment of the tasks. The expenditure would also have to follow the principle of economy and thriftiness.

*We would like to take a stand:*
<!-- TEASER_END -->

Expenditure on the publication of an open access publication, such as APCs and BPCs, has a concrete counterpart, namely the publication of research results in open access. Major research funding agencies, such as the DFG and the European Commission, recommend their funded projects to publish the research results in suitable open access journals and provide funding for this.
There is also something in return for spending on open access infrastructures, such as the [Directory of Open Access Journals (DOAJ)](https://doaj.org/) or community models such as [Knowledge Unlatched (KU)](http://www.knowledgeunlatched.org/). They provide a service that can be used by all and on the basis of which in-house services can be built. For example, the services of the DOAJ in the scientific institutions are used to check whether an open access journal meets the criteria of the DFG-funded publication funds. Parts of the in-house processes are thus dependent on the presence of such aids.

In our opinion, the fact that open access publications and the services of open access infrastructures can be read and used by everyone without restriction is not a problem. Many public institutions produce goods or provide services that are explicit to the public, for example, highways, cultural events, public safety and not least scientific results. It is even one of the central tasks of the state and its institutions to provide goods and services that could not be realized on the market economy or only inadequately - and open access publications are such a good.

The central task of the universities is to gain scientific knowledge as well as the care and development of the sciences through research, teaching, study, promotion of young scientists and knowledge transfer (for example the law on the universities of North Rhine-Westphalia, §3). In this respect, expenditures for open access publications and open access infrastructures, if they work towards the fulfillment of the above tasks, make sense. The principle of economy and thriftiness is usually met as well, since public open access infrastructures are often much cheaper than commercial offers.

**We therefore recommend to make sure that the invoices specifically designate the services (and may be supplemented with further information, such as passing on metadata, download numbers from your own IP range or the possibility of influencing the further development of the infrastructure via voting rights). Enter into dialogue with the colleagues of financial accounting, often a conversation is enough to clarify the importance of certain existing services and their financial security.**
