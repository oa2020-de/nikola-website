<!--
.. title: Haushaltsrechtliche Bedenken in Bezug auf Open Access
.. slug: haushaltsrechtlichebedenkenopenaccess
.. date: 2018-11-06 08:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## Open Access und das Haushaltsrecht

In Diskussionen um die Pilotprojekte des Nationalen Open-Access-Kontaktpunkts wie auch der Open-Access-Transformation im Allgemeinen werden häufig Bedenken bezüglich des Haushaltsrechts geäußert. Zentral ist dabei die Frage, ob einer Zahlung an eine Open-Access-Infrastruktur oder eine Ausgabe für eine Open-Access-Publikation eine konkrete Gegenleistung zugeordnet werden kann. Grundsätzlich seien nur die Ausgaben zulässig, die zur Erfüllung der Aufgaben notwendig sind. Die Ausgaben müssten zusätzlich dem Prinzip der Wirtschaftlichkeit und Sparsamkeit folgen.

*Dazu möchten wir Stellung beziehen:*
<!-- TEASER_END -->

Ausgaben für das Erscheinen einer Open-Access-Publikation, wie APCs und BPCs, haben eine konkrete Gegenleistung, nämlich die Publikation von Forschungsergebnissen im Open Access. Große Forschungsförderer wie die DFG und die Europäische Kommission empfehlen ihren geförderten Projekten die Veröffentlichung der Forschungsergebnisse in geeigneten Open-Access-Zeitschriften und stellen hierzu Mittel bereit.
Für Ausgaben an Open-Access-Infrastrukturen, wie bspw. an das [Directory of Open Access Journals (DOAJ)](https://doaj.org/) oder an Community-Modelle wie bei [Knowledge Unlatched (KU)](http://www.knowledgeunlatched.org/), erhält man ebenfalls eine Gegenleistung. Sie erbringen eine Leistung, die von allen genutzt werden kann und auf deren Basis wiederum hauseigene Leistungen aufgebaut werden können. So werden z. B. die Leistungen des DOAJ in den wissenschaftlichen Einrichtungen genutzt, um zu überprüfen, ob eine Open-Access-Zeitschrift den Kriterien der DFG-geförderten Publikationsfonds genügt. Teile der hausinternen Prozesse sind also abhängig vom Vorhandensein solcher Hilfsmittel.

Dass Open-Access-Publikationen sowie die Dienstleistungen von Open-Access-Infrastrukturen von jedem und jeder ohne Einschränkung gelesen und genutzt werden können, stellt unseres Erachtens kein Problem dar. Viele öffentliche Einrichtungen stellen Güter her oder erbringen Dienstleistungen, die explizit für die Öffentlichkeit sind, so bspw. Autobahnen, kulturelle Veranstaltungen, öffentliche Sicherheit und nicht zuletzt wissenschaftliche Erkenntnisse. Es ist sogar eine der zentralen Aufgaben des Staates und seiner Einrichtungen Güter und Dienstleistungen bereitzustellen, die allein marktwirtschaftlich nicht oder nur unzureichend realisiert werden könnten – und Open-Access-Publikationen sind solch ein Gut.

Die zentrale Aufgabe der Universitäten ist die Gewinnung wissenschaftlicher Erkenntnisse sowie die Pflege und Entwicklung der Wissenschaften durch Forschung, Lehre, Studium, Förderung des wissenschaftlichen Nachwuchses und Wissenstransfer (so beispielsweise das Gesetz über die Hochschulen des Landes Nordrhein-Westfalen, §3). Insofern sind Ausgaben für Open-Access-Publikationen und Open-Access-Infrastrukturen, sofern sie auf die Erfüllung der oben genannten Aufgaben hinwirken, sinnvoll. Dem Prinzip der Wirtschaftlichkeit und Sparsamkeit wird in der Regel ebenso entsprochen, da öffentliche Open-Access-Infrastrukturen oft wesentlich kostengünstiger sind, als kommerzielle Angebote.

**Wir empfehlen also darauf zu achten, dass bei den Rechnungen die Dienstleistungen konkret benannt werden (und eventuell um weitergehende ergänzt werden, wie z. B. Weitergabe von Metadaten, Downloadzahlen aus dem eigenen IP-Bereich oder die Möglichkeit, über ein Stimmrecht Einfluss auf die weitere Entwicklung der Infrastruktur zu nehmen). Treten Sie in den Dialog mit den Kolleginnen und Kollegen der Finanzbuchhaltung, oft reicht ein Gespräch, um die Wichtigkeit bestimmter vorhandener Dienste und deren finanzieller Absicherung zu verdeutlichen.**
