<!--
.. title: Recommendations on supporting open-access infrastructure
.. slug: recommendations-oainfrastructure
.. date: 2018-08-21 14:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

The funding of critical and overt open access infrastructures is one of the prerequisites for a large-scale transformation of scientific publications into open access. Scientific and academic institutions and libraries are therefore called upon to "[create] new mechanisms, structures or bodies, which turn the overarching strategies and resource planning of science-supporting infrastructures beyond local preferences and institutional budget lines into reality." ([DFG](http://www.dfg.de/download/pdf/foerderung/programme/lis/180522_awbi_impulspapier.pdf) (only german))

Such critical and open open access infrastructures may include:

* Directory of Open Access Journals (DOAJ)
* Directory of Open Access Books (DOAB)
* SHERPA (Juliet/RoMEO)
* OpenDOAR
* OAPEN
* OpenAIRE
* Open-source journal management and publishing software  (such as Open Journal Systems OJS)
* and many others

Since many of these services are now, inter alia, fundamentally responsible for implementing open-access and open-science policies and supporting appropriate workflows,
their security has become a growing concern of the broad open-access and open-science community. The whole thing is supported by the Knowledge Exchange Report ["Putting down roots: Securing the future of open access policies"](http://repository.jisc.ac.uk/6269/10/final-KE-Report-V5.1-20JAN2016.pdf), in which the DOAJ and the SHERPA services are considered to be critical in terms of their sustainable funding.

<!-- TEASER_END -->

The DOAJ is a freely accessible, non-commercial database that organizes quality-assured registration of open-access scientific journals. Due to the strict quality control of the inclusion of the individual journals in the DOAJ, it is suitable as an instrument for the selection of the correct publication place for scientists and as a control system for the employees responsible for the open-access publication fund when paying for the APCs.

<img src="/images/DOAJ_Workflowsupport.png" alt="Support of the Open-access workflow through DOAJ" width="650" height="400" display="block" /><br />
*Source: Knowledge Exchange Report 2016, page 16*

The SHERPA services are also freely accessible, non-commercial databases that provide information about the copyright and self-archiving publishing guidelines and the open-access policies of the research funders. While the DOAJ has developed a membership model based on library / consortium funding and a sponsorship program targeting (open access) publishers and commercial aggregators, the SHERPA services are currently fully funded by Jisc. This entails the risk that the existing services will not be developed beyond the current level and primarily address the needs of British users. For both services, the pool of supporters must be significantly extended.

Referring to the KE Report, the Council of the Australian University Librarians ([CAUL](http://www.caul.edu.au/)), [LIBER](http://libereurope.eu/), [EIFL](http://www.eifl.net/) und [SPARC Europe](https://sparceurope.org/) founded the supporter network [SCOSS (Global Sustainability Coalition for Open Science Services)](http://www.scoss.org/) in early 2017. SCOSS’ purpose is to provide a new co-ordinated cost-sharing framework that will ultimately enable the broader open-access and open-science community to support the non-commercial services on which it depends. It will function primarily to help identify and track, via a registry, non-commercial services essential to open science, and to make qualified recommendations on which of these services should be considered for funding support. Concerning the DOAJ, SCOSS proposes that funders pay EUR 2,000 to 8,000 per year to the DOAJ over a 3-year period, depending on their size. For this, the DOAJ offers funding consortia as well as individual institutions a discount of 25%. The agreement will be concluded directly with the DOAJ.

There are many arguments in favor of overt open-access / open-science infrastructures:

* They are freely accessible and non-commercial.
* They are community-based in their design and in their funding.
* They are integrated into the scientific publication process.
* They are integrated into the implementation of open-access and open-science policies.
* They help disseminate necessary persistent identifiers and standards.
* They are easily integrated through the open approach, effective and representative and less prejudicial.
* They serve the quality assurance for open-access publications with simultaneous lowering of the administration costs for it.
* They provide an alternative to (expensive) commercial offers such as Web of Science, Scopus, etc.

We therefore encourage libraries and scientific/academic institutions interested in open-access transformation to contribute to the funding of the Directory of Open Access Journals in line with the SCOSS recommendations, thus ensuring that this non-commercial, freely accessible open-access infrastructure continues to exist.
