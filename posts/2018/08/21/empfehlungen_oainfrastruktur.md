<!--
.. title: Empfehlungen zur Open-Access-Infrastruktur-Förderung
.. slug: empfehlung-oainfrastruktur
.. date: 2018-08-21 14:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Die Förderung kritischer und offener Open-Access-Infrastrukturen gehört zu den Voraussetzungen für eine großflächige Transformation wissenschaftlicher Publikationen in den
Open Access. Wissenschaftliche Einrichtungen und Bibliotheken sind daher aufgefordert „neue Mechanismen, Strukturen oder Organe [zu schaffen], die übergreifende Strategien
und Ressourcenplanungen wissenschaftsstützender Infrastrukturen jenseits lokaler Präferenzen und institutionseigener Haushaltsgrenzen Realität werden lassen.“
([DFG](http://www.dfg.de/download/pdf/foerderung/programme/lis/180522_awbi_impulspapier.pdf))

Zu solchen kritischen und offenen Open-Access-Infrastrukturen gehören u. a.:

* Directory of Open Access Journals (DOAJ)
* Directory of Open Access Books (DOAB)
* SHERPA (Juliet/RoMEO)
* OpenDOAR
* OAPEN
* OpenAIRE
* Open-Source Zeitschriftenmanagement- und -publikationssoftware (z. B. Open Journal Systems OJS)
* uvm.

Da viele dieser Dienste mittlerweile grundlegend u. a. für die Implementierung von Open-Access- und Open-Science-Richtlinien sind und entsprechende Workflows unterstützen,
ist ihre Sicherung zu einem wachsenden Anliegen der breiten Open-Access- und Open-Science-Community geworden. Gestützt wird das Ganze durch den Knowledge Exchange Report
["Putting down roots: Securing the future of open access policies"](http://repository.jisc.ac.uk/6269/10/final-KE-Report-V5.1-20JAN2016.pdf), in welchem das DOAJ und die
SHERPA-Dienste als kritisch in Bezug auf ihre nachhaltige Finanzierung eingestuft werden.

<!-- TEASER_END -->

Das DOAJ ist eine frei zugängliche, nicht-kommerzielle Datenbank, die eine qualitätsgesicherte Registrierung von wissenschaftlichen Open-Access-Zeitschriften organisiert.
Durch die strenge Qualitätskontrolle bei der Aufnahme der einzelnen Zeitschriften in das DOAJ, eignet es sich als Instrument für die Auswahl des richtigen Publikationsortes
für Wissenschaftler_innen und als Kontrollsystem für die für den Open-Access-Publikationsfond zuständigen Mitarbeiter_innen bei der Bezahlung der APCs.

<img src="/images/Grafik_UnterstützungOAWorkflow_DOAJ.JPG" alt="Unterstützung des Open-Access-Workflows durch DOAJ" width="650" height="400" display="block" /><br />
*Quelle: eigene Darstellung in Anlehnung an die Grafik auf S. 16 im Knowledge Exchange Report, 2016*

Die SHERPA-Dienste sind ebenfalls frei zugängliche, nicht-kommerzielle Datenbanken, die Informationen zu den Verlagsrichtlinien bzgl. urheberrechtliche Regelungen und der
Selbstarchivierung sowie zu den Open-Access-Richtlinien der Forschungsförderer bereitstellen.
Während das DOAJ ein Mitgliedschaftsmodell entwickelt hat, basierend auf Bibliotheks-/Konsortialförderung und einem Sponsoringprogramm, das auf (Open-Access-) Verlage und
kommerzielle Aggregatoren zielt, werden die SHERPA-Dienste zur Zeit vollständig von Jisc finanziert. Dies birgt die Gefahr in sich, dass die bestehenden Services nicht
über das aktuelle Level hinaus entwickelt werden und primär die Bedürfnisse der britischen Nutzer_innen adressieren.
Für beide Dienste gilt, dass der Pool an Unterstützer_innen siginifikant erweitert werden muss.

Bezug nehmend auf den KE-Report gründeten das Council of the Australian University Librarians ([CAUL](http://www.caul.edu.au/)), [LIBER](http://libereurope.eu/),
[EIFL](http://www.eifl.net/) und [SPARC Europe](https://sparceurope.org/) Anfang 2017 das Unterstützer_innennetzwerk
[SCOSS (Global Sustainability Coalition for Open Science Services)](http://www.scoss.org/).
Das Ziel von SCOSS ist es, die finanzielle Beteiligung der Open-Access-/Open-Science-Community an derjenigen Infrastruktur zu koordinieren, auf der ihre Services und ihre
Arbeit basiert. Dafür schafft SCOSS ein Verzeichnis von kritischen, nicht-kommerziellen Services und gibt Empfehlungen heraus, welche davon für eine finanzielle
Unterstützung in Frage kommen. In Bezug auf das DOAJ schlägt SCOSS vor, dass Förderer über einen 3-Jahres-Zeitraum je nach ihrer Größe 2.000 bis 8.000 EUR pro Jahr an das
DOAJ zahlen. Hierzu offeriert das DOAJ fördernden Konsortien als auch individuellen Institutionen einen Rabatt von 25%. Die Vereinbarung wird direkt mit dem DOAJ geschlossen.

Es gibt viele Argumente für den Einsatz von offenen Open-Access-/Open-Science-Infrastrukturen:

* Sie sind frei zugänglich und nicht-kommerziell.
* Sie sind communitybasiert in ihrer Ausgestaltung und in ihrer Finanzierung.
* Sie sind integriert in den wissenschaftlichen Publikationsprozess
* Sie sind integriert in die Implementierung von Open-Access- und Open-Science-Richtlinien.
* Sie helfen bei der Verbreitung notwendiger Persistent Identifier und Standards.
* Sie sind durch den offenen Ansatz leicht integrierbar, effektiv und repräsentativ und weniger vorurteilsbehaftet.
* Sie dienen der Qualitätssicherung für Open-Access-Publikationen bei gleichzeitigem Absenken der Administrationskosten dafür.
* Sie stellen eine Alternative zu (teuren) kommerziellen Angeboten wie z. B. Web of Science, Scopus etc. dar.

Wir legen daher den an der Open-Access-Transformation interessierten Bibliotheken und wissenschaftlichen Einrichtungen nahe, sich an der Finanzierung des Directory of Open Access Journals in Höhe der SCOSS-Empfehlungen zu beteiligen, um so zu gewährleisten, dass diese nicht-kommerzielle, frei zugängliche Open-Access-Infrastruktur weiterhin bestehen bleibt.
