<!--
.. title: Mehr als die Hälfte geschafft! 11 Pledges für OPEN Library Politikwissenschaft
.. slug: mehr-als-haelfte-geschafft
.. date: 2018-08-09 08:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## OPEN Library Politikwissenschaft auf gutem Wege ein voller Erfolg zu werden!

Seit dem 06. August liegen 11 Pledges für unser Pilotprojekt [transcript OPEN Library Politikwissenschaft](https://oa2020-de.org/pages/transcriptopenlibrarypowi/) vor. Damit ist zum einen mehr als die Hälfte der Mindestteilnehmer_innen erreicht und zum anderen verdeutlicht der Zuspruch ein hohes Interesse an transparenten und validen Modellen zur Open-Access-Transformation gerade auch in den Geistes- und Sozialwissenschaften.

Daher freuen wir uns sehr über Teilnahme der folgenden Universitätsbibliotheken....

1. Universitäts- und Landesbibliothek Düsseldorf
2. Max Planck Digital Library (MPDL)
3. Universitätsbibliothek Bochum
4. Universitätsbibliothek Bielefeld
5. Universitätsbibliothek J. C. Senckenberg an der Goethe-Universität Frankfurt
6. Kommunikations-, Informations-, Medienzentrum (KIM) der Universität Konstanz
7. Universitäts- und Stadtbibliothek Köln
8. Harvard University Library
9. Universitätsbibliothek Erlangen-Nürnberg
10. Universitätsbibliothek Wuppertal
11. Universitätsbibliothek Leipzig

....und sehen sie als ein Signal für alle anderen wissenschaftlichen Bibliotheken, eine Beteiligung zu diskutieren, zu planen und durchzuführen.

Der Pilot selbst unterstützt Bibliotheken in ihrer (Neu-) Positionierung bei der Produktion, Distribution und Zirkulation wissenschaftlicher Texte. Mit der Transformation wissenschaftlicher Publikationen in den Open Access sind sie direkt daran beteiligt, die digitale Verfügbarkeit wissenschaftlich relevanter Ressourcen zu realisieren.
