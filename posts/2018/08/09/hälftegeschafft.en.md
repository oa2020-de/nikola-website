<!--
.. title: More than half is done! 11 Pledges for OPEN Library Political Science
.. slug: more-than-half-is-done
.. date: 2018-08-09 08:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## OPEN Library Political Science on track to become a complete success!

Since the 6th of August there are 11 pledges for our pilot project [transcript OPEN Library Political Science](https://oa2020-de.org/en/pages/transcriptopenlibrarypols/). On the one hand, more than half of the minimum participants are reached and, on the other hand, the response shows a high level of interest in transparent and valid models for open access transformation, especially in the humanities and social sciences.

Therefore, we are very pleased about the participation of the following university libraries....

1. University and State Library Düsseldorf
2. Max Planck Digital Library (MPDL)
3. University Library Bochum
4. University Library Bielefeld
5. University Library J. C. Senckenberg at Goethe-University Frankfurt
6. Communication, Information, Media Centre (KIM) at the University of Konstanz
7. University Library Cologne
8. Harvard University Library
9. University Library Erlangen-Nürnberg
10. University Library Wuppertal
11. University Library Leipzig

....and see it as a signal for all other academic libraries to discuss, plan and perform an involvement.

The pilot himself supports libraries in their (re) positioning in the production, distribution and circulation of scientific texts. With the transformation of scientific publications into open access, they are directly involved in realizing the digital availability of scientifically relevant resources.
