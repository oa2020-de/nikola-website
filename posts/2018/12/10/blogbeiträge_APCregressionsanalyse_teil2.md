<!--
.. title: APCs – Spiegel des Impact-Factors oder Erbe des Subskriptionsmodells? Die Datenbasis.
.. slug: APCregressionsanalyse_Datenbasis
.. date: 2018-12-10 08:00:00+1:00
.. author: Nina Schönfelder
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Dieser Blogbeitrag ist Teil einer Reihe, die die Studie [„APCs – Spiegel des Impact-Factors oder Erbe des Subskriptionsmodells?“](https://dx.doi.org/10.4119/unibi/2931061) von OA2020-DE erläutert.

In der Studie wird anhand des [OpenAPC-Datensatzes](https://treemaps.intact-project.org/), der im INTACT-Projekt an der Universitätsbibliothek Bielefeld entsteht, analysiert, was die Höhe von tatsächlich gezahlten Artikelbearbeitungsgebühren (in Gegensatz zu Listenpreisen) beeinflusst.
<!-- TEASER_END -->
In der Version des OpenAPC-Datensatzes vom Anfang des Jahres 2018 standen 47.748 Beobachtungen, d.h. tatsächlich gezahlte APCs, zur Analyse bereit. Die meisten APC-Zahlungen wurden aus dem Vereinigten Königreich berichtet. Auch aus Deutschland wurden viele APC-Zahlungen registriert. Das letzte vollständige Berichtsjahr, das bis dahin zur Verfügung stand, ist 2016, für das allein 16.210 APC-Zahlungen verzeichnet sind. Der OpenAPC-Datensatz wurde mit den [„CWTS Journal Indicators“](http://www.journalindicators.com/) der Universität von Leiden (Niederlande) verbunden. Diese enthalten den sogenannten „Source Normalized Impact per Paper“ (SNIP), d.h. ein Maß, das angibt wie oft ein Artikel aus der betreffenden Fachzeitschrift in den letzten drei Jahren durchschnittlich zitiert worden ist, sowie welchem Fachbereich die Zeitschrift zuzuordnen ist. Der SNIP hat Ähnlichkeiten mit dem besser bekannten Journal Impact Factor (IF) von Clarivate Analytics, korrigiert jedoch weitgehend die Schwäche des IF bzgl. der Vergleichbarkeit zwischen den Fachbereichen und ist darüber hinaus frei verfügbar.

## Überblick

In der folgenden Tabelle sind die Übersichtsstatistiken zu dem Gesamtdatensatz aufgeführt.

<img src="/images/APCregressionsanalyse_Uebersichtsstatistiken.png" alt="Übersichtsstatistiken aus der APC-Regressionsanalyse" width="900" height="500" display="block" />


Die meisten Artikel, die durch APCs finanziert und an OpenAPC gemeldet worden sind, wurden bei den Verlagen Elsevier, Springer Nature und PLoS veröffentlicht, von denen zwei große, traditionell subskriptions-basierte Verlage sind. Summiert man die Artikel, die bei Springer Science + Business Media und Springer Nature in den letzten Jahren erschienen sind, so zeigt sich Springer an dieser Stelle als der größte Verlag für Open-Access-Fachartikel. Allerdings sind die meisten Open-Access-Artikel in den Open-Access-Mega-Journalen „PLOS ONE“ und „Scientific Reports“ erschienen. Etwa die Hälfte der APCs wurde für das Publizieren in hybriden Zeitschriften gezahlt, die andere Hälfte an genuine Open-Access-Zeitschriften. Die relativ geringen APC-Zahlungen an Zeitschriften aus den Geistes- und Sozialwissenschaften bestätigen den allgemeinen Eindruck, dass in diesem Fachbereich kostenpflichtiges Open-Access-Publizieren eine geringe Rolle spielt. Ob diese, für OpenAPC gezeigten Feststellungen auch grundsätzlich gelten (d.h. für den gesamten Publikationsmarkt), können jedoch nur genaue bibliometrische Untersuchungen zeigen.

## Verteilung der APCs

Widmen wir uns nun einer genauen Betrachtung der tatsächlich gezahlten APCs: Die Durchschnitts-APC beträgt knapp 2.000 €, der Median liegt etwas geringer bei 1.740 €. Die Verteilung der APCs ist rechtsschief, d.h. es wurden häufig geringe und moderat hohe APCs gezahlt, während hohe APCs eher selten bezahlt wurden. Etwa 50% der gezahlten APCs liegen zwischen 1.255 € bis 2.450 €. 25% der APC-Zahlungen liegen unter 1.255 € und die anderen 25% über 2.450 €.

<img src="/images/APC_Frequency_AnalyseNina.jpg" alt="Verteilung der APC-Zahlungen" width="400" height="450" display="block" />

## Unterschied Großbritannien und Deutschland

Zuletzt gehen wir kurz auf die Unterschiede zwischen den britischen und deutschen Daten ein (Abbildungen und Tabellen im Bericht). Die durchschnittliche, tatsächlich gezahlte APC ist in Deutschland wesentlich niedriger als in Großbritannien. In OpenAPC sind kaum APC-Zahlungen über 2.000 € aus Deutschland verzeichnet, was den Richtlinien der DFG-geförderten Publikationsfond geschuldet ist. Zahlungen, die nicht aus den Publikationsfond bestritten werden, sondern aus Drittmitteln der Forschungsförderung oder Institutsmitteln stammen und sich damit nicht an diese APC-Obergrenze halten müssen, werden so gut wie nie an OpenAPC berichtet. Dies spiegelt sich auch in dem Anteil der APCs für Publikationen in hybriden Zeitschriften vs. OA-Zeitschriften wider. Laut OpenAPC geht gerade mal 1% der deutschen Zahlungen an hybride Zeitschiften, aber fast dreiviertel der britischen Zahlungen. Während in Deutschland eine relativ restriktive Förderpolitik zu APCs implementiert ist, gibt es in Großbritannien keine vergleichbaren Einschränkungen. Aus diesem Grund werden für die APC-Regressionsanalyse, die wir im übernächsten Blogbeitrag behandeln werden, nur die britischen Daten von 2014–2016 verwendet.

## Weitere Informationen

Schönfelder, Nina (2018). *APCs — Mirroring the impact factor or legacy of the subscription-based model?*. Universität Bielefeld. doi:[10.4119/unibi/2931061](https://dx.doi.org/10.4119/unibi/2931061)

Blogbeitrag 1 - [APCs – Spiegel des Impact-Factors oder Erbe des Subskriptionsmodells? Eine Einleitung.](/blog/2018/11/26/APCregressionsanalyse_einleitung/)

Blogbeitrag 3 - [APCs – Spiegel des Impact-Factors oder Erbe des Subskriptionsmodells? Beschreibende Statistiken.](/blog/2019/01/08/APCregressionsanalyse_beschreibendestatistik/)
