<!--
.. title: APCs — Mirroring the impact factor or legacy of the subscription-based model? The database.
.. slug: APCregressionsanalysis_database
.. date: 2018-12-10 08:00:00+1:00
.. author: Nina Schönfelder
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

This blog post is part of a series that explains the study [“APCs — Mirroring the impact factor or legacy of the subscription-based model?”](https://dx.doi.org/10.4119/unibi/2931061) of OA2020-DE.

With data from [OpenAPC](https://treemaps.intact-project.org/), which is part of the INTACT project at the Bielefeld University Library, Germany, we analyzed the determinants for APCs actually paid (in contrast to catalogue prices).
<!-- TEASER_END -->

In the version from February 2018, which is used in this study, the OpenAPC dataset comprises 47,748 observations in total. Most APC-payments were reported from UK, followed by Germany with huge distance. The last completed reporting year was 2016 with 16,210 APC-funded articles. We merged the OpenAPC data set with the [CTWS Journal Indicators](http://www.journalindicators.com/) compiled at the Leiden University (The Netherlands). They contain the so-called “source normalized impact per paper” (SNIP), a measure for the average citation impact of the publications of a journal, as well as the subject area of a journal. The SNIP is similar to the well-known Journal Impact Factor (IF) of Clarivate Analytics, but the SNIP corrects for differences in citation practices between scientific fields. Moreover, the SNIP is freely available.

## Summary statistics

The following table presents summary statistics of the total data set.

<img src="/images/APCregressionsanalyse_Uebersichtsstatistiken.png" alt="Summary statistics of the APC regression analysis" width="900" height="500" display="block" />

Most APC-funded articles reported to OpenAPC were published by Elsevier, Springer Nature and the Public Library of Science (PLoS) — two of them being traditional subscription-based publishers. Adding up the publications at Springer Nature and Springer Science + Business Media, Springer published most of the articles and received most of the APC-payments recorded in OpenAPC. However, the genuine open-access mega-journal PLOS ONE published most articles, followed by Scientific Reports. The journals’ subject areas confirm the practical experience that social sciences and humanities play a minor role in the APC-based open-access journal publishing. About half of the APCs were paid to publish an article in a hybrid journal, the other half for the publication in an open-access journal. However, whether these statements hold true for the total publication market — not only for OpenAPC — can only be assessed via in-depth bibliometric analysis.

## APC distribution

We now turn to a detailed description of the APCs in euro. The mean APC is slightly below EUR 2,000 and the median is about EUR 1,740. The distribution is right-skewed, which means that low to moderate large APCs were paid quite frequently whereas expensive APCs are rather rare. Fifty per cent of the APC-payments range from EUR 1,255 to EUR 2,450. A quarter range below and another quarter above this range.

<img src="/images/APC_Frequency_AnalyseNina.jpg" alt="Distribution of APC-payments" width="400" height="450" display="block" />

## Differences between Great Britain and Germany

Finally, we look at the differences between the British and German data (figures and tables in the report). The average APC is higher in UK and well lower in Germany. The German APC-payments are hardly above EUR 2,000, most probably due to the APC-funding rules (price-cap). Almost three-quarters of the reported British APCs stem from publications in hybrid journals, but only 1 percent in Germany. These differences reflect the different APC-funding rules in the countries. APC-funding in Germany is much more restrictive than in UK. Therefore, we uses the British data in the regression analysis, which we are going to discuss in the next blog posts.

## More information

Schönfelder, Nina (2018). *APCs — Mirroring the impact factor or legacy of the subscription-based model?*. Universität Bielefeld. doi:[10.4119/unibi/2931061](https://dx.doi.org/10.4119/unibi/2931061)

Blogpost 1 - [APCs — Mirroring the impact factor or legacy of the subscription-based model? An introduction.](/en/blog/2018/11/26/APCregressionanalysis_introduction/)

Blogpost 3 - [APCs — Mirroring the impact factor or legacy of the subscription-based model? Descriptive statistics.](/en/blog/2019/01/08/APCregressionsanalyse_descriptivestatistics/)
