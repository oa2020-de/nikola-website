<!--
.. title: Neuerscheinungen des Programmbereichs Politikwissenschaft 2019 werden Open Access
.. slug: transcriptOPENLibrary_vollerErfolg
.. date: 2018-12-11 08:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

# Die transcript Open Library-Community hat’s geschafft: Alle Neuerscheinungen des Programmbereichs Politikwissenschaft 2019 werden Open Access!

BIELEFELD (transcript/OA2020-DE). 46 wissenschaftliche Einrichtungen sind unserer Einladung zu einem kooperativen Open-Access-Modell gefolgt und haben durch ihre Kostenbeteiligung die Open-Access-Bereitstellung von 20 Neuerscheinungen (2019) im Fachbereich Politikwissenschaft ermöglicht. Die - zwecks Kostendeckelung und Planungssicherheit - vorgegebene Teilnehmer_innenzahl von 20 Einrichtungen wurde damit mehr als verdoppelt. Die Fördercommunity erreicht auf diese Weise eine eindrucksvolle Senkung der Open-Access-Kosten für wissenschaftliche Bücher.

**Wir danken den Förderern, die mit ihrem Beitrag dafür sorgen, dass unsere Politik-Frontlist im Open Access für alle Mitglieder des Wissenschaftsbetriebs zur Verfügung steht!**

Durch die hohe Akzeptanz des Projekts und dank der Beteiligung des FID Politikwissenschaft in Höhe von 50% des Gesamtvolumens betragen die tatsächlichen Kosten pro Einrichtung nur noch 52,27 € pro Publikation. Im Sponsoring Light sogar lediglich 26,14 Euro pro Buch - ein Freistellungsbetrag für Open Access, der noch unter unserem durchschnittlichen Ladenpreis liegt!

Die eindrucksvolle Fördercommunity ermöglicht damit nicht nur ein wirtschaftlich tragbares und nachhaltiges Publikationsmodell für professionelles Open Access, sondern schafft gleichzeitig transparente (weil förderungskonforme) und verlässliche Publikationswege für Autor_innen geisteswissenschaftlicher Werke. Damit schafft das [konsortiale Publikationsmodell](/pages/transcriptopenlibrarypowi/) barrierefreies Open Access auf ganzer Linie. Zu den Besonderheiten der 2019er Frontlist gehört u.a. der Start der wissenschaftlichen Reihe „Politik in der digitalen Gesellschaft“ - herausgegeben von Prof. Dr. Jeanette Hofmann, Prof. Dr. Norbert Kersting, Prof. Dr. Claudia Ritzi und Prof. Dr. Wolf J. Schünemann – deren Bände innerhalb des Pilotprojektes ebenfalls Open Access erscheinen.
<!-- TEASER_END -->

Das Publikationsmodell folgt dem Modell »E-Book-Paket«. Statt des Erwerbs einer Campuslizenz mit den bekannten Begrenzungen der Nutzungsmöglichkeiten wird die Open-Access-Bereitstellung einer Frontlist finanziert und steht damit allen Wissenschaftsakteuren zur Verfügung. Konkret werden über das Projekt 20 Novitäten aus dem Programmbereich Politikwissenschaft (d.h. Monografien und Sammelbände aus den Bereichen internationale und europäische Politik, Globalisierung, Parteien, Soziale Bewegung und Zivilgesellschaft, Policy, Politics, Politische Theorie und Polity) im Open Access erscheinen.

Der nächste Schritt ist, das Modell auch für zukünftige Jahre zu sichern. Dazu werden wir Anfang 2019 über eine Umfrage unter den Teilnehmer_innen Anregungen und Rückmeldungen einholen, um das Modell gemeinsam weiterzuentwickeln. Über Neuigkeiten zum Projekt informieren wir über unseren Newsletter, den Sie hier abonnieren können: [https://www.transcript-verlag.de/newsletter/](https://www.transcript-verlag.de/newsletter/). <br />
Wenn Sie direkt mit uns in Kontakt treten wollen, schreiben Sie eine Email an Stefanie Hanneken [open-access@transcript-verlag](mailto:open-access@transcript-verlag)

## Die Teilnehmer_innen

Die „transcript Open Library Politikwissenschaft“ ist ein gemeinschaftliches Pilotprojekt des transcript Verlages und des Nationalen Open Access Kontaktpunktes OA2020-DE. Es wird durch die Infrastruktur von Knowledge Unlatched sowie der Mitarbeit der Bibliothekslieferanten Dietmar Dreier Wissenschaftliche Versandbuchhandlung GmbH, Missing Link Versandbuchhandlung eG und der Schweitzer Fachinformation unterstützt. Die transcript Open Library Community besteht aus 46 Teilnehmer_innen: dem FID Politikwissenschaft, 43 Vollzahler_innen sowie zwei Beteiligungen im Rahmen des „Sponsoring Light“.

Der Fachinformationsdienst (FID) Politikwissenschaft wird geleitet von Frau Maria Elisabeth Müller von der Staats- und Universitätsbibliothek Bremen und Herrn Dr. Philipp Mayr vom GESIS – Leibniz-Institut für Sozialwissenschaften. Er optimiert nachhaltig die Literaturversorgung und die Informationsinfrastruktur im Bereich der Politikwissenschaft in Deutschland. Ziel ist die Versorgung der Fachcommunity mit forschungsrelevanter Literatur, aber auch die Entwicklung innovativer Recherchetools.

## Die Fördercommunity

*50 Prozent des Gesamtvolumens:* POLLUX - Informationsdienst Politikwissenschaft

*Sponsoring Light:* Bundesministerium der Verteidigung, Landesbibliothek Oldenburg

*Full pledges (in alphabetischer Reihenfolge):* Gottfried Wilhelm Leibniz Bibliothek, Niedersächsische Landesbibliothek, Harvard University, Kommunikations-, Informations-, Medienzentrum (KIM) der Universität Konstanz, Max Planck Digital Library (MPDL), Saarländische Universitäts- und Landesbibliothek, Sächsische Landesbibliothek Staats- und Universitätsbibliothek Dresden, Staats- und Universitätsbibliothek Carl von Ossietzky Hamburg, Staatsbibliothek zu Berlin, Technische Informationsbibliothek Hannover, Thüringer Universitäts- und Landesbibliothek Jena (ThULB), ULB Düsseldorf Universitäts- und Landesbibliothek Düsseldorf, Universitätsbibliothek Erfurt, Universitäts- und Landesbibliothek der Technischen Universität Darmstadt, Universitäts- und Landesbibliothek Münster, Universitäts- und Stadtbibliothek Köln, Universitätsbibliothek Bayreuth, Universitätsbibliothek Bielefeld, Universitätsbibliothek der Bauhaus-Universität Weimar, Universitätsbibliothek der FernUniversität Hagen, Universitätsbibliothek der Humboldt-Universität zu Berlin, Universitätsbibliothek der Justus-Liebig-Universität Gießen, Universitätsbibliothek der Ruhr-Universität Bochum, Universitätsbibliothek der Technischen Universität Braunschweig, Universitätsbibliothek der Universität Koblenz Landau, Universitätsbibliothek der Universität Potsdam, Universitätsbibliothek Duisburg-Essen, Universitätsbibliothek Erlangen-Nürnberg, Universitätsbibliothek Freiburg, Universitätsbibliothek Graz, Universitätsbibliothek J.C. Senckenberg an der Goethe-Universität Frankfurt, Universitätsbibliothek Kassel, Universitätsbibliothek Leipzig, Universitätsbibliothek der LMU München, Universitätsbibliothek Mainz, Universitätsbibliothek Marburg, Universitätsbibliothek Oldenburg, Universitätsbibliothek Osnabrück, Universitätsbibliothek Siegen, Universitätsbibliothek Vechta, Universitätsbibliothek Wien, Universitätsbibliothek Wuppertal, Zentral- und Hochschulbibliothek Luzern, Zentralbibliothek Zürich


[[Link zur Pressemitteilung](/assets/files/PM_Polwioa.pdf)]
