<!--
.. title: Big deals - Opportunities to strengthen the negotiation position
.. slug: bigdeals-negotationposition
.. date: 2018-05-24 08:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Recently, two publications have been published that look at the topic of "Big Deals" from different perspectives: In April 2018 the ["EUA Big Deals Survey Report - The First Mapping of Major Scientific Publishing Contracts in Europe"](https://eua.eu/resources/publications/321:eua-big-deals-survey-report-the-first-mapping-of-major-scientific-publishing-contracts-in-europe.html) and in May 2018 the post of Lindsay McKenzie in Inside Higher Ed ["‘Big Deal’ Cancellations Gain Momentum"](https://www.insidehighered.com/news/2018/05/08/more-institutions-consider-ending-their-big-deals-publishers).

The EUA Big Deals Survey Report looks at the current situation in Europe and in 2016 and 2017 questioned negotiators at universities in 28 different EU countries on the amount and terms of their three largest and most expensive contracts with scientific publishers. The answers were anonymised by country and publisher, but they still present a clear picture of the negotiation results that are often not made public. Collectively, the research institutions annually spend approximately €421 million on periodicals, e-books and databases. Of this sum, the vast majority (€384 million) is spent on periodicals and 65% of this expenditure goes to Elsevier.

<!-- TEASER_END -->

<img src="/images/EUA_Survey_Publisher.png" alt="Percentage of periodicals expenditure per publisher per annum" width="400" height="200" display="block" /><br />
*Source: @European University Association*

The survey observes that universities are hesitant about including Article Processing Charges (APCs) in the contracts, but these charges could become an important instrument for the transition to full open access. Therefore the report refers to earlier research on the financial benefits of open access publishing: *"Recent studies have estimated that the transition towards an open access publishing system could result in savings of up to 45% for periodicals alone. This would imply potential savings of around EUR 170,000,000 on journals in Europe, which could be re-allocated to research and/or to moving towards a full-scale open access publishing system."*

This point leads us to the article by Lindsay McKenzie. She describes that in the US and Canada, the big deals with publishers like Elsevier, Wiley, Taylor & Francis, etc. are under scrutiny. Library budgets are not growing at the same rate as the cost of subscriptions, so cancellations are unavoidable and the big deals are increasingly coming into focus.

>"What makes the big deal unsustainable isn’t the structure of the model, but the fact that it absorbs so much of a library’s materials budget, and the price rises steadily from year to year." (Rick Anderson, Associate Dean at Marriott Library at the University of Utah)

In 2015, the [Université de Montréal](https://papyrus.bib.umontreal.ca/xmlui/bitstream/handle/1866/18507/Gagnon_Stephanie_2017_article.pdf) combined usage and citations data with the results of an extensive survey of faculty and students to determine that only 12 percent of the institution’s total subscriptions, and around a third of all titles included in big deals were essential to research needs at the university. A similar analysis has since been performed by 28 university libraries in Canada, to unbundle deals or negotiate better ones. This makes massive savings possible (Montréal saves about 10% of their annual acquisitions budget and can use it for other services and products). Despite these facts, cancellations require a lot of persuasion in the scientific institutions: *“We put in a lot of energy to explain, convince, answer and demonstrate to our staff and to the community what we were doing”* (Stephanie Gagnon).


Both publications highlight different aspects of one and the same topic: The EUA Report is the first ever big deal study at European level and points to the concentration of committed funds on subscription packages and large publishers (Elsevier, Wiley, Springer, Clarivate). Lindsay McKenzie, on the other hand, refers above all to ways of breaking the big deal logic. What the two texts unite are the people involved in the big deals. Thus, in the process from the decision to the extension / modification / canceling of the contracts, there are always identical groups of people.

<img src="/images/Big_DEAL_Process_CommunicationParts.png" alt="Big Deal Process: Involved persons" width="600" height="150" display="block" /><br />
*Source: own representation*

This becomes particularly clear in the first and last part of the process: representatives of the universities and libraries (among other groups) are involved here. Say the groups of people who need to be persuaded to cancel / change the big deal contract may be the same as the one who decides what deal to negotiate in the first place. Pooling these forces and bringing together the groups that are present in only one part of the process can therefore increase the potential for negotiation. The EUA report also demonstrates that the representatives of the universities and libraries play an integral role in the consortia that negotiate with publishers and that the negotiation position can be strengthened by greater university leadership involvement. The knowledge of the relevance of individual titles in the big deals, the actual use of content and the increasing number of open access publications also within the packages should therefore be used to convert existing deals into publish & read models and integrate further core elements such as transparency, efficient administration and monitoring mechanisms.
