<!--
.. title: Big Deals - Möglichkeiten zur Stärkung der Verhandlungsposition
.. slug: BigDeals-Verhandlungsposition
.. date: 2018-05-24 08:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Vor kurzem sind zwei Publikationen erschienen, die das Thema "Big Deals" aus unterschiedlichen Perspektiven betrachten: Im April 2018 der ["EUA Big Deals Survey Report - The First Mapping of Major Scientific Publishing Contracts in Europe"](https://eua.eu/resources/publications/321:eua-big-deals-survey-report-the-first-mapping-of-major-scientific-publishing-contracts-in-europe.html) und im Mai 2018 der Beitrag ["‘Big Deal’ Cancellations Gain Momentum"](https://www.insidehighered.com/news/2018/05/08/more-institutions-consider-ending-their-big-deals-publishers) von Lindsay McKenzie bei Inside Higher Ed.

Der EUA Big Deals Survey Report betrachtet die aktuelle Situation in Europa und hat dafür 2016 und 2017 die Verhandlungsführer_innen an Universitäten in 28 verschiedenen EU-Staaten zur Höhe und den Konditionen ihrer drei größten und teuersten Verträge mit wissenschaftlichen Verlagen befragt. Ergebnis: insgesamt geben die wissenschaftlichen Einrichtungen in der EU pro Jahr ca. 421 Mio. EUR für Zeitschriften, eBooks und Datenbanken aus. Der Großteil davon (ca. 384 Mio EUR) wird für Zeitschriften verwendet und 65% dieser Ausgaben gehen an Elsevier.

<!-- TEASER_END -->

<img src="/images/EUA_Survey_Publisher.png" alt="Ausgaben für Zeitschriften pro Verlag pro Jahr in Prozent" width="400" height="200" display="block" /><br />
*Quelle: @European University Association*

Die Studie zeigt auch, dass die Verhandlungsführer_innen bisher etwas zaghaft mit dem Thema Article Processing Charges (APCs) umgegangen sind (der Anteil an APCs in den Big Deals liegt zurzeit bei 11%), aber gerade diese Kosten können ein wichtiges Instrument für den Übergang zu Open Access werden. So verweist die Studie auch auf vorhergehende Forschung und den finanziellen Benefit von Open Access: *"Recent studies have estimated that the transition towards an open access publishing system could result in savings of up to 45% for periodicals alone. This would imply potential savings of around EUR 170,000,000 on journals in Europe, which could be re-allocated to research and/or to moving towards a full-scale open access publishing system."*

Dieser Punkt führt uns nun zu dem Beitrag von Lindsay McKenzie. Sie beschreibt, dass in den USA und in Kanada die Big Deals mit den großen Verlagen wie Elsevier, Wiley, Taylor & Francis etc. auf dem Prüfstand stehen. Die Bibliotheksetats wachsen nicht im gleichen Maße wie die Zeitschriftenkosten, daher sind Abbestellungen unvermeidbar und die Big Deals rücken verstärkt in den Fokus.

>"What makes the big deal unsustainable isn’t the structure of the model, but the fact that it absorbs so much of a library’s materials budget, and the price rises steadily from year to year." (Rick Anderson, Associate Dean at Marriott Library at the University of Utah)

So hat die Universität Montréal 2015 die Nutzungs- und die Zitationszahlen für ihre abonnierten Zeitschriften ermittelt und diese mit einer Umfrage unter Wissenschaftler_innen und Studierenden kombiniert und [herausgefunden](https://papyrus.bib.umontreal.ca/xmlui/bitstream/handle/1866/18507/Gagnon_Stephanie_2017_article.pdf), dass nur 12% der insgesamt abonnierten Zeitschriftentitel und nur ca. ein Drittel der in den Big Deals enthaltenenen Titel wirklich relevant sind.
Die gleiche Analyse wird seitdem von 28 weiteren kanadischen Universitäten verwendet, um entweder Deals aufzulösen oder bessere zu verhandeln. Dadurch sind massive Einsparungen möglich (Montréal kann dadurch 10% des jährlichen Subskriptionsetats für andere Ausgaben verwenden).
Trotz dieser Fakten benötigen Abbestellungen sehr viel Überzeugungsarbeit in den wissenschaftlichen Einrichtungen: *“We put in a lot of energy to explain, convince, answer and demonstrate to our staff and to the community what we were doing”* (Stephanie Gagnon).


Beide Texte beleuchten unterschiedliche Aspekte ein- und desselben Themas: Der EUA Report ist die erste Big-Deal-Untersuchung überhaupt auf europäischer Ebene und verweist auf die Konzentration der gebundenen Mittel auf Zeitschriftenpakete und große Verlage (Elsevier, Wiley, Springer, Clarivate). Lindsay McKenzie wiederum verweist in ihrem Beitrag vor allem auf Möglichkeiten zum Aufbrechen der Big-Deal-Logik. Was die beiden Texte aber eint sind die an den Big Deals beteiligten Personen. So finden sich in dem Prozess von der Auswahl bis zur Verlängerung/ Änderung/ Auflösung der Verträge immer wieder identische Personengruppen:

<img src="/images/Big_DEAL_Process_CommunicationParts.png" alt="Big Deal Process: Involved persons" width="600" height="150" display="block" /><br />
*Quelle: eigene Darstellung*

Besonders deutlich wird das im ersten und im letzten Prozess-Teil: Hier sind (neben anderen Gruppen) jeweils Vertreter_innen der Universitäten und der Bibliotheken beteiligt. Sprich die Personengruppen, die bei einer Abbbestellung/ Änderung des Big-Deal-Vertrages überzeugt werden müssen, können die gleichen sein, die vorher die Entscheidung darüber treffen, welches Angebot überhaupt verhandelt wird. Eine Bündelung dieser Kräfte und ein Zusammenbringen der Gruppen, die bisher nur in einem Teil des Prozesses vertreten sind, kann daher das Potential von Verhandlungen steigern. Auch der EUA-Report demonstriert, dass die Vertreter_innen der Universitäten und der Bibliotheken eine wesentliche Rolle im verhandelnden Konsortium spielen und durch den Einbezug der Universitätsleitung die Verhandlungsposition gestärkt werden kann. Das Wissen um die Relevanz von einzelnen Titeln in den Big Deals, die tatsächliche Nutzung der Inhalte und die steigende Anzahl an Open-Access-Publikationen auch innerhalb der Pakete sollte daher genutzt werden, um vorhandene Deals in Publish-&-Read-Modelle umzuwandeln und weitere Kernelemente wie Transparenz, effiziente Verwaltung und Monitoring zu integrieren.
