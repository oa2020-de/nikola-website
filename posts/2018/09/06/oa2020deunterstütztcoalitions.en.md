<!--
.. title: OA2020-DE supports cOAlition S
.. slug: oa2020de-supports-coalitions
.. date: 2018-09-06 14:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

>"After 1 January 2020 scientific publications on the results from research funded by public grants provided by national and European research councils and funding bodies, must be published in compliant Open Access Journals or compliant Open Access Platforms."

This core message from cOAlition S, a consortium of 11 European research funding organizations, illustrates the growing pressure on publishers to completely switch their business model to open access in a timely manner. Support is also available from the European Commission and the European Research Council (ERC). The initiative was born from the cooperation between the Heads of the participating Research Funding Organisations, Marc Schiltz the President of Science Europe, and Robert-Jan Smits the Open Access Envoy of the European Commission. It also drew on significant input from the Scientific Council of the ERC. After them also the plan S is named, which contains beside the cited core message 10 points, with which the open access transformation should be accelerated. This includes, for example, the establishment or support of high-quality Open Access ournals and platforms, the coverage of publication costs by the funder / institution of the author, and the strict rejection of hybrid Open Access.

**Particularly noteworthy is the standardization and capping of the Open Access publication fees across Europe, even if the amount is still discussed.**

Europe is strongly in favor of the immediately, freely accessible publication of publicly funded research results, but not at any price.
The research funders involved in cOAlition S will now collaborate with other stakeholders (like universities, research organizations and libraries) and work towards swift and practical implementation of these principles. Other research funders from across the world, both public and private, are invited to join cOAlition S.

**The National Contact Point Open Access OA2020-DE supports the cOAlition S and the Plan S, as they are important components for the Open Access Transformation.**
<br />
At the same time, we recommend that the implementation and further development of the 10 points include non-APC-based models as well as the special features of publishing in the humanities and the relevance of open infrastructures more than ever before.

More information on cOAlition S:

+ [cOAlition S: Making Open Access a Reality by 2020: A Declaration of Commitment by Public Research Funders](https://www.scienceeurope.org/coalition-s/)
+ [Plan S: Accelerating the transition to full and immediate Open Access to scientific publications](https://www.scienceeurope.org/wp-content/uploads/2018/09/Plan_S.pdf)
+ [Statement by Carlos Moedas: 'Plan S' and 'cOAlition S' – Accelerating the transition to full and immediate Open Access to scientific publications](https://ec.europa.eu/commission/commissioners/2014-2019/moedas/announcements/plan-s-and-coalition-s-accelerating-transition-full-and-immediate-open-access-scientific_en)
+ [DFG Statement on the Establishment of “cOAlition S” to Support Open Access](http://www.dfg.de/en/research_funding/announcements_proposals/2018/info_wissenschaft_18_56/index.html)
