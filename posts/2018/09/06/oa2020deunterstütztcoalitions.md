<!--
.. title: OA2020-DE unterstützt die cOAlition S
.. slug: oa2020de-unterstützt-coalitions
.. date: 2018-09-06 14:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

>"Ab dem 1. Januar 2020 müssen wissenschaftliche Veröffentlichungen zu Forschungsergebnissen, die durch die Finanzierung nationaler und europäischer Forschungsförderer enstanden sind, in entsprechenden Open-Access-Zeitschriften oder Open-Access-Plattformen veröffentlicht werden."

Diese Kernbotschaft der cOAlition S, einem Zusammenschluss von 11 europäischen Forschungsförderorganisationen, verdeutlicht den wachsenden Druck auf die Verlage, ihr Geschäftsmodell zeitnah vollständig auf Open Access umzustellen. Unterstützung dafür gibt es auch von der Europäischen Kommission und dem Europäischen Forschungsrat (ERC). Die Initiative entstand durch die Zusammenarbeit von Robert-Jan Smits, Sondergesandter für Open Access der Europäischen Kommission, und Marc Schiltz, Präsident von Science Europe. Nach ihnen ist auch der Plan S benannt, welcher neben der zitierten Kernbotschaft 10 Punkte enthält, mit denen die Open-Access-Transformation beschleunigt werden soll. Dazu gehört z. B. die Einrichtung bzw. Förderung qualitätsgeprüfter Open-Access-Zeitschriften und -Plattformen, die Übernahme von Publikationskosten durch den Förderer / die Einrichtung des Autors oder der Autorin und die strikte Ablehnung von hybridem Open Access.

**Besonders erwähnenswert ist die europaweite Standardisierung und Deckelung der Open-Access-Publikationsgebühren, auch wenn die Höhe noch diskutiert wird.**

Europa macht sich stark für die unmittelbare, frei zugängliche Publikation öffentlich geförderter Forschungsergebnisse, aber eben nicht um jeden Preis.
Die an cOAlition S beteiligten Forschungsförderer werden nun mit anderen Interessengruppen (wie Universitäten, Forschungseinrichtungen und Bibliotheken) zusammenarbeiten und auf eine schnelle und praktische Umsetzung dieser Prinzipien hinarbeiten. Weitere Forschungsförderer aus der ganzen Welt, sowohl öffentliche wie private, sind eingeladen, der cOAlition S beizutreten.

**Der Nationale Open-Access-Kontaktpunkt OA2020-DE unterstützt die cOAlition S und den Plan S, da sie wichtige Bausteine für die Open-Access-Transformation darstellen.** <br />
Gleichzeitig empfehlen wir, bei der Umsetzung und Weiterentwicklung der 10 Punkte auch nicht-APC-basierte Modelle sowie die Besonderheiten geisteswissenschaftlichen Publizierens und die Relevanz offener Infrastrukturen stärker als bisher zu berücksichtigen.

Weitere Informationen zur cOAlition S:

+ [cOAlition S: Making Open Access a Reality by 2020: A Declaration of Commitment by Public Research Funders](https://www.scienceeurope.org/coalition-s/)
+ [Plan S: Accelerating the transition to full and immediate Open Access to scientific publications](https://www.scienceeurope.org/wp-content/uploads/2018/09/Plan_S.pdf)
+ [Statement by Carlos Moedas: 'Plan S' and 'cOAlition S' – Accelerating the transition to full and immediate Open Access to scientific publications](https://ec.europa.eu/commission/commissioners/2014-2019/moedas/announcements/plan-s-and-coalition-s-accelerating-transition-full-and-immediate-open-access-scientific_en)
+ [Stellungnahme der DFG](http://www.dfg.de/foerderung/info_wissenschaft/2018/info_wissenschaft_18_56/index.html)
