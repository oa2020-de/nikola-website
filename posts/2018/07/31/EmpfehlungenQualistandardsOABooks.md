<!--
.. title: Empfehlungen zu Qualitätsstandards für die Open-Access-Stellung von Büchern [Update]
.. slug: empfehlungen_qualitätsstandards_oabücher
.. date: 2018-11-16 10:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Qualitätsstandards in Wissenschaft und Forschung sind wichtig, das hat die Berichterstattung zum Thema "Fake Science" (besser: Junk Science) wieder einmal deutlich gemacht (siehe dazu u.a. [hier](https://projekte.sueddeutsche.de/artikel/wissen/angriff-auf-die-wissenschaft-e398250/?reduced=true), [hier](https://mobil.derstandard.at/2000084063775/Fake-Journals-Zweifel-als-Anfang-der-Weisheit) und [hier](https://blogs.tib.eu/wp/tib/2018/07/25/nachgeprueft-fake-science/)). Das gilt nicht nur für Zeitschriften und Konferenzen, sondern gleichfalls auch für Bücher. Daher haben wir zusammen mit [Knowledge Unlatched](http://www.knowledgeunlatched.org/) und dem [transcript Verlag](https://www.transcript-verlag.de/) „Empfehlungen zu Qualitätsstandards für die Open-Access-Stellung von Büchern“ entwickelt. Ziel ist es, Autor_innen, Verlagen und Bibliotheken eine praxisnahe Richtschnur an die Hand zu geben, die Kriterien für die Produktion, den Vertrieb und die finanzielle Beteiligung an der Open-Access-Stellung von Büchern beschreibt. Sowohl für Verlage, die Open-Access-Angebote für Bücher bereit stellen, als auch für Bibliotheken, die über finanzielle Beteiligungen eine Open-Access-Stellung von Büchern mit ermöglichen wollen, ist es wichtig zu wissen, welche Leistungen angeboten beziehungsweise erwartet werden können.

Um den Einstieg in diesen Prozess insbesondere auch für kleinere Verlage zu erleichtern, schlagen wir die Umsetzung der Empfehlungen in zwei Phasen vor. In der Einstiegsphase zur Open-Access-Stellung von Büchern geht es vor allem um Qualitätsstandards in den Bereichen Formate, Metadaten, Zugänglichkeit, Lizenzen, Kosten und Distribution. Phase zwei baut diese Standards aus und ergänzt sie um Kriterien zur Qualiätsprüfung (Peer Review) sowie Text- und Data-Mining.

Die Empfehlungen liegen als überarbeitete Version auf deutsch und englisch vor:

* [Empfehlungen zu Qualitätsstandards für die Open-Access-Stellung von Büchern](https://pub.uni-bielefeld.de/record/2932189) (Updated version)
* [Recommendations on quality standards for the open access provision of books](https://pub.uni-bielefeld.de/record/2932189) (Updated version)

Vielen Dank an dieser Stelle an die [AG Universitätsverlage](http://blog.bibliothek.kit.edu/ag_univerlage/), die zur Weiterentwicklung der Standards aktiv beigetragten hat und sich selbst auch mit dem Thema auseinander setzt.
