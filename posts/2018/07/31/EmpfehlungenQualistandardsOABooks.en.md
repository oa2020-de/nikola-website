<!--
.. title: Recommendations on quality standards for the open access provision of books [Update]
.. slug: recommendations_qualitystandards_oabooks
.. date: 2018-11-16 10:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Quality standards in science and research are important, as the reporting on fake science (better: junk science) has made clear once again (see [here](http://genr.eu/wp/beyond-fakescience/)). This not only applies to journals and conferences, but also to books. Therefore we developed "Recommendations on quality standards for the open access provision of books" together with [Knowledge Unlatched](http://www.knowledgeunlatched.org/) and the publisher [transcript](https://www.transcript-verlag.de/en). The aim is to provide authors, publishers and libraries with a practical guideline describing criteria for the production, distribution and financial participation in the open access provision of books. It is important to know which services can be offered or expected, both for publishers who provide open access services for books and for libraries who want to enable an open access position for books through financial participation.

In order to facilitate the entry into this process, especially for smaller publishers, we propose implementing the recommendations in two phases. Above all, quality standards in the areas of formats, metadata, accessibility, licenses, costs and distribution are relevant in the entry phase to the open access position of books. Phase two expands these standards and adds quality inspection criteria (peer review), as well as text and data mining.

The updated recommendations are available in German and English:

* [Empfehlungen zu Qualitätsstandards für die Open-Access-Stellung von Büchern](https://pub.uni-bielefeld.de/record/2932189) (Updated version)
* [Recommendations on quality standards for the open access provision of books](https://pub.uni-bielefeld.de/record/2932189) (Updated version)

Many thanks to the representation of German-speaking [university publishers](http://blog.bibliothek.kit.edu/ag_univerlage/), which has actively contributed to the further development of the standards and also deals with the topic itself.
