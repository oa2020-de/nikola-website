<!--
.. title: First pledge for OPEN Library Political Science
.. slug: first-pledge-open-library-politicalscience
.. date: 2018-07-05 15:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

We received our **first pledge** for the ebook-openaccess-transformation-project OPEN Library Political Science! It's good to have [University and State Library Düsseldorf](https://www.ulb.hhu.de/) on board! In addition we received expressions of interest from the university libraries Bielefeld, Mainz, Mannheim and Regensburg, ETH Zurich library and the Max Planck Digital Library! We hope that more will follow.

Find out more about the project:

+ [https://oa2020-de.org/en/pages/transcriptopenlibrarypols/](https://oa2020-de.org/en/pages/transcriptopenlibrarypols/)
+ [http://knowledgeunlatched.org/political-science/](http://knowledgeunlatched.org/political-science/)
+ [https://www.transcript-verlag.de/open-access-politikwissenschaft](https://www.transcript-verlag.de/open-access-politikwissenschaft)
