<!--
.. title: Erster Workshop zur Open-Access-Transformation
.. slug: erster-workshop-zur-oa-transformation
.. date: 2018-02-26 08:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Bezugnehmend auf die Transformationsworkshops der [OA2020 Initiative](https://oa2020.org/events/) und der Veranstaltungen im Rahmen der [ESAC-Initiative](http://esac-initiative.org/) veranstaltet der Nationale Open-Access-Kontaktpunkt OA2020-DE einen Transformationsworkshop für die deutschsprachige Community.
Der Workshop richtet sich in erster Linie an Erwerbungsleiter_innen in den Universitätsbibliotheken und zielt darauf ab, die für eine Open-Access-Transformation relevanten Themen "Erhebung von Kosten- und Publikationsdaten in einer Universität" sowie "alternative Publikationsmodelle für Open-Access-Zeitschriften und Open-Access-Monographien" in Form von Input-Vorträgen und der gemeinsamen Diskussion in Arbeitsgruppen zu bearbeiten.
Am Ende sollen die Teilnehmer_innen wissen, wie eine Open-Access-Transformation ablaufen kann, welche Daten dafür notwendig sind und welche praktischen Implikationen für ihre Einrichtungen daraus entstehen.

**Wann? 19.-20. April 2018** <br />
Wo?   Universitätsbibliothek Bielefeld, Universitätsstr. 25, 33615 Bielefeld <br />
Wer?  Erwerbungsleiter_innen von Universitätsbibliotheken<br />
<!-- TEASER_END -->

**Inhalte**
<br />
- OA2020 & der Nationale Open-Access-Kontaktpunkt OA2020-DE<br />
- Datenanalysen für die Open-Access-Transformation<br />
- Alternative Publikationsmodelle für Open-Access-Zeitschriften<br />
- Open-Access-Ebook Pilot<br />
- Pilot Open-Access-Zeitschriftenkonsortium<br />

[Programm als PDF](/assets/files/Transformationsworkshop_UBErwerber_20180419.pdf)

Der Workshop an sich ist kostenlos, entstehende Reisekosten können nicht übernommen werden.

Hotelliste für Bielefeld: [http://www.uni-bielefeld.de/orientierung/hotels.html](http://www.uni-bielefeld.de/orientierung/hotels.html)

Der Workshop ist interaktiv gestaltet, sodass die Teilnehmerplätze begrenzt sind und nach der first come-first serve-Variante vergeben werden.<br />
Für eine Anmeldung oder weitere Informationen wenden Sie sich bitte an [alexandra.jobmann@uni-bielefeld.de](mailto:alexandra.jobmann@uni-bielefeld.de).

Wenn Sie Interesse an einem solchen Workshop haben, aber der Zeitpunkt nicht passt, informieren Sie mich bitte auch, da wir weitere Workshops planen und Sie gerne darüber informieren.

Alexandra Jobmann<br />
Universität Bielefeld<br />
Universitätsbibliothek<br />
Universitätsstrasse 25<br />
D-33615 Bielefeld<br />
<p></p>
<i class="fa fa-phone-square" aria-hidden="true"></i> +49 521 106-2546<br />
<i class="fa fa-envelope" aria-hidden="true"></i> [alexandra.jobmann@uni-bielefeld.de](mailto:alexandra.jobmann@uni-bielefeld.de)<br />
<i class="fa fa-twitter" aria-hidden="true"></i> [@oa2020de](https://twitter.com/oa2020de)<br />

