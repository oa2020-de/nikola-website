<!--
.. title: Infografik - 5 Wege zur Unterstützung der Open-Access-Transformation
.. slug: infografik
.. date: 2018-02-06 13:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

OA2020-DE präsentiert 5 Wege, mit denen Du und Deine Einrichtung die Transformation subskriptionsbasierter Zeitschriften in den Gold Open Access unterstützen können. Diese Wege sollen Dir helfen, eine Strategie für Deine Einrichtung zu entwickeln und umzusetzen.

1. Unterstütze goldenes Open-Access-Publizieren
2. Publikationsinfrastruktur
3. Analyse von Publikations- und Kostendaten
4. Alternative Publikationsmodelle
5. Vernetzung

<!-- TEASER_END -->

<img style="display:block; margin:0 auto" src="/images/5-wege-der-oa-transformation.png" alt="infografik 5 wege der open access transformation" width="500" height="800" />

Du findest die Infografik auch unter Ressourcen - [Materialien](http://oa2020-de.org/pages/materialien/).
