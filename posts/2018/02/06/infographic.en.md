<!--
.. title: Infographic - 5 ways to support the open access transformation
.. slug: infographic
.. date: 2018-02-06 13:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

OA2020-DE presents 5 ways in which you and your institution can support the transformation of subscription based journals into gold open access. These paths should help you to develop and implement a strategy for your institution.

1. Support gold open access publishing
2. Publishing infrastructure
3. Analysis of publication and cost data
4. Alternative publishing models
5. Networking

<!-- TEASER_END -->

<img style="display:block; margin:0 auto" src="/images/5-ways-of-oa-transformation.png" alt="infographic 5 ways open access transformation" width="500" height="800" />

You can find this infographic in resources - [materials](http://oa2020-de.org/en/pages/materials/).
