<!--
.. title: Offsetting und Open-Access-Transformation
.. slug: offsetting-und-transformation
.. date: 2017-08-28 00:00:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->


Beobachtet man die Verhandlungen, die sowohl Deutschland als auch Finnland zur Zeit mit Elsevier führen (siehe [hier](http://www.sciencemag.org/news/2017/08/bold-open-access-push-germany-could-change-future-academic-publishing) und [hier](https://www.kiwi.fi/pages/viewpage.action?pageId=80157628)), wird deutlich, dass der Weg über die Offsetting-Verträge nur sehr langsam zu einer Erhöhung des Open-Access-Anteils am Publikationsaufkommen beiträgt. Trotz der Kündigung bzw. Nicht-Verlängerung der Verträge mit Elsevier durch die einzelnen wissenschaftlichen [Einrichtungen](https://www.projekt-deal.de/vertragskundigungen-elsevier-2017/) der Länder, ist der Verlag anscheinend nicht bereit, ein angemessenes und nachhaltiges "Read & Publish"-Konzept vorzulegen. Ziel eines solchen Konzeptes ist die Kombination eines Lizenzvertrages mit gleichzeitigen Open-Access-Publikationsmöglichkeiten für die Angehörigen der an der Lizenz beteiligten Einrichtung, ohne zusätzliche Kosten.
<!-- TEASER_END -->
Offsetting-Modelle können sowohl national, regional, für eine Einrichtung oder als globales Konsortium verhandelt werden und es gibt sie in drei verschiedenen Varianten:

1. Eine Einrichtung zahlt im Voraus Lizenzgebühren an den Verlag. Weiterhin zahlt dieselbe Einrichtung über das Jahr verteilt APC-Gebühren für einzelne Artikel ihrer Wissenschaftler_innen. Nach Ablauf des Jahres wird ermittelt, welcher Betrag für APC-Gebühren aufgebracht wurde und dieser Betrag – meist abzüglich einer Verwaltungspauschale – von den Subskriptionsgebühren für das kommende Jahr abgezogen.
2. Eine Einrichtung zahlt zuzüglich zu Lizenzgebühren einen Aufschlag X. Dadurch werden automatisch alle Artikel von Wissenschaftler_innen dieser Einrichtung Open Access gestellt.
3. Eine Einrichtung zahlt anstatt Lizenzgebühren einen Grundbetrag (Reading Fee, Membership Fee o.ä.) und verpflichtet sich zusätzlich zur Einreichung einer bestimmten Anzahl von Artikeln und zur Zahlung von APC Gebühren für diese Artikel ([Quelle](http://arbeitskreis-bibliotheken-informationseinrichtungen.inm-gmbh.de/wp-content/uploads/sites/2/2016/06/2016_05_24_Leibniz_Lizenztag_Offsetting_poeche.pdf)).

Offsetting ist daher nur ein Übergangsmodell auf dem Weg zur vollständigen Open-Access-Transformation. Ein solches Übergangsmodell ist wichtig, da die Erfahrungen, die damit gesammelt werden, die Grundlage für die Entwicklung eines praktikablen und nachhaltigen Pay-as-you-publish-Geschäftsmodells liefern und aufzeigen. Diese Zielsetzung wurde u.a. in dem [Joint-Understanding-of-Offsetting-Paper](http://esac-initiative.org/wp-content/uploads/2016/05/esac_offsetting_joint_understanding_offsetting.pdf) der ESAC-Initiative verabredet.

Die Open-Access-Transformation, also die Umwandlung subskriptionsbasierter wissenschaftlicher Zeitschriften in ein nachhaltiges, auf Publikationszahlen basierendes Open-Access-Modell, erhält eine besondere Bedeutung, da auch 15 Jahre nach der [Budapest-Erklärung](http://www.budapestopenaccessinitiative.org/read) der Gold-Open-Access-Anteil bei den 10 publikationsstärksten Ländern weltweit nur zwischen 8% und 13% liegt (siehe dazu den im INTACT-Projekt entstandenen [Forschungsbericht](http://doi.org/10.13140/RG.2.2.33235.89120) „Publikationen in Gold-Open-Access-Journalen auf globaler und europäischer Ebene sowie in Forschungsorganisationen“). In den Jahren 2008 – 2016 ist in den publikationsstärksten Ländern der Gold-Open-Access-Anteil nur um ca. 1% pro Jahr gestiegen (siehe [Tabelle 2.1](http://doi.org/10.13140/RG.2.2.33235.89120)). Dies kann mehrere Gründe haben, die näher untersucht werden müssen. Ein paar Hypothesen dazu von unserer Seite:

* Viele, vor allem der High-Impact-Journale, stehen nicht als Gold Open Access zur Verfügung, sondern bieten nur ein Hybrid-Modell an. Es sind also noch nicht genügend Gold-OA-Zeitschriften vorhanden.
* An vielen wissenschaftlichen Einrichtungen fehlt derzeit eine institutionelle Unterstützung für das kostenpflichtige Open-Access-Publizieren.
* Wissenschaftliche Einrichtungen und Bibliotheken, die z.B. im Rahmen des [DFG-Förderprogramms](http://www.dfg.de/formulare/12_20/12_20_de.pdf) „Open Access Publizieren“ bereits Open-Access-Publikationsfonds aufgebaut haben, verfügen derzeit nur über begrenzte Mittel zur Finanzierung des kostenpflichtigen Open-Access-Publizierens.

Der konsequenten Open-Access-Transformation kommt insbesondere vor dem Hintergrund des von der EU propagierten Ziels, bis [2020](https://dx.doi.org/10.1126/science.aag0577) alle wissenschaftlichen Publikationen sofort im Open Access verfügbar zu machen, eine wesentliche Bedeutung zu.
