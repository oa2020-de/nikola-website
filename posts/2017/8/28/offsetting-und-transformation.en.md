<!--
.. title: Offsetting and Open Access Transition
.. slug: offsetting-und-transformation
.. date: 2017-08-28 00:00:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

If one observes the negotiations that are currently being conducted with Elsevier by both Germany and Finland (see [here](http://www.sciencemag.org/news/2017/08/bold-open-access-push-germany-could-change-future-academic-publishing) and [here](https://www.kiwi.fi/pages/viewpage.action?pageId=80157628)), it becomes clear that the path via the offsetting agreements is only very slowly contributing to an increase in the share of open access in the volume of publications.
Despite the termination or non-renewal of the contracts with Elsevier by the individual scientific institutions of the [countries](https://www.projekt-deal.de/vertragskundigungen-elsevier-2017/), the publisher does not seem prepared to present an adequate and sustainable "read & publish" concept. The aim of such a concept is the combination of a license agreement with simultaneous open access publication possibilities for the members of the institution involved in the license, without additional costs.
<!-- TEASER_END -->
Offsetting models can be negotiated both nationally, regionally, for an institution or as a global consortium and they come in three different variants:

1. An institution pays license fees in advance to the publisher. Furthermore, the same facility pays APC fees for individual articles of its scientists distributed throughout the year. At the end of the year, the amount of APC fees will be deducted and this amount will be deducted from the subscription fees for the following year, usually less an administration fee.
2. An institution pays an additional fee in addition to royalties. As a result, all articles of this institution's scientists will be open-accessed automatically.
3. An institution pays royalties rather than a basic amount (Reading Fee, Membership Fee, etc.) and undertakes in addition to file a certain number of articles and to pay APC fees for these items ([Source](http://arbeitskreis-bibliotheken-informationseinrichtungen.inm-gmbh.de/wp-content/uploads/sites/2/2016/06/2016_05_24_Leibniz_Lizenztag_Offsetting_poeche.pdf)).

Offsetting is therefore only a transitional model on the way to full open access transformation. Such a transitional model is important because the experience gained with it will provide the foundation for developing a workable and sustainable pay-as-you-publish business model. This objective has been agreed, inter alia, in the [Joint Understanding of Offsetting Paper](http://esac-initiative.org/wp-content/uploads/2016/05/esac_offsetting_joint_understanding_offsetting.pdf) of the ESAC initiative.

The Open Access transformation, ie the transformation of subscription-based scientific journals into a sustainable, publication-based open access model, is of particular importance, as it is also 15 years after the [Budapest Declaration](http://www.budapestopenaccessinitiative .org/read), the gold open access share is only between 8% and 13% in the ten most frequently publishing countries worldwide. (see also the research report of the INTACT [project](http://doi.org/10.13140/RG.2.2.33235.89120) „Publikationen in Gold-Open-Access-Journalen auf globaler und europäischer Ebene sowie in Forschungsorganisationen“). In the years 2008 - 2016, the gold-open access share rose by only around 1% per year in the most frequently publishing countries (see [Table 2.1](http://doi.org/10.13140/RG.2.2.33235.89120)). There may be several reasons for this, which need to be examined more closely. A few hypotheses from our side:

* Many, especially the high-impact journals, are not available as Gold Open Access, but offer only a hybrid model. So there are not enough gold OA journals available yet.
* Many scientific institutions currently lack institutional support for paid Open Access publishing.
* Scientific institutions and libraries that have already set up open access publishing funds as part of the DFG funding program ["Open Access Publishing"](http://www.dfg.de/formulare/12_20/12_20_de.pdf) currently have limited funds to finance paid open access publishing.

The consistent open access transformation is of particular importance against the background of the EU's propagated goal of making all scientific publications available in open access by [2020](https://dx.doi.org/10.1126/science.aag0577).
