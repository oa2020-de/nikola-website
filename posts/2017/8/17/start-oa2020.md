<!--
.. title: Start des Nationalen Open-Access-Kontaktpunkts OA2020-DE
.. slug: start-oa2020
.. date: 2017-08-17 00:00:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

### Hintergrund
Die Allianz der deutschen Wissenschaftsorganisationen hat im Rahmen der Schwerpunktinitiative Digitale Information einen „Nationalen Open-Access-Kontaktpunkt (OA2020-DE)“ für Deutschland eingerichtet. Ziel des Projekts ist das Schaffen von Voraussetzungen für eine großflächige Umsetzung der Open-Access-Transformation wissenschaftlicher Zeitschriften, wie sie die [OA2020-Initiative](https://oa2020.org) auf globaler Ebene verfolgt. Der Fokus der Transformation richtet sich darauf, dass die heute bereits vorhandenen und im Subskriptionsmodell vertriebenen elektronischen Zeitschriften künftig nach einem Open-Access-basierten Geschäftsmodell anzubieten. Hier finden sich auch Überschneidungen zum ebenfalls von der Allianz geförderten [DEAL-Projekt](www.projekt-deal.org).
<!-- TEASER_END -->
Der Kontaktpunkt OA2020-DE ist Teil eines sich etablierenden internationalen Netzwerks nationaler Open-Access-Kontaktpunkte, arbeitet im Rahmen der Schwerpunktinitiative [Digitale Information](http://www.allianzinitiative.de/) der Allianz der deutschen Wissenschaftsorganisationen und kooperiert eng mit den deutschen Hochschulen, außeruniversitären Forschungseinrichtungen, Konsortien und Verlagen.

### Ziele
Wesentliches Ziel des Projektes ist es, weitere Wissenschaftseinrichtungen auf dem Weg der Open-Access-Transformation durch verbesserte Datenerhebung zu Publikationsaufkommen und -kosten zu unterstützen. Weiterhin sollen die Verhandlungsstrategien gegenüber den Wissenschaftsverlagen konzeptionell weiterentwickelt sowie die dabei gewonnenen Erfahrungen aus Deutschland in die internationale Diskussion getragen werden.
Die Arbeit des Nationalen Open-Access-Kontaktpunktes OA2020-DE dient der Vorbereitung der Transformation wissenschaftlicher Zeitschriften hin zu Open Access, daher ist das Wirken darauf ausgerichtet, die Beteiligung einer möglichst großen Zahl von Wissenschaftseinrichtungen in Deutschland an der Transformationsinitiative zu ermöglichen. Die eigentliche Umsetzung der Transformation soll dann unmittelbar und direkt in den einzelnen Wissenschaftseinrichtungen organisiert werden.
Mit der OA2020-Initiative liegt eine konkrete Roadmap für den Transformationsprozess vor. Durch Unterzeichnen der ["Expression of Interest"](https://oa2020.org/mission/) können alle Wissenschaftseinrichtungen ihre Unterstützung der Transformationsinitiative zum Ausdruck bringen.
