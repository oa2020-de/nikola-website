<!--
.. title: Start of the National Contact Point Open Access OA2020-DE
.. slug: start-oa2020
.. date: 2017-08-17 00:00:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

### Background
The Alliance of German Science Organizations has established a "National Contact Point Open Access (OA2020-DE)" for Germany as part of the Digital Information Priority Initiative. The aim of the project is to create conditions for a large-scale implementation of the open access transition of scientific journals, as pursued by the [OA2020 Initiative](https://oa2020.org) on a global level. The focus of the transition is to offer electronic journals that are already available today and distributed in the subscription model according to an open-access-based business model. This also includes overlaps with the Alliance-funded [DEAL project](www.projekt-deal.org).
<!-- TEASER_END -->
The contact point OA2020-DE is part of an established international network of National Contact Points Open Access, works within the framework of the Priority Initiative [Digital Information](http://www.allianzinitiative.de/) of the Alliance of German Science Organizations and cooperates closely with the German universities, non-university research institutions, consortia and publishers.

### Aims
The main goal of the project is to support further scientific institutions on the path of open access transformation through improved data collection on publication volumes and costs. Furthermore, the negotiation strategies in relation to the scientific publishers should be further developed in as well as the experience gained from Germany are carried in the international debate.
The work of the National Contact Point Open Access OA2020-DE serves to prepare the transition of scientific journals into open access, so the work is aimed at enabling the participation of the largest possible number of scientific institutions in Germany in the transformation initiative. The actual implementation of the transformation will then be organized immediately and directly in the various scientific institutions.
The OA2020 initiative provides a concrete roadmap for the transition process. By signing up to the ["Expression of Interest"](https://oa2020.org/mission/), all academic institutions can express their support for the transformation initiative.
