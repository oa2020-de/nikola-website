<!--
.. title: OA2020-DE – What to do with funds after subscriptions with Elsevier are cancelled?
.. slug: oa2020-en
.. date: 2017-10-10 17:00:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->
At the start of 2017, fifty German universities and libraries cancelled their license agreements with Elsevier, and a further 90 or so have announced that they, too, will let their agreements [expire](https://www.projekt-deal.de/vertragskundigungen-elsevier-2017/) at the end of 2017. As allotted funds in subscription budgets must be employed or lost, many librarians in Germany are faced with the decision of how best to use the monies liberated from their Elsevier deals.

**OA2020-DE, the German constituency of the Open Access 2020 Initiative, proposes that institutions seize the funds that were destined to Elsevier renewals and reinvest them, at least in part, in publishing initiatives that support the open access transformation.** The proposal is in line with the [Recommendations](http://doi.org/10.3249/allianzoa.011) for an Open Access Transformation, issued by the Ad hoc Gold OA Working Group of the Alliance of German Scientific Organizations, and the overall strategy of OA2020.
<!-- TEASER_END -->

Academic institutions and libraries who have been looking at piloting an institutional fund to support Open Access publishing could take this opportunity and channel the liberated subscription monies to establishing such a fund. Or if such a fund already exists, this would be an opportunity to replenish and expand it.

Libraries could also consider investing in OA publishing initiatives such as [SciPost Physics](https://scipost.org/), [SCOAP3](https://scoap3.org/), [Knowledge Unlatched](http://www.knowledgeunlatched.org/), [LingOA](http://www.lingoa.eu/), [Open Library of Humanities](https://www.openlibhums.org/), and [F1000](https://f1000.com/), as well as supporting publication of their researchers in journals by a whole range of pure open access publishers ([Copernicus](https://www.copernicus.org/), [Hindawi](https://www.hindawi.com/), [Frontiers](https://www.frontiersin.org/), [MDPI](http://www.mdpi.com/), [PLOS](https://www.plos.org/), etc.).

Using funds freed from Elsevier deals to support OA initiatives not only gives libraries an opportunity to take a lead role in the Open Access transformation, it gives them a chance to pilot emerging business models, create and test new workflows, collaborate more closely with faculty in the research cycle, and have direct impact on the visibility of the scientific publications of their own institution.

OA2020-DE is currently setting up an advisory service to support academic and research institutions who want to get a clear picture of their own publication volume and the costs associated with the subscription and / or distribution of scholarly outputs. Further information on the current status of Open Access in German institutions can be found in the [research report](http://doi.org/10.13140/RG.2.2.33235.89120) "Publications in gold open access journals on a global and European level as well as in research organizations" issued by the [INTACT project](https://intact-project.org/).

OA2020-DE is the coordinating body of some 23 German organizations that have endorsed OA2020 to date, including the HRK (German University Rectors Conference) and the DfG (German Research Foundation).
