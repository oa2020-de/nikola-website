<!--
.. title: Divest of subscriptions, invest in Open Access!
.. slug: divest-subscription
.. date: 2017-10-25 08:00:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

*Divest of subscriptions, invest in Open Access!*<br />
*Open in order to innovate! For [Open Access Week 2017](http://www.openaccessweek.org/), OA2020 launches new website with a call to libraries worldwide to divest of the traditional subscription model in order to finance innovative, sustainable business and publishing models.*

This principle is the foundation of the Open Access 2020 Initiative, and we are encouraged to note it is now also the underlying strategy of an increasing number of new Open Access initiatives with whom we share the common vision of making open the default in scholarly communications. In support of our mutual goal, we are excited to announce the launch of our newly expanded website, [OA2020.org](https://oa2020.org), where libraries and institutions can find practical information, best practice and resources to plan and execute their own transformational roadmaps that address local needs and have impact in the drive for Open Access at a global scale.

To the full [press release](https://oa2020.org/2017/10/23/divest-of-subscriptions-invest-in-open-access/).
