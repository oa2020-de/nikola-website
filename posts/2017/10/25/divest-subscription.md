<!--
.. title: Divest of subscriptions, invest in Open Access!
.. slug: divest-subscription
.. date: 2017-10-25 08:00:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

*DAS SUBSKRIPTIONSWESEN BEENDEN, IN OPEN ACCESS INVESTIEREN!*<br />
*Open in order to innovate! Im Rahmen der internationalen Open Access Woche 2017, startet die Initiative OA2020 ihren neuen Webauftritt mit einem Apell an Wissenschaftseinrichtungen und Bibliotheken weltweit zur Beendigung des traditionellen Subskriptionswesens und zur Reinvestition der in den Subskriptionen gebundenen Finanzmittel in innovative und nachhaltige Open-Access-Modelle*

Das ist die Grundidee von Open Access 2020. Angesichts einer wachsenden Zahl neuer Open-Access-Initiativen, die die gemeinsame Vision teilen, Open Access zum Standard in der Wissenschaftskommunikation zu machen, sieht sich die Initiative darin bestätigt. OA2020 freut sich, rechtzeitig zur [internationalen Open Access Woche 2017](http://www.openaccessweek.org/) den Start der überarbeiteten und erweiterten Website [OA2020.org](https://oa2020.org) bekannt geben zu können, die wissenschaftlichen Institutionen und Bibliotheken praktische Informationen, Arbeitshilfen und weitere Ressourcen zur Planung und Umsetzung eigener, lokaler Transformationsstrategien bietet, um so den globalen Schub in Richtung Open Access zu befördern.

Zur vollständigen [Pressemitteilung](https://oa2020.org/wp-content/uploads/OA2020_20171025_DE.pdf).
