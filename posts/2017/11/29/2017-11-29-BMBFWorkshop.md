<!--
.. title: Bericht vom BMBF-Workshop zu Open Access am 21.11.2017
.. slug: BMBFWorkshop
.. date: 2017-11-29 16:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

>„Um Open Access zu einem Standard des wissenschaftlichen Publizierens zu machen, bedarf es einer gemeinsamen Anstrengung aller Akteure in der Wissenschaftslandschaft. Das Bundesministerium für Bildung und Forschung wird daher einen strukturierten Diskurs zur Einführung von Open Access starten.“

Dieses Vorhaben zum Anlass genommen, hat das BMBF am 21.11.2017 Praxisexpert_innen aus dem Open-Access-Kontext zu einem Informations- und Austauschworkshop eingeladen. Ziel war zum einen festzuhalten, was die aktuellen Diskussionspunkte im Bereich Open Access sind und gleichzeitig eine stärkere Vernetzung der Akteure zu ermöglichen. Zum anderen enthält die Open-Accesss-Strategie des BMBFs den Plan, eine Nationale Kompetenz- und Vernetzungsstelle einzurichten:
<!-- TEASER_END -->

>„Das Bundesministerium für Bildung und Forschung wird den Aufbau einer Nationalen Kompetenz- und Vernetzungsstelle fördern. Diese wird eine zentrale Rolle beim Austausch und der Vernetzung der Verantwortlichen aus Ländern, Hochschulen und Forschungseinrichtungen einnehmen. Sie wird mit Schulungen und Fortbildungen die Kompetenzen der Multiplikatoren vor Ort verbessern. An Einrichtungen, an denen es keine Ansprechpartner gibt, wird die Kompetenz- und Vernetzungsstelle mit Materialien und Beratung den Aufbau der entsprechenden Expertise unterstützen.“
 (Quelle: [Open Access in Deutschland: Die Strategie des Bundesministeriums für Bildung und Forschung](https://www.bmbf.de/pub/Open_Access_in_Deutschland.pdf))

Hier wurde explizit auf die Erfahrungen aus der alltäglichen Praxis der anwesenden Open-Access-Expert_innen gesetzt, um zu ermitteln, welche Fragestellungen die Wissenschaftler_innen beim Publizieren beschäftigen und zu welchen Themen Informationen und Hilfestellungen zentral bereit gestellt werden können und sollen. In der anschließenden Diskussionsrunde wurden auch weitergehende Punkte angesprochen:

* Die Fachgesellschaften als Partner im elektronischen Publizieren und als Multiplikatorengruppe wurden bisher zu wenig beachtet.
* Die wissenschaftlichen Einrichtungen müssen deutlich machen, dass Open-Access-Publikationen den gleichen Stellenwert haben, wie „klassische Publikationen“ und mit einem guten Beispiel voran gehen und die eigenen Publikationen Open Access veröffentlichen.
* Möglicherweise brauchen wir eine Berliner Erklärung 2.0, das heißt es sollten konkrete Wege für eine Umsetzung der Berliner Erklärung festgelegt werden. Politisch unterstützt zum Beispiel durch die Vergabe von Preisen (siehe dazu den [Open-Science-Preis](http://www.schleswig-holstein.de/DE/Landesregierung/III/Presse/PI/2017_neu/September_2017/III_OpenScience.html) des Landes Schleswig-Holstein).
* Auch eine Aufklärung der Herausgeber_innen scheint notwendig. Ihnen ist oft nicht bewusst, dass die „Marke“ Zeitschrift am Titel hängt und sie also die Rechte am Titel behalten und nicht an den Verlag abtreten sollten. Das ist vor allem relevant, wenn sie bei einer Transformation in Open Access den Verlag bzw. die Plattform wechseln.
* Eine stärkere Positionierung des BMBFs aber auch der Länder bezüglich proprietärer oder offener Systeme und eine Ausweitung der Kooperation zwischen Bund und Ländern werden gewünscht.
* Für den Aufbau von Infrastrukturen sollte es einen Kriterienkatalog geben, der Open Source Lösungen und öffentliche Einrichtungen als Betreiber festschreibt sowie Langfristigkeit und Nachhaltigkeit garantiert.

Zum Schluss wurden in einem moderierten Brainstorming Ideen und Antworten zu folgenden Fragestellungen gesammelt: Welchen Informationsbedarf gibt es im Bereich Open Access? / Welche Open Access Aktivitäten sollten an einer Einrichtung stattfinden? / Welche Aktivtäten werden von einer zentralen Open Access Stelle erwartet? Dazu wurden Karten auf Pinnwände gespickt, auch um zu ermitteln, welche Punkte mehrfach genannt werden und somit einen Schwerpunkt setzen. Die abfotografierten Ergebnisse kann man u.a. [hier](https:/twitter.com/EllenEuler/status/932989375813693440) einsehen.
