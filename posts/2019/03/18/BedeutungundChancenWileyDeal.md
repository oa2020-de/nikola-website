<!--
.. title: Bedeutung und Chancen des DEAL-Wiley-Vertrags für die Open-Access-Transformation
.. slug: bedeutung_chancen_wileydeal_openaccesstransformation
.. date: 2019-03-18 8:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## Bedeutung und Chancen des DEAL-Wiley-Vertrags für die Open-Access-Transformation

Mit dem am 15. Januar 2019 unterzeichneten Wiley-Vertrag hat das [Allianzprojekt DEAL](https://www.projekt-deal.de/) wesentliche Projektziele erreicht:

+ die Verbesserung der Informationsversorgung für teilnehmende wissenschaftliche Einrichtungen durch Ausweitung des lesenden Zugriffs auf alle Zeitschriften des Verlags Wiley,
+ die Ausweitung der Open-Access-Publikationsmöglichkeiten in den Wiley-Hybrid- und Gold-Open-Access-Zeitschriften für die Autor_innen der teilnehmenden Einrichtungen.

Wir empfehlen allen teilnahmeberechtigten Einrichtungen (siehe „Welche Einrichtungen können teilnehmen?" in den [DEAL-Wiley-FAQ](https://www.projekt-deal.de/faq-wiley-vertrag/)) die Unterzeichnung des Sign-Up-Letters und der Teilnahmeerklärung bis zum 18.04.2019. Der DEAL-Wiley-Vertrag markiert nicht nur den Einstieg in die großflächige Open-Access-Transformation in Deutschland, er bietet vor allem den Wissenschaftler_innen von teilnehmenden Einrichtungen deutliche Vorteile:
<!-- TEASER_END -->

+ den lesenden Zugang zu ALLEN Zeitschriften des Wiley-Verlags,
+ die zusätzliche Möglichkeit des kostenfreien Open-Access-Publizierens in der überwiegenden Mehrzahl der Hybrid- sowie der Gold-Open-Access-Zeitschriften von Wiley,
+ erhöhte Sichtbarkeit und einfache Nachnutzbarkeit der Publikationen aus teilnehmenden Einrichtungen in Wiley-Zeitschriften unter Wahrung der Rechte der Autor_innen an ihren Artikeln,
+ kein Verwaltungsaufwand für artikelbezogene Open-Access-Publikationsgebühren für Autor_innen, da die Kosten für das Open-Access-Publizieren von den Bibliotheken der teilnehmenden Einrichtungen übernommen werden.

Der DEAL-Wiley-Vertrag ermöglicht eine deutliche Erhöhung des Open-Access-Anteils von Artikeln in Wiley-Hybridzeitschriften bei gleichzeitiger Förderung und Vereinfachung des Publizierens in Wiley-Gold-Open-Access-Zeitschriften. Er bietet weiterhin für die Bibliotheken die Chance, die Dysfunktionalitäten des Subskriptionssystems zu überwinden, in dem derzeit Autor_innen das Copyright an ihren Artikeln abgeben und Bibliotheken ihrem Auftrag der Informationsversorgung nicht mehr nachkommen können.

<img src="/images/blog_deal_grafik1_de.PNG" alt="Grafische Darstelllung Subskriptionsmodell" width="700" height="390" display="block" />

Der DEAL-Wiley-Vertrag verändert diese Situation. Gegen eine moderate Steigerung der bisherigen Subskriptionsausgaben für Bibliotheken und durch Einführung der MPDL Services GmbH als neuen intermediären Akteur wird der lesende Zugriff auf alle Zeitschriften sowie die Open-Access-Stellung der Zeitschriftenartikel von Autor_innen der teilnehmenden Einrichtungen ermöglicht.

<img src="/images/blog_deal_grafik2_de.PNG" alt="Grafische Darstelllung DEAL-Wiley-Vertrag" width="700" height="390" display="block" />

Den Allianzprojekten DEAL und Nationaler Open-Access-Kontaktpunkt OA2020-DE ist bewusst, dass sich gerade für Bibliotheken als einen der zentralen Akteure im wissenschaftlichen Publikationssystem mit dem Einstieg in die Transformation des Subskriptionssystems neue Herausforderungen ergeben, z.B. hinsichtlich der Kostenverteilung innerhalb ihrer Einrichtungen, des Reportings, oder neuer Geschäftsgänge bei den Open-Access-Publikationsfonds. Wir sind jedoch zuversichtlich, dass mit dem Betreten eines dreijährigen Transformationskorridors diese Herausforderungen gemeistert werden können. **DEAL und der Nationale Open-Access-Kontaktpunkt OA2020-DE werden die Bibliotheken der teilnehmenden wissenschaftlichen Einrichtungen bei diesem Transformationsprozess durch entsprechende Hilfestellungen und Workshops begleiten und unterstützen.**
