<!--
.. title: Meaning and Opportunities of the DEAL-Wiley Contract for the Open-Access Transformation
.. slug: meaning_opportunities_wileydeal_openaccesstransformation
.. date: 2019-03-18 8:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## Meaning and Opportunities of the DEAL-Wiley Contract for the Open-Access Transformation

With the signing of the Wiley contract on 15 January 2019, the [alliance project DEAL](https://www.projekt-deal.de/about-deal/) has achieved key project goals:

+ to improve the provision of information to participating academic institutions by extending reading access to all the journals published by Wiley,
+ the expansion of open-access publishing in the Wiley hybrid and gold open-access journals for the authors of the participating institutions.

We recommend that all eligible institutions (see "What institutions can participate?" in the [DEAL Wiley FAQs](https://www.projekt-deal.de/faq-wiley-vertrag/)) sign the sign-up letter and the participation statement by 18 April 2019. The DEAL-Wiley contract marks not only the entry into the large-scale open-access transformation in Germany, it offers above all the scientists of participating institutions clear advantages:
<!-- TEASER_END -->

+ the reading access to ALL journals published by Wiley,
+ the additional option of free open-access publishing in the vast majority of Wiley's hybrid and gold open-access journals,
+ increased visibility and easy reusability of publications from participating institutions in Wiley journals while preserving the rights of authors to their articles,
+ no administrative burden on article-related open-access publication fees for authors, as the costs of open-access publishing are borne by the libraries of the participating institutions.

The DEAL-Wiley agreement enables a significant increase in the open-access share of articles in Wiley hybrid journals while promoting and simplifying publication in Wiley gold open-access journals. It also offers libraries the opportunity to overcome the dysfunctionalities of the subscription system, where authors are currently surrendering copyrights to their articles and libraries are no longer able to fulfil their mission of providing information.

<img src="/images/blog_deal_grafik1_en.PNG" alt="graphical representation subscription model" width="700" height="390" display="block" />

The DEAL-Wiley contract changes this situation. Against a moderate increase of the previous subscription expenses for libraries and the introduction of MPDL Services GmbH as a new intermediary player, reading access to all journals as well as open-access position of the journal articles will be made possible by authors of the participating institutions.

<img src="/images/blog_deal_grafik2_en.PNG" alt="graphical representation deal-wiley contract" width="700" height="390" display="block" />

The alliance projects DEAL and National Contact Point Open Access OA2020-DE are aware that libraries, as one of the key players in the scientific publishing system, are beginning to face new challenges in the transformation of the subscription system, e.g. regarding the distribution of costs within their facilities, reporting, or new business operations in the open-access publishing funds. However, we are confident that these challenges can be mastered by entering a three-year transformation corridor. **DEAL and the National Contact Point Open Access OA2020-DE will assist and support the libraries of the participating institutions in this transformation process through appropriate assistance and workshops.**
