<!--
.. title: 1st DEAL done – All German research articles in Wiley journals to be published open access under new transformative agreement
.. slug: firstdealdone_wiley
.. date: 2019-01-15 13:00:00+1:00
.. author:
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

*(This blog post was published first by Kai Geschuhn at the [ESAC blog](http://esac-initiative.org/1st-deal-done-all-german-research-articles-in-wiley-journals-to-be-published-open-access-under-new-transformative-agreement/)).*

Germany’s Projekt DEAL and the publisher John Wiley & Sons have entered a ground-breaking transformative agreement, in line with the objectives of the Open Access 2020 initiative.

Under this new agreement, all authors affiliated with 700 academic institutions in Germany will retain copyright and their accepted articles will be published open access in Wiley journals. Almost 10,000 articles by German researchers are published a year in Wiley journals, constituting around 9% of the publisher’s total output. The agreement also grants students and faculty read access to the full Wiley journal portfolio including backfiles starting with 1997. The national-level agreement is based on a “publish and read” model in which fees are paid by institutions—not for subscriptions but for open access publishing services.

The agreement will be made public in a month’s time and an English-language FAQ will be released by the Projekt DEAL working group.

Read the official [press release](https://www.hrk.de/press/press-releases/press-release/meldung/wiley-and-projekt-deal-partner-to-enhance-the-future-of-scholarly-research-and-publishing-in-germany/).
