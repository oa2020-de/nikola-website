<!--
.. title: Erster DEAL abgeschlossen - Wissenschaftliche Publikationen aus Deutschland werden künftig Open Access bei Wiley publiziert
.. slug: ersterdealabgeschlossen_wiley
.. date: 2019-01-15 13:00:00+1:00
.. author:
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Das deutsche Projekt DEAL und der Verlag John Wiley & Sons haben eine zukunftsorientierte Partnerschaft zur Erprobung neuer Publikationsmodelle geschlossen, ganz im Sinne der Open-Access-Transformation und der Prinzipien der OA2020-Initiative.

Nach dieser Vereinbarung behalten die affiliierten Autor_innen der beteiligten rund 700 wissenschaftlichen Einrichtungen in Deutschland alle Rechte und die akzeptierten Artikel werden Open Access in den Wiley-Zeitschriften publiziert. Bisher wurden pro Jahr fast 10.000 Artikel von deutschen Forscherinnen und Forschern bei Wiley publiziert, das sind rund 9% des gesamten Publikationsaufkommens des Verlages. Die Vereinbarung gewährt den Beteiligten weiterhin lesenden Zugriff für das gesamte Zeitschriftenportfolio rückwirkend bis 1997.
Diese "Nationallizenz" basiert auf dem sogenannten Publish & Read-Modell, bei dem der finanzielle Beitrag durch die beteiligten wissenschaftlichen Einrichtungen geleistet wird - aber eben nicht für Subskriptionen sondern für Open-Access-Publikationsservices.

Die Vereinbarung wird Mitte Februar veröffentlicht und um eine englischsprachige FAQ durch die DEAL Arbeitsgruppe ergänzt.

Lesen Sie dazu auch die offiziellen Pressemitteilungen der [HRK](https://www.hrk.de/presse/pressemitteilungen/pressemitteilung/meldung/wiley-und-projekt-deal-unterzeichnen-einigung-4493/) und von [Wiley](https://newsroom.wiley.com/press-release/all-corporate-news/wiley-and-projekt-deal-partner-enhance-future-scholarly-research-an).
