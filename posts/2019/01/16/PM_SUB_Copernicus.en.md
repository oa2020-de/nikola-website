<!--
.. title: SUB Göttingen and Copernicus Publications will start the National Consortium for Central Payment Processing of Article Processing Charges on 1.1.2019
.. slug: APC_consortium_copernicus_SUB
.. date: 2019-01-16 08:00:00+1:00
.. author:
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## Press Release

**Göttingen.** 14 scientific institutions have joined the national opt-in consortium for the central payment processing of APCs for articles of all journals of the open-access publishing house Copernicus Publications. This is an important signal for the open-access community and libraries because it makes it clear that open-access transformation – in this case of acquisition funds – includes open-access publishers.

Based on the concept of the National Contact Point Open Access OA2020-DE, the Lower Saxony State and University Library Göttingen and the publisher Copernicus Publications have jointly developed and set up the consortium. The term is for two years (2019–2020), and eligible are all universities and scientific institutions that are also eligible for participation in a national or alliance licence.

The consortium enables corresponding authors affiliated with the participating institutions to publish free of charge for them in the 43 gold open-access journals in the fields of geography, engineering and life sciences for two years. The articles are financed by a prepayment deposited at the beginning of each year. They will be published under a CC-BY license, including the automatic delivery of the repositories of the consortium participants with the XML metadata and the full-text PDF.


**Contact:**

Lower Saxony State and University Library Göttingen, Platz der Göttinger Sieben 1, 37073 Göttingen, Germany; [nationallizenzen@sub.uni-goettingen.de](mailto:nationallizenzen@sub.uni-goettingen.de)

Copernicus Publications, Bahnhofsallee 1e, 37081 Göttingen, Germany; [publications@copernicus.org](mailto:publications@copernicus.org)


[Press Release](/assets/files/PM_Copernicus_SUB_en.pdf)

