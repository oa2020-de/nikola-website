<!--
.. title: SUB Göttingen und Copernicus Publications starten zum 01.01.2019 ein Nationalkonsortium zur zentralen Zahlungsabwicklung von Article Processing Charges
.. slug: APC_consortium_copernicus_SUB
.. date: 2019-01-16 08:00:00+1:00
.. author:
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## Pressemitteilung

**Göttingen.** 14 wissenschaftliche Einrichtungen sind dem nationalen Opt-in-Konsortium für die zentrale Zahlungsabwicklung von APCs für Artikel aller Zeitschriften des Open-Access-Verlags Copernicus Publications beigetreten. Die ist ein wichtiges Signal für die Open-Access-Community und die Bibliotheken, denn es macht deutlich, dass die Open-Access-Transformation – in diesem Falle von Erwerbungsmitteln – auch die Open-Access-Verlage mit einschließt.

Basierend auf dem Konzept des Nationalen Open-Access-Kontaktpunkts OA2020-DE wurde das Konsortium gemeinsam durch die Niedersächsische Staats- und Universitätsbibliothek Göttingen und den Verlag Copernicus Publications verhandelt und organisiert. Die Laufzeit beträgt zwei Jahre (2019-2020); teilnahmeberechtigt sind alle Hochschulen und wissenschaftlichen Einrichtungen, die auch berechtigt für die Teilnahme an einer National- oder Allianzlizenz sind.

Das Konsortium ermöglicht den Corresponding authors, die mit den teilnehmenden Einrichtungen affiliiert sind, in den 43 Gold-Open-Access-Zeitschriften aus den Geo-, Ingenieurs- und Lebenswissenschaften zwei Jahre lang für sie persönlich kostenfrei zu publizieren. Finanziert werden die Artikel über eine zum jeweiligen Kalenderjahr hinterlegte Vorauszahlung. Veröffentlicht werden sie unter einer CC-BY-Lizenz, inklusive der automatischen Belieferung der Repositorien teilnehmender Einrichtungen mit den XML-Metadaten und dem Volltext-PDF.


**Kontakt: **

Niedersächsische Staats- und Universitätsbibliothek Göttingen, Platz der Göttinger Sieben 1, 37073 Göttingen; [nationallizenzen@sub.uni-goettingen.de](mailto:nationallizenzen@sub.uni-goettingen.de)

Copernicus Publications, Bahnhofsallee 1e, 37081 Göttingen; [publications@copernicus.org](mailto:publications@copernicus.org)


[Pressemitteilung](/assets/files/PM_Copernicus_SUB.pdf)




