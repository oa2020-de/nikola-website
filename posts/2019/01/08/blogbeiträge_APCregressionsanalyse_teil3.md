<!--
.. title: APCs – Spiegel des Impact-Factors oder Erbe des Subskriptionsmodells? Beschreibende Statistiken.
.. slug: APCregressionsanalyse_beschreibendestatistik
.. date: 2019-01-08 10:00:00+1:00
.. author: Nina Schönfelder
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Dieser Blogbeitrag ist Teil einer Reihe, die die Studie [„APCs – Spiegel des Impact-Factors oder Erbe des Subskriptionsmodells?“](https://dx.doi.org/10.4119/unibi/2931061) von OA2020-DE erläutert.

Während wir im [letzten Blogbeitrag](/blog/2018/12/10/APCregressionsanalyse_Datenbasis/) die interessierenden Variablen (APCs, SNIP etc.) einzeln betrachtet haben, untersuchen wir nun die Beziehung der Variablen untereinander mit einfachen statistischen Maßen. Finden wir eine Korrelation zweier Variablen bedeutet es jedoch nicht zwingend, dass auch ein kausaler Zusammenhang (ggf. in dieser Stärke) besteht. Dies kann erst die Regressionsanalyse bestätigen, die im nächsten Blogbeitrag diskutiert wird.
<!-- TEASER_END -->

## Zusammenhang zwischen dem SNIP-Wert und der Höhe der APCs

Als erstes wird der Zusammenhang zwischen den tatsächlich gezahlten APCs zur Veröffentlichung von Artikeln in Fachzeitschriften und dem jeweiligen „Source Normalized Impact per Paper“ (SNIP) – einem Maß, das angibt wie oft ein Artikel aus der betreffenden Zeitschrift in den letzten drei Jahren durchschnittlich zitiert worden ist – auf Artikelebene anhand eines Streupunktdiagramms dargestellt. Auf den beiden Achsen des Koordinatensystems ist die Höhe des SNIP bzw. der Artikelbearbeitungsgebühr abzulesen. Jeder einzelne Punkt stellt einen Artikel dar, für dessen Veröffentlichung eine APC gezahlt wurde und der in einer Fachzeitschrift mit einem spezifischen SNIP-Wert erschienen ist. Links unten liegen so viele Punkte (Kombinationen aus APC und SNIP), dass nur noch eine „schwarze Wolke“ zu sehen ist. Die Gerade stellt den einfachen linearen Zusammenhang (Korrelation) zwischen SNIP und APC dar. Die positive Steigung der Geraden bedeutet, dass es einen positiven Zusammenhang zwischen Artikelbearbeitungsgebühren und dem Zitations-Impact gibt. Ein höherer Impact einer Zeitschrift geht tendenziell mit höheren APCs einher. Der Achsenabschnitt zeigt an, dass Zeitschriften mit einem Impact von (nahe) Null „im Durchschnitt“ in etwa 1.400 € für die Veröffentlichung eines Artikels verlangen. Insgesamt sieht der Zusammenhang zwischen APCs und SNIP für das menschliche Auge eher lose aus, dennoch ist er statistisch hoch signifikant.

<img src="/images/APC-SNIP-Korrelation.jpg" alt="Darstellung des Zusammenhangs zwischen dem SNIP-Wert und der Höhe der APCs" width="400" height="400" display="block" />

## Zusammenhang zwischen dem Open-Access-Status einer Zeitschrift und der Höhe der APCs

Ein weiterer, möglicher Erklärungsfaktor für die Unterschiede in den tatsächlich gezahlten APCs liegt darin, ob eine Fachzeitschrift als ganze open access oder hybrid ist. Hybrid beschreibt den Umstand, dass in einer subskriptionsbasierten Zeitschrift einzelne Artikel im Open Access erscheinen. Die folgende Abbildung teilt die Artikel in unserer Stichprobe danach auf, ob sie in einer Open-Access- oder in einer hybriden Zeitschrift publiziert worden sind, und stellt dann die Verteilung der APCs in den beiden Gruppen in zwei Box-Plot-Diagrammen dar. So erhält man eine gute Übersicht darüber, in welchem Bereich die APCs liegen, wie sie sich über diesen Bereich verteilen und wie sich der Bereich bzw. die Verteilung zwischen den beiden Gruppen unterscheidet. Ein Box-Plot besteht aus einem Rechteck (der Box), dessen oberes Ende das obere Quartil, der dicke Strich den Median und das untere Ende das untere Quartil darstellt. D.h., dass in der Box 50% der APCs liegen, während sich jeweils die Hälfte der übrigen 50% oberhalb bzw. unterhalb dieses Bereichs verteilen (Striche). Die Punkte stellen einzelne außergewöhnlich hohe bzw. niedrige APCs dar.

<img src="/images/APC-Hybridstatus-Korrelation.jpg" alt="Darstellung des Zusammenhangs zwischen dem Hybrid-Status und der Höhe der APCs" width="400" height="400" display="block" />

Der Vergleich zwischen den beiden Gruppen zeigt, dass APCs in hybriden Zeitschriften deutlich höher sind als in Open-Access-Zeitschriften. Das trifft zwar nicht auf jeden Einzelfall zu, bestimmt aber das grundsätzliche Bild. Nicht nur das die mittlere APC (Median) bei hybriden Zeitschriften um etwa 1.000 € über der mittleren APC von Open-Access-Zeitschriften liegt. Das untere Quartil für APCs in hybriden Zeitschriften liegt sogar über dem oberen Quartil der APCs für Open-Access-Zeitschriften, was bedeutet, dass in der OpenAPC-Stichprobe 75% der Publikationen in hybriden Zeitschriften mehr gekostet haben als 75% der Publikationen in Open-Access-Zeitschriften.

## Zusammenhang zwischen dem Open-Access-Status einer Zeitschrift und dem SNIP-Wert

Der deutliche Zusammenhang zwischen APC-Höhe und der Tatsache, ob eine Zeitschrift hybrid ist oder open access, lässt sich zum Teil dadurch erklären, dass hybride Zeitschriften tendenziell einen höheren Zitations-Impact haben (siehe Abbildung unten). Zusammenfassend kann man sagen, dass es sowohl einen positiven Zusammenhang zwischen APCs und dem Impact, als auch zwischen APCs und dem Veröffentlichungsmodus einer Fachzeitschrift (hybrid oder open access) sowie zwischen dem Impact und dem Veröffentlichungsmodus einer Fachzeitschrift gibt. Um den Einfluss des Impacts auf die Höhe der APC zu isolieren, muss man die übrigen beiden Einflüsse herausrechnen. Dies ist im Kern das, was die Regressionsanalyse macht, die im nächsten Blogbeitrag vorgestellt werden wird.

<img src="/images/SNIP-Hybridstatus-Korrelation.jpg" alt="Darstellung des Zusammenhangs zwischen dem Hybrid-Status und dem SNIP-Wert" width="400" height="400" display="block" />

## Zusammenhang zwischen dem Verlagsnamen und der Höhe der APCs

Der SNIP und der Veröffentlichungsmodus einer Fachzeitschrift sind vermutlich nicht die einzigen Faktoren, die die Höhe der APCs bestimmen. Darüber hinaus ist denkbar, dass die Verlage unterschiedliche hauseigene Preisfestsetzungsstrategien verfolgen, oder das mit dem Verlagsnamen ein gewisses Renommee verknüpft ist, dass sich nicht im SNIP widerspiegelt. Beides lässt sich analysieren, in dem man die Höhe der APCs für jeden Verlag einzeln betrachtet. Praktikabel ist dies jedoch nur für die größten Verlage (gemessen an der Anzahl der Publikationen in der OpenAPC-Stichprobe. Die APCs bzw. Publikationen der übrigen Verlage werden in einer Gruppe zusammengefasst.

<img src="/images/APC-Verlage-Korrelation.jpg" alt="Darstellung des Zusammenhangs zwischen den einzelnen Verlagen und der Höhe der APCs" width="500" height="550" display="block" />

Die Abbildung stellt Box-Plot-Diagramme für die sechs größten Verlage sowie die Gruppe der übrigen Verlage dar. Es sind große Unterschiede in den Verteilungen der APCs zwischen den Verlagen zu erkennen. Am höchsten sind die APCs bei Elsevier, gefolgt von Wiley-Blackwell. Relativ niedrige APCs stellt PLoS den Autor_innen in Rechnung. Auf Grund des Mega-Journals PLOS ONE streuen diese auch nicht so stark wie bei den übrigen Verlagen. Die Verlage erheben nicht nur deutlich unterschiedliche APCs, ihre Zeitschriftenportfolios unterscheiden sich ebenso hinsichtlich anderer Charakteristika (vgl. Abbildung 6 im Bericht). Deshalb bedarf es einer multivariaten Regressionsanalyse, um die isolierten Einflüsse auf die Höhe der APCs zu identifizieren. Diese Analyse stellen wir im nächsten Blogbeitrag vor.

## Weitere Informationen

Schönfelder, Nina (2018). *APCs — Mirroring the impact factor or legacy of the subscription-based model?*. Universität Bielefeld. doi:[10.4119/unibi/2931061](https://dx.doi.org/10.4119/unibi/2931061)

Blogbeitrag 1 - [APCs – Spiegel des Impact-Factors oder Erbe des Subskriptionsmodells? Eine Einleitung.](/blog/2018/11/26/APCregressionsanalyse_einleitung/)

Blogbeitrag 2 - [APCs – Spiegel des Impact-Factors oder Erbe des Subskriptionsmodells? Die Datenbasis.](/blog/2018/12/10/APCregressionsanalyse_Datenbasis/)
