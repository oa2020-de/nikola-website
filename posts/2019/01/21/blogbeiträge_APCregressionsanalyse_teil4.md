<!--
.. title: APCs – Spiegel des Impact-Factors oder Erbe des Subskriptionsmodells? Die Regressionsanalyse.
.. slug: APCregressionsanalyse
.. date: 2019-01-21 13:00:00+1:00
.. author: Nina Schönfelder
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Dieser Blogbeitrag ist Teil einer Reihe, die die Studie [„APCs – Spiegel des Impact-Factors oder Erbe des Subskriptionsmodells?“](https://dx.doi.org/10.4119/unibi/2931061) von OA2020-DE erläutert.

Im [letzten Blogbeitrag](/blog/2019/01/08/APCregressionsanalyse_beschreibendestatistik/) untersuchten wir mit einfachen, statistischen Kennzahlen die Beziehung jeweils zweier Variablen untereinander. So zeigt sich auf den ersten Blick ein positiver Zusammenhang zwischen der APC-Höhe, dem Zitations-Impact (SNIP) und dem Publikationsmodus einer Zeitschrift (hybrid häufig teurer als Open Access und tendenziell mit höherem Impact). Ob hinter diesen Korrelationen auch kausale Zusammenhänge stehen, kann nur eine sorgfältig durchgeführte Regressionsanalyse zeigen.
<!-- TEASER_END -->

## Der Datensatz

Für die Durchführung der Regressionsanalyse eignet sich nur ein Teil der Daten von [OpenAPC](https://treemaps.intact-project.org/apcdata/openapc/). Um Fehlschlüsse zu vermeiden, muss die Stichprobe für die Grundgesamtheit repräsentativ sein. Dies ist für APC-Zahlungen aus Deutschland ganz offensichtlich nicht der Fall. In OpenAPC sind kaum APC-Zahlungen über 2.000 EUR aus Deutschland verzeichnet, was den Richtlinien der DFG-geförderten Publikationsfond geschuldet ist. Zahlungen, die nicht aus den Publikationsfond bestritten werden, sondern aus Drittmitteln der Forschungsförderung oder Institutsmitteln stammen und sich damit nicht an diese APC-Obergrenzen halten müssen, werden so gut wie nie an OpenAPC gemeldet. Dies spiegelt sich auch darin wieder, dass laut OpenAPC gerade mal 1% der deutschen Zahlungen an hybride Zeitschiften gehen. Diese systematische Verzerrung in dem Berichtsverhalten deutscher, wissenschaftlicher Einrichtungen an OpenAPC führt dazu, dass die deutsche Teil-Stichprobe bei weitem nicht repräsentativ ist. Soweit wir wissen, gibt es in Großbritannien keine vergleichbaren Einschränkungen hinsichtlich der Förderfähigkeit bzw. Kostenübernahme von APCs. Daher werden im Folgenden ausschließlich die britischen APC-Zahlungen von 2014–2016 untersucht.

## Die Regressionsgleichung

Die Höhe der tatsächlich gezahlten APCs wird erklärt durch den Impact der jeweiligen Zeitschrift (SNIP), ob die Zeitschrift hybrid ist oder nicht, zu welchem Verlag die Zeitschrift gehört, in welchem Fachbereich die Zeitschrift herausgegeben wird sowie durch das Jahr der Zahlung. Darüber hinaus prüfen wir, ob der Einfluss des SNIP auf die Höhe der APCs für hybride Zeitschriften anders ist als für Open-Access-Zeitschriften. Folgende Gleichung wurde mittels der Kleinsten-Quadrate-Methode geschätzt (Koeffizienten sind gerundet):

<img src="/images/APC_Regressionsgleichung.PNG" alt="Gleichung der APC_Regressionsanalyse" width="600" height="50" display="block" />

Dies ergibt folgende geschätzte APC-Gleichung, deren Koeffizienten (bis auf den des Verlags „Wiley-Blackwell“) statistisch hoch-signifikant sind.

<img src="/images/APC_Regressionsgleichung_eingesetzt.PNG" alt="Gleichung der APC_Regressionsanalyse mit Werten" width="500" height="100" display="block" />

Der isolierte (d.h. um die anderen Faktoren bereinigte) Einfluss des Zitations-Impact auf die Höhe der APCs beträgt 728 EUR bei Open-Access-Zeitschriften und gerade mal 188 EUR (= 728–540) bei hybriden Zeitschriften je SNIP-Punkt. Beträgt also der SNIP-Wert einer Open-Access-Zeitschrift 2 und bei einer anderen Open-Access-Zeitschrift mit sonst gleichen Charakteristika 1, so verlangt die erste „im Schnitt“ eine um 728 EUR höhere APC; bei zwei hybriden Zeitschriften wären „im Schnitt“ nur 188 EUR mehr fällig. Die geringere Sensitivität von hybriden Zeitschriften in Bezug auf ihren Zitations-Impact bedeutet jedoch nicht, dass diese automatisch günstiger sind. Der Fixbetrag (d.h. der Teil der APC der durch keine der hier betrachteten Variablen erklärt werden kann und der immer anfällt) beträgt für Open-Access-Zeitschriften 519 EUR und für hybride Zeitschriften 1915 EUR. In Anbetracht dessen, dass man für dieses Geld Artikel in zwei gleichen Zeitschriften (gemessen an den hier diskutieren Charakteristika) publizieren kann, die sich nur durch den Publikations-Modus unterscheiden, fragt man sich, welchen Mehrwert hybride Zeitschriften für die zusätzlichen 1396 EUR bieten.

## Geschätzte, lineare Beziehung

Für die große Masse der Artikel, die in Fachzeitschriften mit keinem bis durchschnittlichem Zitations-Impact erscheinen, sind die APCs in hybriden Zeitschriften in Summe deutlich teurer als in Open-Access-Zeitschriften. Wo genau Zeitschriften mit den beiden Publikations-Modi „APC-Gleichstand“ erreichen, ist in der unteren Abbildung zu sehen (Schnittpunkt der roten und blauen Gerade ungefähr bei einem SNIP-Wert von 2,5). Die Abbildung visualisiert die geschätzte, lineare Beziehung zwischen SNIP und APC. Die rote Gerade stellt das Preissetzungsverhalten von hybriden Zeitschriften der Gesundheitswissenschaften bei übrigen Verlagen in 2016 dar; die blaue Linie das für ihre Open-Access-Konterparts. Erscheint die Zeitschrift in einem anderen Fachbereich, bei einem anderen Verlag oder in einem anderen Jahr so verschiebt das beide Geraden gleichermaßen nach oben bzw. unten. So ist die Publikation eines Artikel bei Elsevier am teuersten und bei PLoS am günstigsten, in den Lebenswissenschaften wesentlich teuer als in den Geistes- und Sozialwissenschaften, jeweils unter sonst gleichen Umständen.

<img src="/images/Geschätzte_lineare_Beziehung.PNG" alt="Geschätzte, lineare Beziehung" width="400" height="400" display="block" />

Die geschätzte APC für eine einzelne Zeitschrift kann man ausrechnen, indem man die jeweiligen Zeitschriften-Charakteristika in die obige Gleichung einträgt, wie die beiden Beispiele zeigen:

<img src="/images/geschätzteAPC_PLoS_Elsevier.png" alt="Geschätzte APCs für die Verlage PLoS und Elsevier" width="500" height="130" display="block" />



## Fazit

Die multivariate Regressionsanalyse bestätigt größtenteils den Eindruck, den man durch die Untersuchung der Variablen mittels beschreibender Statistiken ([letzter Blogbeitrag](/blog/2019/01/08/APCregressionsanalyse_beschreibendestatistik/)) erhält und auf die sich die Literatur bis dato beschränkt hat. Sofern die Schätzgleichung richtig spezifiziert ist, können wir jedoch nun

+ bestätigen, dass die Zusammenhänge zwischen APCs und den anderen Variablen nicht zufällig sind,
+ zeigen wie groß (in Euro) der isolierte Effekt jeder Variable auf die APC-Höhe ist,
+ identifizieren, dass es zwei Preissetzungs-Muster gibt (Open Access vs. hybrid),
+ die geschätzte Gleichung für die Projektion von APCs (in Euro) für Closed-Access-Zeitschriften oder für die Zeitschriften, zu denen wir keine APC-Informationen haben, nutzen,
+ projizieren, wie viel hybride Zeitschriften verlangen würden, würden sie gänzlich in den Open Access flippen bzw. nach Open-Access-Schema bepreisen,
+ projizieren, wie viel Open-Access-Zeitschriften verlangen würden, wenn auch sie das Hybrid-Preissetzungs-Muster übernehmen würden.

Welche Schlussfolgerungen wir aus den Regressionsergebnissen ziehen können, und welche finanzielle Bedeutung sie für die Budgets der wissenschaftlichen Einrichtungen und Forschungsförderer haben, wird im nächsten und vorerst letzten Blogbeitrag dieser Reihe diskutiert werden.


## Weitere Informationen

Schönfelder, Nina (2018). *APCs — Mirroring the impact factor or legacy of the subscription-based model?*. Universität Bielefeld. doi:[10.4119/unibi/2931061](https://dx.doi.org/10.4119/unibi/2931061)

Blogbeitrag 1 - [APCs – Spiegel des Impact-Factors oder Erbe des Subskriptionsmodells? Eine Einleitung.](/blog/2018/11/26/APCregressionsanalyse_einleitung/)

Blogbeitrag 2 - [APCs – Spiegel des Impact-Factors oder Erbe des Subskriptionsmodells? Die Datenbasis.](/blog/2018/12/10/APCregressionsanalyse_Datenbasis/)

Blogbeitrag 3 - [APCs – Spiegel des Impact-Factors oder Erbe des Subskriptionsmodells? Beschreibende Statistiken.](/blog/2019/01/08/APCregressionsanalyse_beschreibendestatistik/)
