<!--
.. title: Liste der häufig zitierten Open-Access-Zeitschriften aktualisiert
.. slug: liste-frequentlycitedjournals-aktualisiert
.. date: 2019-06-11 8:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Letzten Monat erschien die aktualisierte Version des CWTS Journal Indicators der Leiden University. Das haben wir zum Anlass genommen, um unsere Liste der häufig zitierten Open-Access-Zeitschriften mit den neuen SNIP-Werten zu bestücken und auch die Metadaten aus dem DOAJ zu aktualisieren. Mittlerweile verzeichnet die durchsuchbare Liste rund 800 Open-Access-Zeitschriften aus fast allen wissenschaftlichen Bereichen und unterstützt Forscher_innen dabei, geeignete Open-Access-Zeitschriften aus ihrer Disziplin für das Einreichen ihres Manuskripts auszuwählen.

Zur Liste: [https://oa2020-de.org/pages/frequentlycitedoajournals/](/pages/frequentlycitedoajournals/)
