<!--
.. title: List of frequently cited open-access journals updated
.. slug: list-frequentlycitedjournals-updated
.. date: 2019-06-11 8:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Last month the updated version of the CWTS Journal Indicator of Leiden University appeared. We took this as an opportunity to update our list of frequently cited open-access journals with the new SNIP values and also to update the metadata from the DOAJ. The searchable list now contains around 800 open-access journals from almost all scientific disciplines and helps researchers to select suitable open-access journals from their discipline for submission.

Link to the list: [https://oa2020-de.org/en/pages/frequentlycitedoajournals/](/en/pages/frequentlycitedoajournals/)
