<!--
.. title: APCs – Spiegel des Impact-Factors oder Erbe des Subskriptionsmodells? Schlussfolgerungen.
.. slug: APCregressionsanalyse_schlussfolgerungen
.. date: 2019-02-12 09:00:00+1:00
.. author: Nina Schönfelder
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Dieser Blogbeitrag ist Teil einer Reihe, die die Studie [„APCs – Spiegel des Impact-Factors oder Erbe des Subskriptionsmodells?“](https://dx.doi.org/10.4119/unibi/2931061) von OA2020-DE erläutert.

Im [letzten Blogbeitrag](/blog/2019/01/21/APCregressionsanalyse/) untersuchten wir mit einer multivariaten Regressionsanalyse, ob und wie die Höhe der tatsächlich gezahlten APCs erklärt wird durch

+ den Impact der jeweiligen Zeitschrift (SNIP),
+ ob die Zeitschrift hybrid ist oder nicht,
+ zu welchem Verlag die Zeitschrift gehört,
+ in welchem Fachbereich die Zeitschrift herausgegeben wird, sowie
+ das Jahr der Zahlung.

Darüber hinaus prüften wir, ob der Einfluss des SNIP auf die Höhe der APCs für hybride Zeitschriften anders ist als für Open-Access-Zeitschriften. Welche Schlussfolgerungen wir aus den Regressionsergebnissen ziehen können und welche finanzielle Bedeutung sie für die Budgets der wissenschaftlichen Einrichtungen und Forschungsförderer haben, diskutieren wir in diesem vorerst letzten Blogbeitrag dieser Reihe.
<!-- TEASER_END -->

## Ergebnis

Der Bericht zeigt, dass der Zitations-Impact wie auch der Access-Status der Zeitschrift (hybrid vs. open access) die wesentlichen Treiber für die Höhe der Artikelbearbeitungsgebühren sind. Wir fanden u.a. einen positiven und statistisch signifikanten Zusammenhang zwischen Artikelbearbeitungsgebühren und dem Zitations-Impact der Zeitschrift. Der Zusammenhang ist besonders bei Open-Access-Zeitschriften ausgeprägt. Die geringere Sensitivität von hybriden Zeitschriften in Bezug auf ihren Zitations-Impact bedeutet jedoch nicht, dass diese automatisch günstiger sind. Vielmehr ist für das Groß der Zeitschriften mit keinem bis mittleren Zitations-Impact das Gegenteil der Fall: Hybride Zeitschriften sind deutlich teurer als Open-Access-Zeitschriften. Um auf den Titel des Berichts zurückzugreifen: APCs sind also ein Spiegel des Impact-Faktors von Open-Access-Zeitschriften und ein Erbe des Subskriptionsmodels in hybriden Zeitschriften.


## Hypothetische Szenarien

Um einen Eindruck zu erhalten, wie sich die beiden Preissetzungschemata (hybrid vs. open-access) auf die Finanzierbarkeit der Open-Access-Transformation von wissenschaftlichen Zeitschriften auswirken, haben wir zwei hypothetische Szenarien kalkuliert. Was wäre die Gesamtsumme aller in OpenAPC verzeichneten APC-Zahlungen gewesen, wenn alle Zeitschriften nach dem Open-Access-Schema bepreist hätten? Und wie hoch wäre die Gesamtsumme gewesen, wenn alle Zeitschriften nach dem Hybrid-Schema bepreist hätten – unter sonst gleichen Umständen? Die folgende Tabelle zeigt die hypothetischen APC-Summen in Euro für die britische Teilstichprobe wie auch für alle Einträge in OpenAPC und vergleicht diese mit den Summen der tatsächlich gezahlten APCs. Die Berechnungen zeigen, dass das britische Hochschul- und Forschungssystem fast 8 Millionen Euro gespart hätte, wenn das Open-Access-Preisschema für alle Artikel Anwendung gefunden hätte. Im Gegensatz dazu hätten alle Länder ca. 17 Millionen Euro mehr für ihre in OpenAPC verzeichneten Artikel gezahlt, wenn durchgehend nach dem Hybrid-Preisschema bepreist worden wäre.


| Stichprobe             | Szenario            | APC Gesamtsumme |
|------------------------|---------------------|-----------------|
| Vereinigtes Königreich | Tatsächlich gezahlt | 52.658.541 EUR  |
|                        | Alles open access   | 44.662.308 EUR  |
|                        | Alles hybrid        | 56.863.847 EUR  |
| Beobachtungen aus allen Ländern | Tatsächlich gezahlt | 83.969.558 EUR  |
|                        | Alles open access   | 72.229.822 EUR  |
|                        | Alles hybrid        | 101.031.495 EUR  |


## Fazit

Welches Preissetzungsschema sich nach einem weitgehenden Zeitschriften-Flipping in Zukunft durchsetzen wird, ist essentiell. Wird die Preissetzung der traditionellen, subskriptionsbasierten Verlage (d.h. hybrid) dominieren, so wird die Open-Access-Transformation zu ungleich höheren Kosten stattfinden, als derzeit von Bibliotheken, Forschungseinrichtungen und ‑förderern erwartet. Daher ist es von größter Bedeutung, Voraussetzungen für einen Wettbewerb zwischen Verlagen und Zeitschriften zu schaffen.


## Weitere Informationen

Schönfelder, Nina (2018). *APCs — Mirroring the impact factor or legacy of the subscription-based model?*. Universität Bielefeld. doi:[10.4119/unibi/2931061](https://dx.doi.org/10.4119/unibi/2931061)

Blogbeitrag 1 - [APCs – Spiegel des Impact-Factors oder Erbe des Subskriptionsmodells? Eine Einleitung.](/blog/2018/11/26/APCregressionsanalyse_einleitung/)

Blogbeitrag 2 - [APCs – Spiegel des Impact-Factors oder Erbe des Subskriptionsmodells? Die Datenbasis.](/blog/2018/12/10/APCregressionsanalyse_Datenbasis/)

Blogbeitrag 3 - [APCs – Spiegel des Impact-Factors oder Erbe des Subskriptionsmodells? Beschreibende Statistiken.](/blog/2019/01/08/APCregressionsanalyse_beschreibendestatistik/)

Blogbeitrag 4 - [APCs – Spiegel des Impact-Factors oder Erbe des Subskriptionsmodells? Die Regressionsanalyse.](/blog/2019/01/21/APCregressionsanalyse/)
