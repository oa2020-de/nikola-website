<!--
.. title: APCs — Mirroring the impact factor or legacy of the subscription-based model? Conclusion.
.. slug: APCregressionsanalyse_conclusion
.. date: 2019-02-12 09:00:00+1:00
.. author: Nina Schönfelder
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

This blog post is part of a series that explains the study [“APCs — Mirroring the impact factor or legacy of the subscription-based model?”](https://dx.doi.org/10.4119/unibi/2931061) of OA2020-DE.

In the [last blog post](/en/blog/2019/01/21/APCregressionanalysis/), we analyzed by means of multivariate regression whether and how the level of actually paid APCs is explained by

+ the citation impact (SNIP) of a journal,
+ whether the journal is hybrid or not,
+ which publisher issues the journal,
+ for which subject area the journal is relevant, and
+ the year of the payment.

Moreover, we checked whether the effect of citation impact differs for open-access vs. hybrid journals. Now, we will discuss the conclusions and their implications for the financial aspects of the open-access transformation.
<!-- TEASER_END -->

## Outcome

The results provide evidence that the journal’s impact as well as the hybrid status are the most important drivers of APC-levels. There is a positive and statistically significant relationship between the citation impact and the requested APC — both, for open-access and hybrid journals. However, two pricing patters emerge. The journal’s impact greatly influences APC-levels in open-access journals, whereas it little alters APCs in hybrid journals. The finding that a hybrid journal is less sensitive to its impact does not mean that it is cheaper than open-access journals. For the vast bulk of articles that appear in zero to average-impact journals, APCs in hybrid journals are much costlier than in open-access counterparts. To sum up, hybrid journals tend to be more expensive and are less sensitive to their citation impact than open-access journals. With reference to the title of this paper, one can say that APCs are mirroring the impact factor of open-access journals, but are a legacy of the subscription-based model in hybrid journals.

## Hypothetical scenarios

To get an idea on what the two pricing patterns imply for the financial aspects of the open-access transformation, we calculated two hypothetical scenarios. What would have been the total APC-amount if all articles recorded in OpenAPC had been charged as if they were published in open-access journals? And what would be the sum if they all were published in hybrid journals (other journals characteristics leaving unchanged)? The table below presents the hypothetical amounts in euro for the UK sub-sample and the total sample and compares it with the actual sums. The calculations show that the UK higher education and research system would have saved almost EUR 8 million if all journal had been charged according to the open-access pricing-pattern. In contrast, all countries would have spent about EUR 17 million more on APCs, if all articles had been charged according to the hybrid-pattern.


| Sample         | Scenario              | Total amount of APCs |
|----------------|-----------------------|----------------------|
| United Kingdom | actually paid         | EUR 52,658,541       |
|                | as if all open access | EUR 44,662,308       |
|                | as if all hybrid      | EUR 56,863,847       |
| Total          | actually paid         | EUR 83,969,558       |
|                | as if all open access | EUR 72,229,822       |
|                | as if all hybrid      | EUR 101,031,495      |


## Conclusion

Which pricing behavior will dominate in the future after a full journal flipping, is crucial. If the pricing behavior of the traditional, subscription-based publishers wins through, the open-access transformation will come at a much higher cost than expected today from libraries, higher education and research institutions. Therefore, provisions to introduce competition between publishers and journals are of utmost importance.


## More information

Schönfelder, Nina (2018). *APCs — Mirroring the impact factor or legacy of the subscription-based model?*. Universität Bielefeld. doi:[10.4119/unibi/2931061](https://dx.doi.org/10.4119/unibi/2931061)

Blogpost 1 - [APCs — Mirroring the impact factor or legacy of the subscription-based model? An introduction.](/en/blog/2018/11/26/APCregressionanalysis_introduction/)

Blogpost 2 - [APCs — Mirroring the impact factor or legacy of the subscription-based model? The database.](/en/blog/2018/12/10/APCregressionsanalysis_database/)

Blogpost 3 - [APCs — Mirroring the impact factor or legacy of the subscription-based model? Descriptive statistics.](/en/blog/2019/01/08/APCregressionsanalyse_descriptivestatistics/)

Blogpost 4 - [APCs — Mirroring the impact factor or legacy of the subscription-based model? Regression analysis.](/en/blog/2019/01/21/APCregressionanalysis/)
