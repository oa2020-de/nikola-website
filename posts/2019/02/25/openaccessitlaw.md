<!--
.. title: FID <intR>², Peter Lang und Knowledge Unlatched starten gemeinsames Open-Access-eBook-Modell: Open Access IT Law
.. slug: openaccess-itlaw
.. date: 2019-02-25 10:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Der Fachinformationsdienst für internationale und interdisziplinäre Rechtsforschung hat gemeinsam mit dem Verlag Peter Lang und Knowledge Unlatched ein Projekt initiiert (angelehnt an der OPEN Library Politikwissenschaft), um eine Open Library IT-Law aufzubauen - das Projekt [Open Access IT Law](/pages/oaitlaw/).

Ziel dieses Projekts ist es zunächst, über drei Jahre hinweg jährlich 10 Frontlist-Titel des Peter Lang Verlages im IT-Recht Open Access zur Verfügung zu stellen. Aufgrund der finanziellen Beteiligung des FID sind Teile der Gesamtkosten bereits gedeckt.

Als Vorgeschmack sind bereits fünf Titel aus der Back-Liste Open Access gestellt und u.a. über das OA-Repositorium des FID auf- und abrufbar ([Open Access IT Law](https://intr2dok.vifa-recht.de/servlets/solr/select?q=%2Bcategory.top:%22intr2dok_projects:5d4bacc2-cc93-4d01-bbc1-89597fcd55d9%22+%2BobjectType:mods+%2Bcategory.top:%22state:published%22&mask=content/browse/intr2dok_projects.xml%23open%5B%5D&sort=mods.dateIssued+desc)).

Weitere Informationen finden Sie auf der [Projektseite](/pages/oaitlaw/) und im [At-a-Glance-Dokument](http://knowledgeunlatched.org/wp-content/uploads/2019/02/At-a-glance-Peter-Lang-IT-Law-Deutsch-06022019.pdf).

Wenn auch Sie sich an der Finanzierung von Open Access IT Law beteiligen möchten, wenden Sie sich bitte direkt an [Catherine Anderson](mailto:catherine@knowledgeunlatched.org) von Knowledge Unlatched.
