<!--
.. title: FID <intR>², Peter Lang and Knowledge Unlatched launch joint open-access eBook model: Open Access IT Law
.. slug: openaccess-itlaw
.. date: 2019-02-25 10:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

The Specialised Information Service for International and Interdisciplinary Legal Research has initiated a project together with the publishing house Peter Lang and Knowledge Unlatched (based on the OPEN Library Political Science) to build an Open Library IT-Law - the project [Open Access IT Law](/en/pages/oaitlaw/).

Under the terms of the agreement, Peter Lang undertakes to publish ten legal monographs in Gold Open Access per year for an initial period of three years starting in 2019. Due to the financial contribution of FID, parts of the total costs are already covered.

As a foretaste are already five titles from the back list provided open access and via the OA repository of the FID retrievable ([Open Access IT Law](https://intr2dok.vifa-recht.de/servlets/solr/select?q=%2Bcategory.top:%22intr2dok_projects:5d4bacc2-cc93-4d01-bbc1-89597fcd55d9%22+%2BobjectType:mods+%2Bcategory.top:%22state:published%22&mask=content/browse/intr2dok_projects.xml%23open%5B%5D&sort=mods.dateIssued+desc)).

More information can be found on the [project page](/en/pages/oaitlaw/) and in the [At-a-Glance document](http://knowledgeunlatched.org/wp-content/uploads/2019/02/At-a-glance-Peter-Lang-IT-Law-21022019.pdf).

If you would also like to participate in the financing of Open Access IT Law, please contact [Catherine Anderson](mailto:catherine@knowledgeunlatched.org) from Knowledge Unlatched directly.
