<!--
.. title: Bibliotheken und Verlage als Partner: 3. Open-Access-Transformationsworkshop des Nationalen Open-Access-Kontaktpunkts in Bielefeld
.. slug: 3oatransformationsworkshop
.. date: 2019-05-07 8:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## Bibliotheken und Verlage als Partner_innen im Open-Access-Publikationsprozess

Wie kann ein gemeinsames Verständnis zwischen den Akteuren des wissenschaftlichen Publikationssystems und ein daraus resultierender gemeinsamer Weg zur Umsetzung der Open-Access-Transformation entwickelt werden? Unter dieser zentralen Fragestellung fand am 03. und 04. April 2019 zum dritten Mal ein von OA2020-DE organisierter Open-Access-Transformationsworkshop in Bielefeld statt ([Agenda](/assets/files/Agenda_OA2020DETransformationsworkshop_April2019.pdf)). Ziel war es, den teilnehmenden Verlagen und Bibliotheken Informationen zu verschiedenen Entwicklungen und Ansätzen der Open-Access-Transformation sowie Anregungen zur Umsetzung in die eigene bibliothekarische und verlegerische Praxis mitzugeben. Außerdem diente der direkte Austausch untereinander der Beantwortung der zentralen Fragestellung.
<!-- TEASER_END -->

### Open Access ist kein Selbstzweck…

In seiner [Begrüßung](/assets/files/Pieper_Einführung_20190403.pdf) ging Dirk Pieper auf die aktuellen Entwicklungen im Kontext der Allianz der deutschen Wissenschaftsorganisationen in Bezug auf die Open-Access-Transformation ein und präsentierte als eine der Diskussionsgrundlagen für den Workshop die gesammelten Kriterien für eine verlegerische bzw. bibliothekarische Beteiligung an Open-Access-Publikationsformen aus dem Hands-On-Lab beim 7. Bibliothekskongress in Leipzig (siehe [Bericht](/blog/2019/05/03/bericht-handsonlab-Bibliothekskongress/)). Dr. Angela Holzer stellte anschließend die [Verbindung](/assets/files/2019_NOAK_Open-Access-Transformation @ DFG.pdf) zur Deutschen Forschungsgemeinschaft her und betonte, dass die Open-Access-Transformation aus Sicht der DFG wissenschaftsgeleitet sein muss und der Weiterentwicklung der Wissenschaftskommunikation dient. Wenn Ende 2020 nach einer 10-jährigen Förderphase die Unterstützung von Publikationsfonds zur Übernahme von Open-Access-Publikationskosten ausläuft, braucht es nach Sicht der DFG eine komplett neue Förderstruktur für den Bereich der Informationsinfrastrukturen. Wie diese aussehen soll, wird zur Zeit auf allen Ebenen der Forschungsförderung diskutiert und war auch Thema bei der Diskussion im Anschluss. Dabei wurde deutlich, dass es ebenfalls eine Unterstützung der Wissenschaftler_innen als Herausgeber_innen wissenschaftlicher Fachpublikationen geben muss, z.B. in Form von institutionell organisierter und bereitgestellter Infrastruktur für elektronische Publikationen und digitale Wissenschaftskommunikation. Ein weiteres identifiziertes Problem ist der stark fragmentierte Open-Access-Monographienmarkt. Verschiedene Verlage, unterschiedliche Fördertöpfe mit unterschiedlichen Vergabekriterien und die schwer einschätzbare Nachfrage machen die Entwicklung einheitlicher Kriterien für eine Förderung und das Preis-Leistungs-Spektrum schwierig.

Zum Abschluss des Blockes stellten die Projektmitarbeiterinnen von OA2020-DE, Alexandra Jobmann und Dr. Nina Schönfelder, drei [Open-Access-Transformationsmodelle](http://doi.org/10.5281/zenodo.2652163) jenseits von DEAL vor - Subscribe to Open S2O, Evidence-based Open Access und das Kooperative Deutschlandkonsortium - die auch schon beim Hands-On-Lab beim Bibliothekskongress in Leipzig der Community zur Bewertung vorgelegt wurden.

Ergänzt wurden diese Ansätze durch die Vorstellung der Open-Access-eBook-Modelle der Verlage transcript, De Gruyter und Nomos. Dr. Karin Werner vom transcript Verlag [präsentierte](/assets/files/Transformationsworkshop_transcript.pdf) ihre Idee für eine neue Publikationskultur als Grundlage für ein zukunftsfähiges Open Access in den Humanities und betonte die Wichtigkeit von einer Partnerschaft aller Beteiligten auf Augenhöhe. Dr. Christina Lembrecht und Martina Naekel von De Gruyter stellten das Open-Access-Modell für die Buchreihe „Empirische Linguistik“ vor, bei dem die Open-Access-Stellung der Backlist komplett durch den Fachinformationsdienst getragen wird und die Open-Access-Stellung der geplanten Titel für die kommenden drei Jahre (insgesamt sechs) durch eine anteilige Finanzierung der Open-Access-Gebühr durch die Autor_innen (über die Print-Publikationsbeihilfe, ca. 1/3 der OA-Gebühr) und den FID- bzw. die Institution (ca. 2/3 der OA-Gebühr) erfolgt. Prof. Dr. Johannes Rux vom Nomos Verlag hob in seinem Beitrag die Bedeutung des Publikationskostenbeihilfe für eine Buchproduktion im Allgemeinen und für ein Open-Access-Buch im Besonderen hervor und berichtete von dem [Vertrag](https://www.mpdl.mpg.de/images/documents/lib_res_center/Auszug_Rahmenvereinbarung_Nomos.pdf), den Nomos mit der MPDL zur Publikation von Open-Access-Büchern der MPG-Wissenschaftler_innen hat.

In den an die Vorträge anschließenden Arbeitsgruppen wurden dann folgende Fragestellungen diskutiert:

* An welchen Stellen können Bibliotheken/Verlage ansetzen?
* Wo ist gemeinsames Handeln sinnvoll/nötig?
* Welche Workflows müssen etabliert/weiterentwickelt werden?

<img style="display:auto; margin:0 20px" src="/images/Ergebnisse_Arbeitsgruppeblau1.jpg" alt="Ergebnisse A der Arbeitsgruppe blau" width="240" height="400" />
<img style="display:auto; margin:20px 20px" src="/images/Ergebnisse_Arbeitsgruppeblau2.jpg" alt="Ergebnisse B der Arbeitsgruppe blau" width="240" height="400" />
<img style="display:auto; margin:20px 20px" src="/images/Ergebnisse_Arbeitsgruppegelb.jpg" alt="Ergebnisse der Arbeitsgruppe gelb" width="240" height="400" />
<img style="display:auto; margin:20px 20px" src="/images/Ergebnisse_Arbeitsgruppegrün.jpg" alt="Ergebnisse der Arbeitsgruppe grün" width="400" height="240" />


### … sondern dient der Weiterentwicklung der Wissenschaftskommunikation.

Der zweite Workshoptag widmete sich den verschiedenen Open-Access-Modellen im Zeitschriftenbereich und dem Verständnis von Bibliothekskonsortien als Akteure der Open-Access-Transformation.

Wie Maja Laisse vom Thieme-Verlag berichtete, setzt dieser auf verschiedene Modelle für verschiedene Zeitschriften. So gibt es für neue Open-Access-Zeitschriften, die noch nicht indexiert sind und keinen Impact Faktor haben, die Möglichkeit des „Pay what you want“ – ein Modell, das von der LMU München forschend begleitet wird. Mit der ZB MED und dem Forschungszentrum Jülich hat der Verlag wiederum ein Transformationsmodell für die Zeitschrift „Hormone & Metabolic Research“ entwickelt, dass die Zugriffsgebühren der beteiligten deutschen Bibliotheken sukzessive in Publikationsgebühren [umwandelt](https://www.thieme.com/for-media/press-releases/1617-dfg-sponsors-open-access-transformation-project-library-consortium-and-thieme-embark-on-collaboration). Langfristiges Ziel ist ein Flip der kompletten Zeitschrift in den Open Access. Das Modell wird von der DFG gefördert.

Agnes Kühlechner vom Hogrefe-Verlag stellte fest, dass ihr Anteil an Open-Access-Publikationen im Vergleich mit den anderen Verlagen sehr niedrig ist, auch wenn alle Zeitschriften in der hybriden Open-Access-Form vorliegen. Eine Erklärung dafür könnte sein, dass sehr viele der Zeitschriften von verschiedenen Fachgesellschaften herausgegeben werden, die sich unterschiedlich stark für Open Access und eine Transformation ihrer Zeitschrift engagieren.

Der De Gruyter-Verlag wiederum hat die Erfahrung gemacht, dass das, was im STM-Bereich funktioniert, nicht für den HSS-Bereich adaptierbar ist. Zum einen, weil APC- oder hybride Modelle nicht akzeptiert sind und zum anderen, weil nationale Transformationsverträge schwierig für (deutschsprachige) geistes- und sozialwissenschaftliche Zeitschriften umzusetzen sind. Für die Transformation von geisteswissenschaftlichen Zeitschriften sind also andere Transformationsmodelle und -logiken nötig. Daher haben sie angefangen, sich für jede Zeitschrift die jeweilige Autor_innen- und Subskriptionsstruktur anzuschauen und darauf basierend ein Transformationsmodell zu entwickeln. Veranschaulicht haben Dr. Christina Lembrecht und Martina Naekel diese Arbeit anhand einer Zeitschrift aus dem Bereich der Library and Information Science.

Der abschließende Vortrag von Renate Wahlig widmete sich dem Thema Konsortien als Akteure der Open-Access-Transformation. Sie betonte, dass die Menge an verschiedenen Open-Access-Modellen, die den Bibliotheken zur Umsetzung empfohlen werden, zu Frust und Ablehnung führen kann. Hohe Komplexität in den Verfahren wiederum führt oft zu Verzögerungen und Nicht-Vertragsabschlüssen. Und auch hier gilt, dass es nicht eine einzelne passende Lösung für alle gibt, sondern dass verschiedene Teilnehmer_innengruppen unterschiedliche Bedürfnisse und Voraussetzungen haben.


### Gewonnene Erkenntnisse

Konsortien und (konsortiale) Fördermittel spielen bei der Weiterentwicklung der Wissenschaftskommunikation eine zentrale Rolle. Die Mittel können u.a. dazu eingesetzt werden, um publikationsstarke Einrichtungen zu entlasten und die Entwicklung von Ausgleichsmechanismen bei höchstmöglicher Transparenz und Bewusstsein für die Kosten von Publikationen unterstützen. Ein Ansatz kann dabei sein, verschiedene Verträge für verschiedene Teilnehmer_innengruppen zu entwickeln – z. B. jeweils für publikationsstarke und weniger publikationsstarke Einrichtungen.

Open Access als zentrales Element der Wissenschaftskommunikation ist genauso divers wie die Publikationskulturen der verschiedenen Wissenschaftsdisziplinen und benötigt entsprechend passgenaue Ansätze. Ob kleine Schritte oder großflächige Ansätze das Mittel der Wahl sind, hängt auch vom Profil der Verlage ab. Wichtig sind klare Verhandlungsziele und das Zulassen von Scheitern.





