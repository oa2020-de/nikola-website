<!--
.. title: Libraries and publishers as partners: 3rd OA2020-DE transformation workshop in Bielefeld
.. slug: 3oatransformationsworkshop
.. date: 2019-05-07 8:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## Libraries and publishers as partners in the open-access publication process

How can a common understanding between the actors of the scientific publication system and a resulting common path for the implementation of the open-access transformation be developed? Under this central question, an open-access transformation workshop organised by OA2020-DE took place for the third time in Bielefeld on 03 and 04 April 2019 ([Agenda](/assets/files/Agenda_OA2020DETransformationsworkshop_April2019.pdf)). The aim was to provide the participating publishers and libraries with information on various developments and approaches of the open-access transformation as well as ideas on how to implement them in their own library and publishing practice. In addition, the direct exchange between them served to answer the central question.
<!-- TEASER_END -->

### Open access is not an end in itself...

In his [welcoming address](/assets/files/Pieper_Einführung_20190403.pdf), Dirk Pieper discussed the current developments in the context of the Alliance of Science Organisations in Germany in relation to the open-access transformation and presented as one of the discussion bases for the workshop the collected criteria for publishing and library participation in open access forms of publication from the Hands-On-Lab at the 7th Library Congress in Leipzig (see [report](/en/blog/2019/05/03/bericht-handsonlab-Bibliothekskongress/)). Dr. Angela Holzer then established the [link](/assets/files/2019_NOAK_Open-Access-Transformation @ DFG.pdf) to the German Research Foundation (DFG) and emphasised that the DFG's view is that the open-access transformation must be guided by science and serves the further development of science communication. If, at the end of 2020, the support of publication funds for the takeover of open access publication costs expires after a 10-year funding period, the DFG believes that a completely new funding structure for the area of ​​information infrastructures is needed. How this should look like is currently being discussed at all levels of research funding and was also a topic of discussion afterwards. It became clear that there must also be support for researchers as editors of scientific publications, e.g. in the form of an institutionally organised and provided infrastructure for electronic publications and digital science communication. Another problem identified is the highly fragmented open-access monograph market. Different publishers, different funding pools with different award criteria and the unpredictable demand make it difficult to develop uniform criteria for funding and the price-performance spectrum.

At the end of the block, the project staff of OA2020-DE, Alexandra Jobmann and Dr. Nina Schönfelder, presented three [open-access transformation models](http://doi.org/10.5281/zenodo.2652163) beyond DEAL - Subscribe to Open S2O, Evidence-based open access and the Cooperative Germany Consortium - which had already been presented to the community for evaluation at the Hands-On-Lab at the Library Congress in Leipzig.

These approaches were complemented by the presentation of the open-access eBook models of the publishers transcript, De Gruyter and Nomos. Dr. Karin Werner of the transcript publishing house [presented](/assets/files/Transformationsworkshop_transcript.pdf) her idea for a new publishing culture as the basis for sustainable open access in the humanities and emphasised the importance of a partnership between all participants on an equal footing. Dr. Christina Lembrecht and Martina Naekel von De Gruyter presented the open-access model for the book series "Empirical Linguistics", in which the open-access position of the backlist is completely borne by the specialised information service and the open-access position of the planned titles for the next three years (a total of six) is financed by the authors on a pro-rata basis (via the print publication subsidy, approx. 1/3 of the OA fee) and the FID or the institution (approx. 2/3 of the OA fee). Prof. Dr. Johannes Rux of Nomos Verlag emphasised in his contribution the importance of publication cost subsidies for book production in general and for an open-access-book in particular and reported on the [contract](https://www.mpdl.mpg.de/21-specials/50-open-access-publishing.html) that Nomos has with the MPDL for the publication of open-access books by MPG researchers.

The following questions were then discussed in the working groups that followed the lectures:

* Where can libraries/publishers start?
* Where is joint action meaningful/necessary?
* Which workflows need to be established/further developed?

<img style="display:auto; margin:0 20px" src="/images/Ergebnisse_Arbeitsgruppeblau1.jpg" alt="Results part I of working group blue" width="240" height="400" />
<img style="display:auto; margin:20px 20px" src="/images/Ergebnisse_Arbeitsgruppeblau2.jpg" alt="Results part II of working group blue" width="240" height="400" />
<img style="display:auto; margin:20px 20px" src="/images/Ergebnisse_Arbeitsgruppegelb.jpg" alt="Results of working group yellow" width="240" height="400" />
<img style="display:auto; margin:20px 20px" src="/images/Ergebnisse_Arbeitsgruppegrün.jpg" alt="Results of working group green" width="400" height="240" />


### ...but serves the further development of science communication.

The second workshop day was devoted to the various open-access models in the field of journals and the understanding of library consortia as actors of the open-access transformation.

As Maja Laisse from Thieme-Verlag reported, the publisher relies on different models for different journals. For new open-sccess journals that are not yet indexed and have no impact factor, for example, there is the possibility of "Pay what you want" - a model that is being researched by the LMU Munich. With the ZB MED and the Research Center Jülich, the publishing house has again developed a transformation model for the journal "Hormone & Metabolic Research", which successively [converts](https://www.thieme.com/for-media/press-releases/1617-dfg-sponsors-open-access-transformation-project-library-consortium-and-thieme-embark-on-collaboration) the access fees of the participating German libraries into publication fees. The long-term goal is to flip the entire journal into open access. The model is funded by the DFG.

Agnes Kühlechner of Hogrefe publishing house stated that their share of open-access publications is very low compared to other publishers, even though all journals are available in hybrid open-access form. One explanation for this could be that many of the journals are published by different learned societies, which are committed to open access and a transformation of their journals to different degrees.

In turn, De Gruyter has learned that what works in the STM area can not be adapted to the HSS sector. On the one hand, because APC or hybrid models are not accepted, and on the other hand, because national transformation contracts are difficult to implement for (German-language) journals in the humanities and social sciences. For the transformation of humanities journals other transformation models and logics are needed. They have therefore begun to look at the respective author and subscription structure for each journal and to develop a transformation model based on this. Dr. Christina Lembrecht and Martina Naekel illustrated this work with a journal from the field of Library and Information Science.

The concluding lecture by Renate Wahlig dealt with the topic of consortia as actors of the open-access transformation. She stressed that the number of different open-access models recommended to libraries for implementation can lead to frustration and rejection. High complexity in procedures, in turn, often leads to delays and noncontracts. Here, too, there is no single suitable solution for all, but different groups of participants have different needs and requirements.


### Lessons learned

Consortia and (consortium) funding play a central role in the further development of science communication. The funds can be used, among other things, to relieve the burden on institutions with high publication rates and to support the development of balancing mechanisms with the highest possible transparency and awareness of the costs of publications. One approach can be to develop different contracts for different groups of participants - e.g. for institutions with high and low publication rates.

Open access as a central element of science communication is just as diverse as the publication cultures of the various scientific disciplines and requires correspondingly tailor-made approaches. Whether small steps or large-scale approaches are the means of choice also depends on the profile of the publishers. It is important to have clear negotiation goals and to allow failure to happen.
