<!--
.. title: Bericht zum Hands-On-Lab „Neue Geschäftsmodelle und Workflows im Open Access“ beim 7. Bibliothekskongress 2019
.. slug: bericht-handsonlab-Bibliothekskongress
.. date: 2019-05-03 8:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## Bericht zum Hands-On-Lab „Neue Geschäftsmodelle und Workflows im Open Access“ beim 7. Bibliothekskongress 2019

Es gibt weit mehr Open-Access-Geschäftsmodelle als die etablierten Varianten der Article oder Book Processing Charges. Das von uns veranstaltete Hands-On-Lab „Neue Geschäftsmodelle und Workflows im Open Access“ beim 7. Bibliothekskongress (= 108. Bibliothekstag) in Leipzig zielte daher darauf ab, den Teilnehmenden Konzepte und Ideen für weitere/andere Geschäftsmodelle im Bereich des Open-Access-Publizierens vorzustellen. Die gemeinsame Grundlage bildet dabei das Verständnis von wissenschaftlichen Verlagen, Intermediären, Händlern und Bibliotheken als Partner_innen im Publikationsprozess.
<!-- TEASER_END -->
<br />
### Drei Modelle für die Open-Access-Transformation

Nach der Begrüßung und Einführung in das Lab ging es auch schon mit dem ersten Impulsvortrag los. Dirk Pieper, Projektleiter von OA2020-DE, stellte das Modell **„Evidence-based open-access transformation“** vor. Das im Erwerbungsbereich bekannte Modell der “evidence-based selection” wird dabei für die Open-Access-Transformation von E-Books (Monographien und Sammelbände) vorwiegend im Bereich der Geistes- und Sozialwissenschaften adaptiert. Dadurch können elektronische Fachbücher nach einer Embargo-Periode von ein bis zwei Jahren in den Open Access gestellt werden. Im Anschluss an den Vortrag unterzogen die Teilnehmer_innen - aufgeteilt auf vier Gruppen - das Modell einer SWOT-Analyse und ermittelten die folgende Stärken und Schwächen:


| Stärken                                                | Schwächen                                                                 |
|--------------------------------------------------------|---------------------------------------------------------------------------|
| Gutes Zusammenspiel von Relevanz und Nutzung           | Delayed Open Access / reaktives Modell                                    |
| Open-Access-Stellung beliebter Titel                   | Ist der Titel überhaupt Open-Access-fähig?                                |
| Qualitätssicherung seitens der Verlag und Fachreferate | Wollen die Autor_innen überhaupt Open Access? Sie werden kaum einbezogen. |
| breiterer Zugang zu Open-Access-eBooks über Bibliothekskataloge | nutzungsbasierte "Bestseller" -> Nischenthemen werden vernachlässigt |
| kostengünstig, niedrigschwellig, integrierbar, bedarfsgerecht, objektiv, transparent | Preisvorstellung Verlag vs. Preisvorstellung Bibliothek |
| Chancen für Geistes- und Sozialwissenschaften für mehr Open Access | Beteiligen sich ausreichend viele Bibliotheken? |
| Komplementäres Modell zum sofortigen Open Access, überführt Altbestände in den Open Access | Verlage haben Mehraufwand im Rechtemanagement: müssen rückwirkend Rechte umwandeln bzw. neue Lizenzen verhandeln |


(Ergebnisse als [Foto](/assets/images/SWOTAnalyse_EBOA.jpg) (Link im neuen Tab öffnen))

<br />
Impulsvortrag 2 widmete sich dem sogenannten **„Kooperativen Open-Access-Deutschlandkonsortium“**. Dieses stellt ein kooperatives Finanzierungsmodell in Abwandlung der SCOAP³-Mechanismen dar. Statt alle Publikationen einer Fachdisziplin im Open Access zu veröffentlichen, zielt es darauf ab, die Finanzierung aller Publikationen mit einem deutschen Corresponding Author bei einem Open-Access-Verlag in Open-Access-Zeitschriften zu organisieren. Auch hier schloss sich direkt wieder die Gruppenarbeit zur Stärken-Schwächen-Chancen-Risiken-Analyse an und brachte die folgenden Ergebnisse:


| Stärken                                                               | Schwächen                                                                                              |
|-----------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------|
| Geringer Verwaltungsaufwand in wissenschaftlichen Einrichtungen       | Hoher konsortialer Koordinierungs- und Verwaltungsaufwand                                              |
| Schlanke Workflows für Bibliotheken und Autor_innen                   | Komplexes Modell                                                                                       |
| Moderate APCs, keine Kosten für Autor_innen                           | Keine Berücksichtigung von Monographien                                                                |
| Einbindung von Geistes- und Sozialwissenschaften möglich              | Fokus auf reine OA-Zeitschriften und OA-Verlage, daher eher auf Natur- und   Gesundheitswissenschaften |
| Größere Verhandlungsmacht gegenüber Verlagen                          | Vorhersagen für Publikationsverhalten sind schwierig                                                   |
| Test für Preisfindungsmechanismen nach weitgehender OA-Transformation | Wie bewertet man Qualität und fachliche Relevanz?   |



(Ergebnisse als [Foto](/assets/images/SWOTAnalyse_Konsortium.jpg) (Link im neuen Tab öffnen))

<br />
Das abschließende dritte Modell **„Subscribe to Open“** basiert darauf, dass die Bibliotheken weiterhin ihre Subskriptionszahlung für ein Abonnement beibehalten, der Inhalt dann jedoch für den jeweiligen Jahrgang für alle Open Access zur Verfügung steht. Eine letzte Runde Gruppenarbeit brachte dabei folgende Stärken und Schwächen des Modells hervor:


| Stärken                                                                                               | Schwächen                                                              |
|-------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------|
| Wenig Aufwand                                                                                         | Keine Kostentransparenz, da keine APCs                                 |
| Keine Mehrkosten für Bibliotheken, keine Kosten für Autor_innen                                       | Problematik der Zeitschriftenkrise nicht gelöst ("Old School")         |
| Eignet sich für Nischenzeitschriften                                                                  | Neues Rechtesystem der Verlage für Autor_innen notwendig               |
| Kann in die bestehenden Prozesse integriert werden                                                    | Fokus auf Abnehmer_innen statt Produzent_innen                         |
| Eignet sich für Zeitschriften mit eher geringer Überschneidung zwischen Leserschaft und Autorenschaft | Community und Autor_innen der Zeitschrift wissen vorher nicht, ob ein Artikel OA erscheint oder nicht -> schwierig in Verbingung mit Förderrichtlinien |
|                                                                                                       | Trittbrettfahrer-Problem nicht vollständig gelöst                      |


(Ergebnisse als [Foto](/assets/images/SWOTAnalyse_S2O.jpg) (Link im neuen Tab öffnen))

<br />
### Ergebnisse und Ausblick

Neben der Diskussion und Bewertung der Modelle durch die teilnehmenden Bibliothekar_innen und Verlagsvertreter_innen galt es weiterhin, Kriterien zu entwickeln, unter denen eine verlegerische und/oder bibliothekarische Beteiligung an Open-Access-Publikationsformen möglich ist. Die rund 35 Teilnehmer_innen einigten sich schlussendlich auf die folgenden Punkte, die erfüllt sein sollten:

* Verlagsrenommee
* Nachhaltige Open-Access-Transformation
* Qualitätssicherung
* Kostentransparenz und Kostenplanbarkeit
* Im Einklang mit der eigenen Open-Access-Policy
* Entsprechend den Fördervorgaben
* Freiheit beim Mitteleinsatz in den Bibliotheken

Das Hands-On-Lab hat gezeigt, dass die Herausforderungen und Chancen, die Open Access allen am Publikationsprozess Beteiligten bietet, vor allem im gemeinsamen Handeln bewältigt werden können. Dieser Eindruck wurde im Anschluss beim dritten Open-Access-Transformationsworkshop Anfang April in Bielefeld bestätigt (siehe [Bericht](/blog/2019/05/07/3oatransformationsworkshop/)). Die Folien zu dem Hands-On-Lab inklusive der Ergebnisse der Gruppenarbeiten stehen online auf dem OPUS-Server des BIB zur [Verfügung](https://nbn-resolving.org/urn:nbn:de:0290-opus4-164013).


Ein etwas ausführlicherer Bericht zu dem Hands-on-Lab ist im Kongressband zum 7. Bibliothekskongress erschienen:<br />
Jobmann, A. (2018). Bericht zum Hands-On-Lab „Neue Geschäftsmodelle und Workflows im Open Access“ beim 7. Bibliothekskongress in Leipzig am 19. März 2019. *O-Bib. Das Offene Bibliotheksjournal, 6*(4), 216-220. doi:[https://doi.org/10.5282/o-bib/2019H4S216-220](https://doi.org/10.5282/o-bib/2019H4S216-220)
