<!--
.. title: Report on the Hands-On-Lab "New business models and workflows in open access" at the 7th Library Congress 2019
.. slug: report-handsonlab-librarycongress
.. date: 2019-05-03 8:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## Report on the Hands-On-Lab "New business models and workflows in open access" at the 7th Library Congress 2019

There are far more open-access business models than the established variants of Article or Book Processing Charges. The Hands-On Lab "New business models and workflows in open access" organised by us at the 7th Library Congress (= 108th Library Day) in Leipzig therefore aimed to present the participants with concepts and ideas for further/other business models in the field of open-access publishing. The common basis is the understanding of scientific publishers, intermediaries, traders and libraries as partners in the publication process.
<!-- TEASER_END -->


### Three models for the open-access transformation

After the greeting and introduction into the lab we started with the first impulse lecture. Dirk Pieper, project manager of OA2020-DE, presented the model **"Evidence-based open-access transformation"**. The model of the "evidence-based selection" known in the field of acquisitions will be adapted for the open-access transformation of e-books (monographs and anthologies) mainly in the humanities and social sciences. This means that electronic textbooks can be made openly accessible after an embargo period of one to two years. Following the lecture, the participants - divided into four groups - subjected the model to a SWOT analysis and identified the following strengths and weaknesses:


| Strengths                                              | Weaknesses                                                                |
|--------------------------------------------------------|---------------------------------------------------------------------------|
| Good interaction of relevance and use                  | Delayed open access / reactive model                                      |
| Open access position of popular titles                 | Is the title open access capable at all?                                  |
| Quality assurance on the part of the publisher and subject specialists | Do the authors want open access at all? They are hardly involved. |
| Broader access to open-access eBooks via library catalogues | Use-based "bestsellers" -> niche topics are neglected |
| Cost-effective, low-threshold, integrable, needs-based, objective, transparent | Expected price publisher vs expected price library |
| Opportunities for the humanities and social sciences for more open access | Are enough libraries involved? |
| Complementary model for immediate open access, transfers old holdings into open access | Publishers have additional work in rights management: they have to retroactively convert rights or negotiate new licenses|


(Results as a [photo](/assets/images/SWOTAnalyse_EBOA.jpg) (please open the link in a new tab))

<br />
Lecture 2 was dedicated to the so-called **"Cooperative Open Access Germany Consortium"**. This represents a cooperative financing model in modification of the SCOAP³ mechanisms. Instead of publishing all publications of a discipline in open access, it aims to organise the financing of all publications with a German corresponding author by an open-access publisher in open-access journals. Here, too, the group work on the strengths-weaknesses-opportunities-risks analysis followed directly and produced the following results:


| Strengths                                                             | Weaknesses                                                                                             |
|-----------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------|
| Low administrative burden in scientific institutions                  | High level of consortium coordination and administrative effort                                        |
| Lean workflows for libraries and authors                              | Complex model                                                                                          |
| Moderate APCs, no costs for authors directly                          | No consideration of monographs                                                                         |
| Integration of humanities and social sciences possible                | Focus on pure OA journals and OA publishers, therefore more for the natural and health sciences        |
| Greater bargaining power vis-à-vis publishers                         | Predicting publication behavior is difficult                                                           |
| Test for pricing mechanisms after extensive OA transformation         | How do you evaluate quality and professional relevance?                                                |


(Results as a [photo](/assets/images/SWOTAnalyse_Konsortium.jpg) (please open the link in a new tab))


<br />
The final third model, **"Subscribe to Open"**, is based on the libraries continuing to keep their subscription payment for a subscription, but the content is then available to all open access for each year. A final round of group work revealed the following strengths and weaknesses of the model:


| Strengths                                                                                             | Weaknesses                                                             |
|-------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------|
| Little effort                                                                                         | No cost transparency, as no APCs are required                          |
| No additional costs for libraries, no costs for authors                                               | Problem of journal crisis not solved ("Old School")                    |
| Suitable for niche journals                                                                           | New rights system for publishers necessary for authors                 |
| Can be integrated into existing processes                                                             | Focus on buyers instead of producers                                   |
| Suitable for journals with rather little overlap between readership and authorship                    | Community and authors of the journal do not know beforehand whether an article OA appears or not -> difficult in connection with funder mandates |
|                                                                                                       | Free-rider problem not completely solved                               |


(Results as a [photo](/assets/images/SWOTAnalyse_S2O.jpg) (please open the link in a new tab))

<br />
### Results and outlook

In addition to the discussion and evaluation of the models by the participating librarians and publishing house representatives, it was also necessary to develop criteria under which a publishing and/or librarian participation in open-access publication forms is possible. The 35 or so participants finally agreed on the following points, which should be fulfilled:

* Publisher's reputation
* Sustainable open-access transformation
* Quality assurance
* Cost transparency and cost planning
* In line with our own open-access policy
* According to the funding requirements
* Freedom in the use of resources in the libraries

The Hands-On-Lab has shown that the challenges and opportunities open access offers to everyone involved in the publication process can be mastered above all through joint action. This impression was confirmed at the third OA2020-DE transformation workshop in Bielefeld at the beginning of April (see [report](/en/blog/2019/05/07/3oatransformationsworkshop/)). The slides for the Hands-On-Lab, including the results of the group work, are available online on the BIB OPUS [server](https://nbn-resolving.org/urn:nbn:de:0290-opus4-164013).


A somewhat more detailed report on the Hands-on-Lab has been published in the conference proceedings of the 7th Library Congress:<br />
Jobmann, A. (2018). Report on the Hands-On-Lab "New Business Models and Workflows in Open Access" at the 7th Library Congress in Leipzig on 19 March 2019. *O-Bib. Das Offene Bibliotheksjournal, 6*(4), 216-220. doi:[https://doi.org/10.5282/o-bib/2019H4S216-220](https://doi.org/10.5282/o-bib/2019H4S216-220)
