<!--
.. title: Subscribe to Open - Ein Modell für die Open-Access-Transformation von Zeitschriften
.. slug: S2O_OAtransformation_Zeitschriften
.. date: 2019-05-27 8:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Im Bereich der Open-Access-Transformation von Zeitschriften haben sich viele verschiedene Wege etabliert. Für die Auswahl des passenden Modells wird nicht nur die Publikationskultur berücksichtigt, sondern auch analysiert, wie sich Leser_innen, Autor_innen und Abonnenten zusammensetzten. Neben Faktoren wie Akzeptanz von Publikationskosten auf Seiten der Autor_innen (eher bei den Natur- und Gesundheitswissenschaften vorhanden), spielen die Überschneidung von Leser_innenschaft und Autor_innenschaft sowie die Menge und Typisierung der abonnierenden Einrichtungen ebenfalls eine Rolle. So lässt sich in den Geistes- und Sozialwissenschaften bspw. ein Article Processing Charges (APCs)-basiertes Modell kaum umsetzen, nicht zuletzt weil die Mittel und die Akzeptanz für APCs dort nicht vorhanden sind. Es braucht hier also alternative Transformationslogiken und -ansätze, die die genannten weiteren Faktoren berücksichtigen.
<!-- TEASER_END -->

## Idee

Ein solches Modell kann das sogenannte Subscribe-to-Open-Modell (S2O) sein. Dieses wurde von Annual Reviews als Ansatz für die Open-Access-Transformation gut eingeführter Subskriptionszeitschriften entwickelt und nutzt bestehende Kund_innen- und Abonnentenbeziehungen sowie Rechnungsworkflows nach. Einrichtungen, die die Inhalte der jeweiligen Zeitschriften kennen und schätzen, abonnieren diese wie gewohnt weiter, d.h. es fallen keine APCs oder zusätzlichen Kosten an. Solange der Verlag die Abonnementeinnahmen wie gewohnt erhält, wird der jeweilige Zeitschriftenband und das Archiv Open Access zur Verfügung gestellt. Wenn die Abonnementeinnahmen signifikant sinken, z.B. weil Einrichtungen sich fürs "Free Riding" entscheiden, kommen die neu erscheinenden Zeitschriftenbände wieder hinter eine Paywall und nur die Abonnenten haben weiterhin Zugang.

Mehr Infos dazu [hier](https://www.annualreviews.org/page/subscriptions/subscribe-to-open) und [hier](https://www.annualreviews.org/page/subscriptions/subscribe-to-open-faq).


## Analyse

Bei einer genauen Betrachtung des Modells erkennt man, dass seine Stärke vor allem darin liegt, mit wenig Aufwand und ohne Mehrkosten komplette Zeitschrifteninhalte in den Open Access zu transformieren. Dass es auf bestehende Workflows aufsetzt, ist aber auch gleichzeitig seine größte Schwäche: es ist ein Modell der "Alten Schule", das heißt es ändert weder etwas an der immer noch existierenden Zeitschriftenkrise noch an den bestehenden Marktlogiken. Da keine Abrechnung pro Publikation (z.B. über APCs) erfolgt, ist eine transparente Darstellung der Kosten ebenfalls schwierig. Hier sind noch entsprechende Mechanismen zu entwickeln. Außerdem muss gewährleistet sein, dass potentielle Autor_innen sicher sein können, dass ein als Open Access geplanter Artikel dann auch wirklich Open Access publiziert wird, schon allein wegen der existierenden Fördermandate.


## Fazit

Aufgrund seiner Charakteristika eignet sich das Subscribe-to-Open-Modell vor allem für Zeitschriften, bei denen die Überschneidung zwischen (potentieller) Autor_innenschaft und Leser_innenschaft gering ist, d. h. wo die Einrichtungen also vor allem an der Rezeption der Inhalte interessiert sind. Sein Potential liegt daher in der Aktivierung dieser Einrichtungen, durch das Beibehalten eines Abonnements den freien Zugang zu den Inhalten für alle zu garantieren.
