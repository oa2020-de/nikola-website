<!--
.. title: Subscribe to Open - A model for the open access-transformation of journals
.. slug: S2O_OAtransformation_journals
.. date: 2019-05-27 8:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

In the field of open-access transformation of scientific journals, many different paths have been established. In selecting the appropriate model, not only the publication culture is taken into account, but also the composition of readers, authors and subscribers. In addition to factors such as acceptance of publication costs on the part of the authors (more likely to exist in the natural and health sciences), the overlap between readers and authors as well as the amount and typology of the subscribing institutions also play a role. For example, an Article Processing Charges (APCs)-based model can hardly be implemented in the humanities and social sciences, not least because the resources and acceptance for APCs are not available there. Thus, alternative transformation logics and approaches are needed, which take into account the other factors mentioned.
<!-- TEASER_END -->

## Idea

Such a model can be the so-called Subscribe to Open model (S2O). Developed by Annual Reviews as an approach to the open-access transformation of well-established subscription journals, it leverages existing customer and subscriber relationships and billing workflows. Institutions that know and appreciate the contents of the respective journals continue to subscribe to them as usual, i.e. there are no APCs or additional costs. As long as the publisher receives the subscription income as usual, the respective journal volume and archive will be made available in open access. If subscription revenues fall significantly, e.g. because institutions opt for "free riding", the newly published journal volumes will again come behind a paywall and only the subscribers will still have access.

For more information see [this](https://www.annualreviews.org/page/subscriptions/subscribe-to-open) and [this](https://www.annualreviews.org/page/subscriptions/subscribe-to-open-faq).


## Analysis

A closer look at the model reveals that its strength lies above all in its ability to transform complete journal content into open access with little effort and at no additional cost. However, the fact that it is based on existing workflows is also its greatest weakness: it is a model of the "old school", i.e. it does not change anything about the still existing journal crisis or the existing market logic. Since there is no accounting per publication (e.g. via APCs), a transparent presentation of costs is also difficult. Here are still appropriate mechanisms to develop. In addition, it must be ensured that potential authors can be sure that an article planned as open access will then be published in the open-access format, if only because of the existing funding mandates.


## Conclusion

Due to its characteristics, the Subscribe to Open model is particularly suitable for journals where there is little overlap between (potential) authors and readers, i.e. where the institutions are primarily interested in the reception of the content. Its potential therefore lies in activating these institutions, by maintaining a subscription to guarantee free access to the content for all.


