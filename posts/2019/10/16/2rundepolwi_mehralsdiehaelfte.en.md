<!--
.. title: More than half is done! 19 Pledges for second run of OPEN Library Political Science
.. slug: morethanhalf_2runpolsci
.. date: 2019-10-16 08:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

### The second round of the OPEN Library Political Science is also well on its way to becoming a complete success!

Since the 14th of October there are 17 full and 2 sponsoring light pledges for the second run of [transcript OPEN Library Political Science 2020](https://oa2020-de.org/en/pages/transcriptopenlibrarypols/). On the one hand, more than half of the minimum participants are reached and, on the other hand, the response shows a continuously high level of interest in transparent and valid models for open-access transformation, especially in the humanities and social sciences.

Therefore, we are very pleased about the participation of the following libraries:
<!-- TEASER_END -->

1. University Library Wuppertal
2. University Library Oldenburg
3. University Library J. C. Senckenberg at Goethe-University Frankfurt
4. State Library Berlin (Staatsbibliothek zu Berlin)
5. University Library Vechta
6. University Library Vienna
7. University Library Bayreuth
8. University Library Bielefeld
9. University Library Hagen
10. University Library Kassel
11. University Library Koblenz-Landau
12. University Library Siegen
13. State and University Library Carl von Ossietzky Hamburg
14. University Library Leipzig
15. University and State Library Düsseldorf
16. University Library Passau
17. University Library Osnabrück
18. Regional Library Oldenburg
19. Federal Ministry of Defence

In addition, the entire project is financially supported by the Political Science Information Service (Pollux) at the Bremen State and University Library with 25% of the total costs.


### Background

Best practice open access is only possible in conjunction with appropriate cost recovery. With the previous financing models (author pays), the open-access transformation of books is limited to individual titles. The "Open-access package model" can be used to enable large-scale open-access transformation, because by distributing the costs incurred among all participants - the Open Library Community - open access becomes much cheaper than funding individual titles and the provision of entire specialist collections can be financed in this way.
Based on the well-known "e-book package" acquisition model, the frontlist - i.e. all planned new publications of the political science collection - will therefore be bundled into a single package. Instead of acquiring the e-book licence as usual, the participating libraries enable the open-access publication for a fee.
