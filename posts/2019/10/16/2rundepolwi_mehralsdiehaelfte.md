<!--
.. title: Mehr als die Hälfte geschafft! 19 Pledges für die zweite Runde der OPEN Library Politikwissenschaft
.. slug: mehr-als-die-hälfte-2runde-polwi
.. date: 2019-10-16 08:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

### Auch die zweite Runde der OPEN Library Politikwissenschaft ist auf einem guten Weg, ein voller Erfolg zu werden!

Seit dem 14. Oktober liegen 17 Voll- und 2 Sponsoring Light-Pledges für die zweite Runde der [transcript OPEN Library Politikwissenschaft 2020](https://oa2020-de.org/pages/transcriptopenlibrarypowi/) vor. Damit ist zum einen mehr als die Hälfte der Mindestteilnehmer_innen erreicht und zum anderen verdeutlicht der Zuspruch weiterhin ein hohes Interesse an transparenten und validen Modellen zur Open-Access-Transformation gerade auch in den Geistes- und Sozialwissenschaften.

Daher freuen wir uns sehr über Teilnahme der folgenden wissenschaftlichen Bibliotheken:
<!-- TEASER_END -->

1. Universitätsbibliothek der Bergischen Universität Wuppertal
2. Universitätsbibliothek Carl von Ossietzky-Universität Oldenburg
3. Universitätsbibliothek J. C. Senckenberg an der Goethe-Universität Frankfurt
4. Staatsbibliothek zu Berlin
5. Universitätsbibliothek Vechta
6. Universitätsbibliothek Wien
7. Universitätsbibliothek Bayreuth
8. Universitätsbibliothek Bielefeld
9. Universitätsbibliothek Hagen (Fernuni Hagen)
10. Universitätsbibliothek Kassel
11. Universitätsbibliothek Koblenz-Landau
12. Universitätsbibliothek Siegen
13. Staats- und Universitätsbibliothek Carl von Ossietzky Hamburg
14. Universitätsbibliothek Leipzig
15. Universitäts- und Landesbibliothek Düsseldorf
16. Universitätsbibliothek Passau
17. Universitätsbibliothek Osnabrück
18. Landesbibliothek Oldenburg
19. Bundesministerium der Verteidigung

Zusätzlich wird das ganze Projekt vom Fachinformationsdienst Politikwissenschaft (Pollux) an der Staats- und Universitätsbibliothek Bremen mit 25% der Gesamtkosten finanziell unterstützt.


### Hintergrund

Best-Practice-Open-Access ist nur in Verbindung mit entsprechender Kostendeckung möglich. Mit den bisherigen Finanzierungsmodellen (author pays) bleibt die Open-Access-Transformation von Büchern auf Einzeltitel beschränkt. Über das »Open-Access-Paketmodell« kann eine großflächige Open-Access-Transformation ermöglicht werden, denn durch die Verteilung der anfallenden Kosten auf alle Teilnehmenden – die Open Library Community – wird Open Access im Vergleich zur Förderung von Einzeltiteln deutlich günstiger und die Bereitstellung ganzer Fachkollektionen auf diese Weise finanzierbar.
Angelehnt an das bekannte Erwerbungsmodell »E-Book-Paket« wird daher die Frontlist – also alle geplanten Neuerscheinungen des Fachbereichs Politik – zu einem Paket gebündelt. Statt wie gewohnt die E-Book-Lizenz zu erwerben, ermöglichen die teilnehmenden Bibliotheken über eine Gebühr die Open-Access-Veröffentlichung.
