<!--
.. title: OA2020-DE veröffentlicht Forschungsbericht zum Mittelbedarf für Open Access
.. slug: bericht_mittelbedarf_openaccess
.. date: 2019-10-29 08:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## OA2020-DE veröffentlicht Transformationsrechnung zur Bestimmung des Mittelbedarfs für Open Access an ausgewählten deutschen Universitäten und Forschungseinrichtungen

Die Transformation subskriptionsbasierter, wissenschaftlicher Fachzeitschriften in den Open Access wird aller Voraussicht nach Änderungen in der finanziellen Belastung wissenschaftlicher Einrichtungen in Deutschland nach sich ziehen. APCs sind im Bereich der international sichtbaren und in einschlägigen bibliographischen Datenbanken indexierten Open-Access-Zeitschriften das derzeit dominierende Geschäftsmodell. Wenn sich dieses Geschäftsmodell durchsetzt, kommt es zu einer Umverteilung von Ausgaben für Fachzeitschriften. Um auf die Open-Access-Transformation adäquat im eigenen Hause zu reagieren und diese aktiv mitzugestalten, sind gesicherte Abschätzungen zu finanziellen Ent- bzw. Belastungen im Hinblick auf die zu erwartenden institutionellen APC-Gesamtausgaben der eigenen Einrichtungen nötig. An dieser Stelle setzt der vorliegende Bericht an.
<!-- TEASER_END -->

Für fünf deutsche Universitäten sowie ein Forschungsinstitut werden auf Basis der Publikationsdaten des Web of Science Abschätzungen zu den Gesamtausgaben für APCs erstellt und mit den derzeitigen Subskriptionsausgaben für die im Web of Science indexierten Zeitschriften verglichen. Der Bericht zeigt, dass die Kostenübernahme auf Basis der projizierten Ausgaben für Publikationen aus nicht-Drittmittel-geförderter Forschung für alle hier betrachteten Einrichtungen ohne Probleme aus den derzeitigen bibliothekarischen Erwerbungsetats für Zeitschriften bestritten werden könnte. Dies setzt jedoch voraus, dass Drittmittelgeber neben der üblichen Forschungsförderung auch für die APCs der aus diesen Projekten resultierenden Publikationen aufkommen. Trifft dies nicht zu und die wissenschaftliche Einrichtung muss für sämtliche Publikationen die APCs selbst tragen, so hängen die budgetären Auswirkungen wesentlich von der zukünftigen Entwicklung der Artikelbearbeitungsgebühren ab.<br />
In diesem Kontext schlägt der Bericht außerdem drei Mechanismen in Grundzügen vor, die sich mit dem finanziellen Ausgleich für sehr publikationsintensive Einrichtungen befassen.

*Schönfelder, N. (2019). Transformationsrechnung: Mittelbedarf für Open Access an ausgewählten deutschen Universitäten und Forschungseinrichtungen. [doi:10.4119/unibi/2937971](http://doi.org/10.4119/unibi/2937971)*

