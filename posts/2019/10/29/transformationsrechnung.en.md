<!--
.. title: OA2020-DE publishes research report on the funding requirements for open access
.. slug: researchreport_fundingrequirements_openaccess
.. date: 2019-10-29 08:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## OA2020-DE publishes transformation calculation to determine funding requirements for open access at selected German universities and research institutions

The transformation of subscription-based scientific journals into open access will in all likelihood lead to changes in the financial burden on scientific institutions in Germany. At present, APCs are the dominant business model for open-access journals that are indexed by relevant, international bibliographic databases. As this or comparable  business models become widely applied to journals and the open-access transition progresses, expenses for journals will be reallocated. In order to react adequately to the open-access transformation within one's own organisation and to play an active role in shaping it, it is necessary to make reliable estimates of the financial relief or burdens with regard to the expected total institutional APC expenditure of one's own institutions. The project OA2020-DE has prepared a report to illustrate how institutions can cost-model and prepare for the transition.
<!-- TEASER_END -->

The present report compares the current subscription costs for Web of Science-indexed journals with estimated, institution-wide open access publishing costs, which are projected based on the publication output data recorded in the Web of Science for five German universities and one research institute. The report shows that all projected APCs can be sustained without difficulty  by  re-purposing  current  journal  subscription  expenditures  for  articles  that  are  the results of third-party unfunded research, provided that the costs of open-access publication of articles resulting from grant-funded research are sustained by research funders. If research funders were to withdraw their support for open-access publishing, the budgetary consequences for institutions and their authors would depend heavily on the future development of APC price points.<br />
In this context, the report also proposes three mechanisms dealing with financial compensation for institutions with a high level of publication activity.

*Schönfelder, N. (2019). Transformation calculation: funding requirements for open access at selected German universities and research institutions. (only in german) [doi:10.4119/unibi/2937971](http://doi.org/10.4119/unibi/2937971)*
