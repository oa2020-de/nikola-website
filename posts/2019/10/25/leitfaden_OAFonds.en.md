<!--
.. title: Guidance document Open Access Publication Funds - Establishment and Funding Conditions
.. slug: guidance-openaccesspublicationfunds
.. date: 2019-10-25 08:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

<img src="/images/2019-Open-Access-Week-ENGLISH-986x310.png" alt="2019 Open Access Week Theme" width="600" height="200" display="block" />

As part of this year's [International Open Access Week](http://www.openaccessweek.org/), we would like to refer you to a publication by the Austrian Open Access Project [AT2OA](https://at2oa.at/): The guidance document **"Open Access Publication Funds - Establishment and Funding Conditions"** was created to provide a framework for the development of funding conditions for Open Access Publication Funds especially for institutions planning to set up an Open Access Publication Fund. In addition, it is meant to support a critical review of the funding conditions of existtng publication funds.

In keeping with this year's theme "Open for Whom? Equity in Open Knowledge", the paper offers a number of recommendations for standardising funding conditions with the aim of providing a clear framework for scientists, increasing cost efficiency and communicating uniform standards to Publishers.

You can find the guidance document on Zenodo: [https://doi.org/10.5281/zenodo.2653724](https://doi.org/10.5281/zenodo.2653724).
