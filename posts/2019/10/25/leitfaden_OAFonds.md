<!--
.. title: Leitfaden Open-Access-Publikationsfonds - Einrichtung und Förderbedingungen
.. slug: leitfaden-openaccesspublikationsfonds
.. date: 2019-10-25 08:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

<img src="/images/2019-Open-Access-Week-ENGLISH-986x310.png" alt="2019 Open Access Week Theme" width="600" height="200" display="block" />

Im Rahmen der diesjährigen [Internationalen Open-Access-Woche](http://www.openaccessweek.org/) möchten wir auf eine Publikation des Österreichischen Open-Access-Projektes [AT2OA](https://at2oa.at/) verweisen: Der Leitfaden **"Open-Access-Publikationsfonds. Einrichtung und Förderbedingungen"** wurde für die Gestaltung der Förderrichtlinien von Open-Access-Publikationsfonds konzipiert. Das Dokument soll insbesondere jenen Institutionen als Hilfsmittel dienen, die erstmals einen Open-Access-Publikationsfonds einrichten. Darüber hinaus möchte es auch Anstoß zur kritischen Durchsicht der Förderbedingungen bereits bestehender Publikationsfonds geben.

Passend zum diesjährigen Thema "Open for Whom? Equity in Open Knowledge" gibt das Papier einen Impuls zur Vereinheitlichung der Förderbedingungen verschiedener Einrichtungen. Eine solche Vereinheitlichung zielt darauf ab, klare Rahmenbedingungen für Wissenschaftler_innen zu schaffen, die Kosteneffizienz zu steigern und gegenüber Verlagen einheitliche Standards kommunizieren zu können.

Der Leitfaden steht unter [https://doi.org/10.5281/zenodo.2653724](https://doi.org/10.5281/zenodo.2653724) allen Interessierten zur Verfügung.

