<!--
.. title: Post-Grant Fund of the Federal Ministry of Education and Research for Open-Access Publications
.. slug: BMBF-postgrantfund-openaccess
.. date: 2019-07-31 08:00:00+1:00
.. author: Nina Schönfelder
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## Financial support possible for open-access publications from completed BMBF-funded projects

In its open-access strategy, the Federal Ministry of Education and Research in Germany (Bundesministerium für Bildung und Forschung - BMBF) aims at fostering the open-access transformation with a set of measures that seek to make open-access standard in the area of scholarly publishing. Along the lines of the [„FP7 post-grant Open Access pilot“](http://cerneu.web.cern.ch/ec-fp7-post-grant-open-access-pilot) 2015–2018 of the European Commission, the BMBF set up a [post-grant fund for open-access publications](https://www.bmbf.de/foerderungen/bekanntmachung-1404.html). Researchers can apply for funds to cover charges for open-access publications that resulted from finished BMBF-funded projects. A full funding can be granted for charges up to EUR 2,000 (excl. VAT) for each open-access publication.

On several occasions, the National Contact Point Open Access OA2020-DE was asked whether this BMBF post-grant funding is applicable to articles in journals only or to books as well. We contacted the project management agency, which is responsible for the implementation of the post-grant fund, with this question. They clarified that also open-access books can be funded via the BMBF post-grant fund under the same conditions (e.g. the same maximum amount).

**We would like to encourage research and higher education organizations with an open-access advisory service to point BMBF-funded researcher to the possibility of this funding for covering open-access publication charges.**
