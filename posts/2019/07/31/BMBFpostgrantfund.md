<!--
.. title: Post-Grant-Fund des Bundesministeriums für Bildung und Forschung für Open-Access-Publikationen
.. slug: BMBF-postgrantfund-openaccess
.. date: 2019-07-31 08:00:00+1:00
.. author: Nina Schönfelder
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## Finanzielle Unterstützung für Open-Access-Publikationen aus abgeschlossenen BMBF-geförderten Projekten möglich

Mit seiner Open-Access-Strategie hat sich das Bundesministerium für Bildung und Forschung (BMBF) zum Ziel gesetzt, den tiefgreifenden Wandel zum Open Access positiv zu begleiten und die Transformation zu unterstützen. Analog zum [„FP7 post-grant Open Access pilot“](http://cerneu.web.cern.ch/ec-fp7-post-grant-open-access-pilot) 2015–2018 der Europäischen Kommission hat das BMBF einen [Post-Grant-Fund](https://www.bmbf.de/foerderungen/bekanntmachung-1404.html) eingerichtet, der es ermöglicht, einen Antrag auf Förderung von Ausgaben für Gebühren zu stellen, die für Open-Access-Publikationen aus abgeschlossenen BMBF-geförderten Projekten entstehen. Eine volle Förderung der Veröffentlichungsausgaben kann bis zu einer Höhe von 2000,00 Euro (netto) je Publikation erfolgen.

Den Nationalen Open-Access-Kontaktpunkt OA2020-DE haben Anfragen erreicht, ob sich die Förderung nur auf Open-Access-Zeitschriftenpublikationen oder auch auf Open-Access-Buchpublikationen beziehe. Auf Nachfrage des Kontaktpunkts hat der für die Umsetzung der Förderrichtlinie zuständige Projektträger klargestellt, dass auch Open-Access-Buchpublikationen durch den BMBF-Post-Grant-Fund gefördert werden können. Die maximale Fördersumme bleibt dabei unverändert.

**Der Kontaktpunkt regt an, dass Einrichtungen im Rahmen ihrer Open-Access-Publikationsberatung BMBF-geförderte Wissenschafter_innen auf die Möglichkeit dieser Kostenübernahme hinweisen.**
