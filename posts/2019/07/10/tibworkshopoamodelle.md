<!--
.. title: Workshop an der TIB Hannover stellt Open-Access-Modelle von 4 Fachgesellschaften vor
.. slug: workshop-tib-oamodelle-fachgesellschaften
.. date: 2019-07-10 12:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Am 26. Juni 2019 präsentierten an der TIB Hannover vier Fachgesellschaften ihre für Deutschland bereits umgesetzten beziehungsweise geplanten Lizenzmodelle:

1. Institute of Physics Publishing (IOP) - Offsetting Vertrag "Institutional Research License"
2. Royal Society of Chemistry (RSC) - Read & Publish-Modell
3. American Chemical Society (ACS) - ab 2020 Offsetting
4. American Institute of Physics (AIP) - Open-Access-Pilotprojekte nach dem Read & Publish-Modell

... und stellten sich im Anschluss der Diskussion und dem Erfahrungsaustausch mit den Bibliothekar_innen.

"Der besondere Reiz der Veranstaltung lag nicht nur in der konzentrierten Information, sondern gerade auch in der Möglichkeit des direkten Vergleichs der verschiedenen Lizenzmodelle und administrativen Lösungen. Dabei wurde zumindest bei den vertretenen vier Organisationen deutlich, dass die Problemstellung nahezu identisch ist. Dies gilt beispielweise für eine sichere und dennoch eindeutige Identifizierung von Autorinnen und Autoren bei der Artikeleinreichung."

Zum vollständigen Workshopbericht bitte [hier](https://blogs.tib.eu/wp/tib/2019/07/02/open-access-lizenzmodelle-in-der-praxis-vier-verlage-stellen-sich-vor/) entlang.
