<!--
.. title: Save the date: ENABLE-Community-Building-Workshop
.. slug: enable_workshop
.. date: 2019-12-12 8:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## Workshop der ENABLE!-Community "Bibliotheken, Verlage und Autor*innen für Open Access in den Humanities und Social Sciences" am 28.01.2020

Bibliotheken sind mit kollektiven Strukturen zum gemeinsamen Erwerb von Inhalten in Form von Konsortien schon länger vertraut. Crowdfunding-Modelle zur Open-Access-Stellung von Publikationen sind für Bibliotheken (und Verlage) jedoch Neuland, da es in diesem Modell nicht nur um eine gemeinsame Finanzierung, sondern vor allem um das gemeinsame Ermöglichen von Open-Access-Publikationen geht. Damit werden Bibliotheken Teil einer Akteurskonstellation im Publikationsprozess, die auch Verlage, Autor*innen, Repositorien und den Buchhandel umfasst.

Von vielen Akteuren getragene Projekte schonen die Budgets der Einzelnen und erhöhen die Wahrscheinlichkeit des Gelingens der Vorhaben. Für alle Beteiligten wirft diese neue Form der Zusammenarbeit aber auch viele Fragen auf. Das Spektrum reicht von Fragen des Budgets, über die Qualitätssicherung, die konkreten Formen sowie die Sichtbarkeit der Kooperation. Da derzeit noch tragende Modi und Institutionen sowie das Vertrauen in diese innovative Partnerschaft fehlen, gilt es, in einen Erfahrungsaustausch zu treten, in dem alle aufkommenden Fragen, aber auch neue Modelle und Angebote, Ideen und Kritik einen Rahmen finden. Ziel des Austausches ist es, für die Zukunft belastbare Modelle und Modi des miteinander Publizierens zu entwickeln.
<!-- TEASER_END -->

Die Plattform [ENABLE! Bibliotheken, Verlage und Autor*innen für Open Access in den Humanities und Social Sciences](https://enable-oa.org/) verfolgt das Ziel, die Herausforderungen und Chancen, die Open Access allen Stakeholdern des Publizierens bietet, in Form eines Community-Building-Prozesses aufzugreifen und gemeinsam innovative Publikationsmodelle zu entwickeln. ENABLE! steht daher all jenen offen, die sich aktiv an neuen und partnerschaftlichen Open-Access-Projekten beteiligen und gemeinsam neue Formen des Open-Access-Co-Publishing entwickeln wollen.

**Vor diesem Hintergrund laden wir ganz herzlich zum initialen Community-Building-Workshop am 28. Januar 2020 an der Universität Bielefeld ein.** Dort wollen wir mit den Teilnehmer*innen Konzept und Zielsetzung der ENABLE!-Community, Partizipationsmöglichkeiten sowie konkrete Vorhaben diskutieren. Wichtig ist dabei, Potenziale und Hemmnisse für das Open-Access-Engagement auf allen Seiten zu identifizieren und gemeinsam an umsetzbaren Lösungen zu arbeiten.

Wenn Sie Interesse an einer Teilnahme haben, bitten wir um eine Anmeldung bis zum 20.12.2019, da die Plätze begrenzt sind. Der Workshop an sich ist kostenlos, die entstehenden Reisekosten können allerdings nicht übernommen werden.

[Hier geht es zu Anmeldung.](https://forms.gle/9TgXAjsjnWAQt5Hq6)

[Vorläufige Agenda](/assets/files/2019-12-11-Agenda Workshop zum Community-Building OA.pdf)
