<!--
.. title: The engagement of the many - A report on the workshop at the Open Access Days 2019 in Hanover
.. slug: workshop_oadays2019
.. date: 2019-12-16 8:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## Shaping sustainable open-access financing with collective approaches

Author/Article Processing Charges (APC) have established themselves as the standard form of financing open-access journal articles for the promotion of open-access publications. These are being supported by an increasing number of funding institutions. In addition, the funding of book publications according to a similar scheme by means of book-processing charges (BPC) is establishing itself. Both, however, have their limits, both in terms of the selection and level of funding as well as the funding mechanisms (which authors / institutions pay, which market distortions arise when individual publications are funded, what effects does APC dominance have on diversity in the publication market). A further challenge for open-access transformation lies in the often narrow limits and difficult coordination processes of existing institutional, regional or, at best, national funding opportunities.

In order to discuss these and similar aspects together, around 40 people from the fields of university libraries, research institutions, publishers, booksellers and learned societies came together for a workshop entitled "The engagment of the many" at the [Open Access Days 2019](https://open-access.net/community/open-access-tage/open-access-tage-2019). The following concrete questions were up for discussion:

1. Which cooperative approaches are regarded as particularly promising and why?
2. Have there already been any experiences with participation in cooperative financing approaches? If so, how do they turn out? And what are the associated risks?
3. What framework conditions need to be considered in order for cooperative approaches to be successful?

After an introductory [lecture](http://doi.org/10.5281/zenodo.3492214) on collective approaches to open-access publishing, the above questions were intensively discussed with the participants in the form of a World Café.
<!-- TEASER_END -->

**Promising cooperative approaches**

Experience from recent years has shown that there can be no one-size-fits-all solution for the organisation of open-access publication costs, but that tailored (cooperative) approaches must be developed based on the publication and reception culture of the respective discipline. Therefore, discipline-oriented approaches, models with memberships or small contributions as well as crowdfunding or crowdpledging approaches (especially for eBooks) are regarded as promising. Equally helpful are aspects such as social control by the community, author-friendly open access and the disclosure or definition of criteria for evaluating open-access models. Libraries can and should act as mediators and/or partners.

<img src="/images/Workshop_OATage2019_5.jpg" alt="Results for cooperative approaches" width="400" height="300" display="block" />

**Experiences with cooperative approaches**

So far, experience has been gained primarily with the following offerings: SCOAP³, SciPost, arXiv, transcript Open Library Political Science, the Open Library of Humanities, and LangSciPress. Models in which memberships either provide financial support for infrastructure (e.g. DOAJ or SCOSS) or enable authors to publish free of charge at a publishing house are also used (e.g. Cogitatio Press). Often project funds or other means are used and not necessarily the means of acquisition.
The exchange of experiences was accompanied by insights into risks and success criteria of cooperative approaches. Among other things, the topic of scalability was named as a risk, which concerns not only the manual determination of potential users but also the potential for application to other disciplines. The question of budgetary conditions must also be answered meaningfully. Often, a balance has to be struck between local responsibility and the general advantages of open-access innovations. This requires new lines of argumentation as well as decision paths and criteria for budgets. For the success of a cooperative model, it is advantageous to specify a specific period of time and to disclose the costs of concluding a contract as well as established products. In order to avoid budgetary problems, the institutions should be offered advantages through participation.

<img src="/images/Workshop_OATage2019_8.jpg" alt="Experiences with cooperative approaches" width="350" height="500" display="block" />

**Framework conditions for the success of cooperative approaches**

The workshop participants identified two main conditions for the success of cooperative approaches to sustainable open-access financing. Firstly, it is important that the models are simple, transparent and require little explanation. The easiest way to achieve this is to use familiar content and established series as well as a focus on small communities. On the other hand, there needs to be a high degree of agreement between producers and funders of research. This also means that the funding institution can see a concrete benefit, that the funding is institutionally regulated and that sustainability can be ensured. Fair partnerships between all actors provide a supportive framework.

<img src="/images/Workshop_OATage2019_9.jpg" alt="Framework conditions" width="350" height="500" display="block" />

**Lessons learned**

For a sustainable (financial) commitment of scientific institutions to open-access models, the question of the relationship between voluntariness and commitment needs to be clarified. The idea of solidarity alone will not be decisive. New/different selection criteria are also needed for these models, which are still new or still to be developed. One approach could be the aggregation of usage data for evidence-based decisions. This procedure is already used in COUNTER statistics and can be adapted to corresponding usage-based models. Here, the actors involved (authors - libraries - publishers - booksellers) must work together.


