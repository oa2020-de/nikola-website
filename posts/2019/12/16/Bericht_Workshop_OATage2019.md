<!--
.. title: Das Engagement der Vielen - Ein Bericht über den Workshop bei den Open-Access-Tagen 2019 in Hannover
.. slug: workshop_oatage2019
.. date: 2019-12-16 8:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## Mit kollektiven Ansätzen die nachhaltige Open-Access-Finanzierung gestalten

Für die Förderung von Open-Access-Publikationen haben sich Author/Article Processing Charges (APC) als Standard-Finanzierungsform von Open-Access-Zeitschriftenartikeln etabliert. Diese werden von immer mehr Förderinstitutionen unterstützt. Daneben etabliert sich die Förderung von Buch-Publikationen nach ähnlichem Schema mittels Book-Processing-Charges (BPC). Beides hat jedoch Grenzen, sowohl was die Auswahl und die Höhe der Finanzierung als auch die Mechanismen der Förderung angeht (welche Autor_innen/Einrichtungen zahlen, welche Marktverzerrungen entstehen, wenn Einzelpublikationen gefördert werden, welche Auswirkungen hat die APC-Dominanz auf die Vielfalt im Publikationsmarkt). Eine weitere Herausforderung für die Open-Access-Transformation besteht in den oft engen Grenzen und schwierigen Abstimmungsprozessen der bisherigen institutionellen, regionalen oder bestenfalls nationalen Finanzierungsmöglichkeiten.

Um diese und ähnliche Aspekte gemeinsam zu diskutieren, kamen bei den [Open-Access-Tagen 2019](https://open-access.net/community/open-access-tage/open-access-tage-2019) rund 40 Personen aus den Bereichen Hochschul- bzw. Universitätsbibliothek, Außeruniversitäre Forschungseinrichtung, Verlag, Buchhandel und Fachgesellschaften für einen Workshop mit dem Titel „Das Engagement der Vielen“ zusammen. Folgende konkrete Fragestellungen standen zur Diskussion:

1. Welche kooperativen Ansätze werden als besonders vielversprechend angesehen und warum?
2. Gibt es schon Erfahrungen bei der Beteiligung an kooperativen Finanzierungsansätzen? Falls ja, wie fallen diese aus? Und welche Risiken sind damit verbunden?
3. Welche Rahmenbedingungen müssen bedacht werden, damit kooperative Ansätze erfolgreich sein können?

Nach einem einführenden [Vortrag](http://doi.org/10.5281/zenodo.3492214) zu kollektiven Ansätzen des Open-Access-Publizierens, wurden die genannten Fragestellungen in Form eines Worldcafes mit den Teilnehmer_innen intensiv diskutiert.
<!-- TEASER_END -->

**Vielversprechende kooperative Ansätze**

Die Erfahrungen aus den letzten Jahren zeigen, dass es keine One-size-fits-all–Lösung für die Organisation der Open-Access-Publikationskosten geben kann, sondern passgenaue (kooperative) Ansätze basierend auf der Publikations- und Rezeptionskultur der jeweiligen Fachdisziplin entwickelt werden müssen. Als vielversprechend werden daher disziplinorientierte Ansätze, Modelle mit Mitgliedschaften bzw. kleinen Beiträgen sowie Crowdfunding bzw. Crowdpledging-Ansätze (hier vor allem für eBooks) angesehen. Ebenfalls hiflreich sind Aspekte wie die soziale Kontrolle durch die Community, autor_innenfreundliches Open Access und die Offenlegung bzw. Festlegung von Kriterien zur Bewertung von Open-Access-Modellen. Bibliotheken können und sollen hierbei als Vermittler_innen und/oder Partner_innen auftreten.

<img src="/images/Workshop_OATage2019_5.jpg" alt="Workshopergebnisse kooperative Ansätze" width="400" height="300" display="block" />

**Erfahrungen mit kooperativen Ansätzen**

Erfahrungen wurden bisher vor allem mit den folgenden Angeboten gemacht: SCOAP³, SciPost, arXiv, transcript Open Library Politikwissenschaft, der Open Library of Humanities sowie  LangSciPress. Auch Modelle, bei denen über Mitgliedschaften entweder Infrastruktur finanziell unterstützt (z. B. das DOAJ oder SCOSS) oder das für die Autor_innen kostenlose Publizieren bei einem Verlag ermöglicht wird, finden Anwendung (z. B. Cogitatio Press). Oft werden dafür Projektgelder oder sonstige Mittel verwendet und nicht zwangsläufig die Erwerbungsmittel.
Der Erfahrungsaustausch wurde begleitet von Erkenntnissen über Risiken und Erfolgskriterien kooperativer Ansätze. Als Risiko wurde u.a. das Thema Skalierbakeit benannt, das neben der manuellen Ermittlung von potentiellen Nutzer_innen und auch das Anwendungspotential auf andere Disziplinen betrifft. Auch die Frage nach haushaltsrechtlichen Bedingungen gilt es sinnvoll zu beantworten. So muss häufig zwischen der lokalen Verantwortung und den allgemeinen Vorteilen von Open-Access-Innovationen abgewogen werden. Dafür sind neue Argumentationslinien sowie Entscheidungswege und -kriterien für Budgets notwendig. Für den Erfolg eines kooperativen Modells sind die Angabe eines konkreten Zeitraums sowie die Offenlegung der Höhe der Kosten für den Vertragsabschluss sowie etablierte Produkte von Vorteil. Um haushaltsrechtliche Probleme zu vermeiden, sollten den Einrichtungen Vorteile durch die Teilnahme angeboten werden.

<img src="/images/Workshop_OATage2019_8.jpg" alt="Erfahrungen mit kooperativen Ansätzen" width="350" height="500" display="block" />

**Rahmenbedingungen für das Gelingen von kooperativen Ansätzen**

Die Teilnehmenden des Workshops identifizierten zwei Hauptbedingungen für das Gelingen von kooperativen Ansätzen zur nachhaltigen Open-Access-Finanzierung. Zum einen es wichtig, dass die Modelle einfach, transparent und wenig erklärungsbedürftig sind. Das lässt sich am einfachsten über bekannte Inhalte und etablierte Reihen sowie den Fokus auf kleine Communities erreichen. Zum anderen braucht es eine hohe Übereinstimmung zwischen Produzierenden und Finanzierenden der Forschung. Dazu gehört auch, dass für die finanzierende Einrichtung ein konkreter Nutzen oder Benefit ersichtlich ist, dass die Finanzierung institutionell geregelt wird und die Nachhaltigkeit sichergestellt werden kann. Faire Partnerschaften zwischen allen Akteuren geben dem Ganzen einen unterstützenden Rahmen.

<img src="/images/Workshop_OATage2019_9.jpg" alt="Rahmenbedingungen" width="350" height="500" display="block" />

**Gewonnene Erkenntnisse**

Für ein nachhaltiges (finanzielles) Engagement von wissenschaftlichen Einrichtungen für Open-Access-Modelle gilt es die Frage des Verhältnisses zwischen Freiwilligkeit und Verpflichtung zu klären. Der Solidaritätsgedanke allein wird nicht den Ausschlag geben. Auch werden neue/andere Auswahlkriterien für diese noch neuen bzw. noch zu entwickelnden Modelle benötigt. Ein Ansatz könnte die Aggregation von Nutzungsdaten für evidenzbasierte Entscheidungen sein. Dieses Verfahren wird auch jetzt schon im Rahmen der COUNTER-Statistiken angewandt und kann an entsprechende, nutzungsbasierte Modelle angepasst werden. Hier müssen die beteiligten Akteure (Autor_innen – Bibliotheken – Verlage – Buchhandel) gemeinsam vorgehen.
