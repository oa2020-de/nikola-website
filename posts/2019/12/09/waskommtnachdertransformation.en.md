<!--
.. title: What follows transformative agreements?
.. slug: what_follows_transformative_agreements
.. date: 2019-12-09 10:00:00+01:00
.. tags:
.. author: Nina Schönfelder
.. category:
.. link:
.. description:
.. type: text
-->

## A proposal to implement tender procedures and introduce competition between publishers in the context of national open-access consortia

Open-access transformation of scholarly publications is a declared goal of [Coalition S](https://www.coalition-s.org/) and the [OA2020 initiative](https://oa2020.org/) and their supporting institutions. To reach a large-scale open-access transition of journals, established subscription-based journals should flip to open access. Currently, transformative agreements are favored to achieve that aim, like the agreements that have been negotiated by the [project DEAL](https://www.projekt-deal.de/about-deal/) with the three biggest international publishers (Elsevier, Springer Nature, and Wiley) for several years. Transformative agreements’ design and conditions are bilaterally negotiated between publisher and library consortia. Often, previous subscription expenditures serve as a starting point for negotiations. In addition, the DEAL-Wiley Read-and-Access agreement takes over APCs for publications in pure open-access journals with a 20% discount on list price. The DEAL-Springer agreement is supposed to be signed under similar conditions.

But what can we expect after the tipping point, when transformative agreement worldwide lead to a widespread flip of scholarly journals to open access? Which actors will become important, how agreements will be shaped, which price-setting mechanisms will be applied, how will be the funding of open-access publishing organized—and finally—will we success in breaking the ever increasing subscription costs with open access, or will we be faced with a new APC price spiral?
<!-- TEASER_END -->

We need to deliberate on these questions to ensure that the path towards open access succeeds in a favorable, long-term outcome. If libraries want to play a decisive role in the future information provision in terms of funding and provision with open-access publications, they have to conclude pure open-access contracts on behalf of their researchers and centrally organize the funding of APCs, BPCs and other open-access costs. In light of a continuously rising publication output on the one hand and fixed budgets of research performing organizations on the other hand, we propose to implement mechanisms that introduce competition on prices and services between publishers to ensure that the open-access publication system is economically sustainable in the long-term.

The basic idea is that libraries conclude contracts only with publishers offering the best value-for-money for publications in open-access journals (APC-service-quality ratio). Eligible researchers (e.g., affiliated corresponding authors) can publish in these journals without author-facing costs or administrative burden. For publications in open-access journals not covered by such contracts, the authors have to organize funding themselves. Therefore, the provision with such contracts has price-signaling and publication-channeling effects. Authors will tend to publish more frequently in journals that are covered by an open-access contract and have a good price-performance ratio, rather than in journals with a poor price-performance ratio, if only because of the effort involved in regulating the assumption of costs. Nonetheless, the freedom of research is preserved to the same extent as today. Authors are free to choose how and where to publish their articles, either in open-access journals, hybrid journals or subscription-based journals.

A published research article is like a mini monopoly; it cannot be substituted with other articles. However, the process of (open-access) publishing is a service that can be substituted between publishers and journals to some degree. Therefore, publishing services can be integrated in competitive processes. [SCOAP³](https://scoap3.org/) successfully established competition on price and service for open-access publishing within high-energy physics.

Even though the integration of open-access journals in transformative agreement is the right path to transform library acquisition budgets from subscription to open-access funding on a large scale, the question is why consortia do not conclude similar agreements with pure open-access publishers.
Asymmetric funding without good reasons can lead to undue disadvantages for less favored publishers. If structures in libraries (agreements, workflows etc.) favor publishing in open-access journals of big, traditionally subscription-based publisher over that of medium-sized and small publishers, the latter run the risk of being forced out of the market. The result would be a further concentration on a few, large providers.

For two reasons, we propose to establish a national open-access consortium with as much research libraries and institutions as possible, that regularly pay significant amounts of APCs to pure open-access journals and publishers: firstly, to better anchor the funding of open-access publishing in higher-education and research institutions, and secondly, to introduce mechanisms that limit cost and price increases, respectively, in the long term. This consortium aims to finance publications of German corresponding authors in open-access journals publishers through a tender procedure, which correlates quality and price. The pilot project is not subject specific and will focus particularly on pure open-access publishers as a start.

The concept for a “German Consortium for Publishing in Open Access Journals” was first presented by Dr Nina Schönfelder at the [OASPA 2019 Annual Conference](https://oaspa.org/event/coasp-2019/) in Copenhagen during the “Show and Tell” session. A conference recording of the talk as well as the presentation slides are available [here](https://oaspavideos.org/video/show-and-tell-2019) (32.–41. min). We currently develop the concept in detail, which will be published at the beginning of the next year.
