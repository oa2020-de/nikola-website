<!--
.. title: Are APCs the dominant business model for open-access journals?
.. slug: APCs-dominantbusinessmodel
.. date: 2019-08-19 08:00:00+1:00
.. author: Nina Schönfelder
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

# Are APCs the dominant business model for open-access journals?

### …Or is it just one important among many others?

That is an open question. To approach it, we looked into available figures on open-access journals.

Out of the 13,337 journals that are listed in the [DOAJ](https://doaj.org), a broad majority (9,752 journals) do not charge APCs (there is no information according APCs for 53 journals, as of 2019-06-05). However, the largest open-access journals, which partially have a very high reputation, pursue quite often an APC business model, for example, PLOS ONE, Scientific Reports, Nature Communications, RSC Advances, Optics Express and Nucleic Acids Research. These journals publish minimum one thousand articles, reviews and proceedings papers per year. So, what’s right?

As often the answer is: It depends.
<!-- TEASER_END -->

If we look at the number of open-access journals, the answer is clearly “no” according to the DOAJ. Only, one quarter of the journals charge APCs. However, some journals have very low APCs so that they probably rely on additional income streams. On the other hand, some journals do not demand APCs from authors (or their parent institution) but charge other organizations for publications, for example, SCOAP3 journals.

If we look at the number of publications in open-access journals, we arrive at a different conclusion. To find out how many articles, review and proceedings papers (in short, publications) were published the last years in DOAJ-indexed journals, we looked into the [„CWTS Journal Indicators“](http://www.journalindicators.com/) of the “Centre for Science and Technology Studies” at the Leiden University, the Netherlands. The CWTS Journal Indicators report the amount of publications from 2015 to 2017 for each journal that is indexed in the bibliographic database [Scopus](https://www.scopus.com/) for a sufficiently long time.

For our analysis, we collected the DOAJ metadata as of 2019-06-05 and the CWTS Journal Indicators from May 2019, and merged both datasets in [R](https://www.r-project.org) to get the intersection. Out of the 13,337 journals that are indexed in the DOAJ, 3,846 have been sufficiently long indexed in Scopus to calculate and publish the CWTS Journal Indicators. Out of these 3,846 journals, 1,831 charge APCs whereas 2,010 do not (no information is available for five journals). In total, 768,509 publications were published in APC-based open-access journals vs. 257,272 in no-APC journals from 2015–2017. The following figure summarizes the numbers:

<img src="/images/Abbildung zum Blogbeitrag APCs das dominierende Geschäftsmodell englisch.png" alt="Illustration of the blog post APC's dominant business model" width="600" height="350" display="block" />

We can conclude that APCs are the dominant business model in open-access journals that are indexed in Scopus. Although many journals do not charge APCs, the majority of articles, reviews and proceedings papers is published in APC-based journals. Unfortunately, we do not know whether this is also true for open-access journals not listed in Scopus. Although the DOAJ additionally provides article-level metadata, their evaluation is much more complex.

In total, APCs are the dominant business model for open-access journals, but there are remarkable differences between subject areas. As Walt Crawford proved in his latest book [„Gold Open Access 2013–2018: Articles in Journals (GOA4)“](https://waltcrawford.name/goa4.pdf), in which he analyzed DOAJ metadata, about 70% of the articles are fee-based (APCs, submission fees etc.) in biomed and STEM journals in contrast to about 20% in the humanities and social sciences. We can confirm this for journals index in both the DOAJ and Scopus.

|       | APCs | No APCs | No information |
|-------|------|------------|--------------|
| Health Sciences | 231,215 | 79,386 | 52 |
| Life Sciences | 313,927 | 37,204 | 0 |
| Physical Sciences | 199,856 | 82,299 | 193 |
| Social Sciences & Humanities | 23,511 | 58,383 | 162 |

*Table: Number of publications 2015–2017 by journal subject area and business model*

In this blog post, we have shown that APCs are the dominant business model for open-access journals in the natural and life sciences that are indexed by relevant, international bibliographic databases. The currently negotiated transformation agreements with the Publish & Read model also aim to operate the journals transformed into open access with the APC-business model in the long term.
