<!--
.. title: Sind APCs das dominierende Geschäftsmodell bei Open-Access-Zeitschriften?
.. slug: APCs-dominierendes-Modell
.. date: 2019-08-19 08:00:00+1:00
.. author: Nina Schönfelder
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

# Sind APCs das dominierende Geschäftsmodell bei Open-Access-Zeitschriften?

### …Oder ist es nur ein wichtiges von vielen?

Diese Frage beschäftigt die Gemüter. Um ihrer Beantwortung ein Stück weit näher zu kommen, haben wir genau in die Zahlen geschaut.

Einerseits erheben von den 13.337 im [DOAJ](https://doaj.org) verzeichneten Zeitschriften 9.752 keine APCs, was eine deutlichen Mehrheit ist (bei 53 Zeitschriften gibt es hierzu keine Informationen, Stand 05.06.2019). Andererseits scheinen die größten, teils auch sehr angesehenen Open-Access-Zeitschriften doch sehr häufig ihr Geschäftsmodell auf APCs aufzubauen (bspw. PLOS ONE, Scientific Reports, Nature Communications, RSC Advances, Optics Express, Nucleic Acids Research). Dies sind Zeitschriften, die jeweils mind. eintausend Artikel, Reviews und Konferenzbeiträge pro Jahr publizieren. Was also stimmt?

Die Antwort heißt wie so häufig: Es kommt darauf an!
<!-- TEASER_END -->

Betrachtet man nur die Anzahl der Open-Access-Zeitschriften, so lautet die Antwort auf Basis des DOAJ ganz klar: Nein. APCs sind bei nur einem Viertel der gelisteten Zeitschriften das zugrunde liegende Geschäftsmodell. Wobei es durchaus Zeitschriften gibt, die eine APC verlangen, diese aber so niedrig ist, dass zum Betrieb noch weitere Einkünfte nötig sind. Auf der anderen Seite gibt es auch Zeitschriften, die nicht den Autor_innen (oder ihren affiliierten Einrichtungen), sondern anderen Organisationen APCs in Rechnung stellen, bspw. SCOAP3-Zeitschriften.

Fügt man nun der Betrachtung noch den Umfang an Publikationen der Zeitschriften als weitere Dimension hinzu (also für wie viele Beiträge in Open-Access-Zeitschriften müssen APCs gezahlt werden), ergibt sich ein etwas anderes Bild. Um herauszufinden, wie viele Beiträge die im DOAJ-gelisteten Open-Access-Zeitschriften in den letzten Jahren publiziert haben, konsultierten wir die [„CWTS Journal Indicators“](http://www.journalindicators.com/) des „Centre for Science and Technology Studies“ an der Leiden Universität in den Niederlanden, die auf der bibliometrischen Datenbank [Scopus](https://www.scopus.com/) basieren. Die CWTS Journal Indicators weisen für alle, lang genug in Scopus indexierten Zeitschriften die Summe der Articles, Conference Proceedings Papers und Reviews (kurz: Publikationen) der Jahre 2015–2017 aus.

Für die Analyse haben wir die DOAJ-Metadaten vom 05.06.2019 sowie die CWTS Journal Indicators vom Mai 2019 in die [Statistiksoftware R](https://www.r-project.org/) eingelesen und die Schnittmenge ermittelt. Von den 13.337 im DOAJ verzeichneten Zeitschriften sind 3.846 lang genug in Scopus indexiert, damit die CWTS Journal Indicators berechnet und veröffentlicht werden konnten. Von diesen 3.846 Zeitschriften erheben 1.831 Zeitschriften APCs; 2.010 Zeitschriften tun dies nicht; zu fünf Zeitschriften liegen keine APC-Informationen vor. Zählt man alle Publikationen zusammen, die in den jeweiligen Zeitschriften in den drei Jahren 2015-2017 publiziert worden sind, ergibt dies 257.272 Publikationen in Zeitschriften, die zurzeit keine APCs erheben vs. 768.509 Publikationen in zurzeit APC-basierten Zeitschriften. Folgende Abbildung fasst die Zahlen zusammen:

<img src="/images/Abbildung zum Blogbeitrag APCs das dominierende Geschäftsmodell.png" alt="Abbildung zum Blogbeitrag APCs das dominierende Geschäftsmodell" width="600" height="350" display="block" />

Aus den Zahlen lässt sich folgern, dass APCs das dominierende Geschäftsmodell für die in Scopus indexierten Open-Access-Zeitschriften sind. Auch wenn viele Open-Access-Zeitschriften keine APCs erheben, wird der Großteil der Publikationen in APC-basierten Open-Access-Zeitschriften veröffentlicht. Ob diese Schlussfolgerung auch für nicht in Scopus indexierte Open-Access-Zeitschriften gilt, können wir zurzeit nicht sagen. Zwar hält das DOAJ Metadaten auf Artikelebene bereit, ihre Auswertung ist jedoch um einiges aufwendiger.

Hinzu kommt, dass es natürlich auch zwischen den Fachbereichen beachtliche Unterschiede im Publikationsaufkommen und der APC-Verbreitung gibt. Walt Crawford hat in seinem jüngsten Buch zu [„Gold Open Access 2013–2018: Articles in Journals (GOA4)“](https://waltcrawford.name/goa4.pdf) auf Basis des DOAJ nachgewiesen, dass in den Bereichen Biomed sowie STEM etwa 70% der Artikel gebührenfinanziert sind (APCs, Einreichungsgebühren etc.), in den Humanities and Social Sciences jedoch nur etwa 20%. Dieses Bild können wir durch die eigene Analyse der Schnittmenge aus DOAJ und Scopus bestätigen:

|       | APCs | Keine APCs | Keine Angabe |
|-------|------|------------|--------------|
| Gesundheitswissenschaften | 231.215 | 79.386 | 52 |
| Lebenswissenschaften | 313.927 | 37.204 | 0 |
| Physik u. ä . | 199.856 | 82.299 | 193 |
| Sozial- und Geisteswissenschaften | 23.511 | 58.383 | 162 |

*Tabelle: Anzahl der Publikationen 2015–2017 in Zeitschriften nach Fachbereich und Geschäftsmodell*

Der Blogbeitrag zeigt, dass APCs für natur- und lebenswissenschaftliche, international sichtbare und in einschlägigen bibliometrischen Datenbanken indexierte Open-Access-Zeitschriften das dominierende Geschäftsmodell sind. Die zur Zeit verhandelten Transformationsverträge mit Publish&Read-Modell zielen ebenfalls langfristig darauf ab, die so
in den Open Access transformierten Zeitschriften mit dem APC-Geschäftsmodell zu betreiben.
