<!--
.. title: 2nd round transcript OPEN Library - first three pledges achieved
.. slug: polwi2020-firstthreepledges
.. date: 2019-08-21 09:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

We received our first three pledges for the ebook-openaccess-transformation-project [transcript OPEN Library Political Science 2020](https://oa2020-de.org/en/pages/transcriptopenlibrarypols/)! It's good to have

+ University Library Frankfurt/Main,
+ University Library Kassel and
+ Staatsbibliothek zu Berlin - Preußischer Kulturbesitz

on board! And we are sure that more will follow.

Find out more about the project:

+ [https://oa2020-de.org/pages/transcriptopenlibrarypowi/](https://oa2020-de.org/en/pages/transcriptopenlibrarypols/)
+ [https://www.transcript-verlag.de/open-library-politikwissenschaft](https://www.transcript-verlag.de/open-library-politikwissenschaft)
+ [http://knowledgeunlatched.org/political-science/](http://knowledgeunlatched.org/political-science/)
