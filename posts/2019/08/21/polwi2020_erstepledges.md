<!--
.. title: 2. Runde transcript OPEN Library - die ersten 3 Pledges sind da
.. slug: polwi2020-erstedreipledges
.. date: 2019-08-21 09:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Für das frisch gestartete Projekt [transcript Open Library Politikwissenschaft 2020](https://www.transcript-verlag.de/open-library-politikwissenschaft) gibt es schon die ersten drei Pledges:

* Die Universitätsbibliothek Frankfurt am Main,
* die Universitätsbibliothek Kassel und
* die Staatsbibliothek zu Berlin - Preußischer Kulturbesitz

haben ihre finanzielle Unterstützung zugesagt. Alle drei Bibliotheken waren schon an der OPEN Library Politikwissenschaft 2019 beteiligt, daher freuen wir uns sehr über die erneute Unterstützung und sind sicher, dass diesem Aufschlag noch viele weitere folgen werden.

Mehr über das Projekt transcript Open Library Politikwissenschaft 2020 erfahren Sie unter:

+ [https://oa2020-de.org/pages/transcriptopenlibrarypowi/](https://oa2020-de.org/pages/transcriptopenlibrarypowi/)
+ [https://www.transcript-verlag.de/open-library-politikwissenschaft](https://www.transcript-verlag.de/open-library-politikwissenschaft)
+ [http://knowledgeunlatched.org/political-science/](http://knowledgeunlatched.org/political-science/)
