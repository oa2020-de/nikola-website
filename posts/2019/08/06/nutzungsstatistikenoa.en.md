<!--
.. title: Organization-specific usage reports for open-access publications: useful or unnecessary?
.. slug: usagereports-openaccess
.. date: 2019-08-06 08:00:00+1:00
.. author: Nina Schönfelder
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## Organization-specific usage reports for open-access publications: useful or unnecessary?

In scientific libraries, loan statistics and standardized usage statistics (COUNTER reports) are well-established instruments for the library acquisition management. In the course of the open-access transformation, the collection and usefulness of organization-specific usage reports are called into question (see, e.g. Rösch, Henriette (2019): Open Access als Zumutung für die Erwerbung? Auswirkungen der Open-Access-Transformation auf die Erwerbungs- und Bestandspolitik der Bibliotheken. In: b.i.t.online 22 (3). Online available at [https://www.b-i-t-online.de/heft/2019-03-fachbeitrag-roesch.pdf](https://www.b-i-t-online.de/heft/2019-03-fachbeitrag-roesch.pdf)). The Association of European Research Libraries (LIBER) considers this issue as an essential requirement in its principles for negotiations with publishers ([“Usage Reports Should Include Open Access”](https://libereurope.eu/blog/2017/09/07/open-access-five-principles-for-negotiations-with-publishers/)); others have a quite different position.
We think that it is important to collect and evaluate organization-specific usage statistics for open-access publications. This blog post explains the reasons and aims at contributing to the ongoing discussion.
<!-- TEASER_END -->

Several funding and business models exist to enable gold open-access publications. They roughly can be categorized via two dimensions:

+ The writing, i.e. the possibility of publication by an academic author, is in the foreground and must be financed by the affiliated institution or a research funder.
+ The Reading, i.e. the possibility of receiving literature for researchers, students and interested parties, is in the foreground. This means that publications that were previously only accessible to subscribers are now to be transformed into open access, whereby the co-financing of such open-access publications not only ensures free access for one's own members, but also for other libraries and the rest of the world.

The library co-funding of gold open-access publications can also be motivated by a combination of “writing” and “reading” approach (with varying weighting), for example if books in collections and series as well as articles in journals are not only read and cited by faculty members and students but also if researcher regularly publish in those venues.


## Examples of useful statistics

The model ["transcript OPEN Library Political Science"](https://doi.org/10.3390/publications7030055) can be used to show how usage statistics can be usefully applied in the open-access area. In this model, an informal library network enables the open-access publishing of the complete 2019’s front list in political science by transcript via crowdfunding. Until the end of the pledging period, the libraries had no information, whose authors would publish in that front list. The publisher was only able to provide information of the number of titles.
Many of the co-funding libraries are familiar with transcript and are used to buy its books. Acquisition librarians could evaluate via loan statistics and COUNTER reports that closed-access transcript books are indeed used by faculty members and student.

Loan statistics are inapplicable on open-access book if libraries do not buy print copies. The discontinuation of organization-specific COUNTER reports for open-access books would cause a complete loss of feedback whether the library co-funded open-access books are indeed used by own faculty members and students. This raises the question why the respective library should engage in the open-access co-funding of future collections from, for example, transcript. The main responsibility of libraries at research and higher-education institutions is to provide their students and faculty members with access to research literature. If the usage cannot be proved, the libraries will eventually withdraw their financial resources until the business model collapses and no new publications can appear in open access.

The organization-specific usage reports also play a prominent role in the funding and business model [„subscribe to open“](https://oa2020-de.org/en/blog/2019/05/27/S2O_OAtransformation_journals/) developed by Annual Reviews. New subscribers to that model will be identified and current subscribers confirmed in their resolution via organization-specific usage reports. The reports prove the usage of the open-access content of Annual Reviews journals by the students and faculty members, so that it makes sense for the respective library to co-finance the open-access publishing of new articles.

## Conclusion

To sum up:

+ Organization-specific usage reports for open-access publications are largely irrelevant for business models that focus exclusively on writing (i.e. the possibility of publication) as a motivation for assuming the costs of open-access publications.
+ Organization-specific usage reports for open-access publications are important for business models where the reception of open-access publications is (partly) the motivation for assuming the costs.

Although usage statistics cannot record the complete usage of open-access publications by members of an organization (IP range of the organization vs. private IP addresses of members), COUNTER reports have the potential to be an important instrument for decision support for libraries when it comes to participate in the co-funding of open-access journals and books, particularly if those content is of interest for their students and faculty members.
