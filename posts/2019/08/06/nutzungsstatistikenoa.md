<!--
.. title: Einrichtungsbasierte Nutzungsstatistiken für Open-Access-Publikationen: Sinnvoll oder unnötig?
.. slug: nutzungsstatistik-openaccess
.. date: 2019-08-06 08:00:00+1:00
.. author: Nina Schönfelder
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## Einrichtungsbasierte Nutzungsstatistiken für Open-Access-Publikationen: Sinnvoll oder unnötig?

In wissenschaftlichen Bibliotheken sind Ausleihstatistiken und standardisierte Nutzungsstatistiken (COUNTER-Reports) bewährte Instrumente zur Steuerung der Literaturerwerbung. Im Zuge der Open-Access-Transformation wird jedoch die Erhebung und der Nutzen von einrichtungsbasierten Nutzungsstatistiken zur Entscheidungsunterstützung hinterfragt (siehe beispielsweise Rösch, Henriette (2019): Open Access als Zumutung für die Erwerbung? Auswirkungen der Open-Access-Transformation auf die Erwerbungs- und Bestandspolitik der Bibliotheken. In: b.i.t.online 22 (3). Online verfügbar unter [https://www.b-i-t-online.de/heft/2019-03-fachbeitrag-roesch.pdf](https://www.b-i-t-online.de/heft/2019-03-fachbeitrag-roesch.pdf)). Während LIBER (Ligue des Bibliothèques Européennes de Recherche – Association of European Research Libraries) diesen Punkt in den Verhandlungsrichtlinien für Transformationsverträge als eine essentielle Forderung ansieht ([„Usage Reports Should Include Open Access“](https://libereurope.eu/blog/2017/09/07/open-access-five-principles-for-negotiations-with-publishers/)), sehen andere Akteure dies anders.
Der Nationale Open-Access-Kontaktpunkt OA2020-DE erachtet es z. B. als wichtig, einrichtungsbasierte Nutzungsstatistiken für Open-Access-Publikationen zu erheben und auszuwerten. Die Gründe hierfür möchten wir in diesem Blogbeitrag erläutern und damit einen Beitrag zu Diskussion leisten.
<!-- TEASER_END -->

Für die Veröffentlichung wissenschaftlicher Publikationen unmittelbar im Open Access (*Goldenes Open Access*) gibt es verschiedene Finanzierung- bzw. Geschäftsmodelle. Diese kann man grob danach einordnen, ob

+ das Schreiben, also die Publikationsmöglichkeit eines wissenschaftlichen Autors oder einer wissenschaftlichen Autorin im Vordergrund steht und über die jeweilige Einrichtung oder einen Forschungsförderer finanziert werden muss, oder
+ das Lesen, also die Rezeptionsmöglichkeit von Literatur für Forschende, Studierende und Interessierte im Vordergrund steht. Es also darum geht Publikationen, die bisher nur Angehörigen von kaufenden oder lizenznehmenden Einrichtungen zugänglich waren, in den Open Access zu stellen, wobei die Mitfinanzierung solcher Open-Access-Publikationen dann nicht nur den freien Zugang für die eigenen Angehörigen sichert, sondern auch für andere Bibliotheken und den Rest der Welt.

Die bibliotheksseitige Mitfinanzierung von Gold-Open-Access-Publikationen kann auch durch eine Kombination vom „schreibenden“ und „lesenden“ Ansatz (mit unterschiedlichen Gewichtungen) motiviert sein. Dies liegt vor, wenn Bücher in Programmbereichen und Reihen sowie Artikel in wissenschaftlichen Fachzeitschriften nicht nur von den eigenen Angehörigen rezipiert werden, sondern diese auch regelmäßig in den jeweiligen Publikationsorganen veröffentlichen.


## Beispiele für sinnvoll eingesetzte Nutzungsstatistiken

Am Modell [„transcript OPEN Library Politikwissenschaft“](https://www.transcript-verlag.de/open-library-politikwissenschaft) lässt sich aufzeigen, wie Nutzungsstatistiken im Open-Access-Bereich sinnvoll Anwendung finden können. In diesem Modell stellt ein informelles Netzwerk von wissenschaftlichen Bibliotheken über Crowdfunding die gesamte 2019er Frontlist des Programmbereichs Politikwissenschaften von transcript in den Open Access. Bis zum Ende des Finanzierungszeitraums war nicht genau bekannt, welche Autor_innen welche Bücher publizieren werden. Nur die Titelmenge war auf Grund der Verlagsarbeit genau bekannt und angekündigt.
Viele der Bibliotheken, die sich in der letzten Finanzierungsrunde an dem Modell beteiligt haben, kennen den Verlag und die Programmreihe und haben früher regelmäßig seine Bücher gekauft. Das Feedback, dass die transcript-Bücher von den Angehörigen ihrer Einrichtung tatsächlich genutzt werden, haben Erwerbungsabteilungen über Ausleihstatistiken und - für die elektronische Version - über COUNTER-Reports erhalten.
Ausleihstatistiken entfallen bei Open-Access-Büchern, wenn keine Print-Version angeschafft wurde. Der Wegfall von einrichtungsbasierten COUNTER-Nutzungsstatistiken bei Open-Access-Büchern würde nun dazu führen, dass Bibliotheken gar keine Anhaltspunkte mehr dafür hätten, ob und wie oft das von ihnen mitfinanzierte Open-Access-Buch von den eigenen Angehörigen genutzt wird. Damit ist die Begründung hinfällig, warum ausgerechnet diese Bibliothek sich an der Finanzierung eines öffentlichen Gutes, dem Open-Access-Buch, beteiligen sollte. Primärer Auftrag der wissenschaftlichen Bibliothek ist die Versorgung der Studierenden und Forschenden mit Literatur. Wenn die Nutzung dieser Open-Access-Literatur nicht nachgewiesen werden kann, werden sich die Bibliotheken aus der Finanzierung über kurz oder lang zurückziehen – bis das Finanzierungsmodell kollabiert und keine neuen Publikationen im goldenen Open Access erscheinen.

Auch bei dem Finanzierungsmodell [„Subscribe to Open“](https://oa2020-de.org/blog/2019/05/27/S2O_OAtransformation_Zeitschriften/) von Annual Reviews spielen die einrichtungsbasierten Nutzungsstatistiken eine große Rolle. Neue Subscribe-to-Open-Abonnent_innen sollen darüber akquiriert, bestehende Abonnent_innen durch diese in ihrer Entscheidung bestärkt werden. Durch einrichtungsbasierte Nutzungsstatistiken weist der Verlag nach, dass seine Inhalte von den jeweiligen Angehörigen rezipiert werden und es deshalb für die Bibliothek sinnvoll ist, sich an der Finanzierung dieser (und weiterer) Open-Access-Inhalte zu beteiligen.


## Fazit

Zusammengefasst kann man folgendes sagen:

+ Für Open-Access-Finanzierungsmodelle, die ausschließlich auf das Schreiben (also die Publikationsmöglichkeit) als Motivation für die Kostenübernahme von Open-Access-Publikationen setzen, sind einrichtungsbasierte Nutzungszahlen weitgehend irrelevant.
+ Für Open-Access-Finanzierungsmodelle, wo die Rezeption von Open-Access-Publikationen (teilweise) Motivation für die Kostenübernahme ist, bleiben einrichtungsbasierte Nutzungsstatistiken wichtig.

Selbst wenn einrichtungsbasierte Statistiken die Nutzung durch Angehörige der wissenschaftlichen Einrichtung nicht vollständig erfassen können (IP-Bereich der Einrichtung vs. IP-Adressen der privaten Wohnung der Angehörigen), sind COUNTER-Statistiken ein wichtiges Instrument zur Entscheidungsunterstützung über die Mitfinanzierung von Open-Access-Publikationen, vor allem wenn den wissenschaftlichen Einrichtungen auch an der Rezeption der Inhalte gelegen ist.
