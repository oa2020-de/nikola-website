<!--
.. title: transcript OPEN Library Political Science enters second round
.. slug: start-polwi2020
.. date: 2019-08-13 13:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

After the successful first round of the [transcript OPEN Library Political Science (2019)](https://www.transcript-verlag.de/open-library-politikwissenschaft), the follow-up project can now be started: The transcript publishing house's entire frontlist Political Science 2020 is now available for open-access transformation in the consortium model (crowdfunding). And once again, the Political Science Information Service (POLLUX) will be there as a supporter, paying 25% of the package costs.

**The transcript OPEN Library Political Science in a nutshell**

+ 22 new titles
+ Instead of purchasing a licence to access the content, participating libraries help to make content available open access
+ Thanks to the collaborative model the costs per institution can be kept at an affordable level
+ A minimum of 30 pledging libraries is required. If the minimum level of 30 libraries is not reached, then no frontlist titles will be made available OA and no library will be charged
+ The maximum price per participant is € 2,420 (excl. VAT)
+ Participation is possible via a library supplier (Dietmar Dreier International Library Supplier, Missing Link mail order bookstore, Schweitzer Fachinformation) or via an account with Knowledge Unlatched
+ The pledging period will remain open until 30th November 2019

Find out more: [https://www.transcript-verlag.de/open-library-politikwissenschaft](https://www.transcript-verlag.de/open-library-politikwissenschaft) and at the pages of OA2020-DE: [transcript OPEN Library Political Science](/en/pages/transcriptopenlibrarypols)

