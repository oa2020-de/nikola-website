<!--
.. title: transcript OPEN Library Politikwissenschaft geht in die zweite Runde
.. slug: start-polwi2020
.. date: 2019-08-13 13:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Nach der erfolgreichen ersten Runde der [transcript Open Library Politikwissenschaft (2019)](https://www.transcript-verlag.de/open-library-politikwissenschaft) kann nun das Folgeprojekt gestartet werden: Die gesamte Frontlist Politikwissenschaft 2020 des transcript-Verlages steht ab sofort im Konsortialmodell (Crowdfunding) für die Open-Access-Tranformation bereit. Und auch diesmal ist der Fachinformationsdienst Politik (POLLUX) als Unterstützer wieder mit dabei und übernimmt 25% der Paketkosten.

**Die Eckdaten auf einen Blick**

+ 22 Neuerscheinungen (der gesamte Programmbereich Politikwissenschaft)
+ Finanzierung eines "E-Book-Pakets": Statt des Kaufs von Campuslizenzen wird die Open-Access-Bereitstellung von Büchern finanziert
+ Durch das Crowdfunding wird die Open-Access-Transformation für alle Akteure finanziell tragbar
+ Mindestteilnehmerzahl: 30 Einrichtungen. Wird das Mindestquorum nicht erreicht, erfolgt keine Fakturierung und kein Open-Access-Erscheinen der Frontlist
+ Der maximale Rechnungsbetrag beträgt 2.420,00 € (zzgl. MwSt) für das gesamte Paket
+ Die Teilnahme ist über einen Bibliothekslieferanten (Schweitzer Fachinformation, Missing Link, Dietmar Dreier) oder über ein Konto bei Knowledge Unlatched möglich
+ Der Finanzierungszeitraum endet am 30.11.2019

Weitere Informationen unter: [https://www.transcript-verlag.de/open-library-politikwissenschaft](https://www.transcript-verlag.de/open-library-politikwissenschaft) und auf den Seiten von OA2020-DE: [transcript OPEN Library Politikwissenschaft](/pages/transcriptopenlibrarypolwi)
