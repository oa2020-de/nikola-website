<!--
.. title: OA2020-DE veröffentlicht Forschungsbericht zum Publikationsaufkommen an deutschen Wissenschaftseinrichtungen
.. slug: forschungsbericht_publikationsaufkommen
.. date: 2019-09-24 08:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## OA2020-DE-Forschungsbericht zum Publikationsaufkommen und zur Verteilung wissenschaftlicher Artikel im Kontext der Open-Access-Transformation an deutschen Wissenschaftseinrichtungen veröffentlicht

Mit der Open-Access-Transformation wird die Umstellung des Standard-Geschäftsmodells für wissenschaftliche Verlage von Subskription auf das Open-Access-Publizieren angestrebt. Damit verbunden sind Änderungen in den Geschäftsprozessen und der Abrechnungslogik, in deren Zentrum zukünftig die einzelne Publikation steht. Der [DEAL-Vertrag mit Wiley](https://www.projekt-deal.de/wiley-vertrag/) ist dabei ein erster Schritt, dem weitere folgen werden (siehe das [DEAL-Springer-MoU](https://www.projekt-deal.de/eckpunkte-des-memorandum-of-understanding-mit-springer-nature/)). Natürlich werden Belastungen und Entlastungen, die sich aus diesem neuen Berechnungsmodell ergeben, ungleich und vor allem anders verteilt sein als im bisherigen Subskriptionsmodell und daraus werden sich Anforderungen an eine finanzielle und strukturelle Neuorganisation an den einzelnen Wissenschaftseinrichtungen ergeben. Wissenschaftliche Einrichtungen darin zu unterstützen, ist eine Aufgabe des Projekts „Nationaler Open-Access-Kontaktpunkt OA2020-DE“, in dessen Rahmen der hier vorgestellte Bericht erstellt wurde.
<!-- TEASER_END -->

Berichte zu Publikationsaufkommen und -verteilung wissenschaftlicher Artikel können als Grundlage dafür dienen, die strategischen und organisatorischen Voraussetzungen für die umfassende Open-Access-Transformation an wissenschaftlichen Einrichtungen zu schaffen. Viele Universitäten, Hochschulen und Forschungseinrichtungen erheben regelmäßig Daten über die Publikationsaktivitäten ihrer wissenschaftlichen Autorinnen und Autoren. Dennoch hat sich insbesondere im direkten Nachgang zum ersten DEAL-Vertragsabschluss deutlich gezeigt, dass die Publikationsdatenanalyse gerade für kleinere Einrichtungen eine Herausforderung darstellt. Dies gilt vor allem dann, wenn es im Kontext der Transformation um eine Aggregation von Publikationsdaten auf Verlagsebene geht.

Der Forschungsbericht hat daher zum Ziel, exemplarisch die Publikationszusammenhänge an sechs deutschen Wissenschaftseinrichtungen bei den einzelnen Wissenschaftsverlagen aufzuzeigen und die Publikationsanteile in reinen Open-Access-Zeitschriften darzustellen. Er dient als Ideengeber, Vorlage und Hilfestellung für eigene Publikationsdatenanalysen und daraus abzuleitende Handlungsmaßnahmen im Kontext der Open-Access-Transformation. Einrichtungen ohne Zugang zu Datenbanken wie dem Web of Science oder Scopus können für eine solche Analyse künftig den [Open-Access-Monitor](https://open-access-monitor.de/#/) des Forschungszentrums Jülich verwenden.


*Schönfelder, N., Jobmann, A., Pollack, P., & Ecker, D. (2019). OA2020-DE-Forschungsbericht zum Publikationsaufkommen und zur Verteilung wissenschaftlicher Artikel im Kontext der Open-Access-Transformation an deutschen Wissenschaftseinrichtungen. [doi:10.4119/unibi/2937155](http://doi.org/10.4119/unibi/2937155)*
