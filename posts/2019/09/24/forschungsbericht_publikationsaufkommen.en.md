<!--
.. title: OA2020-DE research report on the number of publications at German science institutions published
.. slug: researchreport_numberofpublications
.. date: 2019-09-24 08:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## OA2020-DE research report on the number of publications and distribution of scientific articles in the context of open-access transformation at German research institutions published

The aim of the open-access transformation is to convert the standard business model for scientific publishers from subscription to open-access publishing. This will involve changes in the business processes and billing logic, which in future will focus on the individual publication. The [DEAL contract with Wiley](https://www.projekt-deal.de/wiley-contract/) is a first step, which will be followed by others (see the [DEAL-Springer-MoU](https://www.projekt-deal.de/springer-nature-news/)). Of course, the burdens and reliefs resulting from this new calculation model will be unequal and, above all, distributed differently than in the previous subscription model and this will result in requirements for a financial and structural reorganization of the individual scientific institutions. Supporting scientific institutions in this is one of the tasks of the "National Contact Point Open Access OA2020-DE" project, in which the report presented here has been prepared.
<!-- TEASER_END -->

Reports on publication volume and distribution of scientific articles can serve as a basis for creating the strategic and organisational prerequisites for comprehensive open-access transformation at scientific institutions. Many universities, colleges and research institutions regularly collect data on the publication activities of their scientific authors. Nevertheless, especially in the immediate aftermath of the first DEAL contract, it has become clear that publication data analysis is a challenge, especially for smaller institutions. This is particularly true when the context of the transformation involves the aggregation of publication data at publisher level.

The aim of the research report is therefore to illustrate the publication contexts at six German scientific institutions with the individual scientific publishers and to present the publication proportions in pure open-access journals. It serves as a source of ideas, a template and support for own publication data analyses and the resulting action to be taken in the context of open-access transformation. In future, institutions without access to databases such as the Web of Science or Scopus will be able to use the [Open-Access Monitor](https://open-access-monitor.de/#/) of Research Center Jülich for such an analysis.


*Schönfelder, N., Jobmann, A., Pollack, P., & Ecker, D. (2019). OA2020-DE research report on the number of publications and distribution of scientific articles in the context of open-access transformation at German research institutions. (only in german) [doi:10.4119/unibi/2937155](http://doi.org/10.4119/unibi/2937155)*


