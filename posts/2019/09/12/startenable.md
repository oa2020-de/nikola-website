<!--
.. title: How to build a community? Start der ENABLE-Plattform für kooperative Open-Access-Projekte
.. slug: start_enableplattform
.. date: 2019-09-12 08:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## Einladung zur Kooperation – ENABLE! Bibliotheken, Verlage und Autor*innen für Open Access in den Humanities und Social Sciences

Bibliotheken sind mit kollektiven Strukturen zum gemeinsamen Erwerb von Inhalten in Form von Konsortien schon länger vertraut. Crowdfunding-Modelle zur Open-Access-Stellung von Publikationen sind für Bibliotheken (und Verlage) jedoch Neuland, da es in diesem Modell nicht nur um eine gemeinsame Finanzierung, sondern vor allem um das gemeinsame Ermöglichen von Open-Access-Publikationen geht. Damit werden Bibliotheken Teil einer Akteurskonstellation im Publikationsprozess, die auch Verlage und Autor_innen umfasst.

Unser gemeinsames Anliegen ist daher die Entwicklung einer inklusiven und von allen Beteiligten getragene Open-Access-Kultur, die am Bewährten anknüpft und sich zugleich Neuem öffnet. Um dies sicherzustellen, braucht es den Erfahrungsaustausch, neue Formen der Zusammenarbeit sowie veränderte Finanzierungsmodelle. Die Vielheit von Akteuren in der Welt des geistes- und sozialwissenschaftlichen Publizierens möchten wir als Reichtum begreifen und daraus etwas formen, das Vorteile für alle Beteiligten bietet und Bibliotheken und Verlagen in neuer partnerschaftlicher Konstellation eine solide Zukunftsperspektive aufzeigt.
<!-- TEASER_END -->

Die heute gelaunchte Plattform [„ENABLE! Bibliotheken, Verlage und Autor*innen für Open Access in den Humanities und Social Sciences“](https://enable-oa.org/) soll...

+ alle geplanten oder bereits laufenden Kooperationsprojekte zwischen Bibliotheken und Verlagen (und weiteren Akteuren) im Open Access in den Geistes- und Sozialwissenschaften abbilden sowie
+ den Austausch, der diese Projekte begleitet und instruiert (Fragen, Kommentare etc.), kanalisieren.

**Es geht also darum, einen öffentlichen Raum für dieses gemeinsame Ermöglichen von Open Access bereitzustellen.**

<br />
### Konzept

[ENABLE!](https://enable-oa.org/) ist eine thematisch ausgerichtete Community mit dem Fokus auf Open Access in den Geistes- und Sozialwissenschaften (Humanities and Social Sciences). Sie dient dazu, die für eine Open-Access-Publikation notwendigen Akteure mit ihren jeweiligen Rollen zusammen zu bringen. Autor_innen, Verlage, Bibliotheken, Intermediäre, Händler und Forschungsförderer werden eine Gemeinschaft von sogenannten Open-Access-Enablern und gestalten die Open-Access-Transformation der Geistes- und Sozialwissenschaften zusammen. Voraussetzung dafür sind gegenseitiges Vertrauen, stimmige Rollen und Modelle sowie ein hohes Maß an Transparenz.

Die kleinteilige Struktur in den Geistes- und  Sozialwissenschaften stellt dabei eine Chance für ein von Vielen getragenes, faires Open Access dar. Der Zusammenschluss dieser Vielen zu einer Community führt weiterhin zu einer Verringerung der Organisations- und Grenzkosten für die Erstellung des Kollektivguts „Open-Access-Publikation“. Somit wird die Attraktivität und Nützlichkeit des Produktes erhöht und die Wahrscheinlichkeit gesteigert, dass sich eine ausreichende Menge an der Erstellung und Finanzierung beteiligt.

Grundlegend lässt sich also sagen, dass ein von allen Stakeholdern des Publizierens getragenes Open Access in den Geistes- und Sozialwissenschaften

1. eine Open-Access-affine Haltung und die Kapazität der Organisationsentwicklung,
2. ein Low-Profit-Modell für die Open-Access-Charges,
3. Crowdbildung bzw. enge Kooperationen auf allen Ebenen als Weg in die Transformation,
4. die Situation der kleinen Verlage,
5. die Inklusion der Leser_innen/Nutzer_innen,
6. Community-Building-Prozesse und
7. inklusive Geschäftsmodelle braucht.

<br />
### Umsetzung

Im Austausch zwischen dem Nationalen Open-Access-Kontaktpunkt OA2020-DE und dem transcript Verlag entstand die Idee, diesen Community-Building-Prozess in der ersten Phase durch eine einfache Internetplattform zu unterstützen (so ist die [ENABLE!-Plattform](https://enable-oa.org/) entstanden) und, falls sich diese Form bewährt, durch Workshops und andere Formate des Austauschs sowie ggf. weitere Maßnahmen zu ergänzen. Zusammen mit der Plattform geht ein [Forum](https://forum.enable-oa.org/) online, das als Austauschplattform für die Community-Mitglieder_innen dient und dafür verwendet werden kann, Partner_innen für neue, gemeinsame Open-Access-Projekte zu finden, Open-Access-Themen zu diskutieren oder Informationen auszutauschen.

**Hiermit laden wir Sie herzlich ein, sich am Aufbau der Community zu beteiligen!**
