<!--
.. title: How to build a community? Start of ENABLE, a platform for cooperative projects in open access
.. slug: start_enableplatform
.. date: 2019-09-12 08:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## Invitation to cooperation - ENABLE! Libraries, Publishers and Authors for Open Access in the Humanities and Social Sciences

Libraries have long been familiar with collective structures for the joint acquisition of content in the form of consortia. However, crowdfunding models for the open-access provision of publications are new territory for libraries (and publishers), since this model is not only about joint financing, but above all about joint enabling of open-access publications. Libraries thus become part of a constellation of actors in the publication process that also includes publishers and authors.

Our common concern is therefore the development of an inclusive open-access culture, supported by all those involved, which ties in with the tried and tested and at the same time opens up to new ideas. To ensure this, we need an exchange of experience, new forms of cooperation and changed financing models. We would like to see the diversity of actors in the world of humanities and social sciences publishing as a wealth and to use it to create something that offers advantages for all those involved and offers libraries and publishers a solid perspective for the future in a new partnership constellation.
<!-- TEASER_END -->

Today launched platform ["ENABLE! Libraries, Publishers and Authors for Open Access in the Humanities and Social Sciences"](https://enable-oa.org) is intended to

* illustrate all planned or ongoing cooperation projects between libraries and publishers (and other actors) in open access in the humanities and social sciences and
* to channel the exchange that accompanies and instructs these projects (questions, comments, etc.).

**It is therefore a matter of providing a public space for this joint enabling of open access.**

<br />
### Concept

[ENABLE!](https://enable-oa.org) is a thematically oriented community with a focus on open access in the humanities and social sciences. Its purpose is to bring together the actors necessary for an open-access publication with their respective roles. Authors, publishers, libraries, intermediaries, traders and research funders become a community of so-called open-access enablers and shape the open-access transformation of the humanities and social sciences together. This requires mutual trust, coherent roles and models, and a high degree of transparency.

The small-scale structure in the humanities and social sciences represents an opportunity for fair open access supported by many. The merger of these many to form a community also leads to a reduction in the organisational and marginal costs for the production of the collective good "open-access publication". This increases the attractiveness and usefulness of the product and the probability that a sufficient quantity will participate in its production and financing.

Open access in the humanities and social sciences, which is supported by all stakeholders involved in publishing, therefore needs

1. an open-access approach and organisational development capacity,
2. a low-profit model for the open-access charges,
3. crowd formation and close cooperation at all levels as a path to transformation,
4. the situation of small publishers,
5. inclusion of readers and users,
6. community-building processes and
7. inclusive business models.


<br />
### Implementation

The exchange between the National Contact Point Open Access OA2020-DE and transcript publishing house gave rise to the idea of supporting this community-building process in the first phase by means of a simple internet platform (this is how the [ENABLE! platform](https://enable-oa.org) came into being) and, if this form proves its worth, to supplement it with workshops and other exchange formats or activities. Together with the platform, a [forum](https://forum.enable-oa.org/) goes online, which serves as an exchange platform for community members and can be used to find partners for new, joint open-access projects, discuss open-access topics or exchange information.

**We cordially invite you to participate in the development of the community!**
