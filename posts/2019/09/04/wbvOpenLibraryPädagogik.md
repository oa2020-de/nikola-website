<!--
.. title: Erfolgreiche Adaption der OPEN Library auf eine Teildisziplin der Pädagogik
.. slug: wbvmedia_openlibrary_pädagogik
.. date: 2019-09-04 08:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## wbv Media setzt den Weg eines wissenschafts- und bibliotheksfreundlichen Open Access in der Pädagogik fort

[Pressemitteilung]

*Zusammenarbeit mit OA2020-DE und Knowledge Unlatched*

**Bielefeld/Berlin, 3. September 2019**: wbv Publikation - ein Geschäftsbereich von wbv Media GmbH & Co. KG – ruft zusammen mit dem Nationalen Open-Access-Kontaktpunkt OA2020-DE das Projekt wbv OPEN Library Erwachsenbildung, Berufs- und Wirtschaftspädagogik ins Leben. Das Projekt eröffnet durch die Finanzierung im Konsortialmodell die Möglichkeit, ab 2020 20 Neuerscheinungen in den Programmsegmenten Erwachsenenbildung sowie Berufs- und Wirtschaftspädagogik gebündelt im Open Access zu veröffentlichen. Knowledge Unlatched wird die Mittelbeschaffung koordinieren.
<!-- TEASER_END -->

Bibliotheken können bis Ende November 2019 ihre finanzielle Unterstützung erklären. Ein Mindestquorum von 40 Institutionen à 2.400 € ist festgelegt. Der Startpreis pro teilnehmende Einrichtung beläuft sich damit auf 120 € pro Buch. Der endgültige Rechnungsbetrag reduziert sich anteilig, je mehr Einrichtungen sich an der Finanzierung beteiligen.

„Wir freuen uns, gemeinsam mit unseren Partner_innen ein attraktives Titelpaket in der Erwachsenenbildung und Berufs- und Wirtschaftspädagogik Open Access machen zu können“, so Joachim Höper, Mitglied der Geschäftsleitung bei wbv Media.

Weitere Informationen finden Sie unter [https://www.wbv.de/openaccess/wbv-openlibrary.html](https://www.wbv.de/openaccess/wbv-openlibrary.html).

<br />

**Über wbv Media**

wbv Media ist ein Medien-und Logistikunternehmen mit drei Geschäftsbereichen: wbv Publikation, wbv Kommunikation und wbv Distribution. wbv Publikation verlegt Wissenschafts- und Fachliteratur zu den Themenfeldern Sozialwissenschaften und Sozialforschung, Bildung und Beruf sowie Verwaltung und Recht.

**Über Knowledge Unlatched**

KU bietet jedem Leser weltweit freien Zugang zu wissenschaftlichen Inhalten. Die Online-Plattform bietet Bibliotheken weltweit eine zentrale Anlaufstelle zur Unterstützung von Open Access-Modellen führender Verlage und neuer Open Access-Initiativen.

**Über den Nationalen Open-Access-Kontaktpunkt OA2020-DE**

Strategisches Ziel des Nationalen Open-Access-Kontaktpunkt OA2020-DE ist das Schaffen von Voraussetzungen für die großflächige Open-Access-Transformation in Übereinstimmung mit der Allianz der deutschen Wissenschaftsorganisationen. Dazu entwickelt OA2020-DE unter anderem neue, kooperative Open-Access-Geschäftsmodelle.
Weitere Informationen über den Kontaktpunkt unter [http://oa2020-de.org/pages/ziele/](http://oa2020-de.org/pages/ziele/).
