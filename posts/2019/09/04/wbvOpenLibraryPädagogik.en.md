<!--
.. title: Successful adaption of the  OPEN Library to a sub-discipline of pedagogy
.. slug: wbvmedia_openlibrary_pedagogy
.. date: 2019-09-04 08:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## wbv Media is paving the way for Open Access in the field of education to the benefit of scholars and librarians

[Press release]

*A collaboration with OA2020-DE and Knowledge Unlatched*

**Bielefeld/Berlin, September 3rd 2019**: wbv Publikation – a division of wbv Media GmbH & Co. KG – is launching, together with the National Open-Access-Kontaktpunkt OA2020-DE, the project „OPEN Library Erwachsenbildung, Berufs- und Wirtschaftspädagogik“. Thanks to a cooperative financing model, the initiative will allow for 20 frontlist titles in the fields of Adult Education and Vocational & Business Education to be published Open Access as of 2020. Knowledge Unlatched will coordinate the funding efforts.
<!-- TEASER_END -->

Libraries can pledge their support until the end of November 2019. A minimum of 40 institutions investing a maximum of € 2,400 each is required. The starting price per participating institution thus amounts to 120€ per book. The final contribution decreases as more institutions participate.

„We are very delighted to work together with our partners to make an attractive package of titles in the field of Adult Education and Vocational & Business Education Open Access.”, Joachim Höper, member of the direction of wbv Media.

Find out more under [https://www.wbv.de/openaccess/wbv-openlibrary.html](https://www.wbv.de/openaccess/wbv-openlibrary.html) (only in german).

<br />

**About wbv Media**

wbv Media is a media and logistics company with three departments: wbv Publishing, wbv Communication and wbv Distribution. wbv Publishing publishes scolarly and professional books in the fields of social sciences and social research, education and training as well as administration and law.

**About Knowledge Unlatched (KU)**

KU offers every reader worldwide free access to scholarly content. The online platform enables libraries worldwide to centralize their support for Open Access models from leading publishers and new initiatives in favor of Open Access.

**About the National Contact Point Open Access OA2020-DE**

The strategic goal of the National Contact Point Open Access OA2020-DE is to create prerequisites for the large-scale open access transformation in accordance with the Alliance of Science Organisations in Germany. Among other things, OA2020-DE is developing new, cooperative open access business models.
More information about the Contact Point: [http://oa2020-de.org/en/pages/aims/](http://oa2020-de.org/en/pages/aims/).
