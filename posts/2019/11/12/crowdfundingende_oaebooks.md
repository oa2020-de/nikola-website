<!--
.. title: Crowdfunding für Open-Access-eBook-Modelle endet am 30.11.2019
.. slug: ende_crowdfunding_oaebooks
.. date: 2019-11-12 08:00:00+1:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

### Letzte Chance für Bibliotheken disziplinspezifische Open-Access-eBook-Projekte zu unterstützen

Noch bis zum **30. November 2019** haben Bibliotheken die Möglichkeit, wissenschaftsfreundliche und disziplinspezifische Open-Access-Projekte in den Bereichen *Erwachsenenbildung*, *Berufs- und Wirtschaftspädagogik* sowie *Politikwissenschaft* finanziell zu unterstützen. Bis zu diesem Datum läuft noch das Crowdfunding bei den Verlagen wbv media und transcript.

Bei [transcript](https://www.transcript-verlag.de/open-library-politikwissenschaft) geht die Open-Access-Stellung der politikwissenschaftlichen Frontlist schon zum zweiten Mal in die Finanzierung per bibliothekarische Crowd. Der Pilotversuch aus dem letzten Jahr konnte mit mehr als doppelt so vielen Teilnehmer_innen als benötigt positiv abgeschlossen werden und lässt darauf hoffen, dass auch in diesem Jahr wieder viele Bibliotheken die Chance nutzen, sich aktiv für die Ermöglichung von Open-Access-Publikationen einzusetzen.

Für [wbv media](https://www.wbv.de/openaccess/wbv-openlibrary.html) ist es das erste Mal, dass sie ein solches Modell für ihre Disziplinen anbieten. Nach Abschluss der Crowdfunding-Periode werden - sofern sich genug interessierte Bibliotheken finden - 20 Titel aus dem Jahresprogramm 2020 der Bereiche Erwachsenenbildung sowie Berufs- und Wirtschaftspädagogik direkt im Open Access veröffentlicht.

**Bei Fragen zu den Modellen oder bei Interesse zur Teilnahme wenden Sie sich bitte an [Catherine Anderson](mailto:catherine@knowledgeunlatched.org) von Knowledge Unlatched.**


*P.S. Im übrigen haben Sie auch noch bis zum 15.11.2019 Zeit, sich dem FID-Konsortium des FID „Jüdische Studien“, UB Frankfurt (2020) bzw. dem Nationalen Konsortium der SUB Göttingen (2020) in Kooperation mit dem DeGruyter-Verlag anzuschließen. Weitere Informationen dazu finden Sie [hier](https://www.degruyter.com/dg/newsitem/354/gttingen-and-de-gruyter-in-open-access-book-transformation-project-for-the-humanities).*
