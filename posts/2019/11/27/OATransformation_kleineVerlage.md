<!--
.. title: Die Open-Access-Transformation schließt kleinere Verlage ein
.. slug: OATransformation_kleineVerlage
.. date: 2019-11-27 10:00:00+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

## Ein Kommentar zu der Diskussion um DEAL in der FAZ

Die Frankfurter Allgemeine Zeitung (FAZ) fährt in der Diskussion um Open Access und das von der Allianz der deutschen Wissenschaftsorganisationen geförderte Projekt DEAL starke Geschütze auf. Am 20.11.2019 veröffentlichte Thomas Thiel, Leiter des Ressorts Forschung und Lehre der FAZ, einen Beitrag unter dem Titel „Die HRK verkauft die Wissenschaft“.

Der Präsident der Hochschulrektorenkonferenz hat zu diesem Vorwurf in der FAZ vom 27.11.2019 [Stellung](https://edition.faz.net/faz-edition/geisteswissenschaften/2019-11-27/neue-publikationsmodelle-und-kein-verkauf-der-wissenschaft/391579.html) genommen. Der „Nationale Open-Access-Kontaktpunk OA2020-DE“, der wie das DEAL-Projekt von den Partnerorganisationen der Allianz gefördert wird, unterstützt diese Stellungnahme.

Aus Sicht des Kontaktpunkts möchten wir insbesondere auf zwei Punkte im Text von Herrn Thiel eingehen:
<!-- TEASER_END -->

> „Wer den ersten Zugriff auf die Millionenetats hat, die von der Deal-Gruppe verteilt werden, kann die Konkurrenz weit distanzieren.“

Die an dem DEAL-Wiley-Vertrag teilnehmenden Einrichtungen zahlen an die MPDL Services GmbH in den kommenden drei Jahren ihre bisher auch schon an den Verlag Wiley geleisteten Subskriptionszahlungen plus jährliche Preissteigerungen von knapp 3%. Hinzu kommen Zahlungen für Artikel in Wiley-Open-Access-Zeitschriften. Die suggerierte Verschiebung von Etats von anderen Verlagen zu Wiley findet also nicht statt.

> „Die DEAL-Gruppe räumt die kleinen und mittelständischen Verlage aus dem Weg.“

Das [Projekt DEAL](https://www.projekt-deal.de/) hat den expliziten Auftrag der Allianz und der sie unterstützenden Einrichtungen, mit den drei größten Zeitschriftenverlagen Elsevier, Springer Nature und Wiley zu verhandeln. Diese drei Verlage vereinen nach Daten des „Web of Science“ rd. 48% aller Zeitschriftenartikel deutscher Autorinnen und Autoren auf sich. Die nächstgrößeren Verlage (American Chemical Society, Amercican Physical Society, Oxford University Press) kommen jeweils auf Anteile von rd. 3%, der erste deutsche Verlag in dieser Rangreihenfolge, De Gruyter, hat einen Anteil von rd. 1%.

Der DEAL-Ansatz ist nicht auf alle Verlage und alle Disziplinen anwendbar und übertragbar, vor allem nicht auf die kleinen und mittelgroßen Verlage in Deutschland, die neben Fachzeitschriften wesentlich monografische Literatur verlegen. Auch vor diesem Hintergrund fördert die Allianz das Projekt „Nationaler Open-Access-Kontaktpunkt OA2020-DE“. Im Austausch und in Zusammenarbeit mit Verlagen wie zum Beispiel De Gruyter, transcript und wbv media, sind [Pilotprojekte](https://oa2020-de.org/pages/pilotprojekte/) entstanden, die Verlagen die Anpassung ihrer Geschäftsmodelle an die Open-Access-Transformation ermöglichen. Über diese Modelle wird regelmäßig berichtet, zusätzlich hat der Kontaktpunkt [Workshops](https://oa2020-de.org/pages/veranstaltungen/) unter Beteiligung von Verlagen und Erwerbungsfachleuten aus Bibliotheken durchgeführt, um diese Ansätze bekannt zu machen. Weiterhin bietet die Deutsche Forschungsgemeinschaft über ihr [Förderprogramm "Open-Access-Transformationsverträge"](https://www.dfg.de/foerderung/info_wissenschaft/2019/info_wissenschaft_19_17/index.html) auch kleinen und mittelständischen Verlagen Förderung an, wenn sie ihr Geschäftsmodell auf Open Access umstellen wollen. Aktuell werden in diesem Programm die Verlage Cambridge University Press, Nomos und Thieme unterstützt.




