<!--
.. title: Events
.. slug: events
.. date: 2019-05-31 00:00:00
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

Here you will find an overview of the events organised by the National Contact Point Open Access with the materials and presentation slides (if approved).

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[1st OA2020-DE transformation workshop, 19-20 April 2018](/en/blog/2018/04/26/openaccess-and-aquisition-first-workshop/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[2nd OA2020-DE transformation workshop, 17-18 October 218](/en/blog/2018/11/20/2oatransformationsworkshop/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[Hands-on-Lab at the 7th Library Congress, 19 March 2019](/en/blog/2019/05/03/report-handsonlab-librarycongress/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[3rd OA2020-DE transformation workshop, 03-04 April 2019](/en/blog/2019/05/07/3oatransformationsworkshop/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[Workshop at the Open Access Days, 01 October 2019](/en/blog/2019/12/16/workshop_oadays2019/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[4th OA2020-DE transformation workshop, 04-05 November 2019](/en/blog/2019/11/18/4oatransformationsworkshop/)


