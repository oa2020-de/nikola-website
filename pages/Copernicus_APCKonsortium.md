<!--
.. title: Nationales APC-Konsortium mit Copernicus Publications
.. slug: APCkonsortium_Copernicus
.. date: 2018-11-30 00:00:00
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

# Nationales APC-Konsortium mit Copernicus Publications

Laufzeit: 2019-2020

Konsortialführer: SUB Göttingen

Verlag: Copernicus Publications

Vertragsdokument: [Vertrag über die Bezahlung von Article Processing Charges durch Prepayments](/assets/files/Copernicus_SUB_Vertrag_Prepayments_nat_Konsortium_signed.pdf)

[Pressemitteilung](https://idw-online.de/de/news708981) zum Start des Konsortiums.


### Hintergrund

Zu der Erarbeitung von Transformationsmodellen gehört auch die strukturelle Verankerung der Finanzierung kostenpflichtiger Open-Access-Zeitschriftenartikel in wissenschaftlichen Einrichtungen. Bisher erfolgt sie in der Regel über die oft zeitlich und finanziell beschränkten (DFG-geförderten) Publikationsfonds. Benötigt werden also Angebote und Konzepte für eine Weiterführung und Verstetigung dieser Finanzierung. Hinzu kommt, dass es ebenso an Kennzahlen zur Bedarfsabschätzung fehlt, die eine Ausgabenplanung für das kostenpflichtige Open-Access-Publizieren für Bibliotheken analog zur Planung von Subskriptionsausgaben ermöglichen.

Daher bemüht sich OA2020-DE um die Bildung eines Konsortiums mit allen deutschen Bibliotheken und wissenschaftlichen Einrichtungen, die relevante Zahlungen an genuine Open-Access-Verlage leisten. **Ein entsprechendes Pilotprojekt wurde mit dem Copernicus-Verlag erarbeitet**. Ziel ist es, die Finanzierung von Publikationen in genuinen Open-Access-Zeitschriften komplementär zu bestehenden Transformationsvorhaben wie SCOAP³ und DEAL institutionell und standardisiert zu verankern.


### Konditionen

Basierend auf dem Konzept des Nationalen Open-Access-Kontaktpunkts OA2020-DE wurde das Konsortium gemeinsam durch die Niedersächsische Staats- und Universitätsbibliothek Göttingen und den Verlag Copernicus Publications verhandelt und organisiert. Die Laufzeit beträgt zwei Jahre (2019-2020); teilnahmeberechtigt sind alle Hochschulen und wissenschaftlichen Einrichtungen, die auch berechtigt für die Teilnahme an einer National- oder Allianzlizenz sind.

Die teilnehmenden Einrichtungen leisten zu Beginn eines (Haushalts-) Jahres eine Vorauszahlung an Copernicus. Kalkulationsbasis bei Bestandskundinnen und -kunden ist das durchschnittliche Publikationsaufkommen pro Institution, multipliziert mit einer fiktiven Durchschnitts-APC von 1.200 EUR netto. Neukundinnen und -kunden zahlen einen Pauschalpreis. Am Jahresende erfolgen dann Verrechnung und Ausgleich. Die Rechnungsstellung erfolgt von Copernicus an die einzelnen Einrichtungen.

Das Konsortium ermöglicht den Corresponding Authors, die mit den teilnehmenden Einrichtungen affiliiert sind, in den 43 Gold-Open-Access-Zeitschriften aus den Geo-, Ingenieurs- und Lebenswissenschaften zwei Jahre lang für sie persönlich kostenfrei zu publizieren. Die Artikel werden unter einer CC-BY-Lizenz veröffentlicht, inklusive der automatischen Belieferung der Repositorien teilnehmender Einrichtungen mit den XML-Metadaten und dem Volltext-PDF.


### Vorteile

Das Konsortium identifiziert und motiviert auf diese Weise Einrichtungen, die bisher keine Finanzierungsstruktur für das Open-Access-Publizieren etabliert haben. Für die teilnehmenden Institutionen und den Verlag besteht Planungssicherheit durch die Vorauszahlung und die Mittelabschätzung kann durch den Kontaktpunkt unterstützt werden.

In Zusammenarbeit mit der SUB Göttingen und dem Copernicus-Verlag zielt das Pilotmodell darauf ab, Mechanismen zur strukturellen Verankerung der Finanzierung kostenpflichtiger Open-Access-Zeitschriftenartikel in wissenschaftlichen Einrichtungen zu erproben. Die mit diesem Konsortium gesammelten Erfahrungen fließen in die Weiterentwicklung solcher  Mechanismen ein und können und sollen zu erweiterten Modellen kooperativer APC-Finanzierung (ähnlich dem SCOAP³-Modell) führen.


### Ansprechpartner_innen

Bei Interesse an einer künftigen Teilnahme an dem Konsortium oder Fragen zu den Konditionen wenden Sie sich bitte an:

Niedersächsische Staats- und Universitätsbibliothek Göttingen, [nationallizenzen@sub.uni-goettingen.de](mailto:nationallizenzen@sub.uni-goettingen.de)

Copernicus Publications, Göttingen, [publications@copernicus.org](mailto:publications@copernicus.org)
