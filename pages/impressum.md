<!--
.. title: Impressum
.. slug: impressum
.. date: 2017-08-17 00:00:00
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

__Nationaler Open-Access-Kontaktpunkt OA2020-DE__

c/o Universitätsbibliothek<br />
Universitätsstrasse 25<br />
D-33615 Bielefeld<br />

<i class="fa fa-envelope" aria-hidden="true"></i> [dirk.pieper@uni-bielefeld.de](mailto:dirk.pieper@uni-bielefeld.de)<br />
<i class="fa fa-phone-square" aria-hidden="true"></i> +49 521 106-4049<br />

Die Universitätsbibliothek ist eine zentrale Einrichtung der Universität Bielefeld.<br />
Die Gesamtverantwortung für das Informationsangebot der Universität Bielefeld liegt beim Rektor der Universität.<br />

__Verantwortlich__

Barbara Knorn, Ltd. Bibliotheksdirektorin (Anschrift wie oben)

Haftungshinweis: Trotz sorgfältiger inhaltlicher Kontrolle übernehmen wir keine Haftung für die Inhalte externer Links. Für den Inhalt der verlinkten Seiten sind ausschließlich deren Betreiber verantwortlich.


__Datenschutzerklärung__

Hinweise zum Datenschutz an der Universität Bielefeld: [Datenschutzhinweise](http://www.uni-bielefeld.de/(de)/Universitaet/datenschutzhinweise.html)<br />
Diese Datenschutzhinweise gelten ebenfalls für die Webseiten und das Informationsangebot des Nationalen Open-Access-Kontaktpunkts OA2020-DE.

**Facebook-Hinweis**

Die Betreiber von Fanpages können mit Hilfe der Funktion Facebook Insight, die ihnen Facebook als nicht abdingbaren Teil des Benutzungsverhältnisses kostenfrei zur Verfügung stellt, anonymisierte statistische Daten betreffend die Nutzer_innen dieser Seiten erhalten. Diese Daten werden mit Hilfe sogenannter Cookies gesammelt, die jeweils einen eindeutigen Benutzercode enthalten, der für zwei Jahre aktiv ist und den Facebook auf der Festplatte des Computers oder einem anderen Datenträger der Besucher_innen der Fanpage speichert. Der Benutzercode, der mit den Anmeldungsdaten solcher Nutzer_innen, die bei Facebook registriert sind, verknüpft werden kann, wird beim Aufrufen der Fanpages erhoben und verarbeitet.<br />
Hiermit weisen wir Sie darauf hin, dass der Besuch oder die sonstige Interaktion mit der Facebook-Fanpage des Nationalen Open-Access-Kontaktpunktes dazu führt, dass mit Hilfe von Cookies personenbezogenen Daten von Ihnen anonymisiert gespeichert und für statistische Zwecke verarbeitet werden.
