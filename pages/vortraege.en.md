<!--
.. title: Lectures
.. slug: lectures
.. date: 2018-01-29 08:00:00
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

## Lectures 2021

<i class="fa fa-comment" aria-hidden="true"></i>
Lenke, C. (2021, März). *Alternative Transitionsmodelle: Subscribe to Open*, Open-Access-Transformation von Zeitschriften der geowissenschaftlichen Fachgesellschaften ([Online-Veranstaltung](https://www.fidgeo.de/open-access-transformation-von-zeitschriften-der-geowissenschaftlichen-fachgesellschaften/)). [[PDF document](/assets/files/Subscribe_to__Open_Lenke_Christopher.pdf)].

<br />
## Lectures 2020

<i class="fa fa-comment" aria-hidden="true"></i>
Jobmann, A. (2020, January). *Das Projekt ENABLE! Zielstellung, Umsetzungsperspektive und Stand der Dinge*, ENABLE! Community-Building Workshop, Bielefeld. [[PDF document](/assets/files/2020-01-28_ENABLE_Jobmann_Einfuehrung_CCBY.pdf)].

<i class="fa fa-comment" aria-hidden="true"></i>
Pieper, D. (2020, May). *Nationale Strategie zur Open-Access-Transformation*, Kick-off event OA-Policy Hessen, Darmstadt. [[PDF document](/assets/files/2020-05-18_hessen_oa.pdf)].

<br />
## Lectures 2019

<i class="fa fa-comment" aria-hidden="true"></i>
Barbers, I., & Pollack, P. (2019, December). *Open Access Monitor Germany*, SONAR Workshop OA Monitoring, Bern. [[PDF document](https://juser.fz-juelich.de/record/872584/files/OA-Monitor%20Germany_SONAR.pdf)].

<i class="fa fa-comment" aria-hidden="true"></i>
Jobmann, A. (2019, March). *Die Open-Access-Transformation – Grundlagen, Modelle, Möglichkeiten*, 22. Meeting of the Working Group Libraries of the Federal Governmental Research Institutions, Berlin. Zenodo. [http://doi.org/10.5281/zenodo.2595056](http://doi.org/10.5281/zenodo.2595056)

<i class="fa fa-comment" aria-hidden="true"></i>
Jobmann, A. (2019, June). *Daten für die Open-Access-Transformation: Ein theoretischer Überblick über das Was, Woher und Warum?*, FIS Bildung Conference on Open-Access-Monitoring, Frankfurt am Main. Zenodo. [http://doi.org/10.5281/zenodo.3239449](http://doi.org/10.5281/zenodo.3239449)

<i class="fa fa-comment" aria-hidden="true"></i>
Jobmann, A. (2019, September). *Finanzierungsmodelle für Open-Access-Publikationen: Strategien, Wege und Möglichkeiten im Kontext des Open-Access-Repositoriums peDOCS*, Publisher Meeting of the Cooperation Partners of peDOCS & SSOAR during ECER 2019, Hamburg. Zenodo. [http://doi.org/10.5281/zenodo.3403581](http://doi.org/10.5281/zenodo.3403581)

<i class="fa fa-comment" aria-hidden="true"></i>
Jobmann, A. (2019, September). *How to build a community? Wie aus einem Open-Access-eBook-Modell eine Community für Open Access in den Geistes- und Sozialwissenschaften wird*, Austrian Librarian Day 2019, Graz. [[PDF document](/assets/files/2019-09-13_Jobmann_Howtobuildacommunity_ÖBT2019.pdf)].

<i class="fa fa-comment" aria-hidden="true"></i>
Jobmann, A. (2019, October). *Das Engagement der Vielen - wie mit kollektiven Ansätzen die nachhaltige OA-Finanzierung gestaltet werden kann*, Open Access Days 2019, Hanover. Zenodo. [http://doi.org/10.5281/zenodo.3492214](http://doi.org/10.5281/zenodo.3492214)

<i class="fa fa-comment" aria-hidden="true"></i>
Jobmann, A. (2019, October). *OA2020-DE: Mit großen Schritten zur Open-Access-Tranformation*, Learning journey of the University Library Bonn, Bielefeld. [[PDF document](/assets/files/20191007_Lernreise_ULBBonn_Jobmann.pdf)].

<i class="fa fa-comment" aria-hidden="true"></i>
Jobmann, A. (2019, November). *Wie OA2020-DE die Medienwissenschaft in der Open-Access-Transformation unterstützen kann - ein Impuls*. Constellations - Workshop of the Repository for Media Studies media/rep/, Marburg. [[PDF document](https://www.online.uni-marburg.de/blog-mediarep/blog/wp-content/uploads/2019/11/JOBMANN_20191108_mediarep_oa2020deimpuls_Jobmann.pdf)].

<i class="fa fa-comment" aria-hidden="true"></i>
Jobmann, A., & Lembrecht, C. (2019, October). *Open-Access-Transformation für geistes- und sozialwissenschaftliche Zeitschriften: Der Ansatz "Subscribe to Open" und dessen Umsetzung durch De Gruyter*. Autumn session of the dbv-section 4, Marburg. [[PDF document](/assets/files/2019-10-23_JobmannLembrecht_DBVSektion4_S2O_CL.pdf)].

<i class="fa fa-comment" aria-hidden="true"></i>
Jobmann, A., & Pieper, D. (2019, March). *Neue Geschäftsmodelle und Workflows im Open Access*, Hands-On Lab at the 7th Library Congress 2019, Leipzig. [https://nbn-resolving.org/urn:nbn:de:0290-opus4-164013](https://nbn-resolving.org/urn:nbn:de:0290-opus4-164013)

<i class="fa fa-comment" aria-hidden="true"></i>
Pieper, D. (2019, March). *Open Access - Stand und Perspektiven*, Symposium on 100 Years of ZBW: Digital Economics - Opportunities and Challenges, Berlin. [[PDF document](/assets/files/2019-03-12_100jahre_zbw_OA_pieper.pdf)].

<i class="fa fa-comment" aria-hidden="true"></i>
Pieper, D. (2019, September). *Open-Access-Transformation über Community-Bildung: Die transcript Open Library Politikwissenschaft*, Digital Conference for Publisher and Libraries, Munich. [[PDF document](/assets/files/Pieper_akademie_transcript_20190916.pdf)].

<i class="fa fa-comment" aria-hidden="true"></i>
Pollack, P. (2019, June). *NOAK und der OAM. Aufbau eines nationalen OA-Kontaktpunktes und eines OA-Monitors*, KB Consortium meeting, Berlin. [[PDF document](https://juser.fz-juelich.de/record/872586/files/NOAK%20und%20der%20OAM.pdf)].

<i class="fa fa-comment" aria-hidden="true"></i>
Pollack, P., & Ecker, D. (2019, September). *Open Access Monitor Deutschland - Technik. Aufbau eines Open Access Monitors für Deutschland*, Austrian Librarian Day 2019, Graz. [[PDF document](https://juser.fz-juelich.de/record/872591/files/OAM_Graz19.pdf)].

<i class="fa fa-comment" aria-hidden="true"></i>
Pollack, P., & Rosenberger, S. (2019, June). *Der Open Access Monitor Deutschland*, Workshop Open Access Monitor 2019, Jülich. [[PDF document](https://juser.fz-juelich.de/record/863327/files/02_Rosenberger_Pollack_Vorstellung%20OAM.pdf)].

<i class="fa fa-comment" aria-hidden="true"></i>
Schönfelder, N. (2019, January). *Determinants and prediction of APC price levels*, openAPC Workshop for OA data experts, Bielefeld. [[PDF document](/assets/files/APCs_Schoenfelder_2019-01-17.pdf)].

<i class="fa fa-comment" aria-hidden="true"></i>
Schönfelder, N. (2019, April). *APCs: Mirroring the impact factor or legacy of the subscription-based model?*, UKSG 42nd Annual Conference and Exhibition, Telford. [[PDF document](/assets/files/Schoenfelder_Breakout_session_Group_C_2019-04-07.pdf)].

<i class="fa fa-comment" aria-hidden="true"></i>
Schönfelder, N. (2019, September). *German Consortium for Publishing in Open Access Journals*, OASPA’s 11th Conference on Open Access Scholarly Publishing, Copenhagen. [[PDF document](/assets/files/German Consortium for Publishing in Open Access Journals 2019-08-27.pdf)].

<i class="fa fa-comment" aria-hidden="true"></i>
Schönfelder, N. (2019, October). *Open-Access-Transformationsrechnung für wissenschaftliche Einrichtungen in Deutschland*, Open Access Days 2019, Hanover. Zenodo. [http://doi.org/10.5281/zenodo.3490920](http://doi.org/10.5281/zenodo.3490920)

<i class="fa fa-comment" aria-hidden="true"></i>
Schönfelder, N. & Jobmann, A. (2019, April). *Neue Open-Access-Transformationsmodelle jenseits von DEAL*, 3rd OA2020-DE transformation workshop, Bielefeld. Zenodo. [http://doi.org/10.5281/zenodo.2652163](http://doi.org/10.5281/zenodo.2652163)

<br />
## Lectures 2018

<i class="fa fa-comment" aria-hidden="true"></i>
Campbell, C., Brundy, C., & Pieper, D. (2018, July). *Open Access 2020 Initiative: OA2020 Network Progress Update – Q2 2018* [Webinar]. [[PDF document](/assets/files/2018_Q2_OA2020_Update-Campbell-Brundy-Pieper.pdf)].

<i class="fa fa-comment" aria-hidden="true"></i>
Deinzer, G. (2018, March). *OA2020-DE: Der Nationale Open Access Kontaktpunkt: Auf dem Weg zur Open-Access-Tranformation*, 82nd Annual Conference of the DPG and DPG Spring Meeting, Erlangen. [[PDF document](http://doi.org/10.5283/epub.36894)].

<i class="fa fa-comment" aria-hidden="true"></i>
Fromm, M., Förstner, K., & Jobmann, A. (2018, September). *OSR130 Nationaler Open Access Kontaktpunkt OA2020-DE* [Interview]. [http://www.openscienceradio.org/podlove/file/2676/s/download/c/select-show/OSR130-nationaler-open-access-kontaktpunkt-oa2020-de-de.mp3](http://www.openscienceradio.org/podlove/file/2676/s/download/c/select-show/OSR130-nationaler-open-access-kontaktpunkt-oa2020-de-de.mp3)

<i class="fa fa-comment" aria-hidden="true"></i>
Jobmann, A. (2018, April). *Communication strategy of OA2020-DE - "Open in order to transition"*, OpenAIRE-Workshop "Beyond APCs", The Hague (Netherlands). Zenodo. [http://doi.org/10.5281/zenodo.1216293](http://doi.org/10.5281/zenodo.1216293)

<i class="fa fa-comment" aria-hidden="true"></i>
Jobmann, A. (2018, April). *Auf dem Weg zur Open-Access-Transformation: Das Projekt OA2020-DE im Auftrag der Allianz der deutschen Wissenschaftsorganisationen*, Open-Access-day at Trier University, Trier. Zenodo. [http://doi.org/10.5281/zenodo.1226881](http://doi.org/10.5281/zenodo.1226881)

<i class="fa fa-comment" aria-hidden="true"></i>
Jobmann, A. (2018, June). *National Contact Point Open Access OA2020-DE - Tasks and Aims*, 107th German Librarian Day, Berlin. [https://nbn-resolving.org/urn:nbn:de:0290-opus4-36040](https://nbn-resolving.org/urn:nbn:de:0290-opus4-36040)

<i class="fa fa-comment" aria-hidden="true"></i>
Jobmann, A. (2018, October). *Auf dem Weg zur Open-Access-Transformation: Aktueller Stand und künftige Entwicklungen*, Fraunhofer Expert Forum on Information, Karlsruhe. Zenodo. [http://doi.org/10.5281/zenodo.1460975](http://doi.org/10.5281/zenodo.1460975)

<i class="fa fa-comment" aria-hidden="true"></i>
Jobmann, A. (2018, November). *Open in order to transition - Der Nationale Open-Access-Kontaktpunkt OA2020-DE*, 19th DINI Annual Conference, Bielefeld. Zenodo. [http://doi.org/10.5281/zenodo.1479748](http://doi.org/10.5281/zenodo.1479748)

<i class="fa fa-comment" aria-hidden="true"></i>
Jobmann, A. (2018, November). *Open Access-Monographien - Mission impossible oder doch leichter als gedacht?*, "Freie Sicht auf große Werke", Mainz. Zenodo. [http://doi.org/10.5281/zenodo.1492573](http://doi.org/10.5281/zenodo.1492573)

<i class="fa fa-comment" aria-hidden="true"></i>
Jobmann, A. (2018, December). *Open Access im wissenschaftspolitischen Kontext*, General Meeting of the Information Network on International Politics and Regional Geography, Berlin. Zenodo. [http://doi.org/10.5281/zenodo.1966421](http://doi.org/10.5281/zenodo.1966421)

<i class="fa fa-comment" aria-hidden="true"></i>
Jobmann, A., & Schönfelder, N. (2018, May). *Open Access und Transformationsverträge: neue Aufgaben und Geschäftsprozesse für die Erwerbung. Das Projekt "Nationaler Open-Access-Kontaktpunkt OA2020-DE" im Auftrag der Allianz der deutschen Wissenschaftsorganisationen*, ZBIW-Seminar Acquisition in University Libraries, Bensberg. Zenodo. [http://doi.org/10.5281/zenodo.1284311](http://doi.org/10.5281/zenodo.1284311)

<i class="fa fa-comment" aria-hidden="true"></i>
Jobmann, A., & Schönfelder, N. (2018, September). *Session "Nationaler Open-Access-Kontaktpunkt OA2020-DE" - Zentrale Ergebnisse des ersten Projektjahres*, Open Access Days 2018, Graz (Austria). Zenodo. [http://doi.org/10.5281/zenodo.1460968](http://doi.org/10.5281/zenodo.1460968)

<i class="fa fa-comment" aria-hidden="true"></i>
Jobmann, A., & Schönfelder, N. (2018, October). *Nationaler Open-Access-Kontaktpunkt OA2020-DE: Konkrete Investitionsmöglichkeiten in den Open Access*, 2nd OA2020-DE-transition-workshop, Bielefeld. Zenodo. [http://doi.org/10.5281/zenodo.1492539](http://doi.org/10.5281/zenodo.1492539)

<i class="fa fa-comment" aria-hidden="true"></i>
Mittermaier, B. (2018, June). *Auf dem Weg zu einem Open-Access-Monitor*, 107th German Librarian Day, Berlin. [https://nbn-resolving.org/urn:nbn:de:0290-opus4-35938](https://nbn-resolving.org/urn:nbn:de:0290-opus4-35938)

<i class="fa fa-comment" aria-hidden="true"></i>
Pieper, D. (2018, April). *Pilot Open-Access-Zeitschriftenkonsortium: OA2020-DE Transformationsworkshop für Erwerbungs_leiterinnen*, 1st OA2020-DE-transition-workshop, Bielefeld. [[PDF document](/assets/files/Pieper_pilotcopernicus_workshop_20180420_CCBY.pdf)].

<i class="fa fa-comment" aria-hidden="true"></i>
Pieper, D. (2018, June). *Divest subscriptions to invest in open access: OPERAS Conference: Open Scholarly Communication in Europe. Adressing the coordination problem*, OPERAS-Conference, Athens (Greece). [[PDF document](/assets/files/oa2020_operas_20180601.pdf)].

<i class="fa fa-comment" aria-hidden="true"></i>
Pieper, D. (2018, September). *Open Library Politikwissenschaft – ein kooperatives Pilot-Projekt zur Open-Access-Transformation von eBooks*, 10th ebook days, Bremen. [[PDF document](/assets/files/ebookpilot_transcript_bremerebooktage_20180907.pdf)].

<i class="fa fa-comment" aria-hidden="true"></i>
Pieper, D. (2018, October). *How to transform scholary journals from subscription to open access publishing – a proposal by OA2020-DE and Knowledge Unlatched*, Annual European ICOLC Meeting, London (Great Britain). [[PDF document](/assets/files/ICOLC_London_oa2020de_ku_journalflip_20181015.pdf)].

<i class="fa fa-comment" aria-hidden="true"></i>
Schönfelder, N. (2018, April). *Datenanalysen für die Open-Access-Transformation - Intro: OA2020-DE Transformationsworkshop für Erwerbungs_leiterinnen*, 1st OA2020-DE-transition-workshop, Bielefeld. [[PDF document](/assets/files/Schoenfelder_ Datenanalysen_Intro_CCBY.pdf)].

<i class="fa fa-comment" aria-hidden="true"></i>
Schönfelder, N. (2018, June). *APCs – Mirroring the impact factor or legacy of the subscription-based model?*, 3rd ESAC Workshop, Munich. [http://esac-initiative.org/wp-content/uploads/2018/08/Schoenfelder-2018-APCs.pdf](http://esac-initiative.org/wp-content/uploads/2018/08/Schoenfelder-2018-APCs.pdf)

<i class="fa fa-comment" aria-hidden="true"></i>
Wennström, S., van Otegem, M., Pieper, D., & Campbell, C. (2018, August). *Workshop: Libraries leading the Open Access Transformation: Strategies to achieve the vision*, LIBER 2018, Lille (France). Zenodo. [http://doi.org/10.5281/zenodo.1345016](http://doi.org/10.5281/zenodo.1345016)

<br />
## Lectures 2017

<i class="fa fa-comment" aria-hidden="true"></i>
Bertelmann, R. (2017, September). *Der Nationale Open-Access-Kontaktpunkt OA2020-DE: Struktur und Ziele*, Open Access Days 2017, Dresden. [[PDF document](/assets/files/oatage_oa2020de_bertelmann.pdf)].

<i class="fa fa-comment" aria-hidden="true"></i>
Jobmann, A. (2017, December). *Das Projekt OA2020-DE im Auftrag der Allianz der deutschen Wissenschaftsorganisationen: Auf dem Weg zur Open-Access-Transformation*, Open-Access-Workshop at University of Applied Sciences Potsdam, Potsdam. Zenodo. [http://doi.org/10.5281/zenodo.1205901](http://doi.org/10.5281/zenodo.1205901)

<i class="fa fa-comment" aria-hidden="true"></i>
Jobmann, A., & Pieper, D. (2017, September). *Project OA2020-DE on behalf of Alliance of Science Organisations in Germany: Framework for a transition to open access*, Open Access Days 2017, Dresden. Zenodo. [http://doi.org/10.5281/zenodo.891227](http://doi.org/10.5281/zenodo.891227)

<i class="fa fa-comment" aria-hidden="true"></i>
Mittermaier, B. (2017, September). *Die Datenstelle des Nationalen Open-Access-Kontaktpunkts OA2020-DE*, Open Access Days 2017, Dresden. [[Powerpoint](/assets/files/Mittermaier OA-tage.pptx)].

<i class="fa fa-comment" aria-hidden="true"></i>
Mittermaier, B. (2017, March). *OA2020-DE. Country Report Germany*, B13-Conference, Berlin. [[PDF document](https://oa2020.org/wp-content/uploads/pdfs/B13_Bernhard_Mittermaier.pdf)].

<i class="fa fa-comment" aria-hidden="true"></i>
Pieper, D. (2017, March). *A new grip on publication data as essential instrument for the transformation*, B13-Conference, Berlin. ([PDF document](https://oa2020.org/wp-content/uploads/pdfs/B13_Dirk_Pieper.pdf)].

<i class="fa fa-comment" aria-hidden="true"></i>
Pieper, D., & Jobmann, A. (2017, November). *Nationaler Open-Access-Kontaktpunkt: Herbstsitzung der Sektion 4 im dbv*, Autumn session of the dbv-section 4, Regensburg. Zenodo. [http://doi.org/10.5281/zenodo.1072972](http://doi.org/10.5281/zenodo.1072972)

<i class="fa fa-comment" aria-hidden="true"></i>
Schimmer, R. (2017, March). *OA2020: Progress and Outlook*, B13-Conference, Berlin. [[PDF document](https://oa2020.org/wp-content/uploads/pdfs/B13_Ralf_Schimmer_Talk1.pdf)].

<i class="fa fa-comment" aria-hidden="true"></i>
Schimmer, R. (2017, March). *Towards a NCP network and governance structure: OA2020 proposal*, B13-Conference, Berlin. [[PDF document](https://oa2020.org/wp-content/uploads/pdfs/B13_Ralf_Schimmer_Talk2.pdf)].
