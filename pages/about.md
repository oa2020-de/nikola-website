<!--
.. title: Über
.. slug: about
.. date: 2017-08-17 00:00:00
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

Strategisches Ziel des Nationalen Open-Access-Kontaktpunkt OA2020-DE ist das Schaffen von Voraussetzungen für die großflächige Open-Access-Transformation in Übereinstimmung mit der Allianz der deutschen Wissenschaftsorganisationen. In Ergänzung zu der globalen [OA2020-Initiative](https://oa2020.org/) (initiiert von der Max Planck Digital Library) stehen hier die nationalen Implikationen im Vordergrund. So wird der Transformationsprozess durch umfängliche Publikations- und Kostendatenanalysen nationaler Hochschulen und Forschungseinrichtungen unterstützt und darauf aufbauend Open-Access-Finanz- und Geschäftsmodelle entwickelt.

OA2020-DE ist Teil der sich etablierenden OA2020-Community, arbeitet im Rahmen der Arbeitsgruppen der Schwerpunktinitiative [Digitale Information](http://www.allianzinitiative.de) der Allianz und kooperiert mit den deutschen Hochschulen, außeruniversitären Forschungseinrichtungen und Verlagen. Das Projekt arbeitet eng mit [DEAL](https://www.projekt-deal.de/) zusammen und wird den Austausch mit allen einschlägigen Projekten, wie beispielsweise [INTACT](https://www.intact-project.org/) oder dem [Open Access Monitor](https://open-access-monitor.de), suchen.

Das Projekt wird von der [Allianz der deutschen Wissenschaftsorganisationen](http://www.dfg.de/dfg_profil/allianz/) gefördert und endet am 31.07.2021.


## [Ziele](/pages/ziele/)


## [Projektorganisation](/pages/projektorganisation/)



