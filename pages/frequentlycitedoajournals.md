<!--
.. title: Meist-zitierte Open-Access-Zeitschriften
.. slug: frequentlycitedoajournals
.. date: 2018-10-26 08:00:00
.. updated: 2021-02-26 12:22:00
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

Forscher_innen haben mitunter Schwierigkeiten, ein Open-Access-Journal zu finden, das ihren Anforderungen an Qualität, Sichtbarkeit und Relevanz entspricht. Der Nationale Open-Access-Kontaktpunkt OA2020-DE hat daher eine durchsuchbare Liste von über 1000 häufig zitierten Open-Access-Zeitschriften aus fast allen wissenschaftlichen Bereichen zusammengestellt. Die Liste enthält Zeitschriften, die im Verzeichnis der Open-Access-Zeitschriften [(DOAJ)](https://doaj.org/) enthalten sind und einen
überdurchschnittlichen SNIP-Faktor aufweisen. Der SNIP - Source Normalized Impact per Paper ist eine Journalmetrik, die vom Centre for Science and Technology Studies der Universität Leiden [(CWTS Journal Indicators)](http://www.journalindicators.com/) entwickelt wurde und den durchschnittlichen Zitationseinfluss der Veröffentlichungen einer Zeitschrift misst. <br />
Bitte beachten Sie, dass jede Metrik ihre Grenzen hat. Zeitschriftenmetriken sollten nicht angewendet werden, um den Wert einzelner Forschungsgegenstände / Artikel / Befunde oder sogar Karrieren zu bewerten.

In jedem Fall können Sie sicher sein, einen Zitiervorteil zu haben, wenn Sie sich entscheiden, Open Access zu veröffentlichen. Das haben mehrere [Studien](https://sparceurope.org/what-we-do/open-access/sparc-europe-open-access-resources/open-access-citation-advantage-service-oaca/) gezeigt. Generell lässt sich sagen, dass Open-Access-Zeitschriften eine höhere Sichtbarkeit haben und mittlerweile so etabliert sind, dass sie unter den Top-Zeitschriften vieler wissenschaftlicher Fachrichtungen zu finden sind.

Weitere Informationen zu den Metadaten und dem SNIP finden Sie beim [DOAJ](https://doaj.org/) und den [CWTS Journal Indicators](http://www.journalindicators.com/). Bitte überprüfen Sie vor der Einreichung die aktuellen Bedingungen und Article Processing Charges (APCs) auf der Webseite der von Ihnen gewählten Zeitschrift.

<a href="/assets/files/frequentlycitedOAjournals_20210120.html"><img style="display:auto; margin:0, 10" src="/images/Screenshot_frequentlycitedoajournals.png" alt="Liste mit häufig zitierten OA-Zeitschriften" width="750" height="500" /></a>
<br />
<i class="fa fa-chevron-circle-right"></i>
<a href="/assets/files/frequentlycitedOAjournals_20210120.html">Zur vollständigen, sortier- und filterbaren Tabelle bitte anklicken.</a>

*Hinweise zur Suche:*<br />

+ Sortieren Sie absteigend nach SNIP, um die meist-zitierten Zeitschriften zu finden.
+ Zur Eingrenzung auf ein Fach gibt es mehrere Möglichkeiten:
    + Auswahl von "Subject_Area" und/oder Eingabe in "Field", oder
    + Eingabe in "Subject", oder
    + Suche im Zeitschriftennamen über Eingabe in "Journal".
+ Auswahl, ob die Zeitschrift Artikelbearbeitungsgebühren erhebt in "APCs?".
+ Auswahl nach Sprache in "Language".

<br />

*Anmerkungen:*<br />
SNIP: Der “source normalized impact per paper” einer jeweiligen Zeitschrift für das Jahr 2019.
Diese Tabelle enthält nur Open-Access-Zeitschriften, die im DOAJ gelistet sind und einen SNIP-Wert von mindestens 1 aufweisen. Es werden nur Zeitschriften berücksichtigt, deren SNIP auf Basis von mindestens 50 Publikationen berechnet wurde.

Quelle für die ersten vier Spalten (SNIP, Journal, Subject Area, Fields) sind die [CWTS Journal Indicators](http://www.journalindicators.com/) vom April 2020, abgerufen am 19.01.2021. Wir danken Prof. Dr. Ludo Waltman für die Erlaubnis, diese Daten zu nutzen. Wir erheben keinen Anspruch auf Eigentum der Daten.

Quelle für die letzten acht Spalten (Subjects, Publisher, APCs?, APC Amount, Currency, Full Text Language, P-ISSN, E-ISSN) ist das Directory of Open Access Journals ([DOAJ](https://doaj.org/)), abgerufen am 19.01.2021. Die DOAJ-Metadaten stehen unter einer CC-BY-SA Lizenz.

Die Liste erhebt keinen Anspruch auf Vollständigkeit!

Zuletzt aktualisiert am 26.02.2021.

<br />
**Weitere Informationsmöglichkeiten**<br />
Zusätzlich zu unserer Tabelle bietet der [Scimago Journal Rank](https://www.scimagojr.com/) eine Übersicht über Open-Access-Fachzeitschriften (siehe "Only Open Access Journals").


<br />
**Haftungsausschluss**<br />
Die Universitätsbibliothek Bielefeld übernimmt keine Verantwortung für die Vollständigkeit, Richtigkeit und Aktualität der Daten. In keinem Fall kann die Universitätsbibliothek Bielefeld für jedwede Schäden, Verluste oder Beeinträchtigung des Geschäftsbetriebes, die aus der Nutzung dieses Services entstehen oder in Verbindung mit der Nutzung bzw. Beeinträchtigung dieses Services stehen, haftbar gemacht werden.
