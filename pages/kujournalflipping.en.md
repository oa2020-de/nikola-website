<!--
.. title: Knowledge Unlatched Journal Flipping
.. slug: KUjournalflipping
.. date: 2018-11-16 08:00:00
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

# KU Journal Flipping

Term: 2019-2021

Organiser: Knowledge Unlatched

Publisher: AIP, Berghahn, Brill, De Gruyter, IWA Publishing, John Benjamins, Sage


### Background

An essential prerequisite for the open access transformation is the reallocation of library acquisition budgets into the financing of open access publications. In addition to direct support for open access journals, in particular journals from the existing subscription system should also be converted into open access (journal flipping). [Knowledge Unlatched (KU)](http://knowledgeunlatched.org/) and the National Contact Point Open Access OA2020-DE have therefore jointly developed the model "KU Journal Flipping".

With the help of a bidding process, the transformation of quality-assured subscription or hybrid journals of various publishers, who have not hitherto been active in the open access transformation, or who have not hitherto been involved in the open access transformation, takes place. Financed by a national and international crowdfunding of libraries, the growth of gold open access articles can be substantially increased.


### Brief model description

+ “KU Journal Flipping” aims at flipping hybrid and closed-access journals to open access. KU organises the funding for the first three years. As of 2021, the journals raise their funding autonomously, for example, via APCs and remain open access.
+ More than 40 journals from 10 publishers applied for the “KU Journal Flipping” funding.
+ OA2020-DE evaluated all journals according to economic criteria. Based on this assessment, 21 journals will be submitted to the Title Selection Committee.
+ For “KU Journal Flipping”, a special Title Selection Committee is appointed. Members of the committee will be made public.
+ Each member has 10 votes to use to select the journals that they feel are the most relevant.
+ KU compiles a ranking list of journals from the votes of the Title Selection Committee Members. As funding comes in, journals will be flipped to open access one by one in descending order.
+ A total of 3 million euros in funding is targeted for the timeframe of three years.
+ Already paid subscriptions for flipped journals will be refunded.


### Pricing structure

<img src="/images/KU_journalflipping_pricestructure.png" alt="Preisstruktur für das Journal Flipping" width="350" height="150" display="block" />

*Annual participation fee, based on a three year commitment. En bloc-invoicing or annual invoicing available.*


The model will be implemented solely by KU on their own responsibility. Only information provided by KU can be part of a contract. Please direct your enquiries to Mrs. [Catherine Anderson](mailto:catherine@knowledgeunlatched.org).


### Journal title selection

The following list of journals has been submitted to the Title Selection Committee after the economic assessment:

<img src="/images/KU_21journals_titleselectioncommittee.png" alt="21 titles submitted to the KU Title Selection Committee" width="850" height="500" display="block" />
