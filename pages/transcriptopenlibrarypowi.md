<!--
.. title: transcript OPEN Library Politikwissenschaft
.. slug: transcriptopenlibrarypolwi
.. date: 2019-08-13 00:00:00
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

# transcript OPEN Library Politikwissenschaft 2020

Laufzeit: 2020

Organisator_innen: transcript Verlag, Knowledge Unlatched, Fachinformationsdienst Politikwissenschaft

Verlag: transcript

**DANKE!** Auch diese Crowdfunding-Runde ist ein voller Erfolg! **Wir danken den Förderern, die mit ihrem Beitrag dafür sorgen, dass die Politik-Frontlist im Open Access für alle Mitglieder des Wissenschaftsbetriebs zur Verfügung steht!**

[[Link zur Pressemitteilung](/assets/files/PM_OLPolWI2020.pdf)]


### Hintergrund

Im Zuge der Open-Access-Transformation möchten wir neue Impulse für einen offenen Zugang zu wissenschaftlichen Inhalten setzen. Daher haben wir 2018 in Zusammenarbeit mit dem transcript Verlag das Projekt ["transcript Open Library Politikwissenschaft"](https://www.transcript-verlag.de/open-library-politikwissenschaft) entwickelt.
Das Projekt war zunächst als [Pilot](/pages/transcriptopenlibrarypowi2019) geplant und für eine einjährige Laufzeit angelegt, jedoch mit dem Ziel, mit Unterstützung der wissenschaftlichen Gemeinschaft ein für Bibliotheken und Verlage gleichermaßen tragbares, transparentes und nachhaltiges Open-Access-Geschäftsmodell zu schaffen, und so den Weg für die Transformation weiterer Fachkollektionen mit qualitätsgeprüften Publikationen zu bereiten. Die gute Resonanz auf die erste Runde erlaubt nun die Fortsetzung des Modells.  Unterstützt wird das Ganze dabei vom Fachinformationsdienst (FID) [Politikwissenschaft](https://www.pollux-fid.de/) an der SuUB Bremen.


### Konditionen für 2020

Die finalen Konditionen für das Open-Access-Erscheinen der Frontlist „Politikwissenschaft transcript 2020“ hängen wie im Vorjahr wesentlich von der Anzahl der mitfinanzierenden Einrichtungen ab. Während POLLUX im ersten Jahr als Hauptsponsor 50 % der Gesamtkosten übernommen hat, werden in der aktuellen Runde auf diesem Weg 25 % gedeckt und die Kosten für alle weiteren Teilnehmenden gesenkt. Dank eines leicht gesenkten Bedarfs pro Publikation und einer höheren vorausgesetzten Teilnehmer_innenzahl sinken die (Start-)Kosten pro Buch und Teilnehmer_in trotz geringerer Beteiligung des FID. Mit derselben Entwicklung ist die Unterstützung durch POLLUX auch für die folgenden Jahre vorgesehen (sogenanntes Sliding-Scale-Modell):


| Jahr | Novitäten | Anteil FID | Teilnehmende (Start) | Paket     | Pro Buch |
|------|-----------|------------|----------------------|-----------|----------|
| 2019 | 20        | 50%        | 20                   | 2.300,00€ | 115,00€  |
| 2020 | 22        | 25%        | 30                   | 2.420,00€ | 110,00€  |
| 2021 | 22        | 15%        | 35                   | 2.350,86€ | 106,86€  |
| 2022 | 22        | 10%        | 40                   | 2.200,00€ | 100,00€  |

<br />
Um Planungssicherheit in die finanzielle Beteiligung der Bibliotheken zu bringen, wurde für 2020 eine Mindestsponsorenzahl von 30 Einrichtungen festgelegt, auf Basis dessen der maximale Rechnungsbetrag kalkuliert wird (mit FID-Beteiligung 2.420 € pro Einrichtung). Der endgültige Rechnungsbetrag reduziert sich anteilig, je mehr Einrichtungen sich an der Finanzierung beteiligen. Verpflichten sich weniger als 30 Einrichtungen zur Finanzierung, erfolgt keine Fakturierung und kein Open-Access-Erscheinen der Frontlist.


### Sponsoring Light – ein Angebot für Fachhochschulen

Wie bereits im letzten Jahr soll das »Sponsoring Light« auch kleineren Einrichtungen und Fachhochschulen die Teilnahme am Konsortium ermöglichen. Im »Sponsoring Light wird nur die Hälfte des regulären Beitrags fällig. Im Gegenzug wird die Präsentation als »Enabler« auf einen Eintrag in einer Sponsorenliste begrenzt. Den Fachhochschulen steht es selbstverständlich frei, zu den o.g. regulären Bedingungen am Sponsoring teilzunehmen.


**NEU: Mikrosponsoring**

Ergänzend zu den bisherigen Preisstufen gibt es in dieser Runde das »Mikrosponsoring«, über das sich auch Kunst- und Musikhochschulen, Spezial- und Fachbibliotheken, Parlaments- und Gerichtsbibliotheken sowie Regionalbibliotheken und andere Institutionen als Open-Access-Enabler einsetzen können, die üblicherweise nur wenige oder keine Bücher aus dem Politikprogramm von transcript erwerben, die aber dennoch die Open-Access-Transformation unterstützen möchten. Das Mikrosponsoring ist ein frei wählbarer Fixbetrag von mindestens 300 € pro Paket (also knapp 13,63 € pro Open-Access-Publikation) und kann erst ab Erreichen der Mindestteilnehmer_innenzahl in Anspruch genommen werden. Jedes Mikrosponsoring reduziert die Gesamtkosten für alle weiteren Teilnehmenden. Einrichtungen, die nur über das Mikrosponsoring teilnehmen, erhalten kein kostenloses Printexemplar und werden nur ohne Logo als Enabler gelistet.


### Vorteile

Open Access verfolgt das Ziel, die Chancen der Digitalisierung für Autor_innen, Verlage und Bibliotheken gleichermaßen zu nutzen. Mit Hilfe von nachhaltigen und transparenten Angeboten seitens der Verlage sowie der finanziellen Beteiligung durch Bibliotheken ergeben sich für alle Akteure neue Möglichkeiten zur Positionierung im wissenschaftlichen Publikationssystem.

Unser Modell bringt im Vergleich zur gängigen Praxis der Lizenzierung von E-Books u.a. folgende Vorteile mit sich:

+ Durch das Crowdfunding wird die Open-Access-Transformation für alle Akteure finanziell tragbar. Die Kosten pro Buch und Teilnehmer_in lagen im 2019er-Paket bei knapp über 50 EUR, also einem Bruchteil der üblichen Book-Processing Charges und vergleichbar mit Ladenpreisen von Fachliteratur.
+ Durch die Open-Access-Bereitstellung mit einer Creative-Commons-Lizenz stehen die Werke der gesamten Wissenschaftsgemeinschaft (je nach der – vom Autor/von der Autorin gewählten – CC-Lizenz) ohne Einschränkungen und ohne Folgekosten zur Verfügung.
+ Transparente Kostenkalkulation.
+ Schwerpunkt deutschsprachige Literatur, wodurch sichergestellt wird, dass öffentliche Mittel der deutschsprachigen Community zu Gute kommen.
+ Förderungskonformes Open Access einer Verlagspublikation für die Autor_innen.
+ Sinkende Transaktionskosten für E-Book-Erwerb/-Erschließung.
+ Sichtbarkeit der mitfinanzierenden Einrichtungen durch Sponsoring.
+ Verankerung des FID im fachbezogenen Open-Access-Publikationsprozess.

Das Projekt ist auf Solidarität gegründet und ermöglicht durch Ihre Mitarbeit ein wirtschaftlich tragbares, zukunftsfähiges und nachhaltiges Open-Access-Publizieren.

### Mitmachen

Wenn Sie Fragen zum Crowdfunding haben, wenden Sie sich direkt an Frau [Catherine Anderson](mailto:catherine@knowledgeunlatched.org) von Knowledge Unlatched oder an einen der angeschlossenen Bibliotheksdienstleister.

+ [Dietmar Dreier Wissenschaftliche Versandbuchhandlung GmbH](http://www.dietmardreier.de/ebooks/knowledgeunlatched),
+ [Missing Link Versandbuchhandlung eG](http://www.missing-link.de),
+ oder die [Schweitzer Fachinformation](http://www.schweitzer-online.de/info/Knowledge-Unlatched/).

Fragen zum Projekt beantwortet Ihnen Frau [Stefanie Hanneken](mailto:open-access@transcript-verlag.de) vom transcript Verlag gerne.


[Link zum Pilotprojekt transcript OPEN Library Politikwissenschaft 2019](/pages/transcriptopenlibrarypowi2019)
