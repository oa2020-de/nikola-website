<!--
.. title: Knowledge Unlatched Journal Flipping
.. slug: KUjournalflipping
.. date: 2018-11-16 08:00:00
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

# KU Journal Flipping

Laufzeit: 2019-2021

Organisator_in: Knowledge Unlatched

Verlage: AIP, Berghahn, Brill, De Gruyter, IWA Publishing, John Benjamins, Sage


### Hintergrund

Eine wesentliche Voraussetzung für die Open-Access-Transformation ist die Umwidmung von bibliothekarischen Erwerbungsbudgets in die Finanzierung von Open-Access-Publikationen. Neben der direkten Unterstützung von Open-Access-Zeitschriften sollen insbesondere auch Zeitschriften aus dem bestehenden Subskriptionssystem in den Open Access umgewandelt werden (Journal Flipping). [Knowledge Unlatched (KU)](http://knowledgeunlatched.org/) und der Nationale Open-Access-Kontaktpunkt OA2020-DE haben daher gemeinsam das Modell „KU Journal Flipping“ entwickelt.

Mithilfe eines Bieterverfahrens erfolgt die Transformation von qualitätsgesicherten Subskriptions- oder Hybridzeitschriften verschiedener, bisher nicht oder nur wenig in der Open-Access-Transformation engagierter Verlage. Finanziert durch ein nationales und internationales Crowdfunding von Bibliotheken, kann so das Wachstum an Gold-Open-Access-Artikeln maßgeblich gesteigert werden.


### Modellbeschreibung

+ Ziel ist es, Hybrid- und Closed-Access-Fachzeitschriften in den Open Access zu flippen. KU organisiert die Finanzierung für die ersten drei Jahre über Crowdfunding. Ab dem vierten Jahr tragen sich die Zeitschriften selbstständig (bspw. über APCs) und bleiben Open Access.
+ Für das KU „Journal Flipping“ haben sich über 40 Zeitschriften von 10 Verlagen beworben.
+ Von OA2020-DE wurden alle Zeitschriften nach wirtschaftlichen Kriterien evaluiert. Auf Basis dieser Bewertung werden dem Title Selection Committee 21 Zeitschriften vorgelegt.
+ Für das KU „Journal Flipping“ wird ein eigenes Title Selection Committee einberufen. Die Mitglieder des Komitees werden öffentlich gemacht.
+ Jedes Mitglied des Title Selection Committee wählt aus den 21 Zeitschriften 10 Favoriten aus.
+ KU bildet aus den Abstimmungen des Title Selection Committee eine Rangliste der 21 Zeitschriften, nach der das Flipping der Zeitschriften absteigend erfolgt bis die eingenommenen Mittel erschöpft sind.
+ Es wird eine Finanzierungssumme von insgesamt ca. 3 Mio. EUR für die drei Jahre angestrebt.
+ Bereits gezahlte Subskriptionsgelder für geflippte Journals werden den Abonnementen erstattet.


### Preisstruktur

<img src="/images/KU_journalflipping_pricestructure.png" alt="Preisstruktur für das Journal Flipping" width="350" height="150" display="block" />

*Jährliche Teilnahmegebühr, basierend auf einem 3-Jahres-Vertrag. Die Gebühr kann als Gesamtsumme mit einem mal oder jährlich bezahlt werden.*


Die Umsetzung erfolgt ausschließlich durch Knowledge Unlatched und auf deren alleinige Verantwortung. Insofern sind nur die Informationen durch KU vertraglich bindend.
Bei Fragen wenden Sie sich bitte an Frau [Catherine Anderson](mailto:catherine@knowledgeunlatched.org).


### Zeitschriftenauswahl

Folgende Zeitschriftenliste ist nach der ökonomischen Prüfung dem Title Selection Committee vorgelegt worden:

<img src="/images/KU_21journals_titleselectioncommittee.png" alt="21 Titel, die dem KU Title Selection Committee vorgelegt werden" width="850" height="500" display="block" />
