<!--
.. title: Open Access IT Law
.. slug: oaitlaw
.. date: 2019-02-25 10:00:00
.. author:
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

# Open Access IT Law

Laufzeit: 2019-2021

Organisator_innen: Peter Lang Verlag, Knowledge Unlatched, Fachinformationsdienst für internationale und interdisziplinäre Rechtsforschung (&lt;intR>²) der Staatsbibliothek zu Berlin

Verlag: Peter Lang


### Hintergrund

Der Fachinformationsdienst für internationale und interdisziplinäre Rechtsforschung ([&lt;intR>²](https://vifa-recht.de/)) der Staatsbibliothek zu Berlin fühlt sich seit seiner Gründung im Jahre 2014 in hohem Maße dem Paradigma des Open Access verpflichtet. Dieses Bekenntnis hat bisher seinen konkreten Ausdruck im Auf- und Ausbau des disziplinären Open Access-Repositoriums [&lt;intR>²Dok](https://intr2dok.vifa-recht.de/content/index.xml) gefunden, das allen interessierten Angehörigen rechtswissenschaftlicher Forschungseinrichtungen die Erst- oder Zweitveröffentlichung von Texten, Forschungsdaten und audiovisuellen Formaten aus dem Bereich der internationalen und interdisziplinären Rechtsforschung im Open Access ermöglicht.

Unter Vermittlung des Nationalen Open-Access-Kontaktpunkts sowie inspiriert von der [OPEN Library Politikwissenschaft](/pages/transcriptopenlibrarypowi/) hat sich der Fachinformationsdienst für internationale und interdisziplinäre Rechtsforschung – insbesondere mit Blick auf die Open-Access-Transformation von rechtswissenschaftlichen Monographien – entschieden, ein Gemeinschaftsprojekt mit dem Peter Lang Verlag und Knowledge Unlatched zum Aufbau eines Open Access-E-Book-Pools zum Digitalrecht finanziell und organisatorisch zu unterstützen. Der fokussierte Programmbereich ist bewusst gewählt, ziehen sich doch gerade derzeit rechtliche Fragen der Digitalisierung massiv durch alle Bereiche der Gesellschaft.

Ziel dieses Projekts ist es zunächst, über drei Jahre hinweg (beginnend 2019) jährlich 10 Frontlist-Titel des Peter Lang Verlags im IT-Recht Open Access zur Verfügung zu stellen. Die Auswahl wird durch ein wissenschaftliches Fachkollegium, an dem auch der Fachinformationsdienst beteiligt ist, vorgenommen.<br />
**Aufgrund der finanziellen Beteiligung des FID &lt;intR>² sind Teile der Gesamtkosten bereits gedeckt.**

Als Vorgeschmack wurden durch den Verlag bereits fünf Titel aus der Back-List Open Access gestellt, die u.a. über &lt;intR>²Dok abrufbar sind ([Open Access IT Law](https://intr2dok.vifa-recht.de/servlets/solr/select?q=%2Bcategory.top:%22intr2dok_projects:5d4bacc2-cc93-4d01-bbc1-89597fcd55d9%22+%2BobjectType:mods+%2Bcategory.top:%22state:published%22&mask=content/browse/intr2dok_projects.xml%23open%5B%5D&sort=mods.dateIssued+desc)).


### Konditionen

Die Preisstruktur der Finanzierung von Open Access IT Law hängt von der Anzahl der beteiligten Einrichtungen ab, wobei sich der Jahresbetrag beispielsweise pro Einrichtung/Bibliothek von € 2.145 (bei 20 teilnehmenden Institutionen, was der Mindestsponsorenzahl entspricht) auf bis zu € 1.075 (bei 40 teilnehmenden Institutionen) reduzieren kann.

|    | Dollar | Euro  | Brit. Pfund |
|-------------------------------|--------|-------|-----------------|
| 20 teilnehmende Institutionen | 2.530  | 2.145 | 1.888           |
| 30 teilnehmende Institutionen | 1.690  | 1.430 | 1.260           |
| 40 teilnehmende Institutionen | 1.265  | 1.075 | 945             |

*Jährlicher Preis pro Bibliothek über einen Zeitraum von 3 Jahren.*



### Ausblick

Für den Fachinformationsdienst für internationale und interdisziplinäre Rechtsforschung ist dieses Projekt lediglich ein erster Schritt zur Beförderung der Open-Access-Transformation im Bereich der Rechtswissenschaft. Perspektivisch sind daher nicht nur andere Verlage, sondern auch weitere Bereiche der internationalen und interdisziplinären Rechtsforschung in den Fokus dahingehender Aktivitäten zu nehmen.

Wenn auch Sie sich an der Finanzierung von Open Access IT Law beteiligen möchten, wenden Sie sich bitte direkt an [Catherine Anderson](mailto:catherine@knowledgeunlatched.org) von Knowledge Unlatched.

Seitens des Peter Lang Verlags steht Ihnen Herr [Adam Gardner](mailto:a.gardner@peterlang.com) für die Beantwortung von Fragen sehr gerne zur Verfügung.

Seitens des FIDs steht Ihnen Herr [Ivo Vogel](mailto:Ivo.Vogel@sbb.spk-berlin.de) von der Staatsbibliothek zu Berlin bei Fragen zur Verfügung.
