<!--
.. title: transcript OPEN Library Political Science
.. slug: transcriptopenlibrarypols
.. date: 2019-08-13 00:00:00
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

# transcript OPEN Library Political Science 2020

Term: 2020

Organiser: transcript publishing house, Knowledge Unlatched, Political Science Information Service (FID Pollux)

Publisher: transcript

**THANK YOU!** This crowdfunding round is also a complete success!  **We would like to thank the sponsors, whose contribution ensures that the political front list in Open Access is available to all members of the scientific community!**

[[Link to the Press Release (only in german)](/assets/files/PM_OLPolWI2020.pdf)]


### Background

As part of the open-access transformation, we want to give new inspiration for open access to scientific content. Therefore, we have launched in 2018 the ["transcript Open Library Political Science"](https://www.transcript-verlag.de/open-access-politikwissenschaft) project in cooperation with the transcript publishing house.
The project was initially planned as a [pilot](/en/pages/transcriptopenlibrarypols2019) and designed for a one-year term. With the support of the scientific community, a manageable, transparent and sustainable open-access business model was to be created that would be equally acceptable to libraries and publishers, thus paving the way for the transformation of further specialist collections with quality-checked publications. The good response to the first round now allows the model to be continued.
The whole thing is supported by the [Political Science Information Service](https://www.pollux-fid.de/) (FID) at Bremen State and University Library.


### Conditions

The final conditions for the open access appearance of the front list "Political Science at transcript 2020" depends - like last year - essentially on the number of co-financing institutions. While POLLUX took over 50% of the total costs in the first year as the main sponsor, this round will cover 25% in the current round and cut costs for all other participants. Thanks to a slightly reduced need per publication and a higher expected number of participants, the (start) costs per book and participant decrease despite less participation of the FID. With the same development, support from POLLUX is also planned for the following years (so called sliding-scale model):


| Year | Novelties | Part of the FID | Attendees (Start) | Package     | Per Book |
|------|-----------|------------|----------------------|-----------|----------|
| 2019 | 20        | 50%        | 20                   | 2.300,00€ | 115,00€  |
| 2020 | 22        | 25%        | 30                   | 2.420,00€ | 110,00€  |
| 2021 | 22        | 15%        | 35                   | 2.350,86€ | 106,86€  |
| 2022 | 22        | 10%        | 40                   | 2.200,00€ | 100,00€  |

<br />
In order to provide planning certainty for the financial participation of the libraries, in 2020 a minimum number of 30 sponsors has been set, on the basis of which the maximum invoice amount is calculated (with FID participation € 2,420 per institution). The final invoice amount will be proportionately reduced as more entities participate in the funding. If there are less than 30 funding institutions, there will be no billing and no open access to the front list.


### Sponsoring Light - an offer for the universities of applied sciences

In order to allow even smaller institutions to participate in the (informal) consortium, universities of applied sciences have the opportunity to make use of the "Sponsoring Light" offer. "Sponsoring Light" means participation through half funding, i. e. only half of the regular contribution is due. In return, the presentation as "enabler" is limited to an entry in a sponsorship list. Of course the universities of applied sciences are free to participate in regular sponsoring.


**NEW: Microsponsoring**

In this round we have introduced a new option in order to be able to allow small institutions such as arts and music schools, specialist institutions, parliamentary and legal libraries to participate even though they would not normally purchase (m)any Political Science books, but who nonetheless wish to show their support for open-access models. The “Microsponsoring“ model allows such libraries to pledge their support at an amount of their choosing as long as it is over €300 (around € 13.63 per title) and the amount can only count towards the total once the minimum participation level has been reached. Participants at this level do not have the right to receive free print copies and are only listed as “enablers” by name, not logo.


### Advantages

Open Access pursues the goal of making equal use of the opportunities of digitization for authors, publishers and libraries alike. With the help of sustainable and transparent offers on the part of publishers as well as the financial participation by libraries new possibilities for the positioning in the scientific publication system arise for all actors.

Our model has the following advantages compared to the common practice of e-book licensing:

* Thanks to the collaborative model the costs per institution can be kept at an affordable level. The final price per book and per supporter in the 2019 collection was just over € 50 which is far lower than a usual Book Processing Charge and also lower than the average price of HSS monographs.
* By providing open access with a creative-commons license, the works are available to the entire scientific community (depending on the CC license chosen by the author) without restrictions and without follow-up costs.
* Transparency in title selection and calculation.
* Focus on German-language literature, ensuring that public funds benefit the German-speaking community.
* Open-access publication for authors in conformity with funding regulations.
* Descending transaction costs for e-book acquisition / indexing.
* Visibility of the co-financing institutions through sponsorship.
* Anchoring the FID in the subject-specific open access publication process.

The project is based on solidarity and through your cooperation enables an economically manageable, sustainable and viable open-access publishing.


### Practicalities

If you have any questions about the crowdfunding, please contact [Catherine Anderson](mailto:catherine@knowledgeunlatched.org) of Knowledge Unlatched or one of the affiliated library service providers.

+ [Dietmar Dreier International Library Supplier](http://www.dietmardreier.de/ebooks/knowledgeunlatched),
+ [Missing Link mail order bookstore](http://www.missing-link.de),
+ or [Schweitzer Fachinformation](http://www.schweitzer-online.de/info/Knowledge-Unlatched/).

Questions about the project will be answered by [Ms. Stefanie Hanneken](mailto:open-access@transcript-verlag.de) from transcript publishing house.

[Link to the pilot transcript OPEN Library Political Science 2019](/en/pages/transcriptopenlibrarypols2019)
