<!--
.. title: Imprint
.. slug: imprint
.. date: 2017-08-17 00:00:00
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

__National Contact Point Open Access OA2020-DE__

c/o Bielefeld University Library<br />
Universitätsstrasse 25<br />
D-33615 Bielefeld<br />

<i class="fa fa-envelope" aria-hidden="true"></i> [dirk.pieper@uni-bielefeld.de](mailto:dirk.pieper@uni-bielefeld.de)<br />
<i class="fa fa-phone-square" aria-hidden="true"></i> +49 521 106-4049<br />

Bielefeld University Library is a central service unit of Bielefeld University.<br />
Bielefeld University is a public corporation. It is officially represented by the rector Prof Dr Gerhard Sagerer.<br />

__Responsible__

Barbara Knorn, library director (adress same as above)

Liability Disclaimer: Although we take great care to control their content, we accept no liability for external links. The sole responsibility for the content of third-party pages lies with their respective operators.

__Privacy statement__

Information on data protection at the University of Bielefeld: [Privacy statement](http://www.uni-bielefeld.de/(en)/Universitaet/datenschutzhinweise.html)<br />
This privacy statement also applies to the websites and information offered by the National Contact Point Open Access OA2020-DE.


**Facebook information**

Fanpages operators can use the Facebook insight feature, which provides them with Facebook as a non-dispensable part of the use ratio, to obtain anonymized statistical data on the users of these pages. These data are collected with the help of so-called cookies, each containing a unique user code that is active for two years and stores the Facebook on the hard disk of the computer or another disk of the visitors of the fan page. The user code, which can be linked to the login data of users who are registered with Facebook, is collected and processed when the fanpages are called up. <br />
Hereby we point out that the visit or other interaction with the Facebook fanpage of the National Contact Point Open Access leads to the fact that with the help of cookies personal data of you anonymously stored and processed for statistical purposes.
