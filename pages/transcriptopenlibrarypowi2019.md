<!--
.. title: transcript OPEN Library Politikwissenschaft 2019
.. slug: transcriptopenlibrarypowi2019
.. date: 2018-06-10 00:00:00
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

# transcript OPEN Library Politikwissenschaft 2019

Laufzeit: 2019

Organisator_innen: transcript Verlag, Knowledge Unlatched, Fachinformationsdienst Politikwissenschaft

Verlag: transcript


**Hintergrund**

Im Zuge der Open-Access-Transformation möchten wir neue Impulse für einen offenen Zugang zu wissenschaftlichen Inhalten setzen. Daher haben wir in Zusammenarbeit mit dem transcript Verlag das Projekt ["transcript Open Library Politikwissenschaft"](https://www.transcript-verlag.de/open-access-netzwerke-und-projekte#open-library-politikwissenschaft) ins Leben gerufen.
Ziel des Pilotvorhabens ist die Entwicklung eines für Verlag und Bibliotheken gleichermaßen tragbaren, transparenten und ökonomisch nachhaltigen Open-Access-E-Book-Geschäftsmodells. Dabei sollen die Defizite vorhandener Ansätze adäquat adressiert werden. Statt wie gewohnt die E-Book-Lizenz zu erwerben, ermöglicht die teilnehmende Bibliothek über eine Gebühr im Crowdfundingmodell die Open-Access-Veröffentlichung der gesamten Frontlist „Politikwissenschaft transcript Verlag 2019“ (20 Titel).
Die Finanzmittel der Bibliotheken fließen so, statt in den Erwerb kostenpflichtiger E-Books für eine einzelne Bibliothek, in die Finanzierung der freien Verfügbarkeit für alle. Unterstützt wird das Modell dabei vom Fachinformationsdienst (FID) [Politikwissenschaft](https://www.pollux-fid.de/) an der SuUB Bremen.


**Konditionen**

Die finalen Konditionen für das Open-Access-Erscheinen der Frontlist „Politikwissenschaft transcript 2019“ hängen wesentlich von der Anzahl der mitfinanzierenden Einrichtungen ab. Der FID Politikwissenschaft hat eine Beteiligung an der Finanzierung der Frontlist in Höhe von 50% zugesagt (d. h. 46.000 € von 92.000 € für das Paket sind bereits eingeworben). Um Planungssicherheit in die finanzielle Beteiligung der Bibliotheken zu bringen, wurde eine Mindestsponsorenzahl (20 Einrichtungen) festgelegt, auf Basis dessen der maximale Rechnungsbetrag kalkuliert wird (mit FID-Beteiligung 2.300 € pro Einrichtung). Der endgültige Rechnungsbetrag reduziert sich anteilig, je mehr Einrichtungen sich an der Finanzierung beteiligen. Verpflichten sich weniger als 20 Einrichtungen zur Finanzierung, erfolgt keine Fakturierung und kein Open-Access-Erscheinen der Frontlist. Im Gegenzug treten mitfinanzierende Einrichtungen als Sponsoren auf und erhalten auf Wunsch ein kostenloses Print-Exemplar der Titel aus der Frontlist.

<img src="/images/transcript_Konditionen-Bibliotheken.png" alt="Rabattstufen eBook-Paket Politikwissenschaft" width="800" height="142" display="block" />

Die Liste der bisherigen »Community« finden Sie hier: [https://www.transcript-verlag.de/transcript-Open-Library-Politikwissenschaft-Community](https://www.transcript-verlag.de/transcript-Open-Library-Politikwissenschaft-Community).


**Sponsoring Light – ein Angebot für Fachhochschulen**

Um auch kleineren Einrichtungen die Teilnahme am Konsortium zu ermöglichen, haben Fachhochschulen die Möglichkeit, das Angebot Sponsoring Light in Anspruch zu nehmen. Sponsoring Light bedeutet die Teilnahme über die hälftige Finanzierung, d.h. es wird nur die Hälfte des regulären Beitrags fällig. Im Gegenzug wird die Präsentation als "Enabler" auf einen Eintrag in einer Sponsorenliste begrenzt. Den Fachhochschulen steht es selbstverständlich frei, zu den o.g. regulären Bedingungen am Sponsoring teilzunehmen.


**Vorteile**

Open Access verfolgt das Ziel, die Chancen der Digitalisierung für Autor_innen, Verlage und Bibliotheken gleichermaßen zu nutzen. Mit Hilfe von nachhaltigen und transparenten Angeboten seitens der Verlage sowie der finanziellen Beteiligung durch Bibliotheken ergeben sich für alle Akteure neue Möglichkeiten zur Positionierung im wissenschaftlichen Publikationssystem.

Unser Modell bringt im Vergleich zur gängigen Praxis der Lizenzierung von E-Books folgende Vorteile mit sich:

+ Lizenzkosten für E-Books entfallen, Finanzierung durch Umschichtung im Etat möglich
+ Planvolle Verausgabungsmöglichkeit im Sinne der Open-Access-Transformation
+ Sinkende Transaktionskosten für E-Book-Erwerb/-Erschließung
+ Keine Einschränkungen für die Nutzung der Inhalte (kein DRM, Aktivierung für Studium und Lehre, keine Fernleihe nötig, keine komplexe Authentifizierung etc.)
+ Sichtbarkeit der mitfinanzierenden Einrichtungen durch Sponsoring
+ Verankerung des FID im fachbezogenen Open-Access-Publikationsprozess
+ Plurikontexturale Verortung der Bibliotheken im Netzwerk aus Autor_innen – Verlage – Wissenschaftscommunities – Öffentlichkeit
+ Ausschöpfung neuer Medialisierungspotenziale von Open-Access-Content unter Beteiligung von Bibliotheken
+ Freie Nachnutzbarkeit der Metadaten durch eine [OAI-Schnittstelle](https://www.transcript-verlag.de/oai/oai2.php)

Lesen Sie dazu auch die FAQ zum Projekt: [https://www.transcript-verlag.de/transcript-Open-Library-Politikwissenschaft-FAQ](https://www.transcript-verlag.de/transcript-Open-Library-Politikwissenschaft-FAQ).

In Zusammenarbeit mit dem transcript Verlag zielt das Pilotmodell auf Basis einer transparenten Kostenkalkulation für Open-Access-E-Books und eines gemeinschaftlichen Finanzierungskonzepts unter Beteiligung des einschlägigen Fachinformationsdienstes auf die Open-Access-Stellung aller der im kommenden Jahr beim transcript Verlag erscheinenden politikwissenschaftlichen Titel. Bewährt sich das Modell, ist eine Skalierung auf andere Programmbereiche und Verlage möglich und erwünscht.

Wenn Sie pledgen wollen, wenden Sie sich direkt an [Catherine Anderson](mailto:catherine@knowledgeunlatched.org) von Knowledge Unlatched oder an einen der angeschlossenen Bibliotheksdienstleister:

+ [Dietmar Dreier Wissenschaftliche Versandbuchhandlung GmbH](http://www.dietmardreier.de/ebooks/knowledgeunlatched),
+ [Missing Link Versandbuchhandlung eG](http://www.missing-link.de),
+ oder die [Schweitzer Fachinformation](http://www.schweitzer-online.de/info/Knowledge-Unlatched/).

Fragen zum Projekt beantwortet Ihnen Frau [Stefanie Hanneken](mailto:open-access@transcript-verlag.de) vom transcript Verlag gerne.

