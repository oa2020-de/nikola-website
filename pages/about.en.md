<!--
.. title: About
.. slug: about
.. date: 2017-08-17 00:00:00
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

The strategic goal of the National Contact Point Open Access OA2020-DE is to create requirements for the large-scale open access transformation in accordance with the Alliance of Science Organisations in Germany. In addition to the global [OA2020 Initiative](https://oa2020.org/) (initiated by the Max Planck Digital Library), national implications are the focus here. Thus, the transformation process is supported by extensive publication and cost data analyses by German universities and research institutes, and on this basis, open access financial and business models are developed.

OA2020-DE is part of an established OA2020 community, works within the framework of the working group [digital information](http://www.allianzinitiative.de) of the Alliance and cooperates with German universities, non-university research institutes and publishers. The project is working closely with [DEAL](https://www.projekt-deal.de/) and will be responsible for the exchange with all relevant projects such as [INTACT](https://www.intact-project.org/) or the [Open Access Monitor](https://open-access-monitor.de).

The project is funded by the [Alliance of Science Organisations in  Germany](http://www.dfg.de/dfg_profil/allianz/) for three years.


## [Aims](/en/pages/aims/)


## [Project organisation](/en/pages/projectorganisation/)
