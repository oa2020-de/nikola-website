<!--
.. title: Publications
.. slug: publications
.. date: 2018-01-29 08:00:00
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

## Publications 2020

<i class="fa fa-paper-plane" aria-hidden="true"></i>
Ecker, D., Pollack, P. & Rosenberger, S. (2020). Was leistet der Open Access Monitor für Bibliotheken? *ABI-Technik, 40*(1), 101. doi:[10.1515/abitech-2020-1013](https://doi.org/10.1515/abitech-2020-1013)

<i class="fa fa-paper-plane" aria-hidden="true"></i>
Schönfelder, N. (2020). Article processing charges: Mirroring the citation impact or legacy of the subscription-based model? *Quantitative Science Studies, 1*(1), 6-27. doi:[10.1162/qss_a_00015](https://doi.org/10.1162/qss_a_00015)

<i class="fa fa-paper-plane" aria-hidden="true"></i>
Schönfelder, N. (2020). Price Transparency and Structure of Article Processing Charges. *Bibliothek - Forschung und Praxis, 44*(1), 22-29. doi:[https://doi.org/10.1515/bfp-2019-2079](https://doi.org/10.1515/bfp-2019-2079)

<i class="fa fa-paper-plane" aria-hidden="true"></i>
Schönfelder, N. (2020). Proposal for a new model of transformative agreements: A smooth transition from subscriptions to APCs. Bielefeld: University Library Bielefeld. doi:[10.4119/unibi/2939995](https://doi.org/10.4119/unibi/2939995)

<i class="fa fa-paper-plane" aria-hidden="true"></i>
Schönfelder, N., & Pieper, D. (2020). Establishing tender procedures and competition within the framework of national library consortia for open access journals: Description of a pilot project. Bielefeld: University Library Bielefeld. doi:[10.4119/unibi/2939999](https://doi.org/10.4119/unibi/2939999)
<br />
<br />


## Publications 2019

<i class="fa fa-paper-plane" aria-hidden="true"></i>
Copernicus Publications & Lower Saxony State and University Library Göttingen (2019). *National Consortium for Central Payment Processing of Article Processing is started* [Press Release]. Retrieved 16 Jan. 2019, from [https://idw-online.de/de/news708983](https://idw-online.de/de/news708983)

<i class="fa fa-paper-plane" aria-hidden="true"></i>
Jobmann, A. (2019). Report on the Hands-On-Lab "New Business Models and Workflows in Open Access" at the 7th Library Congress in Leipzig on 19 March 2019. *O-Bib. Das Offene Bibliotheksjournal, 6*(4), 216-220. doi:[10.5282/o-bib/2019H4S216-220](https://doi.org/10.5282/o-bib/2019H4S216-220)

<i class="fa fa-paper-plane" aria-hidden="true"></i>
Jobmann, A., & Schönfelder, N. (2019). The Transcript OPEN Library Political Science Model: A Sustainable Way into Open Access for E-Books in the Humanities and Social Sciences. *Publications, 7*(3). doi:[10.3390/publications7030055](https://doi.org/10.3390/publications7030055)

<i class="fa fa-paper-plane" aria-hidden="true"></i>
Schönfelder, N. (2019). Transformationsrechnung: Mittelbedarf für Open Access an ausgewählten deutschen Universitäten und Forschungseinrichtungen. Bielefeld: Universitätsbibliothek Bielefeld. doi:[10.4119/unibi/2937971](http://doi.org/10.4119/unibi/2937971)

<i class="fa fa-paper-plane" aria-hidden="true"></i>
Schönfelder, N., Jobmann, A., Pollack, P., & Ecker, D. (2019). OA2020-DE-Forschungsbericht zum Publikationsaufkommen und zur Verteilung wissenschaftlicher Artikel im Kontext der Open-Access-Transformation an deutschen Wissenschaftseinrichtungen. Bielefeld: Universitätsbibliothek Bielefeld. doi:[10.4119/unibi/2937155](http://doi.org/10.4119/unibi/2937155)

<i class="fa fa-paper-plane" aria-hidden="true"></i>
transcript Verlag & OA2020-DE (2019). *Freier Zugang zu wissenschaftlicher Literatur* [Press Release]. Retrieved 21 Jan. 2020, from [https://idw-online.de/de/news730271](https://idw-online.de/de/news730271)
<br />
<br />


## Publications 2018

<i class="fa fa-paper-plane" aria-hidden="true"></i>
Jobmann, A. (2018). OA2020-DE treibt Open-Access-Transformation von Fachzeitschriften voran. *b.i.t. online KongressNews, 2018*(2), pp. 8. Retrieved 20 Jun. 2018, from [https://www.b-i-t-online.de/daten/berlin/KN_Berlin_2018_2.pdf](https://www.b-i-t-online.de/daten/berlin/KN_Berlin_2018_2.pdf)

<i class="fa fa-paper-plane" aria-hidden="true"></i>
Jobmann, A. (2018). National Contact Point Open Access OA2020-DE: Aims, tasks and achievements. *O-Bib. Das Offene Bibliotheksjournal, 5*(4), 101-112. doi:[10.5282/o-bib/2018H4S101-112](https://doi.org/10.5282/o-bib/2018H4S101-112)

<i class="fa fa-paper-plane" aria-hidden="true"></i>
Jobmann, A. (2018). Transformationsindex zur Einordnung einer wissenschaftlichen Einrichtung in die Open-Access-Transformation. Bielefeld: Universitätsbibliothek Bielefeld. doi:[10.4119/unibi/2931153](https://dx.doi.org/10.4119/unibi/2931153)

<i class="fa fa-paper-plane" aria-hidden="true"></i>
Mittermaier, B., Barbers, I., Ecker, D., Lindstrot, B., Schmiedicke, H., & Pollack, P. (2018). The Open Access Monitor Germany. *O-Bib. Das Offene Bibliotheksjournal, 5*(4), 84-100. doi:[10.5282/o-bib/2018H4S84-100](https://doi.org/10.5282/o-bib/2018H4S84-100)

<i class="fa fa-paper-plane" aria-hidden="true"></i>
Pieper, D. (2018). *OpenAPC – transparent reporting on article processing charges reveals the relative costs of open access publishing*. LSE Impact Blog. Retrieved from [http://blogs.lse.ac.uk/impactofsocialsciences/2018/10/25/openapc-transparent-reporting-on-article-processing-charges-reveals-the-costs-of-open-access-publishing/](http://blogs.lse.ac.uk/impactofsocialsciences/2018/10/25/openapc-transparent-reporting-on-article-processing-charges-reveals-the-costs-of-open-access-publishing/)

<i class="fa fa-paper-plane" aria-hidden="true"></i>
Schönfelder, N. (2018). APCs — Mirroring the impact factor or legacy of the subscription-based model?. Bielefeld: Universitätsbibliothek Bielefeld. doi:[10.4119/unibi/2931061](https://dx.doi.org/10.4119/unibi/2931061)

<i class="fa fa-paper-plane" aria-hidden="true"></i>
transcript Verlag & OA2020-DE (2018). *Neuerscheinungen des Programmbereichs Politikwissenschaft 2019 werden Open Access* [Press Release]. Retrieved 12 Dec. 2018, from [https://idw-online.de/de/news707724](https://idw-online.de/de/news707724)
<br />
<br />


## Publications 2017

<i class="fa fa-paper-plane" aria-hidden="true"></i>
Mittermaier, B. (2017). *Datenarbeit und „Nationaler Kontaktpunkt Open Access“ – ein Interview mit Dr. Bernhard Mittermaier*. ABI Technik, 37(4), pp. 293-296. Retrieved 15 Nov. 2017, from [doi:10.1515/abitech-2017-0062](https://doi.org/10.1515/abitech-2017-0062)

<i class="fa fa-paper-plane" aria-hidden="true"></i>
Jobmann, A., & Beckmann, W. (2017). *Gemeinsame Stellungnahme der Initiativen DEAL und OA2020-DE zum DGUF-Kommentar vom 13.10.2017* [[PDF document](/assets/files/20171017_Kommentar zum DGUF.pdf)].

<i class="fa fa-paper-plane" aria-hidden="true"></i>
Rimmert, C., Bruns, A., Lenke, C., & Taubert, N. C. (2017). ISSN-Matching of Gold OA Journals (ISSN-GOLD-OA) 2.0. Bielefeld: Bielefeld University. doi:[10.4119/unibi/2913654](https://dx.doi.org/10.4119/unibi/2913654)

<i class="fa fa-paper-plane" aria-hidden="true"></i>
Universität Bielefeld (2017). *Nationaler Kontaktpunkt zur Open Access-Umstellung eröffnet* [Pressemitteilung]. Retrieved 16 Oct. 2017, from [https://idw-online.de/de/news682812](https://idw-online.de/de/news682812)

<i class="fa fa-paper-plane" aria-hidden="true"></i>
Wohlgemuth, M., Rimmert, C., & Taubert, N. C. (2017). Publikationen in Gold-Open-Access-Journalen auf globaler und europäischer Ebene sowie in Forschungsorganisationen. Bielefeld: Universität Bielefeld. doi:[10.13140/RG.2.2.33235.89120](https://dx.doi.org/10.13140/RG.2.2.33235.89120)
