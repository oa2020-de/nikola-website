<!--
.. title: Übersicht der Open-Access-Beauftragten an deutschen Hochschulen und Forschungseinrichtungen
.. slug: uebersichtoabeauftragte
.. date: 2019-03-05 00:00:00
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

Stand: 10.02.2020

*Diese Liste erhebt keinen Anspruch auf Vollständigkeit!*

Sie enthält vorrangig den/die in der Bibliothek angesiedelte/n Ansprechpartner_in.

<i class="fa fa-angle-double-right" aria-hidden="true"></i>
Und es gibt von der deutschsprachigen OKF-Arbeitsgruppe "Open Science" eine ähnlich Seite, die ich hier gerne mit verlinke:<br />
[https://ag-openscience.de/open-access-beauftragte-an-deutschen-wissenschaftsinstitutionen-eine-uebersicht/](https://ag-openscience.de/open-access-beauftragte-an-deutschen-wissenschaftsinstitutionen-eine-uebersicht/)


| Bundesland | Hochschule/AUFE | Ansprechpartner_in | Publikationsfonds? | für Zeitschriften/Monographien? |
| ------ | ------ | ------ | ------ | ------ |
| Baden-Württemberg | Universität Freiburg im Breisgau | Nadine Keßler | Ja | Zeitschriften |
|  | Deutsches Krebsforschungszentrum, Heidelberg (DKFZ) | Dagmar Sitek | Nein |  |
|  | Universität Heidelberg | Dr. Maria Effinger | Ja | beides (inkl. Sammelwerke) |
|  | Karlsruher Institut für Technologie | Regine Tobias | Ja | Zeitschriften |
|  | Universität Konstanz | Dr. Anja Oberländer | Ja | beides (inkl. Sammelwerke und Sammelwerksbeiträge) |
|  | Leibniz-Institut für Deutsche Sprache (IDS), Mannheim | Monika Pohlschmidt | Teilnahme an den Publikationsfonds der Leibniz-Gemeinschaft | beides (inkl. Sammelwerken) |
|  | Universität Mannheim | Dr. Philipp Zumstein  | Ja | Zeitschriften |
|  | Universität Stuttgart | Stefan Drößler | Ja | Zeitschriften |
|  | Universität Tübingen | Ute Grimmel-Holzwarth | Ja | Zeitschriften |
|  | Universität Ulm | Uli Hahn | Ja | Zeitschriften |
| Bayern | Universität Augsburg | Sonja Härkönen | Nein |  |
|  | Universität Bamberg | Barbara Ziegler | Ja | Zeitschriften |
|  | Universität Bayreuth | Clemens Engelhardt | Ja | Zeitschriften |
|  | Katholische Universität Eichstätt-Ingolstadt | Constance Dittrich | Ja | Zeitschriften |
|  | Universität Erlangen-Nürnberg | Markus Putnings (Bibliothek), Prof. Dr. rer. nat. Olaf Gefeller (Hochschule) | Ja | Zeitschriften |
|  | Max-Planck-Institut für Plasmaphysik, Garching b. München | Dr. Gerda Maria Lucha | Nein |  |
|  | Fraunhofer-Gesellschaft Zentrale - C9 Informationsmanagement, München | Dr. Michael Erben-Russ | Ja | Zeitschriften |
|  | Helmholtz Zentrum München, Deutsches Forschungszentrum für Gesundheit und Umwelt (HMGU) | Astrid Uerlichs | Nein |  |
|  | LMU München | Volker Schallehn | Nein |  |
|  | TU München | Christian Pauls | Ja | Zeitschriften |
|  | Universität Passau | Michael Zweier | Ja | beides (inkl. Sammelwerke und Sammelwerksbeiträge) |
|  | Universität Regensburg | Dr. Gernot Deinzer | Ja | Zeitschriften |
|  | Universität Würzburg | Dr. Diana Klein | Ja | Zeitschriften |
| Berlin | Alice Salomon Hochschule Berlin | Oliver Roth (kommissarisch) | Nein |  |
|  | Berlin-Brandenburgische Akademie der Wissenschaften | Alexander Czmiel | Nein |  |
|  | Beuth Hochschule für Technik Berlin | Prof. Dr. rer. nat. Sebastian von Klinski (kommissarisch) | Nein |  |
|  | Charité - Universitätsmedizin Berlin | Ursula Flitner (kommissarisch) | Ja | Zeitschriften |
|  | Fritz-Haber-Institut der MPG | Uta Siebeky | Ja | beides (inkl. Sammelwerke und Sammelwerksbeiträge) |
|  | Freie Universität Berlin | Joachim Dinter, Agnieszka Zofia Wenninger  | Ja | beides (inkl. Sammelwerke) |
|  | Helmholtz-Zentrum Berlin für Materialien und Energie (HZB) | Dr. Andreas Tomiak | Nein |  |
|  | Hochschule für Musik Hanns Eisler | Thomas Nierlin | Nein |  |
|  | Hochschule für Schauspielkunst Ernst Busch | Anika Wilde | Nein |  |
|  | Hochschule für Technik und Wirtschaft Berlin | Prof. Dr.-Ing. Horst Schulte | Nein |  |
|  | Hochschule für Wirtschaft und Recht | Frank Wehrand | Nein |  |
|  | HU Berlin | Prof. Dr. Andreas Degkwitz | Ja | beides (inkl. Sammelwerke) |
|  | Leibniz-Institut für Gewässerökologie und Binnenfischerei (IGB) | N.N. | Ja | Zeitschriften |
|  | Max-Delbrück-Centrum für Molekulare Medizin Berlin-Buch (MDC) | Prof. Dr. Ingo Morano, Wolf Schröder-Barkhausen | Ja | beides |
|  | Max-Planck-Institut für Wissenschaftsgeschichte Berlin | Urte Brauckmann | Ja | beides |
|  | Stiftung Wissenschaft und Politik | Dr. Isabelle Tannous | Nein |  |
|  | TU Berlin | Dagmar Schobert (Bibliothek), Prof. Dr. Vera Meyer (Hochschule) | Ja | beides (inkl. Sammelwerken) |
|  | Universität der Künste Berlin | Prof. Dr. Susanne Fontaine | Nein |  |
|  | weißensee kunsthochschule berlin | Olaf Kriseleit | Nein |  |
|  | Wissenschaftszentrum Berlin für Sozialforschung (WZB) | Alessandro Blasetti | Nein | beides (inkl. Sammelwerke und Sammelwerksbeiträge) |
|  | Leibniz-Gemeinschaft | Dr. Julian  Vuorimäki | Ja | beides (inkl. Sammelwerke und Sammelwerksbeiträge) |
|  | Open-Access-Büro Berlin | Maxi Kindling |  |  |
| Brandenburg | BTU Cottbus-Senftenberg | Charlotte Meixner (Bibliothek), Dr. rer. nat. Patrick Hoffmann (Hochschule) | Nein |  |
|  | Leibniz-Institut für Raumbezogene Sozialforschung (IRS) e.V., Erkner | Simone Vogler | Ja | Zeitschriften |
|  | Europa-Universität Frankfurt (Oder) | Marion Dreher | Nein |  |
|  | Leibniz-Zentrum für Agrarlandschaftsforschung (ZALF) e.V., Müncheberg | Dr. Claus Dalchow | Ja | Zeitschriften |
|  | Helmholtz-Zentrum Potsdam, Deutsches GeoForschungsZentrum GFZ | Roland Bertelmann | Ja | Zeitschriften |
|  | Universität Potsdam | Linda Thomas | Ja | Zeitschriften |
|  | Technische Hochschule Wildau | Friederike Borchert | Ja | Zeitschriften |
| Bremen | Leibniz-Zentrum für Marine Tropenforschung (ZMT) GmbH | MEDIA Unit | Ja | Zeitschriften |
|  | Universität Bremen | Benjamin Ahlborn | Ja | Zeitschriften |
|  | Alfred-Wegener-Institut, Helmholtz-Zentrum für Polar- und Meeresforschung (AWI), Bremerhaven | Kathrin Brannemann | Ja | beides |
| Hamburg | Deutsches Elektronen-Synchrotron DESY | Dr. Martin Köhler | Nein |  |
|  | GIGA German Institute of Global and Area Studies, Leibniz-Institut für Globale und Regionale Studien | Jan Lüth | Ja | Zeitschriften |
|  | HafenCity University Hamburg | Dr. Juliane Finger | Nein |  |
|  | Max-Planck-Institut für ausländisches und internationales Privatrecht | David Schröder-Micheel | Nein |  |
|  | Staats- und Universitätsbibliothek Hamburg | Isabella Meinecke | Nein |  |
|  | TU Hamburg-Harburg | Inken Feldsien-Sudhaus | Ja | Zeitschriften |
|  | Universität Hamburg | Dr. Stefan Thiemann | Nein |  |
| Hessen | GSI Helmholtzzentrum für Schwerionenforschung, Darmstadt | Katrin Große | Ja |  |
|  | Hochschule Darmstadt | Catherina Gröninger | Ja | Zeitschriften |
|  | TU Darmstadt | Nicole Rosenke | Ja | Zeitschriften |
|  | Universität Frankfurt am Main | Dr. Roland Wagner | Ja | Zeitschriften |
|  | Hochschule Fulda | Patrick Langner | Ja | Zeitschriften |
|  | Universität Giessen | Florian Ruckelshausen | Ja | Zeitschriften |
|  | Universität Kassel | Dr. Tobias Pohlmann | Ja | Zeitschriften |
|  | Herder-Institut für historische Ostmitteleuropaforschung - Institut der Leibniz Gemeinschaft | Stephanie Palek | Nein |  |
|  | Universität Marburg | Margit L. Hartung | Ja | Zeitschriften |
| Mecklenburg-Vorpommern | Leibniz-Institut für Nutztierbiologie (FBN), Dummerstorf | Dr. Gunther Viereck | Ja | Zeitschriften |
|  | Universität Greifswald | Stefanie Bollin | Ja | Zeitschriften |
|  | Hochschule Neubrandenburg | Dr. Hagen Rogalski | Ja | Zeitschriften |
|  | Universität Rostock | Steffen Malo | Ja | Zeitschriften |
| Niedersachsen | Helmholtz-Zentrum für Infektionsforschung (HZI), Braunschweig | Axel Plähn | Ja |  |
|  | Physikalisch-Technische Bundesanstalt, Braunschweig und Berlin | Dr. Joachim Meier | Ja | Zeitschriften |
|  | TU Braunschweig | Carsten Elsner (Bibliothek), Prof. Dr.-Ing. Peter Hecker (Hochschule) | Ja | Zeitschriften |
|  | TU Clausthal | Silke Frank | Ja | Zeitschriften |
|  | Universität Göttingen | Margo Bargheer | Ja | Zeitschriften |
|  | Universität Hannover | Dr. Ulrike Kändler (Bibliothek), Dr. Reingis Hauck (Hochschule) | Ja | Zeitschriften |
|  | Medizinische Hochschule Hannover | Annette Spremberg | Ja | Zeitschriften |
|  | Tierärztliche Hochschule Hannover | Dr. Julia Dickel | Ja | Zeitschriften |
|  | Universität Hildesheim | PD Dr. Mario Müller | Ja | Zeitschriften |
|  | Universität Lüneburg | Thomas Schwager | Nein |  |
|  | Universität Oldenburg | Kim Braun | Ja | Zeitschriften (Monographien über BIS-Verlag) |
|  | Universität Osnabrück | Sabine Boccalini | Ja | Zeitschriften |
|  | Universität Vechta | Stephanie Hinrichs | Nein |  |
| Nordrhein-Westfalen | RWTH Aachen | Corinna Brückener | Nein |  |
|  | Universität Bielefeld | Susanne Riedel | Ja | beides |
|  | Fachhochschule Bielefeld |  | Ja | Zeitschriften |
|  | Universität Bochum | Kathrin Lucht-Roussel | Ja | Zeitschriften |
|  | Bundesinstitut für Arzneimittel und Medizinprodukte, Bonn | Anett Sollmann | Nein | Nein |
|  | Bundesinstitut für Berufsbildung (BIBB), Bonn | Dr. Bodo Rödel | Ja | Zeitschriften |
|  | Deutsches Institut für Erwachsenenbildung (DIE), Bonn | Dr. Thomas Jung | Teilnahme an den Publikationsfonds der Leibniz-Gemeinschaft | beides |
|  | Deutsches Zentrum für Neurodegenerative Erkrankungen (DZNE), Bonn | Andrea Ciocchetti | Nein |  |
|  | Universität Bonn | Open-Access-Team | Nein |  |
|  | TU Dortmund | Dr. Kathrin Höhner | Ja | Zeitschriften |
|  | Hochschule Düsseldorf |  | Nein |  |
|  | Universität Düsseldorf | Thorsten Lemanski (Bibliothek), Dr. Tobias Winnerling (Hochschule) | Nein |  |
|  | Universität Duisburg Essen | Katrin Falkenstein-Feldhoff | Ja | beides (inkl. Sammelwerke) |
|  | RWI – Leibniz-Institut für Wirtschaftsforschung Essen | Astrid Schürmann | Teilnahme am Publikationsfonds der Leibniz-Gemeinschaft | Zeitschriften |
|  | Forschungszentrum Jülich (FZJ) | Dr. Christoph Holzke | Ja |  |
|  | Deutsches Zentrum für Luft- und Raumfahrt (DLR), Köln | Dr. Jutta Graf | Ja | Zeitschriften |
|  | GESIS Leibniz-Institut für Sozialwissenschaften, Köln/Mannheim | Dr. Agathe Gebert | Teilnahme an den Publikationsfonds der Leibniz-Gemeinschaft | beides |
|  | Technische Hochschule Köln | Kerstin Klein | Nein |  |
|  | Universität zu Köln | Ralf Depping, Katja Halassy | Nein |  |
|  | ZB MED Informationszentrum Lebenswissenschaften, Köln | Dr. Ursula Arning | Ja | beides (inkl. Sammelwerke und Sammelwerksbeiträge) |
|  | Universität Münster | Dr. Viola Voß | Ja | beides (inkl. Sammelwerksbeiträge) |
|  | Universität Siegen | Nicole Walger | Ja | Zeitschriften
|  | Bergische Universität Wuppertal | Dr. Anja Platz-Schliebs, Dr. Christian Schäffer | Nein |  |
| Rheinland-Pfalz | TU Kaiserslautern | Julia Pletsch | Ja | Zeitschriften |
|  | Leibniz-Institut für Europäische Geschichte, Mainz | Dr. Christiane Bacher, Dr. Ines Grund | Nein |  |
|  | Universität Mainz | Karin Eckert | Ja | beides (inkl. Sammelwerke) |
|  | Universität Trier | Dr. Evgenia Grishina | Ja | Zeitschriften |
| Saarland | Universität des Saarlandes | Dr. Ulrich Herb | Ja | Zeitschriften |
| Sachsen | TU Chemnitz | Ute Blumtritt | Ja | Zeitschriften |
|  | Helmholtz-Zentrum Dresden-Rossendorf (HZDR) | N.N. | Ja | beides |
|  | TU Dresden | Dr. Achim Bonte (Bibliothek), Prof. Dr. Gerhard Rödel (Hochschule) | Ja | Zeitschriften |
|  | TU Bergakademie Freiberg | Cornelia Rau | Nein |  |
|  | Helmholtz-Zentrum für Umweltforschung - UFZ, Leipzig | Ilka Rudolf | Ja | Finanzierung von Zeitschriftenartikeln aus dem Bibliotheksbudget |
|  | Universität Leipzig | Dr. Astrid Vieler (Bibliothek), Prof. Dr. Gregory Ralph Crane (Hochschule) | Ja | beides (inkl. Transformationsvorhaben u.a.) |
| Sachsen-Anhalt | Universität Halle-Wittenberg | Dr. Susann Özüyaman (Bibliothek), Prof. Dr. Wolfgang Paul (Hochschule) | Ja | Zeitschriften (+ OA-Projekte) |
|  | Leibniz-Institut für Neurobiologie Magdeburg | Dr. Elke Behrends | Ja | Zeitschriften |
|  | Universität Magdeburg | Anja Matthes | Ja | beides |
| Schleswig-Holstein | Zentrale Hochschulbibliothek Flensburg | Henry Wolff | Ja (beim Land) | Zeitschriften |
|  | Helmholtz-Zentrum Geesthacht, Zentrum für Material- und Küstenforschung (HZG) | Dr. Gisbert Breitbach | Nein |  |
|  | GEOMAR - Helmholtz-Zentrum für Ozeanforschung, Kiel | Barbara Schmidt | Nein |  |
|  | IPN - Leibniz-Institut für die Pädagogik der Naturwissenschaften und Mathematik, Kiel | Barbara Senkbeil-Stoffels | Ja | Zeitschriften |
|  | Universität Kiel | Eike Hentschel (Bibliothek), Prof. Dr. Karin Schwarz (Hochschule) | Ja (beim Land) | Zeitschriften |
|  | Universität Lübeck & Technische Hochschule Lübeck | Inga Larres | Ja (beim Land) | Zeitschriften |
| Thüringen | Universität Erfurt | Gabor Kuhles | Ja | Zeitschriften |
|  | TU Ilmenau | Dr. Peter Blume | Ja | Zeitschriften |
|  | Universität Jena | | Ja | beides (inkl. Sammelwerken und Sammelwerksbeiträgen)
|  | Universität Weimar | Dana Horch | Ja | Zeitschriften |
