<!--
.. title: Ressourcen
.. slug: ressourcen
.. date: 2017-08-17 00:00:00
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

Hier finden Sie Links zu Vorträgen und Publikationen rund um das Thema Open-Access-Transformation sowie Handreichungen und Informationsmaterial.

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[Vorträge](/pages/vorträge/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[Publikationen](/pages/publikationen/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[Materialien](/pages/materialien/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[Übersicht häufig zitierter Open-Access-Zeitschriften](/pages/frequentlycitedoajournals/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[Übersicht Open-Access-Beauftragte](/pages/uebersichtoabeauftragte/)
