<!--
.. title: Subscribe to Open "BIBLIOTHEK – Forschung und Praxis"
.. slug: S2O_BFP
.. date: 2020-05-20 10:00:00
.. author:
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

# Subscribe to Open "BIBLIOTHEK - Forschung und Praxis"

Laufzeit: ab 2021

Organisator_innen: De Gruyter Verlag, Editorial Board BIBLIOTHEK – Forschung und Praxis, Abonnenten der Zeitschrift

Verlag: De Gruyter


### Hintergrund

Das [Subscribe to Open-Modell](https://scholarlykitchen.sspnet.org/2020/03/09/subscribetoopen/) wurde von Annual Reviews als Ansatz für die Open-Access-Transformation gut eingeführter Subskriptionszeitschriften entwickelt und nutzt bestehende Kund_innen- und Abonnentenbeziehungen sowie Rechnungsworkflows nach. Einrichtungen, die die Inhalte der jeweiligen Zeitschriften kennen und schätzen, abonnieren diese wie gewohnt weiter, während die Inhalte Open Access erscheinen. Dadurch fallen für das Publizieren keine APCs oder sonstigen zusätzlichen Kosten an. So lange die vorhandenen Abonnements erhalten und verlängert werden, erscheint die Zeitschrift Open Access unter einer Creative-Commons-Lizenz. Fällt die Anzahl der Abos unter einen vorher festgelegten Wert, wird die “Paywall” wieder aktiv, d.h. nur Abonnenten haben Zugriff.

Mit diesem Modell lassen sich mit wenig Aufwand und ohne Mehrkosten komplette Zeitschrifteninhalte in den Open Access transfomieren. Sein Potential liegt daher vor allem in der Aktivierung der subskribierenden Einrichtungen, durch das Beibehalten eines Abonnements den freien Zugang zu den Inhalten für alle zu garantieren.

Bisher haben neben Annual Reviews mit dem Journal Annual Review of Cancer Biology vor allem der Verlag Berghahn mit dem [Berghahn Open Anthro Projekt](https://www.berghahnjournals.com/page/boa/press-release) und EDP Science mit seinem Journal Mathematical Modelling in Natural Phenomena das Modell umgesetzt. In Deutschland setzt der Nomos-Verlag mit der neu gegründeten Open-Access-Zeitschrift ["Recht und Zugang"](https://www.ruz.nomos.de/) auf dieses Modell und baut zur Finanzierung einen neuen Abonnenten-Stamm auf.


### Konditionen

Die Abonnenten co-finanzieren durch ihr Abo die Open-Access-Publikation der Zeitschrift ["BIBLIOTHEK – Forschung und Praxis"](https://www.degruyter.com/view/journals/bfup/bfup-overview.xml?rskey=otykNS&result=1). Die Teilnahme erfolgt im Rahmen der jährlichen Aboerneuerung. Ansonsten ändert sich für die Bibliotheken nichts gegenüber ihrem normalen Abonnement.


### Vorteile

Bei der Unterstützung von Subscribe to Open (S2O) finanzieren bestehende Bibliotheken weiterhin die von ihnen als relevant eingeordneten Zeitschriften, während neu hinzukommende Bibliotheken ein Modell abonnieren, das es allen Leser_innen und Autor_innen ermöglicht, von Open Access zu profitieren und das die Finanzierung eines nachhaltigen Open Access, insbesondere für kleinere Zeitschriften in den Sozial- und Geisteswissenschaften, leichter zugänglich macht. S2O bietet eine einfache und unkomplizierte Möglichkeit des Übergangs zu Open Access, basierend auf bibliothekarischer Kuratierung, vorhandenen Ressourcen und bewährten Prozessen zur Unterstützung von Zeitschriften.


### Mitmachen

Das S20-Modell der Zeitschrift "BIBLIOTHEK – Forschung und Praxis" können Sie ganz einfach unterstützen, in dem Sie Ihr bestehendes Abonnement auf den gewohnten Wegen verlängern und für 2021 weiterführen.

Bei Fragen wenden Sie sich an den De Gruyter-Verlag: Dr. Christina Lembrecht, Senior Manager Open Access Partnerships, E-Mail: [christina.lembrecht@degruyter.com](mailto:christina.lembrecht@degruyter.com).
