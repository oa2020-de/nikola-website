<!--
.. title: Pilotprojekte
.. slug: pilotprojekte
.. date: 2018-06-10 00:00:00
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[Copernicus APC-Konsortium](/pages/APCkonsortium_Copernicus/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[ENABLE! Bibliotheken, Verlage und Autor*innen für Open Access in den Humanities und Social Sciences](https://enable-oa.org/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[Knowledge Unlatched Journal Flipping](/pages/KUjournalflipping/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[Open Access IT Law](/pages/oaitlaw/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[Subscribe to Open BIBLIOTHEK - Forschung und Praxis](/pages/S2O_BFP/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[transcript OPEN Library Politikwissenschaft](/pages/transcriptopenlibrarypowi/)


