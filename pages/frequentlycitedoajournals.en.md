<!--
.. title: Frequently cited open-access journals
.. slug: frequentlycitedoajournals
.. date: 2018-10-26 08:00:00
.. updated: 2021-02-26 12:22:00 
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

Researchers may have difficulties sometimes to find an open-access journal matching their needs for quality, visibility, and relevance. The National Contact Point Open Access OA2020-DE have therefore put together a searchable list of more than 1000 influential open-access journals from nearly all scientific fields. The list includes journals indexed in the Directory of Open Access Journals ([DOAJ](https://doaj.org/)) featuring an above average SNIP factor. SNIP - Source Normalized Impact per Paper is a journal metric ([CWTS Journal Indicators](http://www.journalindicators.com/)) that measures the average citation impact of the publications of a journal.
Please bear in mind that every metric has its limitations. Journal metrics should not be applied to assess the value of individual research items / articles/ findings or even careers.

In any case, if you choose to publish open access, you can be sure of having a citation advantage, as several [studies](https://sparceurope.org/what-we-do/open-access/sparc-europe-open-access-resources/open-access-citation-advantage-service-oaca/) have shown. In general, open-access journals have a higher visibility and are now so well established that they are among the top journals of many research areas.

For more information on the metadata and the SNIP, visit [DOAJ](https://doaj.org/) and [CWTS Journal Indicators](http://www.journalindicators.com/). Before submission, please check the current conditions and Article Processing Charges (APCs) on the journal’s website of your choice.


<a href="/assets/files/frequentlycitedOAjournals_20210120.html"><img src="/images/Screenshot_frequentlycitedoajournals.png" alt="Frequently cited open acccess journals" width="750" height="500" display="centered" /></a>
<br />
<i class="fa fa-chevron-circle-right"></i>
<a href="/assets/files/frequentlycitedOAjournals_20210120.html">Click on the table for sorting and filtering of the full list.</a>

*How to search in the table:*<br />

+ Sort by SNIP in descending order to find the most-cited journals.
+ There are several possibilities to filter for a research area:
    + Select "Subject_Area" and/or type in "Field", or
    + type in "Subject", or
    + search for a journal title by typing in "Journal".
+ Select whether the journal request article processing charges in "APCs?".
+ Search for language in "Language".

<br />
*Notes:*<br />
SNIP: source normalized impact per paper of the year 2019.
This table includes open-access journals that are listed in the DOAJ, have a minimum SNIP score of one and a minimum number of publications of 50.

The source of the first four columns (SNIP, Journal, Subject Area, Fields) are the [CWTS Journal Indicators](http://www.journalindicators.com/) April 2020, as accessed on 2021-01-19. We thank Prof. Dr. Ludo Waltman for the permission to use the data. We do not claim any ownership of that data.

The source of the last eight columns (Subjects, Publisher, APCs?, APC Amount, Currency, Full Text Language, P-ISSN, E-ISSN) is the Directory of Open Access Journals ([DOAJ](https://doaj.org/)) as accessed on 2021-01-19. The DOAJ-metadata are licensed under CC BY-SA.

The list is not exhaustive!

Last updated on 2021-02-26.

<br />
**More Ressources**<br />
In addition to our table, we recommend to visit the [Scimago Journal Rank](https://www.scimagojr.com/) selecting the checkbox "Only Open Access Journals".


<br />
**Disclaimer**<br />
The Bielefeld University Library takes no responsibility for the completeness, accuracy and currentness of the data. In no event shall the Bielefeld University Library be liable for any damages, lost profits, and business interruption arising out of or in connection with the use or impossibility to use this service.
