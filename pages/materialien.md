<!--
.. title: Materialien
.. slug: materialien
.. date: 2018-01-29 08:00:00
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->


## Informationsmaterial & Handreichungen

**Eigenes**

<i class="fa fa-tag" aria-hidden="true"></i>
Dirk Ecker, Bernhard Mittermaier, Irene Barbers, Philipp Pollack & Sonja Rosenberger (2019). Sammeln, Aufbereiten, Analysieren - Der Weg zum Open Access Monitor - Poster bei den Open-Access-Tagen 2019, Hannover. [https://juser.fz-juelich.de/record/865661/files/OAT19_OA-Monitor.pdf](https://juser.fz-juelich.de/record/865661/files/OAT19_OA-Monitor.pdf)

<i class="fa fa-tag" aria-hidden="true"></i>
Alexandra Jobmann (2020). [How to open up my journal - Pathways into open access](http://doi.org/10.5281/zenodo.3693598). (Entscheidungsbaum)

<i class="fa fa-tag" aria-hidden="true"></i>
Alexandra Jobmann (2020). [Open-Access-Transformationsmodelle für das wissenschaftliche Publikationssystem](/assets/files/2020-02-15-open-access-transformationmodels_de.pdf). (Infografik)

<i class="fa fa-tag" aria-hidden="true"></i>
Alexandra Jobmann (2019). How to promote the open-access transformation at the national level - Poster at the 48th LIBER Annual Conference, Dublin. Zenodo. [http://doi.org/10.5281/zenodo.3250148](http://doi.org/10.5281/zenodo.3250148)

<i class="fa fa-tag" aria-hidden="true"></i>
Alexandra Jobmann (2018). [5 Wege der Open-Access-Transformation](/images/5-wege-der-oa-transformation.png). (Infografik)

<i class="fa fa-tag" aria-hidden="true"></i>
Alexandra Jobmann (2018). National Contact Point Open Access OA2020-DE - Poster at the 14th Berlin Open Access Conference. Zenodo. [http://doi.org/10.5281/zenodo.1972958](http://doi.org/10.5281/zenodo.1972958)

<i class="fa fa-tag" aria-hidden="true"></i>
Alexandra Jobmann (2018). [Open-Access-Transformationsindex](https://gitlab.ub.uni-bielefeld.de/oa2020-de/transformationsindex/-/wikis/Home). (GitLab-Instanz)

<i class="fa fa-tag" aria-hidden="true"></i>
Bernhard Mittermaier, Irene Barbers, Dirk Ecker, Philipp Pollack & Sonja Rosenberger (2018). Open Access Monitor Deutschland - Poster bei der 19. DINI-Jahrestagung 2018, Bielefeld. [https://juser.fz-juelich.de/record/857197/files/PosterOpenAccessMonitor.pdf](https://juser.fz-juelich.de/record/857197/files/Poster%20Open%20Access%20Monitor.pdf)

<i class="fa fa-tag" aria-hidden="true"></i>
Dirk Pieper, Sven Fund, Karin Werner & Alexandra Jobmann (2018). [Qualitätsstandards für den Einstieg in die Open-Access-Stellung von Büchern](https://pub.uni-bielefeld.de/record/2932189) (2nd ed.). ([Link zur ersten Version](/assets/files/OA2020-DE&KU_qualitaetsstandards_oa_buecher.pdf))

--------------

**Von anderen zur Verfügung gestellt**

<i class="fa fa-tag" aria-hidden="true"></i>
David Böhm, Alexander Grossmann, Michael Reiche & Antonia Schrader (2020). Open-Access-Publikationsworkflow für akademische Bücher: Ein Handbuch für Hochschulen und Universitäten. HTWK Leipzig. doi:[10.33968/9783966270175-00](https://dx.doi.org/10.33968/9783966270175-00)

<i class="fa fa-tag" aria-hidden="true"></i>
Jeroen Bosman (2019). Big national deals with an open access component. [Google Spreadsheet](https://docs.google.com/spreadsheets/d/13SULdSVZWK8r-_91bo_rCBKRgye5bS5wfPuD5LIX2vU/edit#gid=1873690517)

<i class="fa fa-tag" aria-hidden="true"></i>
Ellen Collins, Caren Milloy & Graham Stone (2013). Guide to Creative Commons for Humanities and Social Science monograph authors. Working Paper. Jisc Collections, London. doi:[10.5920/oapen-uk/ccguide](http://dx.doi.org/10.5920/oapen-uk/ccguide)

<i class="fa fa-tag" aria-hidden="true"></i>
Sarah Dellmann et al. (2020). [Musteranschreiben zum Aufruf zur Zweitveröffentlichung](/assets/files/Musterschreiben-via-Fachreferenten.docx).

<i class="fa fa-tag" aria-hidden="true"></i>
Claudia Frick (2018). Locked up science: Tearing down paywalls in scholarly communication - Talk at the 35c3. [Video](https://media.ccc.de/v/35c3-9599-locked_up_science)

<i class="fa fa-tag" aria-hidden="true"></i>
Scheila Heidt (o.J.). [BIBB-Flussdiagramm Aufruf zur Zweitveröffentlichung](/assets/files/BIBB_Flussdiagramm Aufruf zur Zweitveröffentlichung.pdf).

<i class="fa fa-tag" aria-hidden="true"></i>
Hybrid Publishing Lab (2014). How to start an open access journal - a starter's guide to familiarize yourself with the key issues in starting an open access journal. [Poster](http://hybridpublishing.org/files/2014/07/HOAJ-POSTER-final-web.pdf)

<i class="fa fa-tag" aria-hidden="true"></i>
Bianca Kramer & Marco Tullney (2019). Plan S implementation by institutions and libraries. Zenodo. doi:[10.5281/zenodo.3257353](http://doi.org/10.5281/zenodo.3257353)

<i class="fa fa-tag" aria-hidden="true"></i>
Lisa Matthias (2018). lmatthia/publisher-oa-portfolios v1.0 (Version v1.0). Zenodo. doi:[10.5281/zenodo.2459746](http://doi.org/10.5281/zenodo.2459746)

<i class="fa fa-tag" aria-hidden="true"></i>
Projekt OGeSoMo (2019). Handreichungen zum Projekt OGeSoMo und weitere Materialien rund um das Thema Open Access. [Webseite](https://www.uni-due.de/ogesomo/materialien)

<i class="fa fa-tag" aria-hidden="true"></i>
Open Access Directory (o.J.). OA journal business models. [Wiki Page](http://bit.ly/oad-journal-models)

<i class="fa fa-tag" aria-hidden="true"></i>
Scholastica (2018). How to start (or flip) an open access journal: Academic-led publishing primer. [Blogpost](https://blog.scholasticahq.com/post/how-to-start-flip-open-access-academic-journal)

<i class="fa fa-tag" aria-hidden="true"></i>
Lara Speicher, Lorenzo Armando, Margo Bargheer, Martin Paul Eve, Sven Fund, Delfim Leão, … Irakleitos Souyioultzoglou. (2018). OPERAS Open Access Business Models White Paper. Zenodo. doi:[10.5281/zenodo.1323708](http://doi.org/10.5281/zenodo.1323708)

<i class="fa fa-tag" aria-hidden="true"></i>
David J. Solomon, Mikael Laakso & Bo-Christer Björk (2016). Converting Scholarly Journals to Open Access: A Review of Approaches and Experiences. [Report](http://nrs.harvard.edu/urn-3:HUL.InstRepos:27803834)
