<!--
.. title: Pilot projects
.. slug: pilotprojects
.. date: 2018-06-10 00:00:00
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[Copernicus APC-Consortium](/en/pages/APCconsortium_Copernicus/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[Knowledge Unlatched Journal Flipping](/en/pages/KUjournalflipping/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[Open Access IT Law](/en/pages/oaitlaw/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[Subscribe to Open BIBLIOTHEK - Forschung und Praxis](/en/pages/S2O_BFP/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[transcript OPEN Library Political Science](/en/pages/transcriptopenlibrarypols/)



