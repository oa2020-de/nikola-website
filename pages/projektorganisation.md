<!--
.. title: Projektorganisation
.. slug: projektorganisation
.. date: 2017-08-17 00:00:00
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

<img src="/images/Projektstruktur_FUG_180108-NEU-de.png" alt="Projektstruktur" width="650" height="500" display="block" />

Die Projektgruppe übernimmt die inhaltliche Rahmenplanung und Steuerung der Aktivitäten im Rahmen des Projektes. Sie besteht aus 9 Personen (und zwei Gästen):

* Dirk Pieper (Projektleiter, UB Bielefeld, HRK)
* Dr. Bernhard Mittermaier (Stellv. Projektleiter, ZB FZ Jülich, DEAL)
* Roland Bertelmann (GFZ Potsdam, Helmholtz)
* Dr. Gernot Deinzer (UB Regensburg, HRK)
* Dr. Michael Erben-Russ (Fraunhofer München, Fraunhofer)
* Kristine Hillenkötter (SUB Göttingen, HRK, AG WP)
* Florian Ruckelshausen (UB Giessen, HRK)
* Olaf Siegert (ZBW Hamburg, Leibniz, AG WP)
* Kai Karin Geschuhn (Gast, MPDL München, MPG, AG WP)
* Dr. Angela Holzer (Gast, DFG Bonn, DFG, AG WP)

Sie trifft sich in regelmäßigen Abständen und berichtet in den Sitzungen der Allianz der Wissenschaftsorganisationen über die Durchführung, den Verlauf und die eingesetzten Ressourcen des Projektes.

Unterstützt wird die Projektgruppe von drei Projektmitarbeiter_innen:

* Alexandra Jobmann, UB Bielefeld (Kommunikation und Öffentlichkeitsarbeit, bis 30.06.2020)
* Dr. Nina Schönfelder, UB Bielefeld (Konzeption)
* Philipp Pollack, ZB FZ Jülich (Bibliometrie, Datenerhebung)


## Kontakt

------------------------------------------
Dirk Pieper (kommissarisch)


Projektleiter<br />
Nationaler Open-Access-Kontaktpunkt OA2020-DE<br />
<p></p>
Universität Bielefeld<br />
Universitätsbibliothek<br />
Universitätsstrasse 25<br />
D-33615 Bielefeld<br />
<p></p>
<i class="fa fa-envelope" aria-hidden="true"></i> [dirk.pieper@uni-bielefeld.de](mailto:dirk.pieper@uni-bielefeld.de)<br />
<i class="fa fa-twitter" aria-hidden="true"></i> [@oa2020de](https://twitter.com/oa2020de)<br />
