<!--
.. title: Subscribe to Open "BIBLIOTHEK - Forschung und Praxis"
.. slug: S2O_BFP
.. date: 2020-05-20 10:00:00
.. author:
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

# Subscribe to Open "BIBLIOTHEK - Forschung und Praxis"

Term: Since 2021

Organiser: De Gruyter, Editorial Board BIBLIOTHEK - Forschung und Praxis, Journal subscriber

Publisher: De Gruyter


### Background

The [Subscribe to Open model](https://scholarlykitchen.sspnet.org/2020/03/09/subscribetoopen/) was developed by Annual Reviews as an approach to the open-access transformation of well-established subscription journals and makes use of existing customer and subscriber relationships and billing workflows. Institutions that know and appreciate the content of the respective journals continue to subscribe to them as usual, while the content is made openly accessible. This means that no APCs or other additional costs are incurred for publishing. As long as existing subscriptions are maintained and renewed, the journal is published open access under a Creative-Commons licence. If the number of subscriptions falls below a pre-defined value, the "paywall" becomes active again, i.e. only subscribers have access.

With this model, complete journal contents can be transformed into open access with little effort and without additional costs. Its potential therefore lies primarily in the activation of subscribing institutions to guarantee free access to content for all by maintaining a subscription.

So far, in addition to Annual Reviews with the Journal Annual Review of Cancer Biology, the Berghahn publishing house with the [Berghahn Open Anthro Project](https://www.berghahnjournals.com/page/boa/press-release) and EDP Science with its journal Mathematical Modelling in Natural Phenomena have implemented the model. In Germany, the Nomos  publishing house is relying on this model with the newly founded open-access journal ["Recht und Zugang"](https://www.ruz.nomos.de/index.php?id=8859&L=1) and is building up a new subscriber base to finance it.


### Conditions

Subscribers co-finance the open-access publication of the journal ["BIBLIOTHEK - Forschung und Praxis"](https://www.degruyter.com/view/journals/bfup/bfup-overview.xml?rskey=otykNS&result=1) through their subscription. Participation is part of the annual subscription renewal and as an additional incentive there is a small discount on the subscription price. Apart from that, nothing changes for the libraries compared to their normal subscription.


### Advantages

In supporting Subscribe to Open (S2O), existing libraries will continue to fund the journals they consider relevant, while new libraries will subscribe to a model that allows all readers and authors to benefit from open access and that makes the funding of sustainable open access more accessible, especially for smaller journals in the social sciences and humanities. S2O provides a simple and straightforward means of transitioning to open access, based on librarian curation, existing resources and proven processes for supporting journals.


### Participate

You can easily support the S20 model of the journal "BIBLIOTHEK - Forschung und Praxis" by extending your existing subscription in the usual ways and continuing it for 2021.

If you have any questions, please contact De Gruyter.
