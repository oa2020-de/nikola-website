<!--
.. title: Projektziele und Meilensteine
.. slug: ziele
.. date: 2017-08-17 00:00:00
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

### 1.Strategische Zielsetzung

Voraussetzungen schaffen für die großflächige Open-Access-Transformation in Übereinstimmung mit der Allianz der deutschen Wissenschaftsorganisationen.


### 2.Operative Ziele

*Schaffung einer zentralen Anlaufstelle.*

Die zentrale Anlaufstelle für Hochschulen und außeruniversitäre Forschungseinrichtungen hat den offenen Austausch über Transformationsmodelle <br />
und die Konsequenzen für die einzelnen Wissenschaftseinrichtungen zum Ziel. Gleichzeitig dient sie der Vernetzung nationaler Akteur_innen und <br />
fungiert als Bindeglied zur globalen OA2020-Community im Rahmen der Transformationsinitiative.

*Unterstützungssystem für einzelne Einrichtungen zur Analyse der Publikations- und Kostenzusammenhänge schaffen.*

Interessierte Wissenschaftseinrichtungen werden dabei unterstützt, sich ein klares Bild über das eigene Publikationsaufkommen und die mit dem Bezug <br />
und/oder der Verbreitung wissenschaftlicher Publikationen verbundenen Kosten zu verschaffen. Dafür wird eine [Plattform](http://www.fz-juelich.de/zb/DE/noak) aufgebaut, <br/>
auf der alle benötigten (Finanz-)Daten zusammengestellt werden und über die Einrichtungen bei der Durchführung der Analysen unterstützt werden.

*Untersuchung der Finanzströme und Erarbeitung von Finanzierungsmodellen.*

OA2020-DE wird die Finanzströme auf mindestens drei Ebenen untersuchen: Fördereinrichtung - Mittelempfänger_innen / publikationsstarke - weniger <br />
publikationsstarke Einrichtungen / institutsinterne Geldflüsse inklusive der administrativen Kosten. Darauf aufbauend werden Open-Access-Finanzierungsmodelle <br />
erarbeitet und mit allen Akteur_innen im Publikationsmarkt (Bibliotheken, Wissenschaftseinrichtungen, Verlagen und Förderern) offen diskutiert.

*Konzeptionelle Weiterentwicklung der Verhandlungsstrategie der Wissenschaftseinrichtungen gegenüber Wissenschaftsverlagen.*

Durch die verbesserte Datenbasis wird es möglich sein, die Verhandlungsstrategien der Wissenschaftseinrichtungen gegenüber den Verlagen <br />
konzeptionell weiter zu entwickeln. OA2020-DE wird die Wissenschaftseinrichtungen darin unterstützen, die analysierten Daten strategisch zu nutzen <br />
und die Erfahrungen, die dabei gemacht wurden, mit anderen Einrichtungen zu teilen.

*Informationsmaterial & Handreichungen erstellen.*

Neben der Durchführung von Veranstaltungen, wie z.B. Roadshows und Workshops, werden geeignete Informationsmaterialien und Handreichungen erstellt.


------------------------------------------------------------------
### Meilensteine

<img style="display:auto; margin:0" src="/images/Grafik_Meilensteine.png" alt="Milestones" width="800" height="559" />


