<!--
.. title: Project Organisation
.. slug: projectorganisation
.. date: 2017-08-17 00:00:00
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

<img src="/images/Projektstruktur_FUG_180108-NEU-en.png" alt="Project structure" width="650" height="500" display="block" />

The project group takes over the content-related framework planning and control of the activities within the framework of the project. It consists of 9 persons (and two guests):

* Dirk Pieper (Project leader, University Library Bielefeld, HRK)
* Dr. Bernhard Mittermaier (Deputy project leader, Library FZ Jülich, DEAL)
* Roland Bertelmann (GFZ Potsdam, Helmholtz)
* Dr. Gernot Deinzer (University Library Regensburg, HRK)
* Dr. Michael Erben-Russ (Fraunhofer München, Fraunhofer)
* Kristine Hillenkötter (Göttingen State and University Library, HRK, AG SPS)
* Florian Ruckelshausen (University Library Giessen, HRK)
* Olaf Siegert (ZBW Hamburg, Leibniz, AG SPS)
* Kai Karin Geschuhn (Guest, MPDL München, MPG, AG SPS)
* Dr. Angela Holzer (Guest, DFG Bonn, DFG, AG SPS)

It meets on a regular basis and reports on the implementation, course and resources of the project in meetings of the Alliance of Science Organisations in Germany.

The project group is supported by three project members:

* Alexandra Jobmann, University Library Bielefeld (Communication and Public Relations, until 2020-06-30)
* Dr. Nina Schönfelder, University Library Bielefeld (Conception)
* Philipp Pollack, Library FZ Jülich (Bibliometrics and Data Collection)


## Contact

------------------------------------------
Dirk Pieper (temporary)


Project Leader<br />
National Contact Point Open Access OA2020-DE<br />
<p></p>
University Bielefeld<br />
University Library<br />
Universitätsstrasse 25<br />
D-33615 Bielefeld<br />
<p></p>
<i class="fa fa-envelope" aria-hidden="true"></i> [dirk.pieper@uni-bielefeld.de](mailto:dirk.pieper@uni-bielefeld.de)<br />
<i class="fa fa-twitter" aria-hidden="true"></i> [@oa2020](https://twitter.com/oa2020de)<br />
