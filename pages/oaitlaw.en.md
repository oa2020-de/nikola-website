<!--
.. title: Open Access IT Law
.. slug: oaitlaw
.. date: 2019-02-25 10:00:00
.. author:
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

# Open Access IT Law

Term: 2019-2021

Organiser: Peter Lang publishing house, Knowledge Unlatched; Specialised Information Service for International and Interdisciplinary Legal Research (&lt;intR>²) at the Staatsbibliothek zu Berlin

Publisher: Peter Lang


### Background

The Specialised Information Service for International and Interdisciplinary Legal Research [(&lt;intR>²)](https://vifa-recht.de/en/) at the Staatsbibliothek zu Berlin has been committed to the open access paradigm since its founding in 2014. This commitment has so far found its concrete expression in the establishment and expansion of the disciplinary open access repository [&lt;intR>²Dok](https://intr2dok.vifa-recht.de/content/index.xml), which enables all interested members of legal research institutions the first or second publication of texts, research data and audiovisual formats in the field of international and interdisciplinary legal research in open access.

Under the auspices of the National Contact Point Open Access and inspired by the [OPEN Library Political Science](https://oa2020-de.org/en/pages/transcriptopenlibrarypols/), the Specialised Information Service for International and Interdisciplinary Legal Research - particularly with regard to the open access transformation of legal monographs - has decided to provide financial and organisational support for a joint project with Peter Lang and Knowledge Unlatched to set up an open access e-book pool on digital law. The focused programme area has been deliberately chosen, as legal issues of digitisation are currently affecting all areas of society on a massive scale.

The aim of this project is initially to provide ten front list titles of the Peter Lang publishing house in the IT law open access every year for three years (beginning in 2019). The selection is made by a scientific review board, which also includes the Specialised Information Service.<br />
**Due to the financial contribution of the FID &lt;intR>² parts of the total costs are already covered.**

As a foretaste are already five titles from the back list provided open access and via the OA repository of the FID retrievable ([Open Access IT Law](https://intr2dok.vifa-recht.de/servlets/solr/select?q=%2Bcategory.top:%22intr2dok_projects:5d4bacc2-cc93-4d01-bbc1-89597fcd55d9%22+%2BobjectType:mods+%2Bcategory.top:%22state:published%22&mask=content/browse/intr2dok_projects.xml%23open%5B%5D&sort=mods.dateIssued+desc)).


### Conditions

The price structure for financing Open Access IT Law depends on the number of institutions involved, whereby the annual amount per institution/library, for example, can be reduced from € 2,145 (for 20 participating institutions, which corresponds to the minimum number of sponsors) to € 1,075 (for 40 participating institutions).

|    | Dollar | Euro  | GBP |
|-------------------------------|--------|-------|-----------------|
| 20 participating institutions | 2,530  | 2,145 | 1,888           |
| 30 participating institutions | 1,690  | 1,430 | 1,260           |
| 40 participating institutions | 1,265  | 1,075 | 945             |

*Annual price per library over a period of 3 years.*


### Outlook

For the Specialised Information Service for International and Interdisciplinary Legal Research, this project is only a first step towards promoting open-access transformation in the field of law. In the future, therefore, not only other publishers, but also other areas of international and interdisciplinary legal research should be the focus of such activities.

If you would also like to participate in the financing of Open Access IT Law, please contact [Catherine Anderson](mailto:catherine@knowledgeunlatched.org) from Knowledge Unlatched directly.

[Adam Gardner](mailto:a.gardner@peterlang.com) of Peter Lang Verlag will be happy to answer any questions you may have.

[Ivo Vogel](mailto:Ivo.Vogel@sbb.spk-berlin.de) from the Staatsbibliothek zu Berlin is available to answer your questions on behalf of the FID.
