<!--
.. title: Veranstaltungen
.. slug: veranstaltungen
.. date: 2019-05-31 00:00:00
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

Hier finden Sie eine Übersicht der vom Nationalen Open-Access-Kontaktpunkt durchgeführten Veranstaltungen mit den Materialien und Vortragsfolien (sofern eine Freigabe vorliegt.)


<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[1. OA2020-DE Transformationsworkshop, 19.-20. April 2018](/blog/2018/04/26/openaccess-und-medienerwerb-erster-workshop/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[2. OA2020-DE Transformationsworkshop, 17.-18. Oktober 2018](/blog/2018/11/20/2oatransformationsworkshop/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[Hands-On-Lab beim 7. Bibliothekskongress, 19. März 2019](/blog/2019/05/03/bericht-handsonlab-Bibliothekskongress/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[3. OA2020-DE Transformationsworkshop, 03.-04. April 2019](/blog/2019/05/07/3oatransformationsworkshop/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[Workshop bei den Open-Access-Tagen, 01. Oktober 2019](/blog/2019/12/16/workshop_oatage2019/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[4. OA2020-DE Transformationsworkshop, 04.-05. November 2019](/blog/2019/11/18/4oatransformationsworkshop/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[Initialworkshop der ENABLE!-Community, 28. Januar 2020](/blog/2020/02/25/initialworkshop_enablecommunity/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[5. OA2020-DE Transformationsworkshop (Webinar), 09. März 2020](/blog/2020/04/07/webinar_wileydeal_lokaleumsetzung/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[Workshop (Bibliotheks-)Konsortien für gebührenfreies Open Access, 05. Mai 2020](/blog/2020/06/15/workshop_konsortien_gebuehrenfreiesOA/)
