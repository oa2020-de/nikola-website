<!--
.. title: transcript OPEN Library Political Science 2019
.. slug: transcriptopenlibrarypols2019
.. date: 2018-06-10 00:00:00
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

# transcript OPEN Library Political Science 2019

Term: 2019

Organiser: transcript publishing house, Knowledge Unlatched, Political Science Information Service (FID Pollux)

Publisher: transcript


**Background**

As part of the open-access transformation, we want to give new inspiration for open access to scientific content. Therefore, we have launched the ["transcript Open Library Political Science"](https://www.transcript-verlag.de/open-access-politikwissenschaft) project in cooperation with the transcript publishing house.
The aim of the pilot project is the development of a publisher and library equally manageable, transparent and economically sustainable open-access e-book business model. The deficits of existing approaches should be adequately addressed. That means, that instead of buying the E-Books, the participating libraries enable the open access publication of all forthcoming books "Political Science at transcript 2019" (20 titles) via a fee in the crowdfunding model. Through that, the library budgets unlatch the titles to the benefit of everybody instead of supporting isolated access for single institutions.
The model is supported by the [Political Science Information Service](https://www.pollux-fid.de/) (FID) at Bremen State and University Library.


**Conditions**

The final conditions for the open access appearance of the front list "Political Science at transcript 2019" depend essentially on the number of co-financing institutions. FID Political Science has pledged 50% of the front-list funding (ie € 46,000 of € 92,000 for the package has already been raised). In order to provide planning certainty for the financial participation of the libraries, a minimum number of sponsors (20 institutions) has been set, on the basis of which the maximum invoice amount is calculated (with FID participation € 2,300 per institution). The final invoice amount will be proportionately reduced as more entities participate in the funding. If there are less than 20 funding institutions, there will be no billing and no open access to the front list. In return, co-financing institutions act as sponsors and receive the opportunity to request one
free print copy of the titles in the package.

<img src="/images/transcript_Konditionen-Bibliotheken.png" alt="discounts e-book package Political Science" width="800" height="142" display="block" />

The list of the previous "community" can be found here: [https://www.transcript-verlag.de/transcript-Open-Library-Politikwissenschaft-Community](https://www.transcript-verlag.de/transcript-Open-Library-Politikwissenschaft-Community).


**Sponsoring Light - an offer for the universities of applied sciences**

In order to allow even smaller institutions to participate in the (informal) consortium, universities of applied sciences have the opportunity to make use of the "Sponsoring Light" offer. "Sponsoring Light" means participation through half funding, i. e. only half of the regular contribution is due. In return, the presentation as "enabler" is limited to an entry in a sponsorship list. Of course the universities of applied sciences are free to participate in regular sponsoring.


**Advantages**

Open Access pursues the goal of making equal use of the opportunities of digitization for authors, publishers and libraries alike. With the help of sustainable and transparent offers on the part of publishers as well as the financial participation by libraries new possibilities for the positioning in the scientific publication system arise for all actors.

Our model has the following advantages compared to the common practice of e-book licensing:

* License costs for e-books are eliminated, financing through reallocations in the budget possible
* Planned overspending opportunity in the sense of the open access transformation
* Descending transaction costs for e-book acquisition / indexing
* No restrictions on the use and provision of Open Access publications in teaching (digital semester apparatus, etc.)
* Visibility of the co-financing institutions through sponsorship
* Anchoring the FID in the subject-specific open access publication process
* Pluricontextual localization of libraries in the network of authors - publishers - science communities - public
* Exploitation of new mediatization potential of open access content with the participation of libraries
* Free Metadata-use [through OAI-interface](https://www.transcript-verlag.de/oai/oai2.php)

See also the FAQ: [https://www.transcript-verlag.de/transcript-Open-Library-Politikwissenschaft-FAQ](https://www.transcript-verlag.de/transcript-Open-Library-Politikwissenschaft-FAQ).

In cooperation with the transcript publishing house, the pilot model aims at the open-access position of all of the political science titles that will be published by transcript next year on the basis of a transparent cost calculation for open access e-books and a joint financing concept involving the relevant Information Service. If the model proves itself, scaling to other program areas and publishers is possible and desirable.

If you want to pledge, contact [Catherine Anderson](mailto:catherine@knowledgeunlatched.org) of Knowledge Unlatched or one of the affiliated library service providers:

+ [Dietmar Dreier International Library Supplier](http://www.dietmardreier.de/ebooks/knowledgeunlatched),
+ [Missing Link mail order bookstore](http://www.missing-link.de),
+ or [Schweitzer Fachinformation](http://www.schweitzer-online.de/info/Knowledge-Unlatched/).

Questions about the project will be answered by [Ms. Stefanie Hanneken](mailto:open-access@transcript-verlag.de) from transcript publishing house.
