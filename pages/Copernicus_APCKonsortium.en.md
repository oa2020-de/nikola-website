<!--
.. title: National Consortium for Central Payment Processing of APCs
.. slug: APCconsortium_Copernicus
.. date: 2018-11-30 00:00:00
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

# National Consortium for Central Payment Processing of APCs with Copernicus

Term: 2019-2020

Consortium leader: Lower Saxony State and University Library Göttingen

Publisher: Copernicus Publications

Contract (only german): [Contract for Central Payment Processing of APCs](/assets/files/Copernicus_SUB_Vertrag_Prepayments_nat_Konsortium_signed.pdf)

[Press release](https://idw-online.de/de/news708983) on the launch of the consortium.


### Background

The development of transformation models also includes the structural anchoring of the financing of fee-based open-access journal articles in academic institutions. To date, this has generally been done through publication funds (DFG-funded), which are often limited in time and money. What is needed, therefore, are offers and concepts for the continuation and stabilization of this funding. In addition, there is also a lack of key figures for estimating demand, which make it possible to plan expenditure for paid open-access publishing for libraries in the same way as for subscription expenditure.

Therefore, OA2020-DE is seeking to form a consortium with all German libraries and academic institutions that make relevant payments to genuine open-access publishers. **A corresponding pilot project was developed with Copernicus Publications**. The aim is to establish the financing of publications in genuine open-access journals as an institutional and standardised complement to existing transformation projects such as SCOAP³ and DEAL.


### Conditions

Based on the concept of the National Contact Point Open Access OA2020-DE, the Lower Saxony State and University Library Göttingen and the publisher Copernicus Publications have jointly developed and set up the consortium. The term is for two years (2019–2020), and eligible are all universities and scientific institutions that are also eligible for participation in a national or alliance licence.

The participating institutions make an advance payment to Copernicus at the beginning of a (household) year. The calculation basis for existing customers is the average number of publications per institution, multiplied by a fictitious average APC of EUR 1,200 net. New customers pay a flat rate. At the end of the year, the costs are invoiced and settled. Copernicus invoices the individual institutions.

The consortium enables corresponding authors affiliated with the participating institutions to publish free of charge for them in the 43 gold open-access journals in the fields of geography, engineering and life sciences for two years. The articles will be published under a CC-BY license, including the automatic delivery of the repositories of the consortium participants with the XML metadata and the full-text PDF.


### Advantages

In this way, the consortium identifies and motivates institutions that have not yet established a financing structure for open-access publishing. For the participating institutions and the publishing house, there is planning security in the form of advance payments, and the estimate of funds can be supported by the contact point.

In cooperation with the Lower Saxony State and University Library Göttingen and Copernicus Publications, the pilot model aims to test mechanisms for structurally anchoring the financing of fee-based open-access journal articles in academic institutions. The experience gained with this consortium will flow into the further development of such mechanisms and can and should lead to extended models of cooperative APC financing (similar to the SCOAP³ model).


### Contact

If you are interested in future participation in the consortium or if you have any questions regarding the conditions, please contact:

Lower Saxony State and University Library Göttingen, [nationallizenzen@sub.uni-goettingen.de](mailto:nationallizenzen@sub.uni-goettingen.de)

Copernicus Publications, [publications@copernicus.org](mailto:publications@copernicus.org)



