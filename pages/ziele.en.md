<!--
.. title: Project objectives and milestones
.. slug: aims
.. date: 2017-08-17 00:00:00
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

### 1.Strategic Objective

Create requirements for the large-scale open access transformation in accordance with the Alliance of Science Organisations in Germany.


### 2.Operational Objectives

*Creation of a one-stop contact point.*

The central point of contact for universities and non-university research institutions aims the open exchange on transformation models and the <br />
consequences for the individual scientific institutions. At the same time, it serves to network national actors and acts as a link to the global <br />
OA2020 community within the framework of the transformation initiative.

*Create a support system for individual institutions to analyse the relationships between publications and publication costs.*

Interested scientific institutions are supported in obtaining a clear picture of their own publication volume and the costs associated with the <br />
purchase and / or distribution of scientific publications. For this purpose, a [platform](http://www.fz-juelich.de/zb/EN/About_us/cooperation/noak_data_office/noak_data_office_node.html) will be set up to collect all the (financial) data needed <br />
and to support it through the facilities in carrying out the analyses.

*Investigation of financial flows and development of financing models.*

OA2020-De will examine the financial flows at least on three levels: funding - credits / organisations with high publication rates - organisations <br />
who publish less / in-house cash flows, including administrative costs. Based on this, open access financing models are developed and openly discussed <br />
with all actors in the publication market (libraries, scientific institutions, publishers and research funders).

*Conceptual development of the negotiation strategy of the scientific institutions towards science publishers.*

The improved database will make it possible to develop the negotiation strategies of scientific institutions over the publishers further conceptual. <br />
OA2020-DE will help the scientific institutions to make strategic use of the analysed data and to share the experience gained with other institutions.

*Create information material & handouts.*

In addition to conducting events, such as Roadshows and workshops, appropriate information materials and handouts are created.


-------------------------------------------------------------------
### Milestones

<img style="display:auto; margin:0" src="/images/Grafik_Meilensteine_en.png" alt="Milestones" width="800" height="559" />


