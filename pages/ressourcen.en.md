<!--
.. title: Resources
.. slug: resources
.. date: 2017-08-17 00:00:00
.. tags: meta
.. category:
.. link:
.. description:
.. type: text
-->

Here you will find links to lectures and publications on the topic of Open Access Transformation as well as handouts and information material.

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[Lectures](/en/pages/lectures/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[Publications](/en/pages/publications/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[Materials](/en/pages/materials/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[Overview of frequently cited open-access journals](/en/pages/frequentlycitedoajournals/)

<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
[Overview of open access representatives](https://gitlab.ub.uni-bielefeld.de/oa2020-de/nikola-website/wikis/%C3%9Cbersicht-Open-Access-Beauftragte-an-deutschen-Hochschulen-und-Forschungseinrichtungen) (only in german)
