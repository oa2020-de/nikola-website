# Anleitung

## Nikola installieren

Einmalig muss Nikola installiert werden.

Anleitung hier: https://getnikola.com/getting-started.html#installAny

Aber statt der aktuellen Version brauchen wir die letzte 7er Version:

```shell
bin/python -m pip install -U "Nikola[extras]==v7.8.15"
```

## Website bauen

Nachdem die Website verändert wurde, müssen die Marktdown-Dateien in HTML-Dateien umgewandelt werden, Navigationsleisten aktualisiert werden usw. Das erledigt alles folgendes Kommando:

```shell
nikola build
```

## Website prüfen

Eine Vorschau auf dem eigenen Computer kann man wie folgt anzeigen lassen:

```shell
nikola serve --browser
```

## Website hochladen

```shell
nikola deploy
```
